﻿// Copyright (C) 2014 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using DynaJson;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static System.Math;

namespace KancolleSniffer.Test
{
    using Sniffer = SnifferTest.TestingSniffer;

    [TestClass]
    public class SnifferTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            ExpressionToCodeConfiguration.GlobalAssertionConfiguration = ExpressionToCodeConfiguration
                .GlobalAssertionConfiguration.WithPrintedListLengthLimit(200).WithMaximumValueLength(1000);
        }

        public class TestingSniffer : KancolleSniffer.Sniffer
        {
            public TestingSniffer(bool start = false) : base(start)
            {
                AdditionalData.UseNumEquipsFile = false;
            }
        }

        public static StreamReader OpenLogFile(string name)
        {
            var dir = Path.GetDirectoryName(Path.GetDirectoryName(Environment.CurrentDirectory));
// ReSharper disable once AssignNullToNotNullAttribute
            var path = Path.Combine(dir, Path.Combine("logs", name + ".log.gz"));
            return new StreamReader(new GZipStream(File.Open(path, FileMode.Open), CompressionMode.Decompress));
        }

        public static void SniffLogFile(Sniffer sniffer, string name, Action<Sniffer> action = null)
        {
            var ln = 0;
            using (var stream = OpenLogFile(name))
            {
                while (!stream.EndOfStream)
                {
                    var triple = new List<string>();
                    foreach (var s in new[] {"url: ", "request: ", "response: "})
                    {
                        string line;
                        do
                        {
                            line = stream.ReadLine();
                            ln++;
                            if (line == null)
                                throw new Exception($"ログの内容がそろっていません: {ln:d}行目");
                        } while (!line.StartsWith(s));
                        triple.Add(line.Substring(s.Length));
                    }
                    var json = JsonObject.Parse(triple[2]);
                    sniffer.Sniff(triple[0], triple[1], json);
                    action?.Invoke(sniffer);
                }
            }
        }

        /// <summary>
        /// 一つもアイテムがない場合のrequire_info
        /// </summary>
        [TestMethod]
        public void NoUseItemRequireInfo()
        {
            var sniffer = new Sniffer(true);
            SniffLogFile(sniffer, "require_info_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 0);
        }

        /// <summary>
        /// 一つもアイテムがない場合のuseitem
        /// </summary>
        [TestMethod]
        public void NoUseItem()
        {
            var sniffer = new Sniffer(true);
            SniffLogFile(sniffer, "useitem_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 0);
        }

        /// <summary>
        /// 熟練度込みの制空値を正しく計算する
        /// </summary>
        [TestMethod]
        public void FighterPowerWithBonus()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "fighterpower_001");
            var fleet = sniffer.Fleets[0];
            PAssert.That(() => fleet.FighterPower == new Range(156, 159));
            SniffLogFile(sniffer, "fighterpower_002");
            PAssert.That(() => fleet.FighterPower == new Range(140, 143), "全滅したスロットがある");
        }

        /// <summary>
        /// 基地航空隊の飛行場拡張と整備レベル強化で設営隊を消費
        /// </summary>
        [TestMethod]
        public void ExpandAirbase()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airbase_expand_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "設営隊 x2");
            PAssert.That(() => sniffer.AirBase[0].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[0].AirCorps.Length == 3);
            PAssert.That(() => sniffer.AirBase[1].MaintenanceLevel == 0);
            PAssert.That(() => sniffer.AirBase[1].AirCorps.Length == 1);

            SniffLogFile(sniffer, "airbase_expand_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.AirBase[0].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[0].AirCorps.Length == 3);
            PAssert.That(() => sniffer.AirBase[1].MaintenanceLevel == 0);
            PAssert.That(() => sniffer.AirBase[1].AirCorps.Length == 2);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].ActionName == "待機");
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Distance.ToString() == "0");
            var fp = sniffer.AirBase[1].AirCorps[1].CalcFighterPower();
            PAssert.That(() => fp.AirCombat.RangeString == "0～0");
            PAssert.That(() => fp.Interception.RangeString == "0～0");

            SniffLogFile(sniffer, "airbase_expand_003");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "新型航空機設計図 x6");
            PAssert.That(() => sniffer.AirBase[0].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[0].AirCorps.Length == 3);
            PAssert.That(() => sniffer.AirBase[1].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[1].AirCorps.Length == 2);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].ActionName == "待機");
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Distance.ToString() == "0");
            fp = sniffer.AirBase[1].AirCorps[1].CalcFighterPower();
            PAssert.That(() => fp.AirCombat.RangeString == "0～0");
            PAssert.That(() => fp.Interception.RangeString == "0～0");
        }

        /// <summary>
        /// 平時にイベント海域の整備レベルを強化してもエラーにならない
        /// </summary>
        [TestMethod]
        public void ExpandEventAreaMaintenanceLevelWhenClose()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airbase_expand_001");
            SniffLogFile(sniffer, "airbase_expand_004");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "設営隊 x1");
        }

        /// <summary>
        /// マップ索敵の判定式(33)を正しく計算する
        /// </summary>
        [TestMethod]
        public void LineOfSight()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "lineofsight_001");
            var fleet = sniffer.Fleets[0];
            PAssert.That(() => Abs(fleet.GetLineOfSights(1) - 39.45) < 0.01);
            PAssert.That(() => Abs(fleet.GetLineOfSights(3) - 115.19) < 0.01);
            PAssert.That(() => Abs(fleet.GetLineOfSights(4) - 153.06) < 0.01);
            SniffLogFile(sniffer, "lineofsight_002");
            PAssert.That(() => Abs(fleet.GetLineOfSights(1) - -25.10) < 0.01, "艦隊に空きがある");
        }

        /// <summary>
        /// 補強増設スロットに見張り員を装備した場合の判定式(33)
        /// </summary>
        [TestMethod]
        public void LineOfSightWithExSlot()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "lineofsight_003");
            var fleet = sniffer.Fleets[0];
            PAssert.That(() => Abs(fleet.GetLineOfSights(1) - -28.8) < 0.01);
        }

        /// <summary>
        /// SGレーダー(初期型)の装備ボーナス込みの索敵スコアと電探搭載艦を計算する
        /// </summary>
        [TestMethod]
        public void LineOfSightWithSgRadar()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "lineofsight_004");
            var fleet = sniffer.Fleets[0];
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(1), 2) == 2.0);

            PAssert.That(() => sniffer.Fleets[0].RadarShips == 2);
            PAssert.That(() => sniffer.Fleets[0].SurfaceRadarShips == 2);
        }

        /// <summary>
        /// TPを計算する
        /// </summary>
        [TestMethod]
        public void TransportPoint()
        {
            var sniffer = new Sniffer();
            var msgs = new[] {"", "鬼怒改二+特大発+おにぎり", "駆逐艦+士魂部隊", "補給艦"};
            var results = new[] {27, 19, 13, 15};
            for (var i = 0; i < msgs.Length; i++)
            {
                SniffLogFile(sniffer, "transportpoint_00" + (i + 1));
                var j = i;
                PAssert.That(() => (int)sniffer.Fleets[0].TransportPoint == results[j], msgs[j]);
            }
        }

        /// <summary>
        /// 対空砲火のパラメータを計算する
        /// </summary>
        [TestMethod]
        public void AntiAirFire()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "antiairfire_101");
            var fleet = sniffer.Fleets[0];
            var ships = fleet.Ships;
            PAssert.That(() => fleet.AntiAir == 22.0);
            PAssert.That(() => fleet.CombinedAntiAir == 56.0);
            PAssert.That(() => Math.Round(fleet.EffectiveAntiAir, 2) == 86.15);
            PAssert.That(() => ships.Select(ship => Math.Round(ship.PropShootdown, 2)).SequenceEqual(new[] {18.4, 19.2, 16.8, 7.2, 19.6, 21.2}));
            PAssert.That(() => ships.Select(ship => Math.Round(ship.FixedShootdown, 2)).SequenceEqual(new[] {14.25, 14.65, 13.65, 9.77, 14.73, 15.37}));

            var guardFleet = sniffer.Fleets[1];
            var guardShips = guardFleet.Ships;
            PAssert.That(() => guardFleet.AntiAir == 34.0);
            PAssert.That(() => guardFleet.CombinedAntiAir == 56.0);
            PAssert.That(() => Math.Round(guardFleet.EffectiveAntiAir, 2) == 86.15);
            PAssert.That(() => guardShips.Select(ship => Math.Round(ship.PropShootdown, 2)).SequenceEqual(new[] {14.88, 31.44, 7.44, 9.6, 0.0, 10.8}));
            PAssert.That(() => guardShips.Select(ship => Math.Round(ship.FixedShootdown, 2)).SequenceEqual(new[] {10.21, 16.88, 7.11, 8.02, 4.14, 8.65}));

            // 敵は通常艦隊
            var enemyFleet = sniffer.Battle.EnemyFleet;
            var enemies = enemyFleet.Ships;
            PAssert.That(() => enemyFleet.AntiAir == 24.0);
            PAssert.That(() => enemyFleet.CombinedAntiAir == 24.0);
            PAssert.That(() => Math.Round(enemyFleet.EffectiveAntiAir, 2) == 76.0);
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.PropShootdown, 2)).SequenceEqual(new[] {5.0, 5.0, 5.0, 23.5, 14.75, 14.75}));
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.FixedShootdown, 2)).SequenceEqual(new[] {9.0, 9.0, 9.0, 15.94, 12.66, 12.66}));

            SniffLogFile(sniffer, "antiairfire_102");
            // 強力な対空装備を搭載した艦が退避したら艦隊防空が落ちるパターンを追加したい

            // 敵は連合艦隊
            enemyFleet = sniffer.Battle.EnemyFleet;
            enemies = enemyFleet.Ships;
            PAssert.That(() => enemyFleet.AntiAir == 33.0);
            PAssert.That(() => enemyFleet.CombinedAntiAir == 62.0);
            PAssert.That(() => Math.Round(enemyFleet.EffectiveAntiAir, 2) == 186.0);
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.PropShootdown, 2)).SequenceEqual(new[] {6.0, 5.6, 4.0, 15.8, 15.8, 18.8}));
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.FixedShootdown, 2)).SequenceEqual(new[] {16.2, 16.05, 15.45, 19.88, 19.88, 21.0}));

            var enemyGuardFleet = sniffer.Battle.EnemyGuardFleet;
            var guardEnemies = enemyGuardFleet.Ships;
            PAssert.That(() => enemyGuardFleet.AntiAir == 29.0);
            PAssert.That(() => enemyGuardFleet.CombinedAntiAir == 62.0);
            PAssert.That(() => Math.Round(enemyGuardFleet.EffectiveAntiAir, 2) == 186.0);
            PAssert.That(() => guardEnemies.Select(enemy => Math.Round(enemy.PropShootdown, 2)).SequenceEqual(new[] {1.44, 16.2, 7.08, 7.08, 7.08, 7.08}));
            PAssert.That(() => guardEnemies.Select(enemy => Math.Round(enemy.FixedShootdown, 2)).SequenceEqual(new[] {8.91, 14.44, 11.02, 11.02, 11.02, 11.02}));
        }

        /// <summary>
        /// 副砲の改修レベルの効果を計算する
        /// </summary>
        [TestMethod]
        public void SecondaryGunFirepowerLevelBonus()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "firepower_001");
            var ships = sniffer.Fleets[0].Ships;
            // ReSharper disable CompareOfFloatsByEqualityOperator
            PAssert.That(() => ships[0].EffectiveFirepower == 93.5);
            PAssert.That(() => ships[1].EffectiveFirepower == 82.5);
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        /// <summary>
        /// 連合艦隊補正の載った火力と電探搭載艦を計算する
        /// </summary>
        [TestMethod]
        public void CombinedFleetFirepower()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "combined_status_001");
            // ReSharper disable CompareOfFloatsByEqualityOperator
            PAssert.That(() => sniffer.Fleets[0].Ships[0].EffectiveFirepower == 117.0);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].EffectiveFirepower == 72.0);
            // ReSharper restore CompareOfFloatsByEqualityOperator

            PAssert.That(() => sniffer.Fleets[0].RadarShips == 0);
            PAssert.That(() => sniffer.Fleets[1].RadarShips == 2);
            PAssert.That(() => sniffer.Fleets[0].SurfaceRadarShips == 0);
            PAssert.That(() => sniffer.Fleets[1].SurfaceRadarShips == 1);
        }

        /// <summary>
        /// 対潜攻撃力を計算する
        /// </summary>
        [TestMethod]
        public void AntiSubmarine()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "antisubmarine_001");
            PAssert.That(() => Abs(sniffer.Fleets[0].Ships[0].EffectiveAsw - 92.16) < 0.01);
            PAssert.That(() => Abs(sniffer.Fleets[0].Ships[1].EffectiveAsw - 105.61) < 0.01);
            PAssert.That(() => Abs(sniffer.Fleets[0].Ships[2].EffectiveAsw - 57.84) < 0.01);
            PAssert.That(() => Abs(sniffer.Fleets[0].Ships[3].EffectiveAsw - 61.37) < 0.01);
        }

        /// <summary>
        /// 航空偵察スコアを計算する
        /// </summary>
        [TestMethod]
        public void AirReconScore()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airrecon_score_001");
            PAssert.That(() => Abs(sniffer.Fleets[0].AirReconScore - 26.88) < 0.01);
        }

        /// <summary>
        /// 編成で空き番号を使ったローテートを正しく反映する
        /// </summary>
        [TestMethod]
        public void RotateFleetMember()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "deck_001");
            PAssert.That(() => new[] {756, 17204, 6156, 28806, 1069, -1}.SequenceEqual(sniffer.Fleets[0].Deck));
        }

        /// <summary>
        /// ドラッグ＆ドロップで離れた空き番号を使って編成をローテートする
        /// </summary>
        [TestMethod]
        public void RotateFleetMemberWithDragAndDrop()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "deck_005");
            PAssert.That(() => new[] {57391, 50, 24475, 113, -1, -1}.SequenceEqual(sniffer.Fleets[0].Deck));
        }

        /// <summary>
        /// 編成で艦隊に配置ずみの艦娘を交換する
        /// </summary>
        [TestMethod]
        public void ExchangeFleetMember()
        {
            var sniffer = new Sniffer();

            SniffLogFile(sniffer, "deck_002");
            PAssert.That(() => new[] {1069, 6156, 756, 3223, -1, -1}.SequenceEqual(sniffer.Fleets[0].Deck),
                "編成で艦隊内で艦娘と交換する");

            SniffLogFile(sniffer, "deck_003");
            PAssert.That(() => new[] {1069, 6156, 14258, 3223, -1, -1}.SequenceEqual(sniffer.Fleets[0].Deck) &&
                               new[] {101, 4487, 756, 14613, 28806, -1}.SequenceEqual(sniffer.Fleets[1].Deck),
                "002に続いて艦隊をまたがって交換する");

            SniffLogFile(sniffer, "deck_004");
            PAssert.That(() => new[] {1069, 6156, 14258, 3223, 756, -1}.SequenceEqual(sniffer.Fleets[0].Deck) &&
                               new[] {101, 4487, 14613, 28806, -1, -1}.SequenceEqual(sniffer.Fleets[1].Deck),
                "003に続いて空き番号にほかの艦隊の艦娘を配置する");
        }

        /// <summary>
        /// 随伴艦一括解除を実行する
        /// </summary>
        [TestMethod]
        public void WithdrawAccompanyingShipsAtOnce()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "deck_006");
            PAssert.That(() => new[] {135, -1, -1, -1, -1, -1}.SequenceEqual(sniffer.Fleets[0].Deck));
        }

        /// <summary>
        /// 編成展開を正しく反映する
        /// </summary>
        [TestMethod]
        public void PresetSelect()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "preset_001");
            PAssert.That(() => new[] {50510, 632, 39843, 113, 478, 47422}.SequenceEqual(sniffer.Fleets[0].Deck));
        }

        /// <summary>
        /// 編成を開いたあと母港に戻った後と、戦闘後の一覧ウィンドウのプリセットにある編成艦隊番号を維持する
        /// </summary>
        [TestMethod]
        public void PresetListWindow()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "preset_101");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(3).Ships[1].Fleet.Number == 0, "編成を開く");
            SniffLogFile(sniffer, "preset_102");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(3).Ships[1].Fleet.Number == 0, "母港に戻る");
            SniffLogFile(sniffer, "preset_103");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(3).Ships[1].Fleet.Number == 0, "戦闘後の更新");
        }

        /// <summary>
        /// 拡張した編成記録枠にすぐに記録してもエラーにならず、ドック開放キーを消費する
        /// </summary>
        [TestMethod]
        public void PresetExpand()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "preset_201");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "ドック開放キー x1");
            SniffLogFile(sniffer, "preset_202");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");
        }

        /// <summary>
        /// 拡張した装備記録枠にすぐに記録してもエラーにならず、ドック開放キーを消費する
        /// </summary>
        [TestMethod]
        public void PresetSlotExpand()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "preset_slot_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "ドック開放キー x1");
            SniffLogFile(sniffer, "preset_slot_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");
        }

        /// <summary>
        /// 装備交換のAPIの仕様変更に対応する
        /// </summary>
        [TestMethod]
        public void SlotExchangeVersion2()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "slot_exchange_002");
            PAssert.That(() => new[] {157798, 59001, 157804, -1, -1}.SequenceEqual(sniffer.Fleets[0].Ships[0].Slot));
        }

        /// <summary>
        /// 近代化改修の結果をすぐに反映する
        /// </summary>
        [TestMethod]
        public void PowerUpResult()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "powerup_001");
            PAssert.That(() => Abs(sniffer.Fleets[0].Ships[0].EffectiveFirepower - 30) < 0.0001);
        }

        /// <summary>
        /// 近代化改修による艦娘数と装備数の変化
        /// </summary>
        [TestMethod]
        public void PowerUpCount()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "powerup_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 204);
            PAssert.That(() => sniffer.ItemCounter.Now == 886);
        }

        /// <summary>
        /// 近代化改修が二重に行われた場合に対応する
        /// </summary>
        [TestMethod]
        public void DuplicatedPowerUp()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "powerup_002");
            PAssert.That(() => sniffer.ShipCounter.Now == 218);
        }

        /// <summary>
        /// 装備解除後の近代化改修
        /// </summary>
        [TestMethod]
        public void PowerUpDetachItem()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "powerup_003");
            PAssert.That(() => sniffer.ShipCounter.Now == 317);
            PAssert.That(() => sniffer.ItemCounter.Now == 1326);
        }

        /// <summary>
        /// 南瓜を使う近代化改修
        /// </summary>
        [TestMethod]
        public void PowerupPumpkin()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "powerup_pumpkin_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(35).ToString() == "南瓜 x2");

            SniffLogFile(sniffer, "powerup_pumpkin_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(35).ToString() == "南瓜 x1");
        }

        /// <summary>
        /// 特殊な素材を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingSpecialMaterial()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "remodeling_special_material_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2965");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x3000");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            var spec = sniffer.ShipList.First(ship => ship.Id == 48837).Spec;
            PAssert.That(() => spec.Id == 390);

            SniffLogFile(sniffer, "remodeling_special_material_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2955");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2980");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x299");
            spec = sniffer.ShipList.First(ship => ship.Id == 48837).Spec;
            PAssert.That(() => spec.Id == 903);
        }

        /// <summary>
        /// 新型砲熕兵装資材を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingNewModelGunMount()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "remodeling_special_material_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2989");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x63");
            var spec = sniffer.ShipList.First(ship => ship.Id == 10).Spec;
            PAssert.That(() => spec.Id == 206);

            SniffLogFile(sniffer, "remodeling_special_material_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2961");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x324");
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "新型砲熕兵装資材 x62");
            spec = sniffer.ShipList.First(ship => ship.Id == 10).Spec;
            PAssert.That(() => spec.Id == 666);
        }

        /// <summary>
        /// 新型高温高圧缶を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingNewModelBoiler()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "remodeling_special_material_201");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x3");
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x66");
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 87) == 12); // id:12 新型高温高圧缶
            var spec = sniffer.ShipList.First(ship => ship.Id == 27212).Spec;
            PAssert.That(() => spec.Id == 136);

            SniffLogFile(sniffer, "remodeling_special_material_202");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x332");
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "新型砲熕兵装資材 x63");
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 87) == 10); // id:12 新型高温高圧缶
            spec = sniffer.ShipList.First(ship => ship.Id == 27212).Spec;
            PAssert.That(() => spec.Id == 911);
        }

        /// <summary>
        /// 補強増設使用の結果を反映する
        /// </summary>
        [TestMethod]
        public void OpenExslot()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "open_exslot_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(21).ToString() == "補強増設 x9");
            var slotex = sniffer.ShipList.First(ship => ship.Id == 7863).SlotEx;
            PAssert.That(() => slotex == 0);

            SniffLogFile(sniffer, "open_exslot_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(21).ToString() == "補強増設 x8");
            slotex = sniffer.ShipList.First(ship => ship.Id == 7863).SlotEx;
            PAssert.That(() => slotex == -1);
        }

        /// <summary>
        /// ship2を待たずにケッコンの結果を反映する
        /// </summary>
        [TestMethod]
        public void MarriageResult()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "marriage_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 33);
            PAssert.That(() => sniffer.UseItems.ElementAt(14).ToString() == "書類一式＆指輪 x1");
            PAssert.That(() => sniffer.Fleets[0].Ships[2].Level == 99);

            SniffLogFile(sniffer, "marriage_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 32);
            PAssert.That(() => sniffer.UseItems.ElementAt(14).ToString() == "艦娘からのチョコ x2");
            PAssert.That(() => sniffer.Fleets[0].Ships[2].Level == 100);
        }

        /// <summary>
        /// 改修による資材の減少をすぐに反映する
        /// </summary>
        [TestMethod]
        public void ConsumptionByRemodelSlot()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "remodel_slot_001");
            PAssert.That(() => sniffer.Material.Current.SequenceEqual(new[] {25292, 25570, 25244, 41113, 1405, 1525, 2137, 8}));
        }

        /// <summary>
        /// 改修更新による特殊資材の減少をすぐに反映する
        /// </summary>
        [TestMethod]
        public void ConsumptionSpecialMaterialByRemodelSlot()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "remodel_slot_101");
            PAssert.That(() => sniffer.Material.Current.SequenceEqual(new[] {344646, 339665, 349398, 350000, 2831, 2696, 2979, 91}));
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x60");
            var upgradeitem = sniffer.SlotItems.First(item => item.Id == 170899);
            PAssert.That(() => upgradeitem.Spec.Name == "16inch Mk.I連装砲");
            PAssert.That(() => upgradeitem.Level == 10);

            SniffLogFile(sniffer, "remodel_slot_102");
            PAssert.That(() => sniffer.Material.Current.SequenceEqual(new[] {344616, 339315, 348918, 350000, 2831, 2696, 2955, 83}));
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x59");
            upgradeitem = sniffer.SlotItems.First(item => item.Id == 170899);
            PAssert.That(() => upgradeitem.Spec.Name == "16inch Mk.V連装砲");
            PAssert.That(() => upgradeitem.Level == 0);
        }

        /// <summary>
        /// 改修更新の確認画面まで行ってキャンセルし、特殊資材を消費しない別の改修を実行して、特殊資材を消費してないのを確認する
        /// </summary>
        [TestMethod]
        public void CancelRemodelSlotWithSpecialMaterial()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "remodel_slot_201");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(23).ToString() == "熟練搭乗員 x29");
            PAssert.That(() => sniffer.UseItems.ElementAt(27).ToString() == "新型航空兵装資材 x52");
            PAssert.That(() => sniffer.RemodelSlotReqUseitem.Count == 0);

            SniffLogFile(sniffer, "remodel_slot_202");
            Console.WriteLine(sniffer.RemodelSlotReqUseitem);
            PAssert.That(() => sniffer.RemodelSlotReqUseitem.Count == 2);
            PAssert.That(() => sniffer.RemodelSlotReqUseitem[77] == 1);
            PAssert.That(() => sniffer.RemodelSlotReqUseitem[70] == 2);

            SniffLogFile(sniffer, "remodel_slot_203");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(23).ToString() == "熟練搭乗員 x29");
            PAssert.That(() => sniffer.UseItems.ElementAt(27).ToString() == "新型航空兵装資材 x52");
            PAssert.That(() => sniffer.RemodelSlotReqUseitem.Count == 0);
        }

        /// <summary>
        /// 装備の数を正しく数える
        /// </summary>
        [TestMethod]
        public void CountItem()
        {
            var createItems = new Sniffer();
            SniffLogFile(createItems, "createitem_001");
            Assert.AreEqual(854, createItems.ItemCounter.Now);
            var createShips = new Sniffer();
            SniffLogFile(createShips, "createship_001");
            Assert.AreEqual(858, createShips.ItemCounter.Now);
            var multiItems = new Sniffer();
            SniffLogFile(multiItems, "createitem_002");
            Assert.AreEqual(1250, multiItems.ItemCounter.Now);
        }

        /// <summary>
        /// 装備数の超過を警告する
        /// </summary>
        [TestMethod]
        public void WarnItemCount()
        {
            Action<int> func = i => { };
            var sniffer1 = new Sniffer();
            sniffer1.ItemCounter.Margin = 51; // 応急修理要員、戦闘糧食、洋上補給は所持数から引かれるようになった
            SniffLogFile(sniffer1, "item_count_001");
            func.Invoke(sniffer1.ItemCounter.Now); // Nowを読まないとAlarmが立たない
            PAssert.That(() => sniffer1.ItemCounter.Alarm, "出撃から母港に戻ったとき");
            var sniffer2 = new Sniffer();
            sniffer2.ItemCounter.Margin = 51; // 応急修理要員、戦闘糧食、洋上補給は所持数から引かれるようになった
            SniffLogFile(sniffer2, "item_count_002");
            func.Invoke(sniffer2.ItemCounter.Now);
            PAssert.That(() => sniffer2.ItemCounter.Alarm, "ログインしたとき");
        }

        /// <summary>
        /// 警告値ギリギリでドロップして帰投しても不要な装備数の超過を警告しない
        /// </summary>
        [TestMethod]
        public void WarnItemCountMarginBorder()
        {
            var sniffer = new Sniffer();
            sniffer.ItemCounter.Margin = 16;
            sniffer.AdditionalData.RecordNumEquips(61, "青葉", 2);

            // ログイン
            SniffLogFile(sniffer, "itemwarning_margin16_101");
            PAssert.That(() => sniffer.ShipCounter.Now == 547);
            PAssert.That(() => sniffer.ItemCounter.Now == 2348);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);

            // 出撃して青葉ドロップ後、資源マスで出撃終了
            SniffLogFile(sniffer, "itemwarning_margin16_102");
            PAssert.That(() => sniffer.ShipCounter.Now == 548);
            PAssert.That(() => sniffer.ItemCounter.Now == 2350);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);

            // 帰投時のapi_portで装備数超過の警告が出てはならない
            SniffLogFile(sniffer, "itemwarning_margin16_103");
            PAssert.That(() => sniffer.ShipCounter.Now == 548);
            PAssert.That(() => sniffer.ItemCounter.Now == 2350);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);

            // api_port後の各apiでも同様
            SniffLogFile(sniffer, "itemwarning_margin16_104");
            PAssert.That(() => sniffer.ShipCounter.Now == 548);
            PAssert.That(() => sniffer.ItemCounter.Now == 2350);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);
        }

        /// <summary>
        /// 装備の所持者を設定する
        /// </summary>
        [TestMethod]
        public void SetItemHolder()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "itemholder_001");
            var itemIds = new HashSet<int> {75298, 76572, 82725, 90213, 5910};
            var items = sniffer.ItemList.Where(status => itemIds.Contains(status.Id));
            PAssert.That(() => items.All(x => x.Holder.Id == 861));
        }

        /// <summary>
        /// 資材の変動を正しく反映する
        /// </summary>
        [TestMethod]
        public void MaterialChanges()
        {
            var sniffer1 = new Sniffer();
            var result1 = new List<int[]>();
            SniffLogFile(sniffer1, "material_001", sn =>
            {
                var cur = sn.Material.Current;
                if (result1.Count == 0)
                {
                    result1.Add(cur);
                }
                else
                {
                    if (!result1.Last().SequenceEqual(cur))
                        result1.Add(cur);
                }
            });
            var expected1 = new List<int[]>
            {
                new[] {0, 0, 0, 0, 0, 0, 0, 0},
                new[] {26178, 26742, 21196, 33750, 1426, 1574, 2185, 10},
                new[] {26178, 26842, 21226, 33750, 1426, 1574, 2185, 10},
                new[] {28951, 29493, 24945, 35580, 1426, 1574, 2185, 10},
                new[] {27451, 27993, 22945, 34580, 1426, 1574, 2184, 10},
                new[] {26074, 26616, 21068, 33700, 1426, 1572, 2183, 10},
                new[] {26171, 26721, 21175, 33750, 1426, 1574, 2185, 10},
                new[] {27023, 27829, 28136, 42404, 1404, 1521, 2142, 15},
                new[] {31208, 29819, 29714, 42345, 1407, 1530, 2155, 13},
                new[] {24595, 25353, 18900, 32025, 1427, 1576, 2187, 10},
                new[] {24515, 25353, 18749, 32025, 1427, 1575, 2187, 10},
                new[] {23463, 24964, 17284, 31765, 1427, 1572, 2187, 10},
                new[] {23463, 25064, 17314, 31765, 1427, 1572, 2187, 10}
            };
            PAssert.That(() => SequenceOfSequenceEqual(expected1, result1));

            var sniffer2 = new Sniffer();
            var result2 = new List<int[]>();
            SniffLogFile(sniffer2, "material_002", sn =>
            {
                var cur = sn.Material.Current;
                if (result2.Count == 0)
                {
                    result2.Add(cur);
                }
                else
                {
                    if (!result2.Last().SequenceEqual(cur))
                        result2.Add(cur);
                }
            });
            var expected2 = new List<int[]>
            {
                new[] {0, 0, 0, 0, 0, 0, 0, 0},
                new[] {201649, 189713, 261490, 123227, 2743, 2828, 3000, 44},
                new[] {201649, 189714, 261491, 123227, 2743, 2828, 3000, 44},
                new[] {201650, 189718, 261500, 123227, 2743, 2828, 3000, 44}
            };
            PAssert.That(() => SequenceOfSequenceEqual(expected2, result2));
        }

        /// <summary>
        /// 基地航空隊における資材の変動を反映する
        /// </summary>
        [TestMethod]
        public void MaterialChangesInAirCorps()
        {
            var sniffer3 = new Sniffer();
            var result3 = new List<int[]>();
            SniffLogFile(sniffer3, "material_003", sn =>
            {
                var cur = sn.Material.Current;
                if (result3.Count == 0)
                {
                    result3.Add(cur);
                }
                else
                {
                    if (!result3.Last().SequenceEqual(cur))
                        result3.Add(cur);
                }
            });
            var expected3 = new List<int[]>
            {
                new[] {0, 0, 0, 0, 0, 0, 0, 0},
                new[] {288194, 282623, 299496, 295958, 3000, 2968, 2997, 7},
                new[] {288185, 282623, 299496, 295943, 3000, 2968, 2997, 7},
                new[] {288161, 282623, 299496, 295903, 3000, 2968, 2997, 7}
            };
            PAssert.That(() => SequenceOfSequenceEqual(expected3, result3), "航空機の補充");

            var sniffer4 = new Sniffer();
            var result4 = new List<int[]>();
            SniffLogFile(sniffer4, "material_004", sn =>
            {
                var cur = sn.Material.Current;
                if (result4.Count == 0)
                {
                    result4.Add(cur);
                }
                else
                {
                    if (!result4.Last().SequenceEqual(cur))
                        result4.Add(cur);
                }
            });
            var expected4 = new List<int[]>
            {
                new[] {0, 0, 0, 0, 0, 0, 0, 0},
                new[] {261012, 252252, 298492, 279622, 3000, 2842, 3000, 22},
                new[] {261012, 252252, 298492, 279538, 3000, 2842, 3000, 22},
                new[] {261012, 252252, 298492, 279454, 3000, 2842, 3000, 22}
            };
            PAssert.That(() => SequenceOfSequenceEqual(expected4, result4), "航空機の配備");
        }

        private bool SequenceOfSequenceEqual<T>(IEnumerable<IEnumerable<T>> a, IEnumerable<IEnumerable<T>> b)
        {
            var aa = a.ToArray();
            var bb = b.ToArray();
            if (aa.Length != bb.Length)
                return false;
            return aa.Zip(bb, (x, y) => x.SequenceEqual(y)).All(x => x);
        }

        /// <summary>
        /// 修復時間が1分以内の艦娘が入渠する
        /// </summary>
        [TestMethod]
        public void NyukyoLessThanOrEqualTo1Min()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "nyukyo_001");
            PAssert.That(() => sniffer.RepairList.Length == 1);
        }

        /// <summary>
        /// 建造してすぐ消費アイテムを一覧に反映する
        /// </summary>
        [TestMethod]
        public void CreateShip()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "createship_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x350000");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2867");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2991");

            SniffLogFile(sniffer, "createship_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x349970");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2867");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2990");

            SniffLogFile(sniffer, "createship_103");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x349939");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2866");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2989");

            SniffLogFile(sniffer, "createship_104");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x348939");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2866");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2988");

            SniffLogFile(sniffer, "createship_105");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x347929");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2856");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2968");

            SniffLogFile(sniffer, "createship_106");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x347929");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2855");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2968");

            SniffLogFile(sniffer, "createship_107");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x347929");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2845");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2968");
        }

        /// <summary>
        /// 一括解体する(装備保管なしとあり)
        /// </summary>
        [TestMethod]
        public void DestroyShip()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "destroyship_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 250);
            PAssert.That(() => sniffer.ItemCounter.Now == 1055);
            PAssert.That(() => sniffer.Material.Current.Take(4).SequenceEqual(new[] {285615, 286250, 291010, 284744}));
        }

        /// <summary>
        /// 第2艦隊までしか解放していなくてもエラーにならないようにする
        /// </summary>
        [TestMethod]
        public void TwoFleets()
        {
            var sniffer = new Sniffer(true);
            SniffLogFile(sniffer, "twofleets_001");
            PAssert.That(() => new[]{5, 5, 0, 0}.SequenceEqual(sniffer.Fleets.Select(f => f.ChargeStatus.Fuel)));
        }

        /// <summary>
        /// ship2がリクエストで指定した艦娘のデータしか返さない
        /// </summary>
        [TestMethod]
        public void Ship2ReturnShipSpecifiedByRequest()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "ship2_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 243);
        }

        /// <summary>
        /// 出撃中にアイテムを取得する
        /// </summary>
        [TestMethod]
        public void ItemGetInSortie()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "itemget_001");
            PAssert.That(() => sniffer.MiscText ==
                               "[獲得アイテム]\r\n" +
                               "燃料: 1115\r\n" +
                               "弾薬: 25\r\n" +
                               "鋼材: 70\r\n" +
                               "家具箱（大）: 1\r\n" +
                               "給糧艦「間宮」: 1\r\n" +
                               "勲章: 1\r\n" +
                               "給糧艦「伊良湖」: 3\r\n" +
                               "プレゼント箱: 1\r\n" +
                               "補強増設: 2\r\n" +
                               "戦闘詳報: 1\r\n" +
                               "瑞雲(六三一空): 1\r\n" +
                               "夜間作戦航空要員: 1\r\n" +
                               "130mm B-13連装砲: 1\r\n" +
                               "潜水空母な桐箪笥: 1\r\n" +
                               "Gambier Bay: 1");
        }

        /// <summary>
        /// 出撃直後に資源を獲得する
        /// </summary>
        [TestMethod]
        public void ItemGetAtStart()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "itemget_002");
            PAssert.That(() => sniffer.MiscText ==
                               "[獲得アイテム]\r\n" +
                               "燃料: 65");
        }

        /// <summary>
        /// 航空偵察でアイテムを取得する
        /// </summary>
        [TestMethod]
        public void ItemGetInAirRecon()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airrecon_001");
            PAssert.That(() =>
                sniffer.MiscText ==
                "[海域ゲージ情報]\r\n 海域選択画面に進むと表示します。\r\n" +
                "[演習情報]\r\n 演習相手を選ぶと表示します。\r\n" +
                "[獲得アイテム]\r\n 帰投したときに表示します。",
                "失敗の場合");

            SniffLogFile(sniffer, "airrecon_002");
            PAssert.That(() => sniffer.MiscText == "[獲得アイテム]\r\n弾薬: 150\r\n開発資材: 1", "成功");

            SniffLogFile(sniffer, "airrecon_003");
            PAssert.That(() => sniffer.MiscText == "[獲得アイテム]\r\n弾薬: 150\r\n開発資材: 1", "途中でリロードして再出撃");
        }

        /// <summary>
        /// 秋刀魚を漁獲して帰投する
        /// </summary>
        [TestMethod]
        public void ItemGetSanma()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "sanma_2get_101");
            PAssert.That(() => sniffer.MiscText ==
                               "[獲得アイテム]\r\n" +
                               "秋刀魚: 2");
        }

        /// <summary>
        /// 海域突破時に選択報酬を決定する
        /// </summary>
        [TestMethod]
        public void SelectRewardOnResult()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "select_reward-on_result_101");
            PAssert.That(() => sniffer.MiscText ==
                "[海域ゲージ]\r\n" +
                "1-5 : 残り 4/4\r\n" +
                "2-5 : 残り 4/4\r\n" +
                "3-5 : 残り 3/4\r\n" +
                "4-5 : 残り 5/5\r\n" +
                "5-5 : 残り 5/5\r\n" +
                "6-5 : 残り 6/6\r\n" +
                "7-2 : 残り 4/4\r\n" +
                "7-3 : 残り 3/3\r\n" +
                "7-4 : 残り 5/5\r\n" +
                "55-2 : HP 20/3200\r\n",
                "戦闘終了して結果画面で報酬選択待ち");
            PAssert.That(() => sniffer.UseItems.ElementAt(0).ToString() == "燃料 x347899");

            SniffLogFile(sniffer, "select_reward-on_result_102");
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\r\n" +
                "燃料: 8000\r\n" +
                "勲章: 1\r\n" +
                "熟練搭乗員: 1\r\n" +
                "新型航空兵装資材: 2\r\n" +
                "戦闘詳報: 1\r\n" +
                "F4U-2 Night Corsair: 1\r\n" +
                "熟練甲板要員: 2\r\n" +
                "熟練甲板要員+航空整備員: 1",
                "報酬を選択して母港に戻る");
            PAssert.That(() => sniffer.UseItems.ElementAt(0).ToString() == "燃料 x355800", "報酬で上限超える");

            SniffLogFile(sniffer, "select_reward-on_result_103");
            PAssert.That(() => sniffer.MiscText ==
                "[海域ゲージ情報]\r\n 海域選択画面に進むと表示します。\r\n" +
                "[演習情報]\r\n 演習相手を選ぶと表示します。\r\n" +
                "[獲得アイテム]\r\n 帰投したときに表示します。",
                "次の出撃で獲得アイテムが初期化されてる");
        }

        /// <summary>
        /// 海域突破時の選択報酬を決定せず、再読み込みで母港に戻る
        /// </summary>
        [TestMethod]
        public void SelectRewardOnPort()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "select_reward-on_port_101");
            PAssert.That(() => sniffer.MiscText ==
                "[海域ゲージ]\r\n" +
                "5-5 : 残り 4/5\r\n" +
                "6-5 : 残り 6/6\r\n" +
                "7-2 : 残り 3/3\r\n" +
                "7-3 : 残り 3/3\r\n" +
                "7-4 : 残り 5/5\r\n" +
                "55-1 : HP 434/3000\r\n",
                "戦闘終了して結果画面で報酬選択待ち");

            SniffLogFile(sniffer, "select_reward-on_port_102");
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\r\n" +
                "応急修理女神: 2\r\n" +
                "給糧艦「間宮」: 2\r\n" +
                "勲章: 1\r\n" +
                "給糧艦「伊良湖」: 2\r\n" +
                "補強増設: 1\r\n" +
                "Loire 130M: 1\r\n" +
                "熟練甲板要員: 2",
                "報酬選択せず再読み込みで母港に戻る");

            SniffLogFile(sniffer, "select_reward-on_port_103");
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\r\n" +
                "応急修理女神: 2\r\n" +
                "給糧艦「間宮」: 2\r\n" +
                "勲章: 1\r\n" +
                "給糧艦「伊良湖」: 2\r\n" +
                "補強増設: 1\r\n" +
                "Loire 130M: 1\r\n" +
                "熟練甲板要員: 2\r\n" +
                "明石: 1",
                "母港で報酬選択して獲得アイテム表示に追加される");

            SniffLogFile(sniffer, "select_reward-on_port_104");
            PAssert.That(() => sniffer.MiscText ==
                "[海域ゲージ情報]\r\n 海域選択画面に進むと表示します。\r\n" +
                "[演習情報]\r\n 演習相手を選ぶと表示します。\r\n" +
                "[獲得アイテム]\r\n 帰投したときに表示します。",
                "次の出撃で獲得アイテムが初期化されてる");
        }

        /// <summary>
        /// 購入済みアイテムと保有アイテムを一覧に表示する
        /// </summary>
        [TestMethod]
        public void ShowPayitemUseitem()
        {
            var sniffer = new Sniffer();
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 0);
            PAssert.That(() => sniffer.PayItems == null);

            SniffLogFile(sniffer, "payitem_useitem_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x350000");
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x91");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x690");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x139");
            PAssert.That(() => sniffer.UseItems.ElementAt(30).ToString() == "捷号章 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x48");
            PAssert.That(() => sniffer.PayItems == null);

            SniffLogFile(sniffer, "payitem_useitem_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 8);
        }

        /// <summary>
        /// アイテムを購入してすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void BuyItem()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "payitem_useitem_buy_001");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");

            SniffLogFile(sniffer, "payitem_useitem_buy_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");
            PAssert.That(() => sniffer.PayItems.ElementAt(1).ToString() == "設営隊 x1");
        }

        /// <summary>
        /// 購入したアイテムを開封してすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void UnboxItem()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "payitem_useitem_unbox_001");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");
            PAssert.That(() => sniffer.PayItems.ElementAt(1).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(24).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 8);
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -1).Count() == 23);

            SniffLogFile(sniffer, "payitem_useitem_unbox_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x7");
            PAssert.That(() => sniffer.PayItems.ElementAt(1).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(24).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 7);
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -1).Count() == 24);

            SniffLogFile(sniffer, "payitem_useitem_unbox_003");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x7");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(24).ToString() == "設営隊 x2");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 7);
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -1).Count() == 24);
        }

        /// <summary>
        /// 購入したアイテムを開封するときに、アイテムあふれ警告の時点では開封しない
        /// </summary>
        [TestMethod]
        public void UnboxItemCaution()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "payitemuse_caution_001");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "ドック増設セット x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");

            SniffLogFile(sniffer, "payitemuse_caution_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "ドック増設セット x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");

            SniffLogFile(sniffer, "payitemuse_caution_003");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x7");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "ドック開放キー x1");
        }

        /// <summary>
        /// プレゼント箱、勲章、家具箱を使用してすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void UseItem()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "payitem_useitem_use_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x87");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x2690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x486");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x139");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x299");
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "プレゼント箱 x113");

            SniffLogFile(sniffer, "payitem_useitem_use_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x88");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x2690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x486");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x139");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x299");
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "プレゼント箱 x112");

            SniffLogFile(sniffer, "payitem_useitem_use_003");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x88");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x2690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x486");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x135");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(18).ToString() == "プレゼント箱 x112");

            SniffLogFile(sniffer, "payitem_useitem_use_004");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x88");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x4690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x476");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x135");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(18).ToString() == "プレゼント箱 x112");
        }

        /// <summary>
        /// 家具を購入してアイテム消費をすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void BuyFurniture()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "buy_furniture_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x30690");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x17");

            SniffLogFile(sniffer, "buy_furniture_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x28747");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x17");

            SniffLogFile(sniffer, "buy_furniture_003");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x9317");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");

            SniffLogFile(sniffer, "buy_furniture_004");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x1517");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x15");
        }

        /// <summary>
        /// 海域ゲージの情報を生成する
        /// </summary>
        [TestMethod]
        public void AreaGauge()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "mapgauge_001");
            PAssert.That(() =>
                sniffer.MiscText ==
                "[海域ゲージ]\r\n" +
                "1-6 : 残り 5/7\r\n" +
                "2-5 : 残り 4/4\r\n" +
                "3-5 : 残り 4/4\r\n" +
                "4-4 : 残り 4/4\r\n");
        }

        /// <summary>
        /// 7-2の2本目の海域ゲージを正しく表示する
        /// </summary>
        [TestMethod]
        public void SecondAreaGauge()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "mapgauge_002");
            PAssert.That(() =>
                sniffer.MiscText ==
                "[海域ゲージ]\r\n" +
                "1-6 : 残り 5/7\r\n" +
                "2-5 : 残り 2/4\r\n" +
                "5-5 : 残り 5/5\r\n" +
                "6-5 : 残り 6/6\r\n" +
                "7-2 : 残り 1/4\r\n");
        }

        /// <summary>
        /// 2019冬イベからクリア済みマップのゲージ情報がない
        /// </summary>
        [TestMethod]
        public void ClearedExMapGage()
        {
            var miscTextInfo = new MiscTextInfo(null, null);
            using (var stream = OpenLogFile("mapgauge_003"))
            {
                miscTextInfo.InspectMapInfo(JsonObject.Parse(stream.ReadToEnd()));
            }
            PAssert.That(() =>
                miscTextInfo.Text ==
                "[海域ゲージ]\r\n" +
                "1-6 : 残り 5/7\r\n" +
                "5-5 : 残り 5/5\r\n" +
                "7-2 : 残り 1/4\r\n" +
                "43-2 : HP 1050/1400\r\n");
        }

        /// <summary>
        /// 演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPoint()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "practice_004");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\r\n敵艦隊名 : 第一艦隊\r\n獲得経験値 : 878\r\nS勝利 : 1053");
        }

        /// <summary>
        /// 新規のドロップ艦の初期装備数を登録する
        /// </summary>
        [TestMethod]
        public void RecordNumEquipsOfNewDropShip()
        {
            var sniffer = new Sniffer();
            PAssert.That(() => sniffer.AdditionalData.NumEquips(565) == -1);
            PAssert.That(() => sniffer.ShipList.FirstOrDefault(s => s.Spec.Id == 565) == null);
            SniffLogFile(sniffer, "dropship_001");
            PAssert.That(() => sniffer.AdditionalData.NumEquips(565) == 2);
            PAssert.That(() => sniffer.ShipList.First(s => s.Spec.Id == 565).Spec.NumEquips == 2);
        }

        /// <summary>
        /// 既知のドロップ艦とその装備をカウントする
        /// </summary>
        [TestMethod]
        public void CountDropShip()
        {
            var sniffer = new Sniffer();
            sniffer.AdditionalData.RecordNumEquips(11, "", 1);
            SniffLogFile(sniffer, "dropship_002");
            PAssert.That(() => sniffer.ShipCounter.Now == 250);
            PAssert.That(() => sniffer.ItemCounter.Now == 1111);
        }

        /// <summary>
        /// ドロップ艦の搭載数が0にならない
        /// </summary>
        [TestMethod]
        public void DropShipOnslot()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "dropship_onslot_001");
            var ship = sniffer.ShipList.First(s => s.Id == 143298);
            PAssert.That(() => ship.Items[1].OnSlot == 2);
            PAssert.That(() => ship.Items[1].MaxEq == 2);
        }

        /// <summary>
        /// 艦娘数を数える
        /// </summary>
        [TestMethod]
        public void CountShips()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "ship_count_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 267 && sniffer.ShipCounter.Alarm, "ログイン");
            SniffLogFile(sniffer, "ship_count_002");
            PAssert.That(() => sniffer.ShipCounter.Now == 266 && sniffer.ShipCounter.Alarm, "建造");
            SniffLogFile(sniffer, "ship_count_003");
            PAssert.That(() => sniffer.ShipCounter.Now == 266 && sniffer.ShipCounter.Alarm, "ドロップ");
        }

        /// <summary>
        /// 714: 「駆逐艦」の改修工事を実施せよ！
        /// </summary>
        [TestMethod]
        public void PowerUp_714()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "powerup_004");
            PAssert.That(() => sniffer.Quests.First(q => q.Id == 714).Count.Now == 2);
        }
    }
}