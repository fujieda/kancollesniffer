﻿// Copyright (C) 2013-2021 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    using Sniffer = SnifferTest.TestingSniffer;
    using static SnifferTest;

    [TestClass]
    public class ShipStatusTest
    {
        private static Sniffer sniffer;

        protected static Sniffer Sniffer
        {
            get
            {
                if (sniffer == null)
                {
                    sniffer = new Sniffer();
                    SniffLogFile(sniffer, "api_start2");
                }
                return sniffer;
            }
        }

        protected static ShipMaster ShipMaster => Sniffer.ShipMaster;

        protected static ItemMaster ItemMaster => Sniffer.ItemMaster;

        protected static ShipInventory ShipInventory => Sniffer.ShipInventory;

        protected static ItemInventory ItemInventory => Sniffer.ItemInventory;

        protected static AdditionalData AdditionalData => ShipMaster.AdditionalData;

        protected static Fleet NewFleet(int number = 0)
        {
            return new Fleet(ShipInventory, number, null)
            {
                CombinedType = CombinedType.None
            };
        }

        protected static ShipStatus NewShip(int specId)
        {
            var ship = new ShipStatus(ShipInventory.MaxId + 1)
            {
                Spec = ShipMaster.GetSpec(specId),
                Level = 99,
                GetItem = itemId => ItemInventory[itemId]
            };
            ShipInventory.Add(ship);
            return ship;
        }

        protected static int NewItem(int specId, int onSlot = 0, int level = 0, int alv = 0)
        {
            var item = new ItemStatus(ItemInventory.MaxId + 1)
            {
                Spec = ItemMaster[specId],
                OnSlot = onSlot,
                MaxEq = onSlot,
                Level = level,
                Alv = alv
            };
            ItemInventory.Add(item);
            return item.Id;
        }

        protected static AirBase.PlaneInfo NewPlaneInfo(int itemId)
        {
            return new AirBase.PlaneInfo{Item = ItemInventory[itemId], State = 1, Cond = 1, EffectiveLandBasedReconModifier = 1.0};
        }

        protected static int 三式水中探信儀()
        {
            return NewItem(47);
        }

        protected static int 三式爆雷投射機()
        {
            return NewItem(45);
        }

        protected static int 九五式爆雷()
        {
            return NewItem(226);
        }

        // ReSharper disable once InconsistentNaming
        protected static int SGレーダー初期型()
        {
            return NewItem(315);
        }

        // ReSharper disable once InconsistentNaming
        protected static int SKレーダー()
        {
            return NewItem(278);
        }

        // ReSharper disable once InconsistentNaming
        protected static int 二式12cm迫撃砲改()
        {
            return NewItem(346);
        }

        protected static int 流星改(int onSlot)
        {
            return NewItem(52, onSlot);
        }

        protected static int カ号観測機(int onSlot = 18, int level = 0, int alv = 0)
        {
            return NewItem(69, onSlot, level, alv);
        }

        protected static int 三式指揮連絡機対潜(int onSlot = 18, int level = 0, int alv = 0)
        {
            return NewItem(70, onSlot, level, alv);
        }

        protected static int 九七式艦攻(int onSlot)
        {
            return NewItem(16, onSlot);
        }

        protected static int 九九式艦爆(int onSlot)
        {
            return NewItem(23, onSlot);
        }

        protected static int 水中聴音機零式()
        {
            return NewItem(132);
        }

        protected static int 九七式艦攻九三一空(int onSlot)
        {
            return NewItem(82, onSlot);
        }

        protected static int Swordfish(int onSlot, int level = 0)
        {
            return NewItem(242, onSlot, level);
        }

        protected static int TBF(int onSlot)
        {
            return NewItem(256, onSlot);
        }

        protected static int Ju87C改二KMX搭載機(int onSlot)
        {
            return NewItem(305, onSlot);
        }

        protected static int 零式艦戦64型複座KMX搭載機(int onSlot = 18)
        {
            return NewItem(447, onSlot);
        }

        protected static int 橘花改(int onSlot)
        {
            return NewItem(200, onSlot);
        }

        protected static int 天山一二型甲(int onSlot)
        {
            return NewItem(372, onSlot);
        }

        protected static int 天山一二型甲改空六号電探改装備機(int onSlot)
        {
            return NewItem(373, onSlot);
        }

        protected static int BarracudaMkII(int onSlot, int level = 0)
        {
            return NewItem(424, onSlot, level);
        }

        protected static int 瑞雲(int onSlot)
        {
            return NewItem(26, onSlot);
        }

        protected static int 瑞雲六三四空(int onSlot)
        {
            return NewItem(79, onSlot);
        }

        protected static int 瑞雲12型(int onSlot)
        {
            return NewItem(80, onSlot);
        }

        protected static int SwordfishMkIII改水上機型(int onSlot)
        {
            return NewItem(368, onSlot);
        }

        protected static int Re2001G改()
        {
            return NewItem(188, 1);
        }

        protected static int 彗星一二型三一号光電管爆弾搭載機(int onSlot = 1)
        {
            return NewItem(320, onSlot);
        }

        protected static int S51J(int onSlot)
        {
            return NewItem(326, onSlot);
        }

        protected static int S51J改(int onSlot = 18)
        {
            return NewItem(327, onSlot);
        }

        protected static int 一式戦隼II型改20戦隊(int onSlot, int level = 0, int alv = 0)
        {
            return NewItem(489, onSlot, level, alv);
        }

        protected static int 一式戦隼III型改熟練20戦隊(int onSlot = 18, int level = 0, int alv = 0)
        {
            return NewItem(491, onSlot, level, alv);
        }

        protected static int 二式大艇(int onSlot, int level = 0, int alv = 0)
        {
            return NewItem(138, onSlot, level, alv);
        }

        protected static int 二式陸上偵察機熟練(int onSlot, int level = 0, int alv = 0)
        {
            return NewItem(312, onSlot, level, alv);
        }

        protected static int _15_5cm三連装副砲()
        {
            return NewItem(12);
        }

        protected static int _61cm三連装魚雷()
        {
            return NewItem(13);
        }

        protected static int 甲標的甲型()
        {
            return NewItem(41);
        }

        protected static int 熟練甲板要員航空整備員(int level = 0)
        {
            return NewItem(478, level: level);
        }

        [TestClass]
        public class OpeningSubmarineAttack
        {
            /// <summary>
            /// 通常の先制対潜
            /// </summary>
            [TestMethod]
            public void CheckStandardCase()
            {
                var ship = NewShip(51);
                ship.Level = 99;
                ship.MaxAsw = 2;
                ship.SetItems(new[] {三式水中探信儀()});

                ship.ShownAsw = 99;
                Assert.IsFalse(ship.EnableOpeningAsw, "対潜不足");
                ship.ShownAsw = 100;
                Assert.IsTrue(ship.EnableOpeningAsw);
                ship.SetItems(new int[0]);
                Assert.IsFalse(ship.EnableOpeningAsw, "ソナー未搭載");
            }

            /// <summary>
            /// 海防艦の先制対潜
            /// </summary>
            [TestMethod]
            public void CheckCoastGuard()
            {
                var ship = NewShip(517);
                ship.SetItems(new[] {九五式爆雷()});
                ship.ShownAsw = 74;
                Assert.IsFalse(ship.EnableOpeningAsw, "対潜不足");

                ship.ShownAsw = 75;
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {SGレーダー初期型()});
                Assert.IsFalse(ship.EnableOpeningAsw, "装備対潜不足");

                ship.SetItems(new[] {SGレーダー初期型(), SGレーダー初期型()});
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 無条件で先制対潜が可能
            /// </summary>
            [DataTestMethod]
            [DataRow(141, "五十鈴改二")]
            [DataRow(478, "龍田改二")]
            [DataRow(394, "Jervis改")]
            [DataRow(893, "Janus改")]
            [DataRow(681, "Samuel B.Roberts改")]
            [DataRow(562, "Johnston")]
            [DataRow(689, "Johnston改")]
            [DataRow(596, "Fletcher")]
            [DataRow(692, "Fletcher改")]
            [DataRow(624, "夕張改二丁")]
            [DataRow(628, "Fletcher改 Mod.2")]
            [DataRow(629, "Fletcher Mk.II")]
            [DataRow(726, "Heywood L.E.改")]
            public void CheckNonConditional(int id, string name)
            {
                var ship = NewShip(id);
                Assert.AreEqual(name, ship.Name);
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 未改は無条件先制対潜が不可
            /// </summary>
            [DataTestMethod]
            [DataRow(519, "Jervis")]
            [DataRow(520, "Janus")]
            [DataRow(561, "Samuel B.Roberts")]
            [DataRow(941, "Heywood L.E.")]
            public void CheckFalseNonConditional(int id, string name)
            {
                var ship = NewShip(id);
                Assert.AreEqual(name, ship.Name);
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 大鷹改・改二、雲鷹改・改二、神鷹改・改二、加賀改二護
            /// </summary>
            [DataTestMethod]
            [DataRow(380, "大鷹改")]
            [DataRow(529, "大鷹改二")]
            [DataRow(382, "雲鷹改")]
            [DataRow(889, "雲鷹改二")]
            [DataRow(381, "神鷹改")]
            [DataRow(536, "神鷹改二")]
            [DataRow(646, "加賀改二護")]
            public void CheckAntiSubmarineAircraftCarrier(int id, string name)
            {
                var ship = NewShip(id);
                Assert.AreEqual(name, ship.Name);
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {流星改(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {流星改(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式指揮連絡機対潜(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式指揮連絡機対潜(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {九九式艦爆(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {九九式艦爆(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {彗星一二型三一号光電管爆弾搭載機()});
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 春日丸、大鷹、八幡丸、雲鷹、神鷹
            /// </summary>
            [DataTestMethod]
            [DataRow(521, "春日丸")]
            [DataRow(526, "大鷹")]
            [DataRow(522, "八幡丸")]
            [DataRow(884, "雲鷹")]
            [DataRow(534, "神鷹")]
            public void CheckFalseAntiSubmarineAircraftCarrier(int id, string name)
            {
                var ship = NewShip(id);
                Assert.AreEqual(name, ship.Name);

                ship.SetItems(new[] {流星改(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式指揮連絡機対潜(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {九九式艦爆(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            [DataTestMethod]
            public void CheckLightAircraftCarrierLevel50()
            {
                var ship = NewShip(89);
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 49;
                ship.SetItems(new[] {水中聴音機零式(), 九七式艦攻九三一空(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 50;
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 九七式艦攻九三一空(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 九七式艦攻九三一空(0), Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 九七式艦攻九三一空(0), TBF(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), TBF(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 三式指揮連絡機対潜(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 三式指揮連絡機対潜(0), Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 三式指揮連絡機対潜(0), TBF(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 三式指揮連絡機対潜(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), カ号観測機(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), カ号観測機(0), Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), カ号観測機(0), TBF(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), カ号観測機(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            [DataTestMethod]
            public void CheckLightAircraftCarrierLevel65()
            {
                var ship = NewShip(89);
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 64;
                ship.SetItems(new[] {九七式艦攻九三一空(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 65;
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {九七式艦攻九三一空(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {九七式艦攻九三一空(0), Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {九七式艦攻九三一空(0), TBF(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {TBF(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(0), Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(0), TBF(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式指揮連絡機対潜(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式指揮連絡機対潜(0), Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式指揮連絡機対潜(0), TBF(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式指揮連絡機対潜(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            [DataTestMethod]
            public void CheckLightAircraftCarrierLevel100()
            {
                var ship = NewShip(89);
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 100;
                ship.SetItems(new[] {水中聴音機零式(), カ号観測機(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), カ号観測機(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 流星改(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 流星改(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 九九式艦爆(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 九九式艦爆(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.ShownAsw = 99;
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 100;
                ship.SetItems(new[] {水中聴音機零式(), Re2001G改()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {水中聴音機零式(), 彗星一二型三一号光電管爆弾搭載機()});
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            [DataTestMethod]
            public void CheckAtacktypeLightAircraftCarrier()
            {
                var ship = NewShip(508); // 鈴谷航改二
                ship.SetItems(new[] {S51J改(1), S51J改(1), S51J改(1), 水中聴音機零式()});
                ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
                Assert.AreEqual(50, ship.ShownAsw);
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship = NewShip(509); // 熊野航改二
                ship.SetItems(new[] {S51J改(1), S51J改(1), S51J改(1), 水中聴音機零式()});
                ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
                Assert.AreEqual(50, ship.ShownAsw);
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            [TestMethod]
            public void 揚陸艦()
            {
                var ship = NewShip(626);
                ship.ShownAsw = 48;
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式水中探信儀(), カ号観測機(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式水中探信儀(), 瑞雲(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 100;

                ship.SetItems(new[] {カ号観測機(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {瑞雲(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式水中探信儀()});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式水中探信儀(), カ号観測機(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式水中探信儀(), カ号観測機(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式水中探信儀(), 瑞雲(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {三式水中探信儀(), 瑞雲(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            [TestMethod]
            public void 日向改二()
            {
                var ship = NewShip(554);
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(1)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(1), カ号観測機(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {カ号観測機(1), カ号観測機(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {S51J(0)});
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.SetItems(new[] {S51J(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.SetItems(new[] {S51J改(1)});
                Assert.IsTrue(ship.EnableOpeningAsw);
            }
        }

        protected static int TBM3D(int onSlot)
        {
            return NewItem(257, onSlot);
        }

        protected static int F6F3N(int onSlot)
        {
            return NewItem(254, onSlot);
        }

        protected static int 夜間作戦航空要員()
        {
            return NewItem(259);
        }

        protected static int 九八式水上偵察機夜偵(int onSlot)
        {
            return NewItem(102, onSlot);
        }

        protected static int 零式水上偵察機11型乙改夜偵(int onSlot = 4)
        {
            return NewItem(469, onSlot);
        }

        protected static int _14cm単装砲()
        {
            return NewItem(4);
        }

        protected static int _15_5cm三連装砲()
        {
            return NewItem(5);
        }

        protected static int _20_3cm連装砲()
        {
            return NewItem(6);
        }

        protected static int _15_2cm単装砲()
        {
            return NewItem(11);
        }

        protected static int _15_2cm連装砲()
        {
            return NewItem(65);
        }

        protected static int _203mm53連装砲()
        {
            return NewItem(162);
        }

        protected static int _6inch連装速射砲MkXXI()
        {
            return NewItem(359);
        }

        protected static int _6inchMkXXIII三連装砲()
        {
            return NewItem(399);
        }

        protected static int _5inch連装両用砲集中配備()
        {
            return NewItem(362);
        }

        protected static int _8inch三連装砲Mk9()
        {
            return NewItem(356);
        }

        protected static readonly Fleet NormalFleet = NewFleet();

        [TestClass]
        public class DayFirepower
        {
            [TestMethod]
            public void 軽巡に軽巡砲を装備して火力命中補正()
            {
                var ship = NewShip(52);
                ship.Fleet = NormalFleet;
                ship.Firepower = 21;
                ship.ImprovedFirepower = 0;
                ship.SetItems(new[] {_20_3cm連装砲(), _14cm単装砲()});
                Assert.AreEqual(27, ship.EffectiveFirepower, "軽巡に火力補正");
                Assert.AreEqual(107.9, Math.Round(ship.EffectiveAccuracy, 2), "命中はマイナス補正");

                var ship2 = NewShip(58);
                ship2.Fleet = NormalFleet;
                ship2.Firepower = 21;
                ship2.ImprovedFirepower = 0;
                ship2.SetItems(new[] {_20_3cm連装砲(), _15_2cm連装砲()});
                Assert.AreEqual(28, ship2.EffectiveFirepower, "雷巡に火力補正");
                Assert.AreEqual(109.9, Math.Round(ship2.EffectiveAccuracy, 2), "命中はマイナス補正");

                var ship3 = NewShip(343);
                ship3.Fleet = NormalFleet;
                ship3.Firepower = 30;
                ship3.ImprovedFirepower = 0;
                ship3.SetItems(new[] {_14cm単装砲(), _15_2cm連装砲(), _15_2cm単装砲(), _6inch連装速射砲MkXXI()});
                Assert.AreEqual(39.24, Math.Round(ship3.EffectiveFirepower, 2), "練巡に火力補正");
                Assert.AreEqual(116.9, Math.Round(ship3.EffectiveAccuracy, 2), "命中はマイナス補正");

                var ship4 = NewShip(262);
                ship4.Fleet = NormalFleet;
                ship4.Firepower = 50;
                ship4.ImprovedFirepower = 0;
                ship4.SetItems(new[] {_14cm単装砲(), _15_2cm連装砲(), _15_2cm単装砲(), _6inch連装速射砲MkXXI()});
                Assert.AreEqual(55, ship4.EffectiveFirepower, "重巡は補正なし");
                Assert.AreEqual(116.9, Math.Round(ship4.EffectiveAccuracy, 2), "命中も補正なし");
            }

            [TestMethod]
            public void 軽巡中口径砲命中補正()
            {
                var ship = NewShip(52);
                ship.Fleet = NormalFleet;
                ship.Lucky = 5;
                var agano = NewShip(137);
                agano.Fleet = NormalFleet;
                agano.Lucky = 5;
                var atlanta = NewShip(597);
                atlanta.Fleet = NormalFleet;
                atlanta.Lucky = 5;
                var ooyodo = NewShip(183);
                ooyodo.Fleet = NormalFleet;
                ooyodo.Lucky = 5;

                ship.Level = 99;
                ship.SetItems(new[] {_14cm単装砲()});
                Assert.AreEqual(114.25, Math.Round(ship.EffectiveAccuracy, 2), "Lv99では補正なし");
                ship.Level = 100;
                Assert.AreEqual(115.35, Math.Round(ship.EffectiveAccuracy, 2), "Lv100で加算補正");
                ship.SetItems(new[] {_14cm単装砲(), _14cm単装砲()});
                Assert.AreEqual(117.35, Math.Round(ship.EffectiveAccuracy, 2), "加算補正2本分");

                ship.Level = 99;
                ship.SetItems(new[] {_15_2cm連装砲()});
                Assert.AreEqual(116.25, Math.Round(ship.EffectiveAccuracy, 2), "Lv99では補正なし");
                ship.Level = 100;
                Assert.AreEqual(117.35, Math.Round(ship.EffectiveAccuracy, 2), "Lv100で加算補正");
                ship.SetItems(new[] {_15_2cm連装砲(), _15_2cm連装砲()});
                Assert.AreEqual(121.35, Math.Round(ship.EffectiveAccuracy, 2), "加算補正2本分");

                agano.Level = 99;
                agano.SetItems(new[] {_6inch連装速射砲MkXXI()});
                Assert.AreEqual(120.25, Math.Round(agano.EffectiveAccuracy, 2), "阿賀野型は加算補正");
                agano.SetItems(new[] {_6inch連装速射砲MkXXI(), _6inch連装速射砲MkXXI()});
                Assert.AreEqual(124.32, Math.Round(agano.EffectiveAccuracy, 2), "加算補正2本分");
                agano.Level = 100;
                Assert.AreEqual(125.84, Math.Round(agano.EffectiveAccuracy, 2), "Lv100で加算補正");

                ship.Level = 99;
                ship.SetItems(new[] {_6inchMkXXIII三連装砲()});
                Assert.AreEqual(111.25, Math.Round(ship.EffectiveAccuracy, 2), "減算補正");
                ship.Level = 100;
                Assert.AreEqual(112.55, Math.Round(ship.EffectiveAccuracy, 2), "Lv100で減算補正軽減");
                ship.SetItems(new[] {_6inchMkXXIII三連装砲(), _6inchMkXXIII三連装砲()});
                Assert.AreEqual(111.75, Math.Round(ship.EffectiveAccuracy, 2), "減算補正2本分");

                ship.Level = 99;
                ship.SetItems(new[] {_5inch連装両用砲集中配備()});
                Assert.AreEqual(112.25, Math.Round(ship.EffectiveAccuracy, 2), "減算補正");
                ship.Level = 100;
                Assert.AreEqual(113.55, Math.Round(ship.EffectiveAccuracy, 2), "Lv100で減算補正軽減");
                ship.SetItems(new[] {_5inch連装両用砲集中配備(), _5inch連装両用砲集中配備()});
                Assert.AreEqual(113.75, Math.Round(ship.EffectiveAccuracy, 2), "減算補正2本分");

                atlanta.Level = 99;
                atlanta.SetItems(new[] {_5inch連装両用砲集中配備()});
                Assert.AreEqual(115.25, Math.Round(atlanta.EffectiveAccuracy, 2), "アトランタ級は補正なし");
                atlanta.Level = 100;
                Assert.AreEqual(116.85, Math.Round(atlanta.EffectiveAccuracy, 2), "Lv100で加算補正");
                atlanta.SetItems(new[] {_5inch連装両用砲集中配備(), _5inch連装両用砲集中配備()});
                Assert.AreEqual(120.35, Math.Round(atlanta.EffectiveAccuracy, 2), "加算補正2本分");

                ship.Level = 99;
                ship.SetItems(new[] {_15_5cm三連装砲()});
                Assert.AreEqual(111.75, Math.Round(ship.EffectiveAccuracy, 2), "減算補正");
                ship.Level = 100;
                Assert.AreEqual(113.85, Math.Round(ship.EffectiveAccuracy, 2), "Lv100で減算補正軽減");
                ship.SetItems(new[] {_15_5cm三連装砲(), _15_5cm三連装砲()});
                Assert.AreEqual(114.35, Math.Round(ship.EffectiveAccuracy, 2), "減算補正2本分");

                ooyodo.Level = 99;
                ooyodo.SetItems(new[] {_15_5cm三連装砲()});
                Assert.AreEqual(113.25, Math.Round(ooyodo.EffectiveAccuracy, 2), "阿賀野型は減算補正軽減");
                ooyodo.Level = 100;
                Assert.AreEqual(113.75, Math.Round(ooyodo.EffectiveAccuracy, 2), "Lv100で減算補正軽減");
                ooyodo.SetItems(new[] {_15_5cm三連装砲(), _15_5cm三連装砲()});
                Assert.AreEqual(114.15, Math.Round(ooyodo.EffectiveAccuracy, 2), "減算補正2本分");

                ship.Level = 99;
                ship.SetItems(new[] {_20_3cm連装砲()});
                Assert.AreEqual(110.25, Math.Round(ship.EffectiveAccuracy, 2), "減算補正");
                ship.Level = 100;
                Assert.AreEqual(111.55, Math.Round(ship.EffectiveAccuracy, 2), "Lv100で減算補正軽減");
                ship.SetItems(new[] {_20_3cm連装砲(), _20_3cm連装砲()});
                Assert.AreEqual(109.75, Math.Round(ship.EffectiveAccuracy, 2), "減算補正2本分");

                agano.Level = 99;
                agano.SetItems(new[] {_20_3cm連装砲()});
                Assert.AreEqual(109.25, Math.Round(agano.EffectiveAccuracy, 2), "阿賀野型は強減算");
                agano.SetItems(new[] {_20_3cm連装砲(), _20_3cm連装砲()});
                Assert.AreEqual(105.25, Math.Round(agano.EffectiveAccuracy, 2), "強減算2本分");
                agano.Level = 100;
                Assert.AreEqual(107.35, Math.Round(agano.EffectiveAccuracy, 2), "Lv100で強減算軽減");

                ooyodo.Level = 99;
                ooyodo.SetItems(new[] {_20_3cm連装砲()});
                Assert.AreEqual(108.25, Math.Round(ooyodo.EffectiveAccuracy, 2), "大淀型は強減算");
                ooyodo.Level = 100;
                Assert.AreEqual(110.35, Math.Round(ooyodo.EffectiveAccuracy, 2), "Lv100で強減算軽減");
                ooyodo.SetItems(new[] {_20_3cm連装砲(), _20_3cm連装砲()});
                Assert.AreEqual(107.35, Math.Round(ooyodo.EffectiveAccuracy, 2), "強減算2本分");

                ship.Level = 99;
                ship.SetItems(new[] {_8inch三連装砲Mk9()});
                Assert.AreEqual(102.25, Math.Round(ship.EffectiveAccuracy, 2), "減算補正");
                ship.Level = 100;
                Assert.AreEqual(104.55, Math.Round(ship.EffectiveAccuracy, 2), "Lv100で減算補正軽減");
                ship.SetItems(new[] {_8inch三連装砲Mk9(), _8inch三連装砲Mk9()});
                Assert.AreEqual(95.75, Math.Round(ship.EffectiveAccuracy, 2), "減算補正2本分");
            }

            [TestMethod]
            public void イタリア重巡にイタリア重巡砲を装備して火力命中補正()
            {
                var ship = NewShip(448);
                ship.Fleet = NormalFleet;
                ship.Firepower = 53;
                ship.ImprovedFirepower = 0;
                ship.SetItems(new[] {_20_3cm連装砲(), _203mm53連装砲()});
                Assert.AreEqual(59, ship.EffectiveFirepower);
                Assert.AreEqual(108.9, Math.Round(ship.EffectiveAccuracy, 2));

                var ship2 = NewShip(361);
                ship2.Fleet = NormalFleet;
                ship2.Firepower = 59;
                ship2.ImprovedFirepower = 0;
                ship2.SetItems(new[] {_203mm53連装砲(), _203mm53連装砲()});
                Assert.AreEqual(65.41, Math.Round(ship2.EffectiveFirepower, 2));
                Assert.AreEqual(107.9, Math.Round(ship2.EffectiveAccuracy, 2));

                var ship3 = NewShip(59);
                ship3.Fleet = NormalFleet;
                ship3.Firepower = 48;
                ship3.ImprovedFirepower = 0;
                ship3.SetItems(new[] {_203mm53連装砲(), _203mm53連装砲()});
                Assert.AreEqual(53, ship3.EffectiveFirepower, "イタリア以外の重巡は補正なし");
                Assert.AreEqual(105.9, Math.Round(ship3.EffectiveAccuracy, 2), "命中も補正なし");
            }

            [TestMethod]
            public void 空母に艦攻0機で昼攻撃不可()
            {
                var ship = NewShip(545);
                ship.Firepower = 68;
                ship.Torpedo = 0;
                ship.SetItems(new[] {流星改(0)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 空母に艦爆0機で昼攻撃不可()
            {
                var ship = NewShip(545);
                ship.Firepower = 68;
                ship.Torpedo = 0;
                ship.SetItems(new[] {九九式艦爆(0)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 空母に艦攻0機艦爆0機熟練甲板要員航空整備員で昼攻撃不可()
            {
                var ship = NewShip(545);
                ship.Firepower = 68;
                ship.Torpedo = 0;
                ship.SetItems(new[] {流星改(0), 九九式艦爆(0), 熟練甲板要員航空整備員(10)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                Assert.AreEqual(0, ship.EffectiveFirepower);
                Assert.AreEqual(1, ship.Items[0].ApBonus);
                Assert.AreEqual(1, ship.Items[1].ApBonus);
            }

            [TestMethod]
            public void 空母に艦攻0機艦爆0機一式戦隼II型改20戦隊で昼攻撃不可()
            {
                var ship = NewShip(529);
                ship.Firepower = 41;
                ship.Torpedo = 0;
                ship.SetItems(new[] {流星改(0), 九九式艦爆(0), 一式戦隼II型改20戦隊(10)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻装備で空母と同じ昼爆撃()
            {
                var ship = NewShip(352);
                ship.Firepower = 36;
                ship.Torpedo = 13;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {流星改(6)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(128, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に瑞雲装備で通常の砲撃()
            {
                var ship = NewShip(352);
                ship.Firepower = 36;
                ship.Torpedo = 13;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {瑞雲(6)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(41, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 山汐丸改に艦爆装備で空母と同じ昼爆撃()
            {
                var ship = NewShip(717);
                ship.Firepower = 23;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {零式艦戦64型複座KMX搭載機(8)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(94, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 山汐丸改に対潜機装備で通常の砲撃()
            {
                var ship = NewShip(717);
                ship.Firepower = 24;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {カ号観測機(8)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(29, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻と水上機装備で水上機の攻撃力が加算()
            {
                var ship = NewShip(352);
                ship.Firepower = 36;
                ship.Torpedo = 13;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {流星改(6), 瑞雲(1)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(136, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻0機で昼攻撃不可()
            {
                var ship = NewShip(352);
                ship.Firepower = 36;
                ship.Torpedo = 13;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {流星改(0)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻0機と水上機でも昼攻撃不可()
            {
                var ship = NewShip(352);
                ship.Firepower = 36;
                ship.Torpedo = 13;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {流星改(0), 瑞雲(1)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 山汐丸改に艦爆と一式戦隼II型改20戦隊で空母と同じ昼爆撃に加算する()
            {
                var ship = NewShip(717);
                ship.Firepower = 23;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;

                ship.SetItems(new[] {零式艦戦64型複座KMX搭載機(8), 一式戦隼II型改20戦隊(6)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(103, ship.EffectiveFirepower);
                var bomberPower = ship.Items[1].CalcBomberPower();
                Assert.AreEqual(1, bomberPower.Length);
                Assert.AreEqual(25, Math.Round(bomberPower[0], 2), "爆装は0で計算");

                ship.SetItems(new[] {零式艦戦64型複座KMX搭載機(8), 一式戦隼II型改20戦隊(6, 8)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(103, ship.EffectiveFirepower, "改修値は昼爆撃に影響しない");
                bomberPower = ship.Items[1].CalcBomberPower();
                Assert.AreEqual(1, bomberPower.Length);
                Assert.AreEqual(25, Math.Round(bomberPower[0], 2), "改修値は空戦に影響しない");
            }

            [TestMethod]
            public void 山汐丸改に対潜機装備と一式戦隼II型改20戦隊で通常の砲撃に加算しない()
            {
                var ship = NewShip(717);
                ship.Firepower = 24;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {カ号観測機(8), 一式戦隼II型改20戦隊(6)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(29, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 不明な空母には雷装ボーナスなし()
            {
                var ship = NewShip(197);
                ship.Firepower = 63;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(163, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦につかない");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "天山に雷装ボーナスがつかない");
                Assert.AreEqual(57.41, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "天山の雷装ボーナスが航空戦につかない");
                Assert.AreEqual(107.65, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "天山の雷装ボーナスが航空戦につかない");
            }

            [TestMethod]
            public void 翔鶴改二の天山に雷装ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 63 + 1;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(166, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(1, ship.Items[0].TbBonus, "天山に雷装ボーナスがつく");
                Assert.AreEqual(61.57, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "天山の雷装ボーナスが航空戦につく");
                Assert.AreEqual(115.44, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "天山の雷装ボーナスが航空戦につく");
            }

            [TestMethod]
            public void 翔鶴改二の流星改に雷装ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 63 + 1;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27), 流星改(27)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(185, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "天山の雷装ボーナスは天山につかない");
                Assert.AreEqual(57.41, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(107.65, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(1, ship.Items[1].TbBonus, "天山の雷装ボーナスは流星改につく");
                Assert.AreEqual(78.2, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "雷装ボーナスは流星改の航空戦につく");
                Assert.AreEqual(146.62, Math.Round(ship.Items[1].CalcBomberPower()[1], 2), "雷装ボーナスは流星改の航空戦につく");
            }

            [TestMethod]
            public void 翔鶴改二の搭載0の流星改には雷装ボーナスつかない()
            {
                var ship = NewShip(461);
                ship.Firepower = 63 + 1;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27), 流星改(0)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(185, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(1, ship.Items[0].TbBonus, "天山の雷装ボーナスは天山につく");
                Assert.AreEqual(61.57, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "雷装ボーナスは天山の航空戦につく");
                Assert.AreEqual(115.44, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "雷装ボーナスは天山の航空戦につく");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "天山の雷装ボーナスは流星改につかない");
                Assert.AreEqual(0, ship.Items[1].CalcBomberPower().Length, "雷装ボーナスは流星改の航空戦につかない");
            }

            [TestMethod]
            public void 翔鶴改二の機数が多いJu87Cに雷装ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 63 + 1;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {夜間作戦航空要員(), 天山一二型甲(26), Ju87C改二KMX搭載機(27)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(182, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "天山の雷装ボーナスは天山につかない");
                Assert.AreEqual(56.71, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(106.34, Math.Round(ship.Items[1].CalcBomberPower()[1], 2), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(1, ship.Items[2].TbBonus, "天山の雷装ボーナスはJu87Cにつく");
                Assert.AreEqual(76.96, Math.Round(ship.Items[2].CalcBomberPower()[0], 2), "雷装ボーナスはJu87Cの航空戦につく");
            }

            [TestMethod]
            public void 翔鶴改二のスロットが上にある橘花改に雷装ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 63 + 2;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {夜間作戦航空要員(), 橘花改(27), 天山一二型甲改空六号電探改装備機(27)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(193, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(2, ship.Items[1].TbBonus, "夜攻天山の雷装ボーナスは橘花改につく");
                Assert.AreEqual(82.16, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "雷装ボーナスは通常の航空戦のみ");
                Assert.AreEqual(65.44, Math.Round(ship.Items[1].CalcBomberPower()[1], 2), "雷装ボーナスは通常の航空戦のみ");
                Assert.AreEqual(0, ship.Items[2].TbBonus, "夜攻天山の雷装ボーナスは夜攻天山につかない");
                Assert.AreEqual(65.73, Math.Round(ship.Items[2].CalcBomberPower()[0], 2), "雷装ボーナスは夜攻天山の航空戦につかない");
                Assert.AreEqual(123.24, Math.Round(ship.Items[2].CalcBomberPower()[1], 2), "雷装ボーナスは夜攻天山の航空戦につかない");
            }

            [TestMethod]
            public void 翔鶴改二の夜攻天山に雷装ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 63 + 2 + 1;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27), 天山一二型甲改空六号電探改装備機(27)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(185, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "天山の雷装ボーナスは天山につかない");
                Assert.AreEqual(57.41, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(107.65, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(1, ship.Items[1].TbBonus, "天山の雷装ボーナスは夜攻天山につく");
                Assert.AreEqual(69.88, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "雷装ボーナスは夜攻天山の航空戦につく");
                Assert.AreEqual(131.03, Math.Round(ship.Items[1].CalcBomberPower()[1], 2), "雷装ボーナスは夜攻天山の航空戦につく");
            }

            [TestMethod]
            public void 空母に熟練甲板要員航空整備員()
            {
                var ship = NewShip(197);
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27), 九九式艦爆(27), 熟練甲板要員航空整備員()});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                ship.Firepower = 63 + ship.Items.Sum(item => item.Spec.Firepower);
                ship.Torpedo = 0 + ship.Items.Sum(item => item.Spec.Torpedo);

                Assert.AreEqual(185, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦につかない");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "天山に雷装ボーナスがつかない");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "九九式艦爆に雷装ボーナスがつかない");
                Assert.AreEqual(0, ship.Items[0].ApBonus, "天山に航空要員ボーナスがつかない");
                Assert.AreEqual(0, ship.Items[1].ApBonus, "九九式艦爆に航空要員ボーナスがつかない");
                Assert.AreEqual(57.41, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "天山のボーナスが航空戦につかない");
                Assert.AreEqual(107.65, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "天山のボーナスが航空戦につかない");
                Assert.AreEqual(50.98, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "九九式艦爆のボーナスが航空戦につかない");
            }

            [TestMethod]
            public void 空母に熟練甲板要員航空整備員ボーナス()
            {
                var ship = NewShip(197);
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27), 九九式艦爆(27), 熟練甲板要員航空整備員(4)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                ship.Firepower = 63 + 1 + ship.Items.Sum(item => item.Spec.Firepower);
                ship.Torpedo = 0 + ship.Items.Sum(item => item.Spec.Torpedo);

                Assert.AreEqual(193, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦につかない");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "天山に雷装ボーナスがつかない");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "九九式艦爆に雷装ボーナスがつかない");
                Assert.AreEqual(0, ship.Items[0].ApBonus, "天山に航空要員ボーナスがつかない");
                Assert.AreEqual(1, ship.Items[1].ApBonus, "九九式艦爆に航空要員ボーナスがつく");
                Assert.AreEqual(57.41, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "天山のボーナスが航空戦につかない");
                Assert.AreEqual(107.65, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "天山のボーナスが航空戦につかない");
                Assert.AreEqual(56.18, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "九九式艦爆の雷装ボーナスが航空戦につかないが、航空要員ボーナスはつく");
            }

            [TestMethod]
            public void 空母に熟練甲板要員航空整備員ボーナスと雷装ボーナス()
            {
                var ship = NewShip(461);
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {天山一二型甲(27), 九九式艦爆(27), 熟練甲板要員航空整備員(7), 熟練甲板要員航空整備員(4)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                ship.Firepower = 63 + 1 + 2 + ship.Items.Sum(item => item.Spec.Firepower);
                ship.Torpedo = 0 + 1 + 1 + ship.Items.Sum(item => item.Spec.Torpedo);

                Assert.AreEqual(216, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(1, ship.Items[0].TbBonus, "天山に雷装ボーナスがつく");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "九九式艦爆に雷装ボーナスがつかない");
                Assert.AreEqual(1, ship.Items[0].ApBonus, "天山に航空要員ボーナスがつく");
                Assert.AreEqual(1, ship.Items[1].ApBonus, "九九式艦爆に航空要員ボーナスがつく");
                Assert.AreEqual(65.73, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "天山の雷装ボーナスと航空要員ボーナスが航空戦につく");
                Assert.AreEqual(123.24, Math.Round(ship.Items[0].CalcBomberPower()[1], 2), "天山の雷装ボーナスと航空要員ボーナスが航空戦につく");
                Assert.AreEqual(56.18, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "九九式艦爆の雷装ボーナスが航空戦につかないが、航空要員ボーナスはつく");
            }

            [TestMethod]
            public void GotlandのSwordfishは雷装ボーナスなし()
            {
                var ship = NewShip(574);
                ship.SetItems(new[] {SwordfishMkIII改水上機型(2)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(0, ship.Items[0].TbBonus, "Swordfishに雷装ボーナスがつかない");
                Assert.AreEqual(34.9, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "Swordfishの爆装ボーナスが航空戦につかない");
            }

            [TestMethod]
            public void Gotland改二のSwordfishに雷装ボーナス()
            {
                var ship = NewShip(630);
                ship.SetItems(new[] {SwordfishMkIII改水上機型(2)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(2, ship.Items[0].TbBonus, "Swordfishに雷装ボーナスがつく");
                Assert.AreEqual(37.73, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "Swordfishの爆装ボーナスが航空戦につく");
            }

            [TestMethod]
            public void Gotland改二の雷装ボーナス付加は爆装で判定()
            {
                var ship = NewShip(630);
                ship.SetItems(new[] {瑞雲六三四空(2), SwordfishMkIII改水上機型(2)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(0, ship.Items[0].TbBonus, "Swordfishの雷装ボーナスは瑞雲につかない");
                Assert.AreEqual(33.49, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "雷装ボーナスは瑞雲の航空戦につかない");
                Assert.AreEqual(2, ship.Items[1].TbBonus, "Swordfishの雷装ボーナスはSwordfishにつく");
                Assert.AreEqual(37.73, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "雷装ボーナスはSwordfishの航空戦につく");
            }

            [TestMethod]
            public void Gotland改二の瑞雲に雷装ボーナス()
            {
                var ship = NewShip(630);
                ship.SetItems(new[] {瑞雲12型(2), SwordfishMkIII改水上機型(2)});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(2, ship.Items[0].TbBonus, "Swordfishの雷装ボーナスは瑞雲につく");
                Assert.AreEqual(37.73, Math.Round(ship.Items[0].CalcBomberPower()[0], 2), "雷装ボーナスは瑞雲の航空戦につく");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "Swordfishの雷装ボーナスはSwordfishにつかない");
                Assert.AreEqual(34.9, Math.Round(ship.Items[1].CalcBomberPower()[0], 2), "雷装ボーナスはSwordfishの航空戦につかない");
            }

            [TestMethod]
            public void 基地航空でも対潜機で対潜可能かつ一式戦隼II型改20戦隊のみ爆撃可能()
            {
                var airBasePlane = NewPlaneInfo(カ号観測機(18));
                Assert.AreEqual(0, airBasePlane.ShipBomberPower.Count);
                Assert.AreEqual(0, airBasePlane.LandBomberPower.BomberPower);
                Assert.AreEqual(60, airBasePlane.AswBomberPower.Max);
                airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18));
                Assert.AreEqual(0, airBasePlane.ShipBomberPower.Count);
                Assert.AreEqual(0, airBasePlane.LandBomberPower.BomberPower);
                Assert.AreEqual(51, airBasePlane.AswBomberPower.Max);
                airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18));
                Assert.AreEqual(47, airBasePlane.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(47, airBasePlane.LandBomberPower.BomberPower);
                Assert.AreEqual(56, airBasePlane.AswBomberPower.Max);

                airBasePlane = NewPlaneInfo(カ号観測機(18, level: 10));
                Assert.AreEqual(0, airBasePlane.ShipBomberPower.Count);
                Assert.AreEqual(0, airBasePlane.LandBomberPower.BomberPower);
                Assert.AreEqual(70, airBasePlane.AswBomberPower.Max);
                airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18, level: 10));
                Assert.AreEqual(0, airBasePlane.ShipBomberPower.Count);
                Assert.AreEqual(0, airBasePlane.LandBomberPower.BomberPower);
                Assert.AreEqual(61, airBasePlane.AswBomberPower.Max);
                airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18, level: 10));
                Assert.AreEqual(47, airBasePlane.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(47, airBasePlane.LandBomberPower.BomberPower);
                Assert.AreEqual(66, airBasePlane.AswBomberPower.Max);
            }
        }

        [TestClass]
        public class TorpedoEnabled
        {
            [TestMethod]
            public void 通常の駆逐軽巡重順は初期状態で雷撃可能()
            {
                var ship = NewShip(1);
                ship.Fleet = NormalFleet;
                ship.Torpedo = 18;
                ship.ImprovedTorpedo = 0;
                Assert.AreEqual(23, ship.EffectiveTorpedo);

                var ship2 = NewShip(51);
                ship2.Fleet = NormalFleet;
                ship2.Torpedo = 18;
                ship2.ImprovedTorpedo = 0;
                Assert.AreEqual(23, ship2.EffectiveTorpedo);

                var ship3 = NewShip(59);
                ship3.Fleet = NormalFleet;
                ship3.Torpedo = 12;
                ship3.ImprovedTorpedo = 0;
                Assert.AreEqual(17, ship3.EffectiveTorpedo);
            }

            [TestMethod]
            public void 雷装初期値0のままでは雷撃不可()
            {
                var ship = NewShip(183);
                ship.Fleet = NormalFleet;
                ship.Torpedo = 0;
                ship.ImprovedTorpedo = 0;
                Assert.AreEqual(0, ship.EffectiveTorpedo);

                ship.SetItems(new[] {_61cm三連装魚雷()});
                ship.Torpedo = ship.Items[0].Spec.Torpedo;
                Assert.AreEqual(0, ship.EffectiveTorpedo);

                ship.Torpedo = 1;
                ship.ImprovedTorpedo = 1;
                ship.SetItems(new int[0]);
                Assert.AreEqual(6, ship.EffectiveTorpedo);
            }

            [TestMethod]
            public void 空母水母速吸改神威改はどんな装備でも雷撃不可()
            {
                var ship = NewShip(89);
                ship.Fleet = NormalFleet;
                ship.Torpedo = 13;
                ship.ImprovedTorpedo = 0;
                ship.SetItems(new[] {流星改(8)});
                Assert.AreEqual(0, ship.EffectiveTorpedo);

                var ship2 = NewShip(352);
                ship2.Fleet = NormalFleet;
                ship2.Torpedo = 13;
                ship2.ImprovedTorpedo = 0;
                ship2.SetItems(new[] {流星改(6)});
                Assert.AreEqual(0, ship2.EffectiveTorpedo);

                var ship3 = NewShip(499);
                ship3.Fleet = NormalFleet;
                ship3.Torpedo = 12;
                ship3.ImprovedTorpedo = 0;
                ship3.SetItems(new[] {甲標的甲型()});
                Assert.AreEqual(0, ship3.EffectiveTorpedo);

                var ship4 = NewShip(451);
                ship4.Fleet = NormalFleet;
                ship4.Torpedo = 4;
                ship4.ImprovedTorpedo = 0;
                ship4.SetItems(new[] {瑞雲(12)});
                Assert.AreEqual(0, ship4.EffectiveTorpedo);
            }
        }

        protected static int 甲標的丙型(int level = 0)
        {
            return NewItem(309, level: level);
        }

        protected static int 天山一二型甲改熟練空六号電探改装備機(int onSlot)
        {
            return NewItem(374, onSlot);
        }

        protected static int 零戦62型爆戦岩井隊(int onSlot, int level = 0)
        {
            return NewItem(154, onSlot, level);
        }

        protected static int TBM3W3S(int onSlot)
        {
            return NewItem(389, onSlot);
        }

        protected static int 烈風改二戊型一航戦熟練(int onSlot)
        {
            return NewItem(339, onSlot);
        }

        protected static int SwordfishMkIII熟練(int onSlot)
        {
            return NewItem(244, onSlot);
        }

        protected static int F6F5N(int onSlot)
        {
            return NewItem(255, onSlot);
        }

        [TestClass]
        public class NightBattlePower
        {
            [TestMethod]
            public void 甲標的の改修効果()
            {
                var ship = NewShip(58);
                ship.Torpedo = 102;
                ship.SetItems(new[] {甲標的丙型(4)});
                Assert.AreEqual(104, ship.NightBattlePower);
            }

            [TestMethod]
            public void LuigiTorelliの夜戦判定()
            {
                var ship = NewShip(535);
                ship.Firepower = 10;
                ship.Torpedo = 62;
                Assert.AreEqual(72, ship.NightBattlePower, "未改は夜戦可能");

                ship = NewShip(605);
                ship.Firepower = 8;
                ship.Torpedo = 32;
                Assert.AreEqual(0, ship.NightBattlePower, "改は夜戦不可");
            }

            [TestMethod]
            public void 宗谷の夜戦判定()
            {
                var ship = NewShip(699);
                ship.Firepower = 9;
                Assert.AreEqual(9, ship.NightBattlePower, "特務は夜戦可能");

                ship = NewShip(645);
                ship.Firepower = 1;
                Assert.AreEqual(0, ship.NightBattlePower, "灯台は夜戦不可");

                ship = NewShip(650);
                ship.Firepower = 1;
                Assert.AreEqual(0, ship.NightBattlePower, "南極は夜戦不可");
            }

            [TestMethod]
            public void Langleyの夜戦判定()
            {
                var ship = NewShip(925);
                ship.Firepower = 32;
                Assert.AreEqual(0, ship.NightBattlePower, "未改は夜戦不可");

                ship = NewShip(930);
                ship.Firepower = 40;
                Assert.AreEqual(0, ship.NightBattlePower, "改も夜戦不可");
            }

            [TestMethod]
            public void GambierBayの夜戦判定()
            {
                var ship = NewShip(396);
                ship.Firepower = 32;
                Assert.AreEqual(0, ship.NightBattlePower, "改は夜戦不可");

                ship = NewShip(707);
                ship.Firepower = 55;
                Assert.AreEqual(0, ship.NightBattlePower, "Mk.IIも夜戦不可");
            }

            [TestMethod]
            public void 大鷹型の夜戦判定()
            {
                var ship = NewShip(380);
                ship.Firepower = 23;
                Assert.AreEqual(0, ship.NightBattlePower, "大鷹改は夜戦不可");

                ship = NewShip(529);
                ship.Firepower = 39;
                Assert.AreEqual(39, ship.NightBattlePower, "大鷹改二は夜戦可能");

                ship = NewShip(381);
                ship.Firepower = 24;
                Assert.AreEqual(0, ship.NightBattlePower, "神鷹改は夜戦不可");

                ship = NewShip(536);
                ship.Firepower = 37;
                Assert.AreEqual(37, ship.NightBattlePower, "神鷹改二は夜戦可能");

                ship = NewShip(381);
                ship.Firepower = 24;
                Assert.AreEqual(0, ship.NightBattlePower, "雲鷹改は夜戦不可");

                ship = NewShip(536);
                ship.Firepower = 39;
                Assert.AreEqual(39, ship.NightBattlePower, "雲鷹改二は夜戦可能");
            }

            [TestMethod]
            public void GrafZeppelinの夜戦判定()
            {
                var ship = NewShip(432);
                ship.Firepower = 40;
                Assert.AreEqual(40, ship.NightBattlePower, "未改は夜戦可能");

                ship = NewShip(353);
                ship.Firepower = 52;
                Assert.AreEqual(52, ship.NightBattlePower, "改も夜戦可能");
            }

            [TestMethod]
            public void Saratogaの夜戦判定()
            {
                var ship = NewShip(433);
                ship.Firepower = 45;
                Assert.AreEqual(45, ship.NightBattlePower, "未改は夜戦可能");

                ship = NewShip(438);
                ship.Firepower = 53;
                Assert.AreEqual(0, ship.NightBattlePower, "改は夜戦不可");
            }

            [TestMethod]
            public void 加賀の夜戦判定()
            {
                var ship = NewShip(698);
                ship.Firepower = 56;
                Assert.AreEqual(0, ship.NightBattlePower, "改二は夜戦不可");

                ship = NewShip(610);
                ship.Firepower = 62;
                Assert.AreEqual(0, ship.NightBattlePower, "改二戊も夜戦不可");

                ship = NewShip(646);
                ship.Firepower = 60;
                Assert.AreEqual(60, ship.NightBattlePower, "改二護は夜戦可能");
            }

            [TestMethod]
            public void 夜戦不可空母()
            {
                var ship = NewShip(461);
                ship.Firepower = 68 + 2 + 7 + 3;
                ship.Torpedo = 9 + 1 + 1;
                ship.SetItems(new[] {TBM3D(32), F6F3N(24), 熟練甲板要員航空整備員(10)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void 速吸改に夜戦装備しても砲撃()
            {
                var ship = NewShip(352);
                ship.Firepower = 36 + 2;
                ship.Torpedo = 9;
                ship.SetItems(new[] {TBM3D(6), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(47, ship.NightBattlePower);
            }

            [TestMethod]
            public void 日向改二に夜戦装備しても砲撃()
            {
                var ship = NewShip(554);
                ship.Firepower = 86;
                ship.Torpedo = 0;
                ship.SetItems(new[] {F6F3N(2), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(86, ship.NightBattlePower);
            }

            // 空母夜戦火力はwiki参考 https://wikiwiki.jp/kancolle/%E5%A4%9C%E6%88%A6#x345rgc6
            [TestMethod]
            public void サラトガmk2にTBM3D()
            {
                var ship = NewShip(545);
                ship.Firepower = 68 + 2;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(32)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(68 + 2 + 9 + 144.37, Math.Round(ship.NightBattlePower, 2));
            }

            public void サラトガmk2にTBM3Dを0機()
            {
                var ship = NewShip(545);
                ship.Firepower = 68 + 2;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(0)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void サラトガmk2にF6F3N()
            {
                var ship = NewShip(545);
                ship.Firepower = 68;
                ship.Torpedo = 0;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {F6F3N(32)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(68 + 106.18, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void サラトガmk2にF6F3Nを0機()
            {
                var ship = NewShip(545);
                ship.Firepower = 68;
                ship.Torpedo = 0;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {F6F3N(0)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void サラトガmk2にTBM3DとF6F3N()
            {
                var ship = NewShip(545);
                ship.Firepower = 68 + 2;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(32), F6F3N(24)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(68 + 2 + 9 + 144.36 + 80.82, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void サラトガmk2にTBM3Dを0機とF6F3N()
            {
                var ship = NewShip(545);
                ship.Firepower = 68 + 2;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(0), F6F3N(24)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(68 + 80.82, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void サラトガmk2にTBM3DとF6F3Nを0機()
            {
                var ship = NewShip(545);
                ship.Firepower = 68 + 2;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(32), F6F3N(0)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(68 + 2 + 9 + 144.37, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void サラトガmk2にTBM3DとF6F3Nを0機とSwordfishと熟練甲板要員航空整備員()
            {
                var ship = NewShip(545);
                ship.Firepower = 68 + 2 + 2;
                ship.Torpedo = 9 + 3;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(0), F6F3N(0), Swordfish(18), 熟練甲板要員航空整備員(10)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void 夜戦不可空母に夜間作戦航空要員()
            {
                var ship = NewShip(461);
                ship.Firepower = 68 + 2 + 3;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(32), F6F3N(24), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(68 + 2 + 9 + 144.36 + 80.82, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void 夜戦不可空母に夜間作戦航空要員と熟練甲板要員航空整備員()
            {
                var ship = NewShip(461);
                ship.Firepower = 68 + 2 + 3 + 7;
                ship.Torpedo = 9 + 1;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(32), F6F3N(24), 夜間作戦航空要員(), 熟練甲板要員航空整備員()});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                Assert.AreEqual(68 + 2 + 9 + 144.36 + 80.82, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void 夜戦不可空母に夜間作戦航空要員と熟練甲板要員航空整備員ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 68 + 2 + 3 + 7 + 1;
                ship.Torpedo = 9 + 1 + 1;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3D(32), F6F3N(24), 夜間作戦航空要員(), 熟練甲板要員航空整備員(4)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                Assert.AreEqual(68 + 2 + 9 + 2 + 144.36 + 80.82, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void 赤城改二戊に天山一二型甲改熟練空六号電探改装備機と零戦62型爆戦岩井隊と九九式艦爆()
            {
                var ship = NewShip(599);
                ship.Firepower = 67;
                ship.Torpedo = 13;
                ship.ImprovedFirepower = 67;
                ship.SetItems(new[] {天山一二型甲改熟練空六号電探改装備機(40), 零戦62型爆戦岩井隊(16, 10), 九九式艦爆(4)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(67 + 13 + 4 + Math.Round(176.92 + 11.56, 2), Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void 加賀改二戊にTBM3W3Sと彗星一二型三一号光電管爆弾搭載機と九七式艦攻()
            {
                var ship = NewShip(610);
                ship.Firepower = 62 + 3;
                ship.Torpedo = 10 + 5;
                ship.ImprovedFirepower = 62;
                ship.SetItems(new[] {TBM3W3S(40), 彗星一二型三一号光電管爆弾搭載機(18), 九七式艦攻(8)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(Math.Round(62 + 3 + 10 + 11 + 5 + 208.92 + 14.00, 2), Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void 龍鳳改二戊に烈風改二戊型一航戦熟練とSwordfishMkIII熟練とカ号観測機()
            {
                var ship = NewShip(883);
                ship.Firepower = 45 + 2 + 4;
                ship.Torpedo = 8;
                ship.ImprovedFirepower = 45;
                ship.SetItems(new[] {烈風改二戊型一航戦熟練(21), SwordfishMkIII熟練(12), カ号観測機(3)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(45 + 2 + 4 + 8 + 67.13 + 22.86, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void サラトガmk2にTBM3W3Sと零戦62型爆戦岩井隊とSwordfishMkIII熟練とF6F5N()
            {
                var ship = NewShip(545);
                ship.Firepower = 68 + 3 + 4;
                ship.Torpedo = 10 + 8;
                ship.ImprovedFirepower = 68;
                ship.SetItems(new[] {TBM3W3S(32), 零戦62型爆戦岩井隊(24, 10), SwordfishMkIII熟練(18), F6F5N(6)});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(68 + 3 + 4 + 10 + 4 + 8 + Math.Round(180.00 + 13.45 + 28.00 + 23.52, 2), Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void 不明な空母の夜戦に雷装ボーナスなし()
            {
                var ship = NewShip(197);
                ship.Firepower = 63;
                ship.Torpedo = 0;
                ship.ImprovedFirepower = 63;
                ship.SetItems(new[] {天山一二型甲改空六号電探改装備機(27), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(63 + 131.75, Math.Round(ship.NightBattlePower, 2), "雷装ボーナスは夜戦につかない");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "夜攻天山に雷装ボーナスがつかない");
            }

            [TestMethod]
            public void 翔鶴改二の夜戦に雷装ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 63;
                ship.Torpedo = 0;
                ship.ImprovedFirepower = 63;
                ship.SetItems(new[] {天山一二型甲改空六号電探改装備機(27), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(63 + 2 + 131.75, Math.Round(ship.NightBattlePower, 2), "雷装ボーナスは夜戦にもつく");
                Assert.AreEqual(2, ship.Items[0].TbBonus, "夜攻天山に雷装ボーナスがつく");

                ship.SetItems(new[] {TBM3D(27), 天山一二型甲(27), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(63 + 1 + 136.43, Math.Round(ship.NightBattlePower, 2), "夜攻ではない機体の雷装ボーナスが夜戦にもつく");
                Assert.AreEqual(1, ship.Items[0].TbBonus, "天山の雷装ボーナスはTBM3Dにつく");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "天山の雷装ボーナスは天山につかない");

                ship.SetItems(new[] {天山一二型甲改空六号電探改装備機(27), 流星改(27), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(63 + 2 + 131.75, Math.Round(ship.NightBattlePower, 2), "夜攻ではない機体についた雷装ボーナスが夜戦にもつく");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "夜攻天山の雷装ボーナスは夜攻天山につかない");
                Assert.AreEqual(2, ship.Items[1].TbBonus, "夜攻天山の雷装ボーナスは流星改につく");
            }

            [TestMethod]
            public void 翔鶴改二の夜戦に雷装ボーナスと熟練甲板要員航空整備員ボーナス()
            {
                var ship = NewShip(461);
                ship.Firepower = 63 + 2 + 1 + 7 + 2;
                ship.Torpedo = 0 + 2 + 1 + 1;
                ship.ImprovedFirepower = 63;
                ship.SetItems(new[] {天山一二型甲改空六号電探改装備機(27), 流星改(27), 夜間作戦航空要員(), 熟練甲板要員航空整備員(7), 熟練甲板要員航空整備員(4)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
                Assert.AreEqual(63 + 2 + 4 + 131.75, Math.Round(ship.NightBattlePower, 2), "夜攻ではない機体についた雷装ボーナスが夜戦にもつく");
                Assert.AreEqual(0, ship.Items[0].TbBonus, "夜攻天山の雷装ボーナスは夜攻天山につかない");
                Assert.AreEqual(2, ship.Items[1].TbBonus, "夜攻天山の雷装ボーナスは流星改につく");
            }

            [TestMethod]
            public void GambierBayMkIIに夜間作戦航空要員()
            {
                var ship = NewShip(707);
                ship.Firepower = 55 + 2 + 3;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 52;
                ship.SetItems(new[] {TBM3D(24), F6F3N(20), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(52 + 3 + 2 + 9 + 113.89 + 68.05, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void GambierBayMkIIは夜戦不可()
            {
                var ship = NewShip(707);
                ship.Firepower = 55;
                ship.Torpedo = 13 + 13;
                ship.ImprovedFirepower = 52;
                ship.SetItems(new[] {流星改(24), 流星改(20), _15_5cm三連装副砲(), _15_5cm三連装副砲()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void グラーフツェッペリンに夜間作戦航空要員()
            {
                var ship = NewShip(432);
                ship.Firepower = 10 + 2 + 3;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 0;
                ship.SetItems(new[] {TBM3D(20), F6F3N(13), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(10 + 2 + 9 + 98.24 + 45.49, Math.Round(ship.NightBattlePower, 2));
            }

            [TestMethod]
            public void グラーフツェッペリンに流星改と15_5cm三連装副砲2スロずつ()
            {
                var ship = NewShip(432);
                ship.Firepower = 10 + 7 + 7;
                ship.Torpedo = 13 + 13;
                ship.ImprovedFirepower = 0;
                ship.SetItems(new[] {流星改(20), 流星改(13), _15_5cm三連装副砲(), _15_5cm三連装副砲()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(50, ship.NightBattlePower);
            }

            [TestMethod]
            public void グラーフツェッペリンに流星改0機と15_5cm三連装副砲2スロずつ()
            {
                var ship = NewShip(432);
                ship.Firepower = 10 + 7 + 7;
                ship.Torpedo = 13 + 13;
                ship.ImprovedFirepower = 0;
                ship.SetItems(new[] {流星改(0), 流星改(0), _15_5cm三連装副砲(), _15_5cm三連装副砲()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(50, ship.NightBattlePower);
            }

            [TestMethod]
            public void アークロイヤル改に夜間作戦航空要員()
            {
                var ship = NewShip(393);
                ship.Firepower = 50 + 2 + 3;
                ship.Torpedo = 9;
                ship.ImprovedFirepower = 50;
                ship.SetItems(new[] {TBM3D(24), F6F3N(30), 夜間作戦航空要員()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual((50 + 2) * 100 + 9 * 100 + 11388 + 9986, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void アークロイヤル改にSwordfishを2スロットとBarracudaMkIIと15_5cm三連装副砲()
            {
                var ship = NewShip(393);
                ship.Firepower = 50 + 2 + 2 + 7 + 7;
                ship.Torpedo = 3 + 3;
                ship.ImprovedFirepower = 50;
                ship.SetItems(new[] {Swordfish(24, 9), Swordfish(30), BarracudaMkII(12, 10), _15_5cm三連装副砲()});
                ship.ResolveTbBonus(AdditionalData);

                Assert.AreEqual(0, ship.Items[0].TbBonus, "Barracudaの雷装ボーナスはSwordfishにつかない");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "Barracudaの雷装ボーナスはSwordfishにつかない");
                Assert.AreEqual(3, ship.Items[2].TbBonus, "Barracudaの雷装ボーナスはBarracudaにつく");
                Assert.AreEqual(66, ship.NightBattlePower, "改修値はSwordfishのみ、雷装ボーナスは夜間機同様に有効");
            }

            [TestMethod]
            public void アークロイヤル改にSwordfishを2スロットとBarracudaMkIIと熟練甲板要員航空整備員()
            {
                var ship = NewShip(393);
                ship.Firepower = 50 + 2 + 2 + 7 + 7 + 3;
                ship.Torpedo = 3 + 3 + 7 + 1 + 1;
                ship.ImprovedFirepower = 50;
                ship.SetItems(new[] {Swordfish(24, 9), Swordfish(30), BarracudaMkII(12, 10), 熟練甲板要員航空整備員(10)});
                ship.ResolveTbBonus(AdditionalData).ResolveApBonus();

                Assert.AreEqual(0, ship.Items[0].TbBonus, "Barracudaの雷装ボーナスはSwordfishにつかない");
                Assert.AreEqual(0, ship.Items[1].TbBonus, "Barracudaの雷装ボーナスはSwordfishにつかない");
                Assert.AreEqual(3, ship.Items[2].TbBonus, "Barracudaの雷装ボーナスはBarracudaにつく");
                Assert.AreEqual(71, ship.NightBattlePower, "改修値はSwordfishのみ、雷装ボーナスは夜間機同様に有効");
            }

            [TestMethod]
            public void アークロイヤル改にSwordfish0機と15_5cm三連装副砲2スロずつ()
            {
                var ship = NewShip(393);
                ship.Firepower = 50 + 2 + 2 + 7 + 7;
                ship.Torpedo = 3 + 3;
                ship.ImprovedFirepower = 50;
                ship.SetItems(new[] {Swordfish(0), Swordfish(0), _15_5cm三連装副砲(), _15_5cm三連装副砲()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void アークロイヤル改に九七式艦攻と15_5cm三連装副砲2スロずつ()
            {
                var ship = NewShip(393);
                ship.Firepower = 50 + 7 + 7;
                ship.Torpedo = 5 + 5;
                ship.ImprovedFirepower = 50;
                ship.SetItems(new[] {流星改(24), 流星改(30), _15_5cm三連装副砲(), _15_5cm三連装副砲()});
                ship.ResolveTbBonus(AdditionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void 合計夜間接触確率()
            {
                var fleet = NewFleet();
                var ship1 = NewShip(662);
                ship1.Level = 99;
                var ship2 = NewShip(662);
                ship2.Level = 60;

                ship1.SetItems(new[] {_15_5cm三連装副砲(), 九八式水上偵察機夜偵(2)});
                ship2.SetItems(new[] {_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1)});
                fleet.SetShips(new[] {ship1.Id, ship2.Id});
                Assert.AreEqual(84.64, AirContactRates.Calc(fleet).Night.Select5 * 100, "2隻に夜偵1スロットずつ");

                ship1.SetItems(new[] {_15_5cm三連装副砲(), 九八式水上偵察機夜偵(2)});
                ship2.SetItems(new[] {_15_5cm三連装副砲()});
                Assert.AreEqual(68, AirContactRates.Calc(fleet).Night.Select5 * 100, "1隻目だけに夜偵1スロット");

                ship1.SetItems(new[] {_15_5cm三連装副砲()});
                ship2.SetItems(new[] {_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1)});
                Assert.AreEqual(52, AirContactRates.Calc(fleet).Night.Select5 * 100, "2隻目だけに夜偵1スロット");

                ship1.SetItems(new[] {_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1), 九八式水上偵察機夜偵(1)});
                ship2.SetItems(new[] {_15_5cm三連装副砲()});
                Assert.AreEqual(89.76, AirContactRates.Calc(fleet).Night.Select5 * 100, "1隻目だけに夜偵2スロット");

                ship1.SetItems(new[] {_15_5cm三連装副砲(), 零式水上偵察機11型乙改夜偵(1)});
                ship2.SetItems(new[] {_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1)});
                var contactRate = AirContactRates.Calc(fleet);
                Assert.AreEqual(88, contactRate.Night.Select7 * 100, "夜偵+7と");
                Assert.AreEqual(6.24, Math.Round(contactRate.Night.Select5 * 100, 2), "夜偵+5の混在");
                Assert.AreEqual(5.76, contactRate.Night.Fail * 100, "+7と+5と不発率の合計は100");

                ship1.Level = 175;
                contactRate = AirContactRates.Calc(fleet);
                Assert.AreEqual(100, contactRate.Night.Select7 * 100, "Lv125以上でも表示は100%で止める");
                Assert.AreEqual(0, contactRate.Night.Select5 * 100, "11型夜偵ですでに100%なので、九八式夜偵は0%");

                ship1.Level = 99;
                fleet.SetShips(new[] {ship1.Id});
                fleet.CombinedType = CombinedType.Carrier;
                var fleet2 = NewFleet(1);
                fleet2.SetShips(new[] {ship2.Id});
                fleet2.CombinedType = CombinedType.Carrier;
                Assert.AreEqual(52, AirContactRates.Calc(fleet, fleet2).Night.Select5 * 100, "連合艦隊では護衛の夜偵のみ有効");
           }
        }

        [TestClass]
        public class FighterPower
        {
            [TestMethod]
            public void 対潜機は熟練度ありでも制空なしだが一式戦隼II型改20戦隊は制空あり()
            {
                var ship = NewShip(717);
                ship.Firepower = 24;
                ship.Torpedo = 0;
                ship.Fleet = NormalFleet;

                ship.SetItems(new[] {カ号観測機(8), 三式指揮連絡機対潜(6), 一式戦隼II型改20戦隊(2)});
                Assert.AreEqual(0, ship.Items[0].CalcFighterPower().Max);
                Assert.AreEqual(0, ship.Items[1].CalcFighterPower().Max);
                Assert.AreEqual(9, ship.Items[2].CalcFighterPower().Max);

                ship.SetItems(new[] {カ号観測機(8, level: 10), 三式指揮連絡機対潜(6, level: 10), 一式戦隼II型改20戦隊(2, level: 10)});
                Assert.AreEqual(0, ship.Items[0].CalcFighterPower().Max);
                Assert.AreEqual(0, ship.Items[1].CalcFighterPower().Max);
                Assert.AreEqual(9, ship.Items[2].CalcFighterPower().Max);

                ship.SetItems(new[] {カ号観測機(8, alv: 7), 三式指揮連絡機対潜(6, alv: 7), 一式戦隼II型改20戦隊(2, alv: 7)});
                Assert.AreEqual(0, ship.Items[0].CalcFighterPower().Max);
                Assert.AreEqual(0, ship.Items[1].CalcFighterPower().Max);
                Assert.AreEqual(33, ship.Items[2].CalcFighterPower().Max);
            }

            [TestMethod]
            public void 基地航空でも対潜機は熟練度ありでも制空なしだが一式戦隼II型改20戦隊は制空あり()
            {
                var airBasePlane = NewPlaneInfo(カ号観測機(18));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);
                airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);
                airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18));
                Assert.AreEqual(26, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(26, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(カ号観測機(18, level: 10));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);
                airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18, level: 10));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);
                airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18, level: 10));
                Assert.AreEqual(26, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(26, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(カ号観測機(18, alv: 7));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);
                airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18, alv: 7));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);
                airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18, alv: 7));
                Assert.AreEqual(50, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(50, airBasePlane.FighterPower.Interception.Max);
            }

            [TestMethod]
            public void 二式大艇の改修値による基地航空での制空上昇()
            {
                var airBasePlane = NewPlaneInfo(二式大艇(4));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(二式大艇(4, level: 3));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(二式大艇(4, level: 4));
                Assert.AreEqual(1, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(1, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(1, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(1, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(二式大艇(3, level: 4));
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(0, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(0, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(二式大艇(4, alv: 7));
                Assert.AreEqual(3, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(3, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(3, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(3, airBasePlane.FighterPower.Interception.Max);

                var ship = NewShip(450);
                ship.Fleet = NormalFleet;
                ship.SetItems(new[] {二式大艇(4, level: 4, alv: 7)});
                Assert.AreEqual(0, ship.Items[0].CalcFighterPower().Min, "艦載時は制空なし");
                Assert.AreEqual(0, ship.Items[0].CalcFighterPower().Max, "艦載時は制空なし");
            }

            [TestMethod]
            public void 陸上偵察機は改修2かつ内部熟練度最大で制空上昇()
            {
                var airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(4, level: 2));
                Assert.AreEqual(6, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(6, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(6, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(6, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(4, alv: 7));
                Assert.AreEqual(9, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(9, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(9, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(9, airBasePlane.FighterPower.Interception.Max);

                airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(4, level: 2, alv: 7));
                Assert.AreEqual(9, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(9, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(10, airBasePlane.FighterPower.AirCombat.Max, "内部熟練度最大時のみ");
                Assert.AreEqual(10, airBasePlane.FighterPower.Interception.Max, "内部熟練度最大時のみ");

                airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(3, level: 2, alv: 7));
                Assert.AreEqual(8, airBasePlane.FighterPower.AirCombat.Min);
                Assert.AreEqual(8, airBasePlane.FighterPower.Interception.Min);
                Assert.AreEqual(9, airBasePlane.FighterPower.AirCombat.Max);
                Assert.AreEqual(9, airBasePlane.FighterPower.Interception.Max);
            }

            [TestMethod]
            public void _23春e2乙最終基地空襲制空()
            {
                var ships = new List<ShipStatus>();
                var ship1 = NewShip(2045);
                ship1.SetItems(new[] {NewItem(1617), NewItem(1618), NewItem(1619), NewItem(1619)});
                ship1.FillOnSlotMaxEq(ship1.Spec.MaxEq);
                ships.Add(ship1);

                var ship2 = NewShip(2106);
                ship2.SetItems(new[] {NewItem(1581), NewItem(1574), NewItem(1582), NewItem(1583)});
                ship2.FillOnSlotMaxEq(ship2.Spec.MaxEq);
                ships.Add(ship2);

                var ship34 = NewShip(2103);
                ship34.SetItems(new[] {NewItem(1547), NewItem(1574), NewItem(1574), NewItem(1583)});
                ship34.FillOnSlotMaxEq(ship34.Spec.MaxEq);
                ships.Add(ship34);
                ships.Add(ship34);

                Assert.AreEqual(627, ships.Sum(e => e.EnemyInterception().FighterPower));
            }

            [TestMethod]
            public void _23春e3_3甲前哨対基地制空()
            {
                var ships = new List<ShipStatus>();
                var ship1 = NewShip(2148);
                ship1.SetItems(new[] {NewItem(1617), NewItem(1618), NewItem(1619), NewItem(1633), NewItem(1608)});
                ship1.FillOnSlotMaxEq(ship1.Spec.MaxEq);
                ships.Add(ship1);

                var ship2 = NewShip(2107);
                ship2.SetItems(new[] {NewItem(1581), NewItem(1574), NewItem(1582), NewItem(1583)});
                ship2.FillOnSlotMaxEq(ship2.Spec.MaxEq);
                ships.Add(ship2);

                var ship34 = NewShip(2103);
                ship34.SetItems(new[] {NewItem(1547), NewItem(1574), NewItem(1574), NewItem(1583)});
                ship34.FillOnSlotMaxEq(ship34.Spec.MaxEq);
                ships.Add(ship34);
                ships.Add(ship34);

                var ship5 = NewShip(1792);
                ship5.SetItems(new[] {NewItem(1584), NewItem(1584), NewItem(1584), NewItem(1525)});
                ship5.FillOnSlotMaxEq(ship5.Spec.MaxEq);
                ships.Add(ship5);

                var ship6 = NewShip(1543);
                ship6.SetItems(new[] {NewItem(1509), NewItem(1509), NewItem(1525), NewItem(1529)});
                ship6.FillOnSlotMaxEq(ship6.Spec.MaxEq);
                ships.Add(ship6);

                var ship7 = NewShip(1592);
                ship7.SetItems(new[] {NewItem(1550), NewItem(1550), NewItem(1545), NewItem(1525)});
                ship7.FillOnSlotMaxEq(ship7.Spec.MaxEq);
                ships.Add(ship7);

                Assert.AreEqual(703, ships.Sum(e => e.EnemyInterception().FighterPower));
            }

            [TestMethod]
            public void 触接()
            {
                var ship1 = NewShip(321);
                ship1.SetItems(new[] {零式水上偵察機11型乙(1)});
                var fleet1 = NewFleet();
                fleet1.SetShips(new[] {ship1.Id});
                Assert.AreEqual("触接開始: 28.0/17.5/12.7"
                            + "\n触接17%: 12.0/6.5/4.2"
                            , AirContactRates.Calc(fleet1).ToString());

                ship1.SetItems(new[] {零式水上偵察機11型乙(2)});
                Assert.AreEqual("触接開始: 36.0/22.5/16.3"
                            + "\n触接17%: 15.4/8.4/5.4"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "機数で開始率上昇");

                ship1.SetItems(new[] {零式水上偵察機11型乙(2), 零式水上偵察機11型乙(1)});
                Assert.AreEqual("触接開始: 60.0/37.5/27.2"
                            + "\n触接17%: 40.4/22.8/15.1"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "複数スロットで選択率上昇");

                ship1.SetItems(new[] {零式水上偵察機11型乙(20)});
                Assert.AreEqual("触接開始: 108.0/67.5/49.0"
                            + "\n触接17%: 42.8/25.3/16.3"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "確保で開始率100%超え");

                ship1.SetItems(new[] {零式水上偵察機11型乙(30)});
                Assert.AreEqual("触接開始: 132.0/82.5/60.0"
                            + "\n触接17%: 42.8/30.9/19.9"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "確保で開始率100%超えからさらに上昇しても、選択率は変わらず");

                ship1.SetItems(new[] {零式水上偵察機11型乙(0)});
                Assert.AreEqual("触接開始: 0.0", AirContactRates.Calc(fleet1).ToString());

                ship1.SetItems(new[] {九七式艦攻(1)});
                Assert.AreEqual("触接開始: 4.0/2.5/1.8"
                            + "\n触接12%: 0.2/0.1/0.1"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "艦攻だけでも低確率で触接可能");

                ship1.SetItems(new[] {九七式艦攻(2)});
                Assert.AreEqual("触接開始: 4.0/2.5/1.8"
                            + "\n触接12%: 0.2/0.1/0.1"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "艦攻は機数が増えても開始率は上昇しない");

                ship1.SetItems(new[] {九七式艦攻(2), 九七式艦攻(1)});
                Assert.AreEqual("触接開始: 4.0/2.5/1.8"
                            + "\n触接12%: 0.5/0.3/0.1"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "艦攻でも複数スロットで選択率は上昇");

                ship1.SetItems(new[] {九七式艦攻(0)});
                Assert.AreEqual("触接開始: 0.0", AirContactRates.Calc(fleet1).ToString());

                ship1.SetItems(new[] {Ju87C改二KMX搭載機(1), 瑞雲(1)});
                Assert.AreEqual("触接開始: 0.0", AirContactRates.Calc(fleet1).ToString(), "艦爆瑞雲は触接不可");

                ship1.SetItems(new[] {零式水上偵察機11型乙(1), Ju87C改二KMX搭載機(1), 瑞雲(1)});
                Assert.AreEqual("触接開始: 28.0/17.5/12.7"
                            + "\n触接17%: 12.0/6.5/4.2"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "艦爆瑞雲は選択も不可");


                ship1.SetItems(new[] {零式水上偵察機11型乙(1), 九八式水上偵察機夜偵(1)});
                Assert.AreEqual("触接開始: 40.0/25.0/18.1"
                            + "\n触接17%: 17.1/9.3/6.0"
                            + "\n触接12%: 4.8/2.9/2.0"
                            + "\n触接失敗: 77.9/87.6/91.9"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "触接効果が複数の場合は失敗率を表示");

                ship1.SetItems(new[] {零式水上偵察機11型乙(1), 九八式水上偵察機夜偵(1), TBM3W3S(1)});
                Assert.AreEqual("触接開始: 40.0/25.0/18.1"
                            + "\n触接20%: 28.5/15.6/10.1"
                            + "\n触接17%: 4.8/3.5/2.6"
                            + "\n触接12%: 1.3/1.0/0.8"
                            + "\n触接失敗: 65.1/79.7/86.3"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "触接効果3種全表示");

                ship1.SetItems(new[] {零式水上偵察機11型乙(1)});
                var ship2 = NewShip(321);
                ship2.SetItems(new[] {九八式水上偵察機夜偵(1), TBM3W3S(1)});
                var fleet2 = NewFleet(1);
                fleet2.SetShips(new[] {ship2.Id});
                Assert.AreEqual("触接開始: 28.0/17.5/12.7"
                            + "\n触接17%: 12.0/6.5/4.2"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1, fleet2, false).ToString()
                            , "連合で敵通常の場合は本隊のみ有効");
                Assert.AreEqual("触接開始: 40.0/25.0/18.1"
                            + "\n触接20%: 28.5/15.6/10.1"
                            + "\n触接17%: 4.8/3.5/2.6"
                            + "\n触接12%: 1.3/1.0/0.8"
                            + "\n触接失敗: 65.1/79.7/86.3"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1, fleet2, true).ToString()
                            , "連合で敵連合の場合は護衛も有効");

                ship1.SetItems(new[] {九八式水上偵察機夜偵(1), TBM3W3S(1)});
                ship2.SetItems(new[] {零式水上偵察機11型乙(1)});
                Assert.AreEqual("触接開始: 16.0/10.0/7.2"
                            + "\n触接20%: 11.4/6.2/4.0"
                            + "\n触接12%: 0.9/0.7/0.5"
                            + "\n触接失敗: 87.5/93.0/95.4"
                            , AirContactRates.Calc(fleet1, fleet2, false).ToString()
                            , "連合では護衛の夜偵のみ有効");
                Assert.AreEqual("触接開始: 40.0/25.0/18.1"
                            + "\n触接20%: 28.5/15.6/10.1"
                            + "\n触接17%: 4.8/3.5/2.6"
                            + "\n触接12%: 1.3/1.0/0.8"
                            + "\n触接失敗: 65.1/79.7/86.3"
                            , AirContactRates.Calc(fleet1, fleet2, true).ToString()
                            , "連合では敵連合でも護衛の夜偵のみ有効");

                ship1.SetItems(new int[0]);
                ship2.SetItems(new[] {九八式水上偵察機夜偵(1), TBM3W3S(1)});
                Assert.AreEqual("触接開始: 0.0"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1, fleet2, false).ToString()
                            , "連合で敵通常または開幕夜戦のときは夜偵を表示");
                Assert.AreEqual("触接開始: 16.0/10.0/7.2"
                            + "\n触接20%: 11.4/6.2/4.0"
                            + "\n触接12%: 0.9/0.7/0.5"
                            + "\n触接失敗: 87.5/93.0/95.4"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1, fleet2, true).ToString()
                            , "連合かつ敵連合でようやく触接可能");


                var itemId = 零式水上偵察機11型乙(20);
                var item = ItemInventory[itemId];
                item.Spec = new ItemSpec
                {
                    Type = 10,
                    LoS = 14,
                    Accuracy = 3
                };
                ship1.SetItems(new[] {itemId, 零式水上偵察機11型乙(1)});
                Assert.AreEqual("触接開始: 276.0/172.5/125.4"
                            + "\n触接20%: 100.0/87.5/77.7"
                            + "\n触接17%: 0.0/4.6/7.4"
                            + "\n触接失敗: 0.0/7.8/14.8"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "確保で選択率100%なら以降は0%表示");

                item.Spec.LoS = 18;
                Assert.AreEqual("触接開始: 348.0/217.4/158.1"
                            + "\n触接20%: 100.0/100.0/100.0"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "劣勢でも選択率100%ならそこで表示終了");

                item.Spec.Accuracy = 0;
                Assert.AreEqual("触接開始: 348.0/217.4/158.1"
                            + "\n触接17%: 42.8/37.5/33.3"
                            + "\n触接12%: 57.1/62.5/66.6"
                            , AirContactRates.Calc(fleet1).ToString()
                            , "劣勢でも選択率合計100%ならそこで表示終了し、触接失敗を表示しない");

                ship1.Escaped = true;
                ship2.Escaped = false;
                Assert.AreEqual("触接開始: 16.0/10.0/7.2"
                            + "\n触接20%: 11.4/6.2/4.0"
                            + "\n触接12%: 0.9/0.7/0.5"
                            + "\n触接失敗: 87.5/93.0/95.4"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1, fleet2, true).ToString()
                            , "退避艦の機体での触接は無効");

                ship1.Escaped = false;
                ship2.Escaped = true;
                Assert.AreEqual("触接開始: 348.0/217.4/158.1"
                            + "\n触接17%: 42.8/37.5/33.3"
                            + "\n触接12%: 57.1/62.5/66.6"
                            + "\n夜偵: 0.0/68.0/31.9"
                            , AirContactRates.Calc(fleet1, fleet2, true).ToString()
                            , "退避艦の夜偵はなぜか有効");
            }
        }

        protected static int 流星改一航戦(int onSlot = 18)
        {
            return NewItem(342, onSlot);
        }

        protected static int 流星改一航戦熟練(int onSlot = 18)
        {
            return NewItem(343, onSlot);
        }

        protected static int SB2C5(int onSlot = 18)
        {
            return NewItem(421, onSlot);
        }

        protected static int 瑞雲改二(int onSlot = 18)
        {
            return NewItem(322, onSlot);
        }

        protected static int 瑞雲改二熟練(int onSlot = 18)
        {
            return NewItem(323, onSlot);
        }

        protected static int 零式水上偵察機11型乙(int onSlot = 4)
        {
            return NewItem(238, onSlot);
        }

        protected static int 彩雲(int onSlot = 4)
        {
            return NewItem(54, onSlot);
        }

        protected static int 噴式景雲改(int onSlot = 18)
        {
            return NewItem(199, onSlot);
        }

        protected static int 一式陸攻三四型(int onSlot = 18)
        {
            return NewItem(186, onSlot);
        }

        protected static int 一式陸攻野中隊(int onSlot = 18)
        {
            return NewItem(170, onSlot);
        }

        protected static int 銀河江草隊(int onSlot = 18)
        {
            return NewItem(388, onSlot);
        }

        protected static int Do217E5Hs293初期型(int onSlot = 18)
        {
            return NewItem(405, onSlot);
        }

        protected static int Do217K2FritzX(int onSlot = 18)
        {
            return NewItem(406, onSlot);
        }

        protected static int 四式重爆飛龍イ号一型甲誘導弾(int onSlot = 18)
        {
            return NewItem(444, onSlot);
        }

        protected static int 四式重爆飛龍熟練イ号一型甲誘導弾(int onSlot = 18)
        {
            return NewItem(484, onSlot);
        }

        protected static int キ102乙(int onSlot = 18)
        {
            return NewItem(453, onSlot);
        }

        protected static int キ102乙改イ号一型乙誘導弾(int onSlot = 18)
        {
            return NewItem(454, onSlot);
        }

        protected static int B25(int onSlot = 18)
        {
            return NewItem(459, onSlot);
        }

        protected static int 爆装一式戦隼III型改65戦隊(int onSlot = 18)
        {
            return NewItem(224, onSlot);
        }

        protected static int 試製東海(int onSlot = 18)
        {
            return NewItem(269, onSlot);
        }

        protected static int 深山(int onSlot = 9)
        {
            return NewItem(395, onSlot);
        }

        protected static int 一式戦隼II型64戦隊(int onSlot = 18)
        {
            return NewItem(225, onSlot);
        }

        protected static int SpitfireMkV(int onSlot = 18)
        {
            return NewItem(251, onSlot);
        }

        protected static int 秋水(int onSlot = 18)
        {
            return NewItem(352, onSlot);
        }

        protected static int 二式陸上偵察機(int onSlot = 4)
        {
            return NewItem(311, onSlot);
        }

        protected static int 二式陸上偵察機熟練(int onSlot = 4)
        {
            return NewItem(312, onSlot);
        }

        [TestClass]
        public class AirbaseBomberPower
        {
            [TestMethod]
            public void 陸偵補正と制空()
            {
                var corps = new AirBase.AirCorpsInfo();
                var rikutei = NewPlaneInfo(二式陸上偵察機());
                var rikuteijukuren = NewPlaneInfo(二式陸上偵察機熟練());

                var nonakatai = NewPlaneInfo(一式陸攻野中隊());
                corps.Planes = new []{nonakatai};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(12, corps.CalcFighterPower().AirCombat.Min, "制空補正なし");
                Assert.AreEqual(12, corps.CalcFighterPower().Interception.Min, "対基地制空補正なし");
                Assert.AreEqual(12, nonakatai.FighterPower.AirCombat.Min);
                Assert.AreEqual(12, nonakatai.FighterPower.Interception.Min);
                Assert.AreEqual(133.2, Math.Round(nonakatai.ShipBomberPower[0].BomberPower, 1), "対艦補正なし");
                Assert.AreEqual(102, nonakatai.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(142.2, Math.Round(nonakatai.LandBomberPower.BomberPower, 1), "対地補正なし");
                Assert.AreEqual(284.4, Math.Round(nonakatai.LandBomberPower.IsolatedIsland, 1), "離島補正なし");
                Assert.AreEqual(478.8, nonakatai.LandBomberPower.SupplyDepot, "集積補正なし");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Max, "対潜不可");
                Assert.AreEqual(27, corps.CostForSortie[0]);
                Assert.AreEqual(12, corps.CostForSortie[1]);

                corps.Planes = new []{nonakatai, rikutei};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(20, corps.CalcFighterPower().AirCombat.Min, "制空補正1.15");
                Assert.AreEqual(21, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.18");
                Assert.AreEqual(12, nonakatai.FighterPower.AirCombat.Min);
                Assert.AreEqual(12, nonakatai.FighterPower.Interception.Min);
                Assert.AreEqual(149.85, Math.Round(nonakatai.ShipBomberPower[0].BomberPower, 2), "対艦補正1.125");
                Assert.AreEqual(102, nonakatai.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(159.975, Math.Round(nonakatai.LandBomberPower.BomberPower, 3), "対地補正1.125");
                Assert.AreEqual(319.95, Math.Round(nonakatai.LandBomberPower.IsolatedIsland, 2), "離島補正1.125");
                Assert.AreEqual(538.65, nonakatai.LandBomberPower.SupplyDepot, "集積補正1.125");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Max, "対潜不可");
                Assert.AreEqual(31, corps.CostForSortie[0]);
                Assert.AreEqual(15, corps.CostForSortie[1]);

                corps.Planes = new []{nonakatai, rikuteijukuren};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(21, corps.CalcFighterPower().AirCombat.Min, "制空補正1.18");
                Assert.AreEqual(22, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.23");
                Assert.AreEqual(12, nonakatai.FighterPower.AirCombat.Min);
                Assert.AreEqual(12, nonakatai.FighterPower.Interception.Min);
                Assert.AreEqual(153.18, nonakatai.ShipBomberPower[0].BomberPower, "対艦補正1.15");
                Assert.AreEqual(102, nonakatai.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(163.53, nonakatai.LandBomberPower.BomberPower, "対地補正1.15");
                Assert.AreEqual(327.06, nonakatai.LandBomberPower.IsolatedIsland, "離島補正1.15");
                Assert.AreEqual(550.62, nonakatai.LandBomberPower.SupplyDepot, "集積補正1.15");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Max, "対潜不可");
                Assert.AreEqual(31, corps.CostForSortie[0]);
                Assert.AreEqual(15, corps.CostForSortie[1]);

                corps.Planes = new []{nonakatai, rikuteijukuren, rikutei};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(28, corps.CalcFighterPower().AirCombat.Min, "複数種類の陸偵でも倍率は上限");
                Assert.AreEqual(29, corps.CalcFighterPower().Interception.Min, "複数種類の陸偵でも倍率は上限");
                Assert.AreEqual(12, nonakatai.FighterPower.AirCombat.Min);
                Assert.AreEqual(12, nonakatai.FighterPower.Interception.Min);
                Assert.AreEqual(153.18, nonakatai.ShipBomberPower[0].BomberPower, "複数種類の陸偵でも倍率は上限");
                Assert.AreEqual(102, nonakatai.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(163.53, nonakatai.LandBomberPower.BomberPower, "複数種類の陸偵でも倍率は上限");
                Assert.AreEqual(327.06, nonakatai.LandBomberPower.IsolatedIsland, "複数種類の陸偵でも倍率は上限");
                Assert.AreEqual(550.62, nonakatai.LandBomberPower.SupplyDepot, "複数種類の陸偵でも倍率は上限");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, nonakatai.AswBomberPower.Max, "対潜不可");
                Assert.AreEqual(35, corps.CostForSortie[0]);
                Assert.AreEqual(18, corps.CostForSortie[1]);


                var tokai = NewPlaneInfo(試製東海());
                corps.Planes = new []{tokai};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(0, corps.CalcFighterPower().AirCombat.Min, "制空補正なし");
                Assert.AreEqual(0, corps.CalcFighterPower().Interception.Min, "対基地制空補正なし");
                Assert.AreEqual(0, tokai.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, tokai.FighterPower.Interception.Min);
                Assert.AreEqual(36, tokai.ShipBomberPower[0].BomberPower, "雷装0補正なし");
                Assert.AreEqual(95, tokai.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(52.2, tokai.LandBomberPower.BomberPower);
                Assert.AreEqual(102.6, Math.Round(tokai.LandBomberPower.IsolatedIsland, 2));
                Assert.AreEqual(289.8, tokai.LandBomberPower.SupplyDepot);
                Assert.AreEqual(102.6, Math.Round(tokai.AswBomberPower.Min, 2), "対潜下限補正なし");
                Assert.AreEqual(145.8, tokai.AswBomberPower.Max, "対潜上限補正なし");
                Assert.AreEqual(27, corps.CostForSortie[0]);
                Assert.AreEqual(12, corps.CostForSortie[1]);

                corps.Planes = new []{tokai, rikutei};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(6, corps.CalcFighterPower().AirCombat.Min, "制空補正1.15");
                Assert.AreEqual(7, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.18");
                Assert.AreEqual(0, tokai.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, tokai.FighterPower.Interception.Min);
                Assert.AreEqual(40.5, tokai.ShipBomberPower[0].BomberPower, "雷装0補正1.125");
                Assert.AreEqual(95, tokai.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(58.725, tokai.LandBomberPower.BomberPower);
                Assert.AreEqual(115.425, Math.Round(tokai.LandBomberPower.IsolatedIsland, 3));
                Assert.AreEqual(326.025, Math.Round(tokai.LandBomberPower.SupplyDepot, 3));
                Assert.AreEqual(115.425, Math.Round(tokai.AswBomberPower.Min, 3), "対潜下限補正1.125");
                Assert.AreEqual(164.025, tokai.AswBomberPower.Max, "対潜上限補正1.125");
                Assert.AreEqual(31, corps.CostForSortie[0]);
                Assert.AreEqual(15, corps.CostForSortie[1]);


                var hayabusa64 = NewPlaneInfo(一式戦隼II型64戦隊());
                corps.Planes = new []{hayabusa64};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(78, corps.CalcFighterPower().AirCombat.Min, "制空補正なし");
                Assert.AreEqual(76, corps.CalcFighterPower().Interception.Min, "対基地制空補正なし");
                Assert.AreEqual(78, hayabusa64.FighterPower.AirCombat.Min);
                Assert.AreEqual(76, hayabusa64.FighterPower.Interception.Min);
                Assert.AreEqual(18, corps.CostForSortie[0]);
                Assert.AreEqual(11, corps.CostForSortie[1]);

                corps.Planes = new []{hayabusa64, rikutei};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(96, corps.CalcFighterPower().AirCombat.Min, "制空補正1.15");
                Assert.AreEqual(96, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.18");
                Assert.AreEqual(78, hayabusa64.FighterPower.AirCombat.Min);
                Assert.AreEqual(76, hayabusa64.FighterPower.Interception.Min);
                Assert.AreEqual(22, corps.CostForSortie[0]);
                Assert.AreEqual(14, corps.CostForSortie[1]);

                corps.Planes = new []{hayabusa64, rikuteijukuren};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(99, corps.CalcFighterPower().AirCombat.Min, "制空補正1.18");
                Assert.AreEqual(101, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.23");
                Assert.AreEqual(78, hayabusa64.FighterPower.AirCombat.Min);
                Assert.AreEqual(76, hayabusa64.FighterPower.Interception.Min);
                Assert.AreEqual(22, corps.CostForSortie[0]);
                Assert.AreEqual(14, corps.CostForSortie[1]);

                var suiteiotsu = NewPlaneInfo(零式水上偵察機11型乙());
                corps.Planes = new []{hayabusa64, suiteiotsu};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(80, corps.CalcFighterPower().AirCombat.Min, "水偵は制空補正なし");
                Assert.AreEqual(85, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.1");
                Assert.AreEqual(78, hayabusa64.FighterPower.AirCombat.Min);
                Assert.AreEqual(76, hayabusa64.FighterPower.Interception.Min);
                Assert.AreEqual(22, corps.CostForSortie[0]);
                Assert.AreEqual(14, corps.CostForSortie[1]);

                var saiun = NewPlaneInfo(彩雲());
                corps.Planes = new []{hayabusa64, saiun};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(78, corps.CalcFighterPower().AirCombat.Min, "艦偵は制空補正なし");
                Assert.AreEqual(98, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.3");
                Assert.AreEqual(78, hayabusa64.FighterPower.AirCombat.Min);
                Assert.AreEqual(76, hayabusa64.FighterPower.Interception.Min);
                Assert.AreEqual(22, corps.CostForSortie[0]);
                Assert.AreEqual(14, corps.CostForSortie[1]);


                var keiun = NewPlaneInfo(噴式景雲改(18));
                corps.Planes = new []{keiun};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(25, corps.CalcFighterPower().AirCombat.Min, "制空補正なし");
                Assert.AreEqual(25, corps.CalcFighterPower().Interception.Min, "対基地制空補正なし");
                Assert.AreEqual(25, keiun.FighterPower.AirCombat.Min);
                Assert.AreEqual(25, keiun.FighterPower.Interception.Min);
                Assert.AreEqual("噴式強襲", keiun.ShipBomberPower[0].Target);
                Assert.AreEqual(88.64, Math.Round(keiun.ShipBomberPower[0].BomberPower, 2), "噴式強襲補正なし");
                Assert.AreEqual(95, keiun.ShipBomberPower[0].Accuracy, "噴式強襲は命中固定");
                Assert.AreEqual("対艦", keiun.ShipBomberPower[1].Target);
                Assert.AreEqual(78, keiun.ShipBomberPower[1].BomberPower, "対艦補正なし");
                Assert.AreEqual(102, keiun.ShipBomberPower[1].Accuracy);
                Assert.AreEqual(78, keiun.LandBomberPower.BomberPower, "対地補正なし");
                Assert.AreEqual(156, keiun.LandBomberPower.IsolatedIsland, "離島補正なし");
                Assert.AreEqual(263, keiun.LandBomberPower.SupplyDepot, "集積補正なし");
                Assert.AreEqual(0, keiun.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, keiun.AswBomberPower.Max, "対潜不可");
                Assert.AreEqual(50, keiun.Item.SteelCost);

                corps.Planes = new []{keiun, rikuteijukuren};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(36, corps.CalcFighterPower().AirCombat.Min, "制空補正1.18");
                Assert.AreEqual(38, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.18");
                Assert.AreEqual(25, keiun.FighterPower.AirCombat.Min);
                Assert.AreEqual(25, keiun.FighterPower.Interception.Min);
                Assert.AreEqual("噴式強襲", keiun.ShipBomberPower[0].Target);
                Assert.AreEqual(88.64, Math.Round(keiun.ShipBomberPower[0].BomberPower, 2), "噴式強襲は補正無効");
                Assert.AreEqual(95, keiun.ShipBomberPower[0].Accuracy, "噴式強襲は命中固定");
                Assert.AreEqual("対艦", keiun.ShipBomberPower[1].Target);
                Assert.AreEqual(89.7, Math.Round(keiun.ShipBomberPower[1].BomberPower, 2), "対艦補正1.15");
                Assert.AreEqual(102, keiun.ShipBomberPower[1].Accuracy);
                Assert.AreEqual(89.7, Math.Round(keiun.LandBomberPower.BomberPower, 2), "対地補正1.15");
                Assert.AreEqual(179.4, Math.Round(keiun.LandBomberPower.IsolatedIsland, 1), "離島補正1.15");
                Assert.AreEqual(302.45, keiun.LandBomberPower.SupplyDepot, "集積補正1.15");
                Assert.AreEqual(0, keiun.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, keiun.AswBomberPower.Max, "対潜不可");
                Assert.AreEqual(50, keiun.Item.SteelCost);


                var shinzan = NewPlaneInfo(深山());
                corps.Planes = new []{shinzan};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(3, corps.CalcFighterPower().AirCombat.Min, "制空補正なし");
                Assert.AreEqual(3, corps.CalcFighterPower().Interception.Min, "対基地制空補正なし");
                Assert.AreEqual(3, shinzan.FighterPower.AirCombat.Min);
                Assert.AreEqual(3, shinzan.FighterPower.Interception.Min);
                Assert.AreEqual(73, shinzan.ShipBomberPower[0].BomberPower, "対艦補正なし");
                Assert.AreEqual(95, shinzan.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(93, shinzan.LandBomberPower.BomberPower, "対地補正なし");
                Assert.AreEqual(187, shinzan.LandBomberPower.IsolatedIsland, "離島補正なし");
                Assert.AreEqual(296, shinzan.LandBomberPower.SupplyDepot, "集積補正なし");
                Assert.AreEqual(0, shinzan.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, shinzan.AswBomberPower.Max, "対潜不可");

                corps.Planes = new []{shinzan, rikutei};
                corps.CalcLandBasedReconModifier();
                Assert.AreEqual(10, corps.CalcFighterPower().AirCombat.Min, "制空補正1.15");
                Assert.AreEqual(10, corps.CalcFighterPower().Interception.Min, "対基地制空補正1.15");
                Assert.AreEqual(3, shinzan.FighterPower.AirCombat.Min);
                Assert.AreEqual(3, shinzan.FighterPower.Interception.Min);
                Assert.AreEqual(82.125, shinzan.ShipBomberPower[0].BomberPower, "対艦補正なし");
                Assert.AreEqual(95, shinzan.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(104.625, shinzan.LandBomberPower.BomberPower, "対地補正なし");
                Assert.AreEqual(210.375, shinzan.LandBomberPower.IsolatedIsland, "離島補正なし");
                Assert.AreEqual(333, shinzan.LandBomberPower.SupplyDepot, "集積補正なし");
                Assert.AreEqual(0, shinzan.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, shinzan.AswBomberPower.Max, "対潜不可");


                var isshiki34 = NewPlaneInfo(一式陸攻三四型());
                Assert.AreEqual(16, isshiki34.FighterPower.AirCombat.Min);
                Assert.AreEqual(16, isshiki34.FighterPower.Interception.Min);
                Assert.AreEqual(126, isshiki34.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(102, isshiki34.ShipBomberPower[0].Accuracy);

                isshiki34.Item.Alv = 7;
                Assert.AreEqual(20, isshiki34.FighterPower.AirCombat.Min, "一式陸攻三四型は熟練度最大で制空+4");
                Assert.AreEqual(20, isshiki34.FighterPower.Interception.Min, "一式陸攻三四型は熟練度最大で制空+4");

                isshiki34.Item.Alv = 0;
                isshiki34.Item.Level = 10;
                Assert.AreEqual(23, isshiki34.FighterPower.AirCombat.Min, "改修で制空上昇");
                Assert.AreEqual(23, isshiki34.FighterPower.Interception.Min, "改修で制空上昇");
                Assert.AreEqual(144, isshiki34.ShipBomberPower[0].BomberPower, "改修で対艦攻撃力上昇");
                Assert.AreEqual(102, isshiki34.ShipBomberPower[0].Accuracy);

                var hayabusa65 = NewPlaneInfo(爆装一式戦隼III型改65戦隊());
                Assert.AreEqual(25, hayabusa65.FighterPower.AirCombat.Min);
                Assert.AreEqual(25, hayabusa65.FighterPower.Interception.Min);
                Assert.AreEqual(36, hayabusa65.ShipBomberPower[0].BomberPower, "雷装0でも対艦攻撃可能");
                Assert.AreEqual(123, hayabusa65.ShipBomberPower[0].Accuracy);

                hayabusa65.Item.Level = 10;
                Assert.AreEqual(32, hayabusa65.FighterPower.AirCombat.Min);
                Assert.AreEqual(32, hayabusa65.FighterPower.Interception.Min);
                Assert.AreEqual(54, hayabusa65.ShipBomberPower[0].BomberPower, "雷装0でも改修で対艦攻撃力上昇");
                Assert.AreEqual(123, hayabusa65.ShipBomberPower[0].Accuracy);

                var spitfire = NewPlaneInfo(SpitfireMkV());
                Assert.AreEqual(50, spitfire.FighterPower.AirCombat.Min);
                Assert.AreEqual(72, spitfire.FighterPower.Interception.Min);

                spitfire.Item.Alv = 7;
                Assert.AreEqual(76, spitfire.FighterPower.AirCombat.Min, "熟練度最大で制空上昇");
                Assert.AreEqual(97, spitfire.FighterPower.Interception.Min, "熟練度最大で制空上昇");

                spitfire.Item.Alv = 0;
                spitfire.Item.Level = 10;
                Assert.AreEqual(59, spitfire.FighterPower.AirCombat.Min, "改修で制空上昇");
                Assert.AreEqual(80, spitfire.FighterPower.Interception.Min, "改修で制空上昇");
            }

            [TestMethod]
            public void 高高度防空()
            {
                var corps1 = new AirBase.AirCorpsInfo();
                var corps2 = new AirBase.AirCorpsInfo();
                corps1.Action = 2;
                corps2.Action = 2;
                var airBase = new AirBase.BaseInfo();
                airBase.AirCorps = new []{corps1, corps2};
                airBase.AreaId = 50;

                var spitfire = NewPlaneInfo(SpitfireMkV());
                corps1.Planes = new []{spitfire};
                corps1.CalcLandBasedReconModifier();
                var hayabusa64 = NewPlaneInfo(一式戦隼II型64戦隊());
                corps2.Planes = new []{hayabusa64};
                corps2.CalcLandBasedReconModifier();
                Assert.AreEqual(74, airBase.CalcInterceptionFighterPower(highAltitude: true).Min, "ロケット機がないので補正0.5");

                var shusui = NewPlaneInfo(秋水());
                corps1.Planes = new []{spitfire, shusui};
                corps1.CalcLandBasedReconModifier();
                Assert.AreEqual(189, airBase.CalcInterceptionFighterPower(highAltitude: true).Min, "ロケット機1なので補正0.8");

                corps2.Planes = new []{hayabusa64, shusui};
                corps2.CalcLandBasedReconModifier();
                Assert.AreEqual(358, airBase.CalcInterceptionFighterPower(highAltitude: true).Min, "ロケット機1なので補正1.1");

                var saiun = NewPlaneInfo(彩雲());
                corps1.Planes = new []{spitfire, shusui, saiun};
                corps1.CalcLandBasedReconModifier();
                Assert.AreEqual(411, airBase.CalcInterceptionFighterPower(highAltitude: true).Min, "彩雲配備でさらに1.3");
            }

            [TestMethod]
            public void 艦載機配備()
            {
                var ryusei1 = NewPlaneInfo(流星改一航戦());
                Assert.AreEqual(8, ryusei1.FighterPower.AirCombat.Min);
                Assert.AreEqual(8, ryusei1.FighterPower.Interception.Min);
                Assert.AreEqual(104, ryusei1.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(102, ryusei1.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(104, ryusei1.LandBomberPower.BomberPower);
                Assert.AreEqual(123, ryusei1.LandBomberPower.IsolatedIsland);
                Assert.AreEqual(204, ryusei1.LandBomberPower.SupplyDepot);
                Assert.AreEqual(0, ryusei1.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, ryusei1.AswBomberPower.Max, "対潜不可");

                var ryusei1juku = NewPlaneInfo(流星改一航戦熟練());
                Assert.AreEqual(12, ryusei1juku.FighterPower.AirCombat.Min);
                Assert.AreEqual(12, ryusei1juku.FighterPower.Interception.Min);
                Assert.AreEqual(110, ryusei1juku.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(109, ryusei1juku.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(110, ryusei1juku.LandBomberPower.BomberPower);
                Assert.AreEqual(130, ryusei1juku.LandBomberPower.IsolatedIsland);
                Assert.AreEqual(210, ryusei1juku.LandBomberPower.SupplyDepot);
                Assert.AreEqual(22, ryusei1juku.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(51, ryusei1juku.AswBomberPower.Max, "対潜上限");

                var sb2c5 = NewPlaneInfo(SB2C5());
                Assert.AreEqual(8, sb2c5.FighterPower.AirCombat.Min);
                Assert.AreEqual(8, sb2c5.FighterPower.Interception.Min);
                Assert.AreEqual(93, sb2c5.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(109, sb2c5.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(93, sb2c5.LandBomberPower.BomberPower);
                Assert.AreEqual(187, sb2c5.LandBomberPower.IsolatedIsland);
                Assert.AreEqual(295, sb2c5.LandBomberPower.SupplyDepot);
                Assert.AreEqual(0, sb2c5.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, sb2c5.AswBomberPower.Max, "対潜不可");

                var zero64kmx = NewPlaneInfo(零式艦戦64型複座KMX搭載機());
                Assert.AreEqual(16, zero64kmx.FighterPower.AirCombat.Min);
                Assert.AreEqual(16, zero64kmx.FighterPower.Interception.Min);
                Assert.AreEqual(42, zero64kmx.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(95, zero64kmx.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(42, zero64kmx.LandBomberPower.BomberPower);
                Assert.AreEqual(83, zero64kmx.LandBomberPower.IsolatedIsland);
                Assert.AreEqual(188, zero64kmx.LandBomberPower.SupplyDepot);
                Assert.AreEqual(24, zero64kmx.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(56, zero64kmx.AswBomberPower.Max, "対潜上限");

                var zuiun2 = NewPlaneInfo(瑞雲改二());
                Assert.AreEqual(16, zuiun2.FighterPower.AirCombat.Min);
                Assert.AreEqual(16, zuiun2.FighterPower.Interception.Min);
                Assert.AreEqual(81, zuiun2.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(109, zuiun2.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(81, zuiun2.LandBomberPower.BomberPower);
                Assert.AreEqual(163, zuiun2.LandBomberPower.IsolatedIsland);
                Assert.AreEqual(272, zuiun2.LandBomberPower.SupplyDepot);
                Assert.AreEqual(0, zuiun2.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, zuiun2.AswBomberPower.Max, "対潜不可");

                var zuiun2juku = NewPlaneInfo(瑞雲改二熟練());
                Assert.AreEqual(21, zuiun2juku.FighterPower.AirCombat.Min);
                Assert.AreEqual(21, zuiun2juku.FighterPower.Interception.Min);
                Assert.AreEqual(87, zuiun2juku.ShipBomberPower[0].BomberPower);
                Assert.AreEqual(116, zuiun2juku.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(87, zuiun2juku.LandBomberPower.BomberPower);
                Assert.AreEqual(175, zuiun2juku.LandBomberPower.IsolatedIsland);
                Assert.AreEqual(283, zuiun2juku.LandBomberPower.SupplyDepot);
                Assert.AreEqual(22, zuiun2juku.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(51, zuiun2juku.AswBomberPower.Max, "対潜上限");

                var yateiotsu = NewPlaneInfo(零式水上偵察機11型乙改夜偵());
                Assert.AreEqual(2, yateiotsu.FighterPower.AirCombat.Min);
                Assert.AreEqual(2, yateiotsu.FighterPower.Interception.Min);
                Assert.AreEqual(0, yateiotsu.ShipBomberPower.Count);
                Assert.AreEqual(0, yateiotsu.LandBomberPower.BomberPower);
                Assert.AreEqual(0, yateiotsu.LandBomberPower.IsolatedIsland);
                Assert.AreEqual(0, yateiotsu.LandBomberPower.SupplyDepot);
                Assert.AreEqual(0, yateiotsu.AswBomberPower.Min, "対潜不可");
                Assert.AreEqual(0, yateiotsu.AswBomberPower.Max, "対潜不可");

                var suiteiotsu = NewPlaneInfo(零式水上偵察機11型乙());
                Assert.AreEqual(2, suiteiotsu.FighterPower.AirCombat.Min);
                Assert.AreEqual(2, suiteiotsu.FighterPower.Interception.Min);
                Assert.AreEqual(0, suiteiotsu.ShipBomberPower.Count);
                Assert.AreEqual(0, suiteiotsu.LandBomberPower.BomberPower);
                Assert.AreEqual(15, suiteiotsu.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(35, suiteiotsu.AswBomberPower.Max, "対潜上限");

                var kagou = NewPlaneInfo(カ号観測機());
                Assert.AreEqual(0, kagou.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, kagou.FighterPower.Interception.Min);
                Assert.AreEqual(0, kagou.ShipBomberPower.Count);
                Assert.AreEqual(0, kagou.LandBomberPower.BomberPower);
                Assert.AreEqual(26, kagou.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(60, kagou.AswBomberPower.Max, "対潜上限");

                var s51jkai = NewPlaneInfo(S51J改());
                Assert.AreEqual(0, s51jkai.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, s51jkai.FighterPower.Interception.Min);
                Assert.AreEqual(0, s51jkai.ShipBomberPower.Count);
                Assert.AreEqual(0, s51jkai.LandBomberPower.BomberPower);
                Assert.AreEqual(69, s51jkai.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(98, s51jkai.AswBomberPower.Max, "対潜上限");

                var sanshiki = NewPlaneInfo(三式指揮連絡機対潜());
                Assert.AreEqual(0, sanshiki.FighterPower.AirCombat.Min);
                Assert.AreEqual(0, sanshiki.FighterPower.Interception.Min);
                Assert.AreEqual(0, sanshiki.ShipBomberPower.Count);
                Assert.AreEqual(0, sanshiki.LandBomberPower.BomberPower);
                Assert.AreEqual(22, sanshiki.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(51, sanshiki.AswBomberPower.Max, "対潜上限");

                var hayabusa20juku = NewPlaneInfo(一式戦隼III型改熟練20戦隊(alv: 7));
                Assert.AreEqual(54, hayabusa20juku.FighterPower.AirCombat.Min, "熟練度補正が戦闘機と同じ");
                Assert.AreEqual(54, hayabusa20juku.FighterPower.Interception.Min, "熟練度補正が戦闘機と同じ");
                Assert.AreEqual(53, hayabusa20juku.ShipBomberPower[0].BomberPower, "対艦爆撃可能");
                Assert.AreEqual(102, hayabusa20juku.ShipBomberPower[0].Accuracy);
                Assert.AreEqual(53, hayabusa20juku.LandBomberPower.BomberPower, "対地爆撃可能");
                Assert.AreEqual(107, hayabusa20juku.LandBomberPower.IsolatedIsland, "離島補正は有効と仮定");
                Assert.AreEqual(212, hayabusa20juku.LandBomberPower.SupplyDepot, "修正補正は有効と仮定");
                Assert.AreEqual(24, hayabusa20juku.AswBomberPower.Min, "対潜下限");
                Assert.AreEqual(56, hayabusa20juku.AswBomberPower.Max, "対潜上限");
            }

            [TestMethod]
            public void 特定艦種特効()
            {
                var nonakatai = NewPlaneInfo(一式陸攻野中隊());
                Assert.AreEqual("駆逐", nonakatai.ShipBomberPower[1].Target);
                Assert.AreEqual(133.2, Math.Round(nonakatai.ShipBomberPower[1].BomberPower, 1));
                Assert.AreEqual(107, nonakatai.ShipBomberPower[1].Accuracy);

                var gingaegusa = NewPlaneInfo(銀河江草隊());
                Assert.AreEqual("駆逐", gingaegusa.ShipBomberPower[1].Target);
                Assert.AreEqual(158.4, gingaegusa.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(121, gingaegusa.ShipBomberPower[1].Accuracy);

                var hs293 = NewPlaneInfo(Do217E5Hs293初期型());
                Assert.AreEqual("駆逐", hs293.ShipBomberPower[1].Target);
                Assert.AreEqual(153, hs293.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(109, hs293.ShipBomberPower[1].Accuracy);

                var fritzx = NewPlaneInfo(Do217K2FritzX());
                Assert.AreEqual("戦艦", fritzx.ShipBomberPower[1].Target);
                Assert.AreEqual(232.2, fritzx.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(109, fritzx.ShipBomberPower[1].Accuracy);

                var hiryumissile = NewPlaneInfo(四式重爆飛龍イ号一型甲誘導弾());
                Assert.AreEqual("駆逐", hiryumissile.ShipBomberPower[1].Target);
                Assert.AreEqual(176.4, hiryumissile.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(95, hiryumissile.ShipBomberPower[1].Accuracy);
                Assert.AreEqual("軽巡", hiryumissile.ShipBomberPower[2].Target);
                Assert.AreEqual(176.4, hiryumissile.ShipBomberPower[2].BomberPower, 1);
                Assert.AreEqual(109, hiryumissile.ShipBomberPower[2].Accuracy);
                Assert.AreEqual("重巡軽空戦艦", hiryumissile.ShipBomberPower[3].Target);
                Assert.AreEqual(176.4, hiryumissile.ShipBomberPower[3].BomberPower, 1);
                Assert.AreEqual(102, hiryumissile.ShipBomberPower[3].Accuracy);

                var hiryumissileskilled = NewPlaneInfo(四式重爆飛龍熟練イ号一型甲誘導弾());
                Assert.AreEqual("駆逐", hiryumissileskilled.ShipBomberPower[1].Target);
                Assert.AreEqual(194.4, hiryumissileskilled.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(116, hiryumissileskilled.ShipBomberPower[1].Accuracy);
                Assert.AreEqual("軽巡", hiryumissileskilled.ShipBomberPower[2].Target);
                Assert.AreEqual(194.4, hiryumissileskilled.ShipBomberPower[2].BomberPower, 1);
                Assert.AreEqual(123, hiryumissileskilled.ShipBomberPower[2].Accuracy);
                Assert.AreEqual("重巡軽空戦艦", hiryumissileskilled.ShipBomberPower[3].Target);
                Assert.AreEqual(194.4, hiryumissileskilled.ShipBomberPower[3].BomberPower, 1);
                Assert.AreEqual(116, hiryumissileskilled.ShipBomberPower[3].Accuracy);

                var ki102 = NewPlaneInfo(キ102乙());
                Assert.AreEqual("駆逐", ki102.ShipBomberPower[1].Target);
                Assert.AreEqual(126, ki102.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(130, ki102.ShipBomberPower[1].Accuracy);

                var ki102missile = NewPlaneInfo(キ102乙改イ号一型乙誘導弾());
                Assert.AreEqual("駆逐", ki102missile.ShipBomberPower[1].Target);
                Assert.AreEqual(167.4, ki102missile.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(101, ki102missile.ShipBomberPower[1].Accuracy);
                Assert.AreEqual("軽巡", ki102missile.ShipBomberPower[2].Target);
                Assert.AreEqual(167.4, ki102missile.ShipBomberPower[2].BomberPower, 1);
                Assert.AreEqual(123, ki102missile.ShipBomberPower[2].Accuracy);
                Assert.AreEqual("重巡軽空戦艦", ki102missile.ShipBomberPower[3].Target);
                Assert.AreEqual(167.4, ki102missile.ShipBomberPower[3].BomberPower, 1);
                Assert.AreEqual(116, ki102missile.ShipBomberPower[3].Accuracy);

                var b25 = NewPlaneInfo(B25());
                Assert.AreEqual("駆逐", b25.ShipBomberPower[1].Target);
                Assert.AreEqual(196.2, b25.ShipBomberPower[1].BomberPower, 1);
                Assert.AreEqual(116, b25.ShipBomberPower[1].Accuracy);
                Assert.AreEqual("軽巡", b25.ShipBomberPower[2].Target);
                Assert.AreEqual(181.8, b25.ShipBomberPower[2].BomberPower, 1);
                Assert.AreEqual(116, b25.ShipBomberPower[2].Accuracy);
                Assert.AreEqual("重巡", b25.ShipBomberPower[3].Target);
                Assert.AreEqual(165.6, b25.ShipBomberPower[3].BomberPower, 1);
                Assert.AreEqual(116, b25.ShipBomberPower[3].Accuracy);
                Assert.AreEqual("軽空戦艦補給", b25.ShipBomberPower[4].Target);
                Assert.AreEqual(129.6, b25.ShipBomberPower[4].BomberPower, 1);
                Assert.AreEqual(95, b25.ShipBomberPower[4].Accuracy);

                var hayabusa65 = NewPlaneInfo(爆装一式戦隼III型改65戦隊());
                Assert.AreEqual("駆逐PT", hayabusa65.ShipBomberPower[1].Target);
                Assert.AreEqual(239.4, hayabusa65.ShipBomberPower[1].BomberPower);
                Assert.AreEqual(123, hayabusa65.ShipBomberPower[1].Accuracy);

                var hayabusa20juku = NewPlaneInfo(一式戦隼III型改熟練20戦隊());
                Assert.AreEqual("駆逐", hayabusa20juku.ShipBomberPower[1].Target);
                Assert.AreEqual(195, hayabusa20juku.ShipBomberPower[1].BomberPower);
                Assert.AreEqual(102, hayabusa20juku.ShipBomberPower[1].Accuracy);
            }
        }

        [TestClass]
        public class AntiSubmarine
        {
            [TestMethod]
            public void 軽空母()
            {
                var ship = NewShip(560);
                ship.Level = 99;
                ship.MaxAsw = 47;
                ship.ShownAsw = 47 + 11;
                ship.SetItems(new[] {水中聴音機零式()});
                Assert.IsFalse(ship.EnableAsw, "艦載機なし");

                ship.ShownAsw = 47 + 18;
                ship.SetItems(new[] {九七式艦攻九三一空(0), 水中聴音機零式()});
                Assert.IsFalse(ship.EnableAsw, "艦載機0機");

                ship.SetItems(new[] {九七式艦攻九三一空(1), 水中聴音機零式()});
                Assert.IsTrue(ship.EnableAsw, "艦載機あり");
                Assert.AreEqual(48.71, Math.Round(ship.EffectiveAsw, 2), "艦載機あり");
            }

            [TestMethod]
            public void 水上機母艦()
            {
                var ship = NewShip(102);
                ship.ShownAsw = 10;
                ship.SetItems(new[] {三式水中探信儀()});
                Assert.IsFalse(ship.EnableAsw, "艦載機なし");

                ship.ShownAsw = 19;
                ship.SetItems(new[] {三式水中探信儀(), カ号観測機(0)});
                Assert.IsFalse(ship.EnableAsw, "艦載機0機");

                ship.SetItems(new[] {三式水中探信儀(), カ号観測機(1)});
                Assert.IsTrue(ship.EnableAsw, "艦載機あり");
                Assert.AreEqual(36.5, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 速吸改()
            {
                var ship = NewShip(352);
                ship.Level = 99;
                ship.MaxAsw = 36;
                ship.ShownAsw = 36;
                ship.SetItems(new[] {Re2001G改()});
                Assert.IsFalse(ship.EnableAsw, "Re2001G改のみで対潜不可");

                ship.SetItems(new[] {Re2001G改(), 流星改(0)});
                Assert.IsFalse(ship.EnableAsw, "Re2001G改 + 流星改0機");

                ship.SetItems(new[] {Re2001G改(), 流星改(3)});
                Assert.IsTrue(ship.EnableAsw, "Re2001G改 + 流星改");

                ship.SetItems(new[] {Re2001G改(), 瑞雲(0)});
                Assert.IsFalse(ship.EnableAsw, "Re2001G改 + 瑞雲0機");

                ship.SetItems(new[] {Re2001G改(), 瑞雲(1)});
                Assert.IsTrue(ship.EnableAsw, "Re2001G改 + 瑞雲");

                ship.SetItems(new[] {Re2001G改(), カ号観測機(0)});
                Assert.IsFalse(ship.EnableAsw, "Re2001G改 + カ号観測機0機");

                ship.SetItems(new[] {Re2001G改(), カ号観測機(1)});
                Assert.IsTrue(ship.EnableAsw, "Re2001G改 + カ号観測機");

                ship.SetItems(new int[0]);
                Assert.IsTrue(ship.EnableAsw, "無装備");

                ship.SetItems(new[] {流星改(0)});
                Assert.IsFalse(ship.EnableAsw, "流星改0機");

                ship.SetItems(new[] {流星改(6)});
                Assert.IsTrue(ship.EnableAsw, "流星改");

                ship.SetItems(new[] {瑞雲(0)});
                Assert.IsFalse(ship.EnableAsw, "瑞雲0機");

                ship.SetItems(new[] {瑞雲(1)});
                Assert.IsTrue(ship.EnableAsw, "瑞雲");

                ship.SetItems(new[] {カ号観測機(0)});
                Assert.IsFalse(ship.EnableAsw, "カ号観測機0機");

                ship.SetItems(new[] {カ号観測機(1)});
                Assert.IsTrue(ship.EnableAsw, "カ号観測機");

                ship.SetItems(new[] {流星改(0), 瑞雲(1)});
                Assert.IsFalse(ship.EnableAsw, "流星改0機＋瑞雲");

                ship.SetItems(new[] {流星改(0), カ号観測機(1)});
                Assert.IsFalse(ship.EnableAsw, "流星改0機＋カ号観測機");
            }

            [TestMethod]
            public void 山汐丸()
            {
                var ship = NewShip(900);
                ship.Level = 78;
                ship.MaxAsw = 63;
                ship.ShownAsw = 54 + 7 + 1; // 装備ボーナス +1

                ship.SetItems(new[] {三式指揮連絡機対潜(0)});
                Assert.IsFalse(ship.EnableAsw, "三式指揮連絡機対潜0機");

                ship.SetItems(new[] {三式指揮連絡機対潜(8)});
                Assert.IsTrue(ship.EnableAsw, "三式指揮連絡機対潜");
                Assert.AreEqual(34.7, Math.Round(ship.EffectiveAsw, 2));

                ship.SetItems(new[] {三式爆雷投射機()});
                Assert.IsTrue(ship.EnableAsw, "三式爆雷投射機");
                Assert.AreEqual(39.7, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 山汐丸改()
            {
                var ship = NewShip(717);
                ship.Level = 78;
                ship.MaxAsw = 82;
                ship.ShownAsw = 71 + 8;

                ship.SetItems(new[] {零式艦戦64型複座KMX搭載機(0)});
                Assert.IsFalse(ship.EnableAsw, "零式艦戦64型複座KMX搭載機0機");

                ship.SetItems(new[] {零式艦戦64型複座KMX搭載機(8)});
                Assert.IsTrue(ship.EnableAsw, "零式艦戦64型複座KMX搭載機");
                Assert.AreEqual(36.85, Math.Round(ship.EffectiveAsw, 2));

                ship.SetItems(new[] {三式爆雷投射機()});
                Assert.IsTrue(ship.EnableAsw, "三式爆雷投射機");
                Assert.AreEqual(41.85, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 第百一号輸送艦()
            {
                var ship = NewShip(945);
                ship.Level = 1;
                ship.MaxAsw = 0;
                Assert.IsFalse(ship.UnresolvedAsw);
                Assert.IsFalse(ship.EnableAsw);

                ship.SetItems(new[] {三式爆雷投射機()});
                ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
                Assert.IsFalse(ship.UnresolvedAsw);
                Assert.IsTrue(ship.EnableAsw);
                Assert.AreEqual(25, ship.EffectiveAsw);
            }

            [TestMethod]
            public void 対潜装備一つ()
            {
                var ship = NewShip(195);
                ship.Level = 99;
                ship.MaxAsw = 63;
                ship.ShownAsw = 63 + 10;
                ship.SetItems(new[] {三式水中探信儀()});
                Assert.AreEqual(43.87, Math.Round(ship.EffectiveAsw, 2));

                ship.ShownAsw = 63 + 8;
                ship.SetItems(new[] {三式爆雷投射機()});
                Assert.AreEqual(40.87, Math.Round(ship.EffectiveAsw, 2));

                ship.ShownAsw = 63 + 4;
                ship.SetItems(new[] {九五式爆雷()});
                Assert.AreEqual(34.87, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 爆雷投射機と爆雷()
            {
                var ship = NewShip(195);
                ship.Level = 99;
                ship.MaxAsw = 63;
                ship.ShownAsw = 63 + 12;
                ship.SetItems(new[] {三式爆雷投射機(), 九五式爆雷()});
                Assert.AreEqual(51.56, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void ソナーとそれ以外()
            {
                var ship = NewShip(195);
                ship.Level = 99;
                ship.MaxAsw = 63;
                ship.ShownAsw = 63 + 18;
                ship.SetItems(new[] {三式水中探信儀(), 三式爆雷投射機()});
                Assert.AreEqual(64.26, Math.Round(ship.EffectiveAsw, 2));

                ship.ShownAsw = 63 + 14;
                ship.SetItems(new[] {三式水中探信儀(), 九五式爆雷()});
                Assert.AreEqual(57.36, Math.Round(ship.EffectiveAsw, 2));

                ship.ShownAsw = 63 + 13;
                ship.SetItems(new[] {三式水中探信儀(), 二式12cm迫撃砲改()});
                Assert.AreEqual(55.63, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 三種コンビネーション()
            {
                var ship = NewShip(195);
                ship.Level = 99;
                ship.MaxAsw = 63;
                ship.ShownAsw = 63 + 22;
                ship.SetItems(new[] {三式水中探信儀(), 三式爆雷投射機(), 九五式爆雷()});
                Assert.AreEqual(88.94, Math.Round(ship.EffectiveAsw, 2));

                ship.ShownAsw = 63 + 17;
                ship.SetItems(new[] {三式水中探信儀(), 二式12cm迫撃砲改(), 九五式爆雷()});
                Assert.AreEqual(62.53, Math.Round(ship.EffectiveAsw, 2), "三種コンビネーションにならない");

                ship.ShownAsw = 63 + 23;
                ship.SetItems(new[] {水中聴音機零式(), 三式爆雷投射機(), 九五式爆雷()});
                Assert.AreEqual(80.17, Math.Round(ship.EffectiveAsw, 2), "大型ソナーは三種倍率が落ちる");
            }

            [TestMethod]
            public void Lvから素対潜を計算()
            {
                var ship = NewShip(714);
                ship.Level = 37;
                ship.MaxAsw = 81;
                Assert.IsFalse(ship.UnresolvedAsw);
                Assert.AreEqual(ship.RawAsw, 54);

                ship.Level = 41;
                Assert.AreEqual(ship.RawAsw, 55);
                ship.Level++;
                Assert.AreEqual(ship.RawAsw, 56);
            }

            [TestMethod]
            public void 装備ボーナス()
            {
                var ship = NewShip(662);
                ship.Level = 99;
                ship.MaxAsw = 84;
                ship.ShownAsw = 84 + 7 + 3;
                ship.SetItems(new[] {零式水上偵察機11型乙(1)});
                Assert.AreEqual(84, ship.RawAsw);
                Assert.AreEqual(35.83, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 対潜改修()
            {
                var ship = NewShip(555);
                ship.Level = 99;
                ship.MaxAsw = 0;
                ship.ShownAsw = 10;
                ship.SetItems(new[] {SwordfishMkIII熟練(21)});
                Assert.AreEqual(0, ship.RawAsw);
                Assert.AreEqual(23.0, Math.Round(ship.EffectiveAsw, 2));

                ship.ImprovedAsw = 9;
                ship.ShownAsw += ship.ImprovedAsw;
                Assert.AreEqual(9, ship.RawAsw);
                Assert.AreEqual(29.0, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 対潜最小値不明な新艦()
            {
                var ship = new ShipStatus
                {
                    Level = 84,
                    MaxAsw = 86,
                    Spec = new ShipSpec
                    {
                        Id = 58800,
                        ShipType = 2,
                        Name = "山風改二",
                    },
                    ShownAsw = 77 + 10 + 3,
                };
                ship.Spec.GetMinAsw = () => AdditionalData.MinAsw(ship.Spec.Id);
                ship.GetItem = itemId => ItemInventory[itemId];
                ship.SetItems(new[] {三式水中探信儀()});
                Assert.IsTrue(ship.UnresolvedAsw);
                Assert.AreEqual(80, ship.RawAsw);
                Assert.AreEqual(45.89, Math.Round(ship.EffectiveAsw, 2));

                ship.Spec.Id = 588;
                Assert.IsFalse(ship.UnresolvedAsw);
                Assert.AreEqual(77, ship.RawAsw);
                Assert.AreEqual(50.05, Math.Round(ship.EffectiveAsw, 2));
            }

            [TestMethod]
            public void 対潜支援()
            {
                var ship = NewShip(717);

                ship.SetItems(new[] {カ号観測機(8), 三式指揮連絡機対潜(8), 一式戦隼II型改20戦隊(8)});
                Assert.AreEqual(35.7, Math.Round(ship.Items[0].CalcAswSupportPower()[0], 2));
                Assert.AreEqual(29.4, Math.Round(ship.Items[1].CalcAswSupportPower()[0], 2));
                Assert.AreEqual(29.4, Math.Round(ship.Items[2].CalcAswSupportPower()[0], 2));

                ship.SetItems(new[] {カ号観測機(8, level: 10), 三式指揮連絡機対潜(8, level: 10), 一式戦隼II型改20戦隊(8, level: 10)});
                Assert.AreEqual(35.7, Math.Round(ship.Items[0].CalcAswSupportPower()[0], 2), "改修値は影響しない");
                Assert.AreEqual(29.4, Math.Round(ship.Items[1].CalcAswSupportPower()[0], 2), "改修値は影響しない");
                Assert.AreEqual(29.4, Math.Round(ship.Items[2].CalcAswSupportPower()[0], 2), "改修値は影響しない");
            }
        }

        [TestClass]
        public class LoS
        {
            [TestMethod]
            public void アメリカ艦にSGレーダー初期型()
            {
                var ship = NewShip(440);
                ship.LoS = 71 + 8 + 4 + 8 + 4;
                ship.SetItems(new[] {SGレーダー初期型(), SGレーダー初期型()});
                Assert.AreEqual(79, ship.RawLoS, "SGレーダー初期型の索敵ボーナスは無視されるバグは解消した");
            }

            [TestMethod]
            public void アメリカ艦にSKレーダー()
            {
                var ship = NewShip(440);
                ship.LoS = 71 + 10 + 1 + 10 + 1;
                ship.SetItems(new[] {SKレーダー(), SKレーダー()});
                Assert.AreEqual(73, ship.RawLoS, "SGレーダー初期型以外の索敵ボーナスは加算される");
            }

            [TestMethod]
            public void 丹陽雪風改二にSGレーダー初期型()
            {
                var ship = NewShip(656);
                ship.LoS = 48 + 8 + 3 + 8 + 0;
                ship.SetItems(new[] {SGレーダー初期型(), SGレーダー初期型()});
                Assert.AreEqual(51, ship.RawLoS, "1個目のSGレーダー初期型の索敵ボーナスだけ加算される");
            }
        }

        // ReSharper disable once InconsistentNaming
        protected static int A12cm30連装噴進砲改二()
        {
            return NewItem(274);
        }

        // ReSharper disable once InconsistentNaming
        protected static int A25mm三連装機銃集中配備()
        {
            return NewItem(131);
        }

        protected static int A10cm連装高角砲群集中配備()
        {
            return NewItem(464);
        }

        protected static int A12_7cm単装高角砲改二()
        {
            return NewItem(379);
        }

        [TestClass]
        public class AntiAir
        {
            // https://twitter.com/nishikkuma/status/1555193708475486210
            [TestMethod]
            public void 装備ボーナス大和改二重()
            {
                var fleet = NewFleet();
                var ship = NewShip(916);
                ship.SetItems(new[] {A10cm連装高角砲群集中配備(), A10cm連装高角砲群集中配備(), A12cm30連装噴進砲改二()});
                ship.AntiAir = 112 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 112 - ship.Spec.MinAntiAir;
                fleet.SetShips(new[] {ship.Id});
                fleet.CalcAntiAir();
                Assert.AreEqual(9, fleet.AntiAir);
                Assert.AreEqual(256, ship.EffectiveAntiAirForShip);
                Assert.AreEqual(64.0, ship.PropShootdown);
                Assert.AreEqual(26.98, Math.Round(ship.FixedShootdown, 2));

                ship.AntiAir += 10;
                Assert.AreEqual(262, ship.EffectiveAntiAirForShip);
                Assert.AreEqual(65.5, ship.PropShootdown);
                Assert.AreEqual(28.08, Math.Round(ship.FixedShootdown, 2));
            }

            // https://twitter.com/noro_006/status/1555206653586571264
            [TestMethod]
            public void 装備ボーナス由良改二()
            {
                var fleet = NewFleet();
                var ship = NewShip(488);
                ship.SetItems(new[] {A12_7cm単装高角砲改二()});
                ship.AntiAir = 88 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 88 - ship.Spec.MinAntiAir;
                fleet.SetShips(new[] {ship.Id});
                fleet.CalcAntiAir();
                Assert.AreEqual(1, fleet.AntiAir);
                Assert.AreEqual(108, ship.EffectiveAntiAirForShip);
                Assert.AreEqual(27.0, ship.PropShootdown);
                Assert.AreEqual(10.95, Math.Round(ship.FixedShootdown, 2));

                ship.AntiAir += 4;
                Assert.AreEqual(110, ship.EffectiveAntiAirForShip);
                Assert.AreEqual(27.5, ship.PropShootdown);
                Assert.AreEqual(11.35, Math.Round(ship.FixedShootdown, 2));
            }

            [TestMethod]
            public void 噴進砲改二なし()
            {
                var ship = NewShip(73);
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(0, ship.AntiAirPropellantBarrageChance);
            }

            [TestMethod]
            public void 噴進砲改二1つ()
            {
                var ship = NewShip(286);
                ship.SetItems(new[] {A12cm30連装噴進砲改二()});
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(61.71, Math.Round(ship.AntiAirPropellantBarrageChance, 2));
            }

            [TestMethod]
            public void 補強増設に噴進砲改二()
            {
                var ship = NewShip(83);
                ship.SetItems(new int[0], A12cm30連装噴進砲改二());
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(61.71, Math.Round(ship.AntiAirPropellantBarrageChance, 2));
            }

            [TestMethod]
            public void 噴進砲改二2つ()
            {
                var ship = NewShip(89);
                ship.SetItems(new[] {A12cm30連装噴進砲改二(), A12cm30連装噴進砲改二()});
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(93.79, Math.Round(ship.AntiAirPropellantBarrageChance, 2));
            }

            [TestMethod]
            public void 噴進砲改二2つと機銃()
            {
                var ship = NewShip(102);
                ship.SetItems(new[] {A12cm30連装噴進砲改二(), A12cm30連装噴進砲改二(), A25mm三連装機銃集中配備()});
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(113.01, Math.Round(ship.AntiAirPropellantBarrageChance, 2), "噴進砲改二2+機銃");
            }

            [TestMethod]
            public void 伊勢型()
            {
                var ship = NewShip(82);
                ship.SetItems(new[] {A12cm30連装噴進砲改二()});
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(86.71, Math.Round(ship.AntiAirPropellantBarrageChance, 2));
            }

            [TestMethod]
            public void 噴進砲改二に装備ボーナス()
            {
                var ship = NewShip(73);
                ship.SetItems(new[] {A12cm30連装噴進砲改二(), A25mm三連装機銃集中配備()});
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(80.93, Math.Round(ship.AntiAirPropellantBarrageChance, 2));

                ship.AntiAir += 12;
                Assert.AreEqual(84.48, Math.Round(ship.AntiAirPropellantBarrageChance, 2));
            }

            [TestMethod]
            public void 空母水母航戦航巡以外は噴進弾幕不可()
            {
                var ship = NewShip(78);
                ship.SetItems(new[] {A12cm30連装噴進砲改二()});
                ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
                ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
                ship.Lucky = 46;
                Assert.AreEqual(0, ship.AntiAirPropellantBarrageChance, 2);
            }
        }

        protected static int Daihatsu()
        {
            return NewItem(68);
        }
        protected static int Tokudaihatsu()
        {
            return NewItem(193);
        }
        protected static int DaihatsuMax()
        {
            return NewItem(68, level: 10);
        }
        protected static int TokudaihatsuMax()
        {
            return NewItem(193, level: 10);
        }
        protected static int Daihatsu89Max()
        {
            return NewItem(166, level: 10);
        }
        protected static int Type2TankMax()
        {
            return NewItem(167, level: 10);
        }
        protected static int Daihatu11th()
        {
            return NewItem(230);
        }
        protected static int M4A1DD()
        {
            return NewItem(355);
        }
        protected static int ArmoredBoatMax()
        {
            return NewItem(408, level: 10);
        }
        protected static int ArmedDaihatsu()
        {
            return NewItem(409);
        }
        protected static int NorthAfrica()
        {
            return NewItem(436);
        }

        [TestClass]
        public class DaihatsuBonus
        {
            [TestMethod]
            public void DaihatsuBonusKinuKai2()
            {
                var ship1 = NewShip(487); // 鬼怒改二
                ship1.SetItems(new[] {Daihatsu(), Daihatsu()});
                var ship2 = NewShip(348); // 瑞穂改
                ship2.SetItems(new[] {Tokudaihatsu(), Tokudaihatsu(), Tokudaihatsu()});
                var fleet = NewFleet();
                fleet.SetShips(new[] {ship1.Id, ship2.Id});
                Assert.AreEqual(25.2, fleet.DaihatsuBonus);
            }

            [TestMethod]
            public void DaihatsuBonusMax()
            {
                var ship1 = NewShip(586); // 日進甲
                ship1.SetItems(new[] {DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax()});
                var ship2 = NewShip(372); // Commandant Teste改
                ship2.SetItems(new[] {DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax()});
                var fleet = NewFleet();
                fleet.SetShips(new[] {ship1.Id, ship2.Id});
                Assert.AreEqual(28, fleet.DaihatsuBonus);
            }

            [TestMethod]
            public void DaihatsuBonusOverMax()
            {
                var ship1 = NewShip(487); // 鬼怒改二
                ship1.SetItems(new[] {DaihatsuMax(), TokudaihatsuMax()});
                var ship2 = NewShip(586); // 日進甲
                ship2.SetItems(new[] {DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax()});
                var ship3 = NewShip(372); // Commandant Teste改
                ship3.SetItems(new[] {DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax()});
                var fleet = NewFleet();
                fleet.SetShips(new[] {ship1.Id, ship2.Id, ship3.Id});
                Assert.AreEqual(28, fleet.DaihatsuBonus);
            }

            [TestMethod]
            public void Daihatsu89Type2TankBonus()
            {
                var ship = NewShip(348); // 瑞穂改
                ship.SetItems(new[] {Daihatsu89Max(), Type2TankMax(), NorthAfrica()});
                var fleet = NewFleet();
                fleet.SetShips(new[] {ship.Id});
                Assert.AreEqual(5.3, fleet.DaihatsuBonus);
            }

            [TestMethod]
            public void OtherDaihatsuBonus1()
            {
                var ship = NewShip(348); // 瑞穂改
                ship.SetItems(new[] {TokudaihatsuMax(), NorthAfrica()});
                var fleet = NewFleet();
                fleet.SetShips(new[] {ship.Id});
                Assert.AreEqual(9.3, fleet.DaihatsuBonus);
            }

            [TestMethod]
            public void OtherDaihatsuBonus2()
            {
                var ship = NewShip(348); // 瑞穂改
                ship.SetItems(new[] {TokudaihatsuMax(), Daihatu11th(), M4A1DD()});
                var fleet = NewFleet();
                fleet.SetShips(new[] {ship.Id});
                Assert.AreEqual(7.5, fleet.DaihatsuBonus);
            }

            [TestMethod]
            public void OtherDaihatsuBonus3()
            {
                var ship = NewShip(348); // 瑞穂改
                ship.SetItems(new[] {TokudaihatsuMax(), ArmoredBoatMax(), ArmedDaihatsu()});
                var fleet = NewFleet();
                fleet.SetShips(new[] {ship.Id});
                Assert.AreEqual(12.6, fleet.DaihatsuBonus);
            }
        }

        protected static int Damecon()
        {
            return NewItem(42);
        }
        protected static int Megami()
        {
            return NewItem(43);
        }

        [TestClass]
        public class DamageControl
        {
            [TestMethod]
            public void PreparedDamageControl()
            {
                var ship = NewShip(38);
                ship.MaxHp = 16;
                ship.SetItems(new[] {三式水中探信儀(), Damecon()});

                ship.NowHp = 5;
                Assert.AreEqual(-1, ship.PreparedDamageControl, "大破してない");

                ship.NowHp = 4;
                Assert.AreEqual(42, ship.PreparedDamageControl, "応急修理要員が選択される");

                ship.SetItems(new[] {三式水中探信儀(), 三式水中探信儀()});
                Assert.AreEqual(-1, ship.PreparedDamageControl, "ダメコンを装備してない");

                ship.SetItems(new[] {Megami(), Damecon()});
                Assert.AreEqual(43, ship.PreparedDamageControl, "応急修理女神が選択される");

                ship.SetItems(new[] {三式水中探信儀(), Megami()}, Damecon());
                Assert.AreEqual(42, ship.PreparedDamageControl, "応急修理要員が選択される");
            }
        }
    }
}