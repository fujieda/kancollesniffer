﻿// Copyright (C) 2019 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using DynaJson;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    using Sniffer = SnifferTest.TestingSniffer;
    using static SnifferTest;

    [TestClass]
    public class QuestCountTest
    {
        [TestMethod]
        public void AdjustCount()
        {
            var count = new QuestCount
            {
                Spec = new QuestSpec {Max = 7},
                Now = 3
            };
            count.AdjustCount(0);
            Assert.AreEqual(3, count.Now);
            count.AdjustCount(50);
            Assert.AreEqual(4, count.Now);
            count.AdjustCount(80);
            Assert.AreEqual(6, count.Now);
            count.AdjustCount(100);
            Assert.AreEqual(7, count.Now);
            count.Now = 14;
            count.AdjustCount(100);
            Assert.AreEqual(14, count.Now);
            count.AdjustCount(80);
            Assert.AreEqual(6, count.Now);
            count.AdjustCount(50);
            Assert.AreEqual(5, count.Now);
            count.AdjustCount(0);
            Assert.AreEqual(3, count.Now);
        }

        [TestMethod]
        public void AdjustCountWithShift()
        {
            var count = new QuestCount
            {
                Spec = new QuestSpec {Max = 7, Shift = 1},
                Now = 3
            };
            count.AdjustCount(0);
            Assert.AreEqual(2, count.Now);
            count.AdjustCount(50);
            Assert.AreEqual(3, count.Now);
            count.AdjustCount(80);
            Assert.AreEqual(6, count.Now);
            count.AdjustCount(100);
            Assert.AreEqual(7, count.Now);
            count.Now = 14;
            count.AdjustCount(100);
            Assert.AreEqual(14, count.Now);
            count.AdjustCount(80);
            Assert.AreEqual(6, count.Now);
            count.AdjustCount(50);
            Assert.AreEqual(5, count.Now);
            count.AdjustCount(0);
            Assert.AreEqual(2, count.Now);
        }

        [TestMethod]
        public void AdjustCountMax3WithShift2()
        {
            var count = new QuestCount
            {
                Spec = new QuestSpec {Max = 3, Shift = 2},
                Now = 0
            };
            count.AdjustCount(0);
            Assert.AreEqual(0, count.Now);
            count.AdjustCount(50);
            Assert.AreEqual(1, count.Now);
            count.AdjustCount(80);
            Assert.AreEqual(2, count.Now);
            count.AdjustCount(100);
            Assert.AreEqual(3, count.Now);
            count.Now = 4;
            count.AdjustCount(100);
            Assert.AreEqual(4, count.Now);
            count.AdjustCount(80);
            Assert.AreEqual(2, count.Now);
            count.AdjustCount(50);
            Assert.AreEqual(1, count.Now);
            count.AdjustCount(0);
            Assert.AreEqual(0, count.Now);
        }

        [TestMethod]
        public void AdjustCount80Percent()
        {
            var count = new QuestCount
            {
                Spec = new QuestSpec()
            };
            for (var shift = 0; shift <= 1; shift++)
            {
                for (var max = 2; max <= 6; max++)
                {
                    count.Spec.Max = max;
                    count.Spec.Shift = shift;
                    count.Now = 1;
                    count.AdjustCount(80);
                    Assert.AreEqual(count.Spec.Max - 1, count.Now);
                }
            }
        }

        [TestMethod]
        public void AdjustCountNowArray()
        {
            var count = new QuestCount
            {
                Spec = new QuestSpec {MaxArray = new[] {36, 6, 24, 12}},
                NowArray = new[] {1, 2, 3, 4}
            };
            count.AdjustCount(50);
            Assert.IsTrue(count.NowArray.SequenceEqual(new[] {1, 2, 3, 4}));
            count.AdjustCount(100);
            Assert.IsTrue(count.NowArray.SequenceEqual(new[] {36, 6, 24, 12}));
            count.NowArray = new[] {38, 12, 19, 12};
            count.AdjustCount(100);
            Assert.IsTrue(count.NowArray.SequenceEqual(new[] {38, 12, 24, 12}));
        }

        /// <summary>
        /// カウンターを文字列表記にする
        /// </summary>
        [TestMethod]
        public void ToStringTest()
        {
            var status = new Status
            {
                QuestCountList = new[]
                {
                    new QuestCount {Id = 211, Now = 2},
                    new QuestCount {Id = 214, NowArray = new[] {20, 7, 10, 8}},
                    new QuestCount {Id = 854, NowArray = new[] {2, 1, 1, 1}},
                    new QuestCount {Id = 426, NowArray = new[] {1, 1, 1, 1}},
                    new QuestCount {Id = 428, NowArray = new[] {1, 1, 1}},
                    new QuestCount {Id = 873, NowArray = new[] {1, 1, 1}},
                    new QuestCount {Id = 888, NowArray = new[] {1, 1, 1}},
                    new QuestCount {Id = 688, NowArray = new[] {2, 1, 2, 1}},
                    new QuestCount {Id = 893, NowArray = new[] {1, 1, 1, 1}},
                    new QuestCount {Id = 894, NowArray = new[] {1, 1, 1, 1, 1}},
                    new QuestCount {Id = 280, NowArray = new[] {1, 1, 1, 1}},
                    new QuestCount {Id = 872, NowArray = new[] {1, 1, 1, 1}},
                    new QuestCount {Id = 284, NowArray = new[] {1, 1, 1, 1}},
                    new QuestCount {Id = 226, Now = 2},
                    new QuestCount {Id = 434, NowArray = new[] {1, 0, 1, 0, 1}},
                    new QuestCount {Id = 436, NowArray = new[] {1, 0, 1, 1, 1}},
                    new QuestCount {Id = 437, NowArray = new[] {1, 0, 1, 1}},
                    new QuestCount {Id = 438, NowArray = new[] {1, 0, 1, 1}},
                    new QuestCount {Id = 439, NowArray = new[] {1, 0, 1, 1}},
                    new QuestCount {Id = 440, NowArray = new[] {1, 0, 1, 1, 1}}
                }
            };
            var countList = new QuestCountList();
            countList.SetMissionNames(new JsonObject(new[]
            {
                new {api_id = 1, api_name = "練習航海"},
                new {api_id = 2, api_name = "長距離練習航海"},
                new {api_id = 3, api_name = "警備任務"},
                new {api_id = 4, api_name = "対潜警戒任務"},
                new {api_id = 5, api_name = "海上護衛任務"},
                new {api_id = 9, api_name = "タンカー護衛任務"},
                new {api_id = 10, api_name = "強行偵察任務"},
                new {api_id = 11, api_name = "ボーキサイト輸送任務"},
                new {api_id = 40, api_name = "水上機前線輸送"},
                new {api_id = 41, api_name = "ブルネイ泊地沖哨戒"},
                new {api_id = 46, api_name = "南西海域戦闘哨戒"},
                new {api_id = 100, api_name = "兵站強化任務"},
                new {api_id = 101, api_name = "海峡警備行動"},
                new {api_id = 102, api_name = "長時間対潜警戒"},
                new {api_id = 104, api_name = "小笠原沖哨戒線"},
                new {api_id = 105, api_name = "小笠原沖戦闘哨戒"},
                new {api_id = 110, api_name = "南西方面航空偵察作戦"},
                new {api_id = 114, api_name = "南西諸島捜索撃滅戦"},
                new {api_id = 142, api_name = "強行鼠輸送作戦"}
            }));
            new QuestInfo(countList).LoadState(status);
            Assert.AreEqual("2/3", status.QuestCountList[0].ToString());
            Assert.AreEqual("20/36 7/6 10/24 8/12", status.QuestCountList[1].ToString());
            var z = status.QuestCountList[2];
            Assert.AreEqual("2\u200a1\u200a1\u200a1", z.ToString());
            Assert.AreEqual("2-4: 2  6-1: 1  6-3: 1  6-4: 1", z.ToToolTip());
            z.NowArray = new[] {0, 0, 0, 0};
            Assert.AreEqual("2-4: 0  6-1: 0  6-3: 0  6-4: 0", z.ToToolTip());
            var q426 = status.QuestCountList[3];
            Assert.AreEqual("1\u200a1\u200a1\u200a1", q426.ToString());
            Assert.AreEqual("警備任務: 1\n対潜警戒任務: 1\n海上護衛任務: 1\n強行偵察任務: 1", q426.ToToolTip());
            var q428 = status.QuestCountList[4];
            Assert.AreEqual("対潜警戒任務: 1\n海峡警備行動: 1\n長時間対潜警戒: 1", q428.ToToolTip());
            q428.NowArray = new[] {0, 1, 0};
            Assert.AreEqual("対潜警戒任務: 0\n海峡警備行動: 1\n長時間対潜警戒: 0", q428.ToToolTip());
            var q873 = status.QuestCountList[5];
            Assert.AreEqual("1\u200a1\u200a1", q873.ToString());
            Assert.AreEqual("3-1: 1  3-2: 1  3-3: 1", q873.ToToolTip());
            var q888 = status.QuestCountList[6];
            Assert.AreEqual("1\u200a1\u200a1", q888.ToString());
            Assert.AreEqual("5-1: 1  5-3: 1  5-4: 1", q888.ToToolTip());
            var q688 = status.QuestCountList[7];
            Assert.AreEqual("艦戦2 艦爆1 艦攻2 水偵1", q688.ToToolTip());
            var q893 = status.QuestCountList[8];
            Assert.AreEqual("1-5: 1  7-1: 1  7-2-1: 1  7-2-2: 1", q893.ToToolTip());
            var q894 = status.QuestCountList[9];
            Assert.AreEqual("1\u200a1\u200a1\u200a1\u200a1", q894.ToString());
            Assert.AreEqual("1-3: 1  1-4: 1  2-1: 1  2-2: 1  2-3: 1", q894.ToToolTip());
            var q280 = status.QuestCountList[10];
            Assert.AreEqual("1\u200a1\u200a1\u200a1", q280.ToString());
            Assert.AreEqual("1-2: 1  1-3: 1  1-4: 1  2-1: 1", q280.ToToolTip());
            var q872 = status.QuestCountList.First(q => q.Id == 872);
            Assert.AreEqual("1\u200a1\u200a1\u200a1", q872.ToString());
            Assert.AreEqual("7-2-2: 1  5-5: 1  6-2: 1  6-5: 1", q872.ToToolTip());
            var q284 = status.QuestCountList.First(q => q.Id == 284);
            Assert.AreEqual("1\u200a1\u200a1\u200a1", q284.ToString());
            Assert.AreEqual("1-4: 1  2-1: 1  2-2: 1  2-3: 1", q284.ToToolTip());
            var q226 = status.QuestCountList.First(q => q.Id == 226);
            Assert.AreEqual("2/5", q226.ToString());
            Assert.AreEqual("", q226.ToToolTip());
            var q434 = status.QuestCountList.First(q => q.Id == 434);
            Assert.AreEqual("1\u200a0\u200a1\u200a0\u200a1", q434.ToString());
            Assert.AreEqual("警備任務: 1\n海上護衛任務: 0\n兵站強化任務: 1\n海峡警備行動: 0\nタンカー護衛任務: 1", q434.ToToolTip());
            var q436 = status.QuestCountList.First(q => q.Id == 436);
            Assert.AreEqual("1\u200a0\u200a1\u200a1\u200a1", q436.ToString());
            Assert.AreEqual("練習航海: 1\n長距離練習航海: 0\n警備任務: 1\n対潜警戒任務: 1\n強行偵察任務: 1", q436.ToToolTip());
            var q437 = status.QuestCountList.First(q => q.Id == 437);
            Assert.AreEqual("1\u200a0\u200a1\u200a1", q437.ToString());
            Assert.AreEqual("対潜警戒任務: 1\n小笠原沖哨戒線: 0\n小笠原沖戦闘哨戒: 1\n南西方面航空偵察作戦: 1", q437.ToToolTip());
            var q438 = status.QuestCountList.First(q => q.Id == 438);
            Assert.AreEqual("1\u200a0\u200a1\u200a1", q438.ToString());
            Assert.AreEqual("兵站強化任務: 1\n対潜警戒任務: 0\nタンカー護衛任務: 1\n南西諸島捜索撃滅戦: 1", q438.ToToolTip());
            var q439 = status.QuestCountList.First(q => q.Id == 439);
            Assert.AreEqual("1\u200a0\u200a1\u200a1", q439.ToString());
            Assert.AreEqual("海上護衛任務: 1\n兵站強化任務: 0\nボーキサイト輸送任務: 1\n南西方面航空偵察作戦: 1", q439.ToToolTip());
            var q440 = status.QuestCountList.First(q => q.Id == 440);
            Assert.AreEqual("1\u200a0\u200a1\u200a1\u200a1", q440.ToString());
            Assert.AreEqual("ブルネイ泊地沖哨戒: 1\n海上護衛任務: 0\n水上機前線輸送: 1\n強行鼠輸送作戦: 1\n南西海域戦闘哨戒: 1", q440.ToToolTip());
        }
    }

    [TestClass]
    public class QuestCounterTest
    {
        private JsonObject Js(object obj) => new JsonObject(obj);

        private object CreateQuestList(int[] ids) => Js(new
        {
            api_list =
                ids.Select(id => new
                {
                    api_no = id,
                    api_category = id / 100,
                    api_type = 1,
                    api_state = 2,
                    api_title = "",
                    api_detail = "",
                    api_get_material = new int[0],
                    api_progress_flag = 0
                })
        });

        private QuestCount InjectQuest(int id)
        {
            InjectQuestList(new[] {id});
            return _questInfo.Quests[0].Count;
        }

        private void InjectQuestList(int[] ids)
        {
            _questInfo.InspectQuestList("api_tab_id=0", CreateQuestList(ids));
        }

        private void InjectMapStart(int map, int eventId)
        {
            _questCounter.InspectMapStart(CreateMap(map, eventId));
        }

        private void InjectMapNext(int map, int eventId)
        {
            _questCounter.InspectMapNext(CreateMap(map, eventId));
        }

        private object CreateMap(int map, int eventId)
        {
            return Js(new
            {
                api_maparea_id = map / 10,
                api_mapinfo_no = map % 10,
                api_event_id = eventId
            });
        }

        private void InjectBattleResult(string result)
        {
            _questCounter.InspectBattleResult(Js(new {api_win_rank = result}));
        }

        private void InjectPracticeResult(string result)
        {
            _questCounter.InspectPracticeResult(Js(new {api_win_rank = result}));
        }

        protected ShipStatus NewShip(int specId)
        {
            return new ShipStatus(specId)
            {
                MaxHp = 1,
                NowHp = 1,
                Spec = _sniffer.ShipMaster.GetSpec(specId),
                GetItem = itemId => _sniffer.ItemInventory[itemId]
            };
        }


        private Sniffer _sniffer;
        private BattleInfo _battleInfo;
        private QuestInfo _questInfo;
        private QuestCounter _questCounter;

        [TestInitialize]
        public void Initialize()
        {
            _sniffer = new Sniffer();
            SniffLogFile(_sniffer, "api_start2");
            _battleInfo = new BattleInfo(null, null, null);
            _battleInfo.InjectDeck(1);
            _questInfo = new QuestInfo(new QuestCountList(), () => new DateTime(2015, 1, 1));
            _questCounter = new QuestCounter(_questInfo, _sniffer.ItemInventory, _sniffer.ShipInventory, _battleInfo);
        }

        /// <summary>
        /// 201: 敵艦隊を撃滅せよ！
        /// 210: 敵艦隊を10回邀撃せよ！
        /// 214: あ号
        /// 216: 敵艦隊主力を撃滅せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_201_216_210_214()
        {
            InjectQuestList(new[] {201, 216, 210, 214});

            InjectMapStart(11, 4);
            var counts = _questInfo.Quests.Select(q => q.Count).ToArray();
            // 出撃カウント
            Assert.AreEqual(214, counts[2].Id);
            Assert.AreEqual(1, counts[2].NowArray[0]);
            InjectBattleResult("S");
            // 道中S勝利
            PAssert.That(() => counts.Select(c => new {c.Id, c.Now}).SequenceEqual(new[]
            {
                new {Id = 201, Now = 1}, new {Id = 210, Now = 1},
                new {Id = 214, Now = 0}, new {Id = 216, Now = 1}
            }));
            PAssert.That(() => counts[2].NowArray.SequenceEqual(new[] {1, 1, 0, 0}));

            InjectMapNext(11, 5);
            // ボスB勝利
            InjectBattleResult("B");
            PAssert.That(() => counts.Select(c => new {c.Id, c.Now}).SequenceEqual(new[]
            {
                new {Id = 201, Now = 2}, new {Id = 210, Now = 2},
                new {Id = 214, Now = 0}, new {Id = 216, Now = 2}
            }));
            // ボス敗北
            PAssert.That(() => counts[2].NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
            InjectBattleResult("C");
            PAssert.That(() => counts.Select(c => new {c.Id, c.Now}).SequenceEqual(new[]
            {
                new {Id = 201, Now = 2}, new {Id = 210, Now = 3},
                new {Id = 214, Now = 0}, new {Id = 216, Now = 2}
            }));
            PAssert.That(() => counts[2].NowArray.SequenceEqual(new[] {1, 1, 2, 1}));

            // 1-6 ゴール
            InjectMapNext(16, 8);
            Assert.AreEqual(2, counts[0].Now);
        }

        /// <summary>
        /// 211: 敵空母を3隻撃沈せよ！
        /// 212: 敵輸送船団を叩け！
        /// 213: 海上通商破壊作戦
        /// 218: 敵補給艦を3隻撃沈せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_211_212_213_218_220_221()
        {
            InjectQuestList(new[] {211, 212, 213, 218, 220, 221});
            // 補給艦1隻と空母2隻
            _battleInfo.InjectResultStatus(new ShipStatus[0], new ShipStatus[0], new[]
            {
                new ShipStatus {NowHp = 0, MaxHp = 130, Spec = _sniffer.ShipMaster.GetSpec(1558)},
                new ShipStatus {NowHp = 0, MaxHp = 90, Spec = _sniffer.ShipMaster.GetSpec(1543)},
                new ShipStatus {NowHp = 0, MaxHp = 90, Spec = _sniffer.ShipMaster.GetSpec(1543)},
                new ShipStatus {NowHp = 0, MaxHp = 96, Spec = _sniffer.ShipMaster.GetSpec(1528)},
                new ShipStatus {NowHp = 0, MaxHp = 70, Spec = _sniffer.ShipMaster.GetSpec(1523)},
                new ShipStatus {NowHp = 1, MaxHp = 70, Spec = _sniffer.ShipMaster.GetSpec(1523)}
            }, new ShipStatus[0]);
            InjectBattleResult("A");
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[]
                    {
                        new {Id = 211, Now = 2}, new {Id = 212, Now = 1}, new {Id = 213, Now = 1},
                        new {Id = 218, Now = 1}, new {Id = 220, Now = 2}, new {Id = 221, Now = 1}
                    }));
        }

        /// <summary>
        /// 228: 海上護衛戦
        /// 230: 敵潜水艦を制圧せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_228_230()
        {
            InjectQuestList(new[] {228, 230});
            // 潜水艦3
            _battleInfo.InjectResultStatus(new ShipStatus[0], new ShipStatus[0], new[]
            {
                new ShipStatus {NowHp = 0, MaxHp = 27, Spec = _sniffer.ShipMaster.GetSpec(1532)},
                new ShipStatus {NowHp = 0, MaxHp = 19, Spec = _sniffer.ShipMaster.GetSpec(1530)},
                new ShipStatus {NowHp = 0, MaxHp = 19, Spec = _sniffer.ShipMaster.GetSpec(1530)}
            }, new ShipStatus[0]);
            InjectBattleResult("S");
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[]
                    {
                        new {Id = 228, Now = 3}, new {Id = 230, Now = 3}
                    }));
        }

        /// <summary>
        /// 潜水航空戦での支援軽空は撃沈判定にならない
        /// 211: 敵空母を3隻撃沈せよ！
        /// 230: 敵潜水艦を制圧せよ！
        /// </summary>
        [TestMethod]
        public void BattleResultAswAirBattle()
        {
            InjectQuestList(new[] {211, 230});
            // 潜水4 支援軽空1
            _battleInfo.InjectResultStatus(new ShipStatus[0], new ShipStatus[0], new[]
            {
                new ShipStatus {NowHp = 0, MaxHp = 19, Spec = _sniffer.ShipMaster.GetSpec(1530)},
                new ShipStatus {NowHp = 0, MaxHp = 19, Spec = _sniffer.ShipMaster.GetSpec(1530)},
                new ShipStatus {NowHp = 0, MaxHp = 19, Spec = _sniffer.ShipMaster.GetSpec(1530)},
                new ShipStatus {NowHp = 0, MaxHp = 19, Spec = _sniffer.ShipMaster.GetSpec(1530)},
                new ShipStatus {NowHp = -1, MaxHp = -1, Spec = _sniffer.ShipMaster.GetSpec(1510)}
            }, new ShipStatus[0]);
            InjectBattleResult("S");
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[]
                    {
                        new {Id = 211, Now = 0}, new {Id = 230, Now = 4}
                    }));
            PAssert.That(() => _battleInfo.Result.Enemy.Main[4].SupportShip);
        }

        /// <summary>
        /// 226: 南西諸島海域の制海権を握れ！
        /// </summary>
        [TestMethod]
        public void BattleResult_226()
        {
            var count = InjectQuest(226);

            InjectMapStart(21, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(21, 5);
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);
            InjectBattleResult("B");
            Assert.AreEqual(2, count.Now);
        }

        /// <summary>
        /// // 243: 南方海域珊瑚諸島沖の制空権を握れ！
        /// </summary>
        [TestMethod]
        public void BattleResult_243()
        {
            var count = InjectQuest(243);

            InjectMapNext(52, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(52, 5);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);

            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);
        }

        /// <summary>
        /// 249: 「第五戦隊」出撃せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_249()
        {
            var count = InjectQuest(249);
            var myoukou2  = NewShip(319);
            var nachi2    = NewShip(192);
            var haguro2   = NewShip(194);
            var ashigara2 = NewShip(193);
            var chikuma2  = NewShip(189);
            var tone2     = NewShip(188);

            _battleInfo.InjectResultStatus(
                new[] {myoukou2, nachi2, haguro2, ashigara2, chikuma2, tone2},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(25, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(25, 5);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[3].NowHp = 0;
            InjectBattleResult("S");
            Assert.AreEqual(2, count.Now, "足柄改二轟沈");

            _battleInfo.Result.Friend.Main[1].NowHp = 0;
            InjectBattleResult("S");
            Assert.AreEqual(2, count.Now, "那智改二轟沈");
        }

        /// <summary>
        /// 257: 「水雷戦隊」南西へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_257()
        {
            var count = InjectQuest(257);
            var destroyer      = NewShip(1);
            var lightcruiser   = NewShip(51);
            var torpedocruiser = NewShip(58);

            _battleInfo.InjectResultStatus(
                new[] {lightcruiser, destroyer, destroyer, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(14, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(14, 5);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);

            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);
            _questInfo.Quests[0].Count.Now = 0;

            _battleInfo.Result.Friend.Main[0].NowHp = 0;
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "軽巡轟沈");
            _battleInfo.Result.Friend.Main[0].NowHp = 1;

            _battleInfo.Result.Friend.Main[0] = destroyer;
            _battleInfo.Result.Friend.Main[1] = lightcruiser;
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "旗艦が駆逐");
            _battleInfo.Result.Friend.Main[0] = lightcruiser;

            _battleInfo.Result.Friend.Main[2] = lightcruiser;
            _battleInfo.Result.Friend.Main[3] = lightcruiser;
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "軽巡が4隻");

            _battleInfo.Result.Friend.Main[0] = lightcruiser;
            _battleInfo.Result.Friend.Main[3] = torpedocruiser;
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "駆逐軽巡以外");

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, lightcruiser, lightcruiser};
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "駆逐が不在");

            // 0隻でクラッシュしない
            var checker = new QuestFleetChecker(new ShipStatus[0], 0);
            checker.Check(count.Id);
        }

        /// <summary>
        /// 257: 「水上打撃部隊」南方へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_259()
        {
            var count = InjectQuest(259);
            var ooyodo1    = NewShip(321);
            var mutsu1     = NewShip(276);
            var fusou2     = NewShip(411);
            var yamashiro2 = NewShip(412);
            var ashigara2  = NewShip(193);
            var haguro2    = NewShip(194);
            var yamato1    = NewShip(136);
            var kitakami1  = NewShip(58);
            var ise2       = NewShip(553);

            _battleInfo.InjectResultStatus(
                new[] {ooyodo1, mutsu1, fusou2, yamashiro2, ashigara2, haguro2},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(51, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(51, 5);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);

            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);
            _questInfo.Quests[0].Count.Now = 0;

            _battleInfo.Result.Friend.Main[0].NowHp = 0;
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "軽巡轟沈");
            _battleInfo.Result.Friend.Main[0].NowHp = 1;

            _battleInfo.Result.Friend.Main[4] = yamato1;
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "戦艦4隻");
            _battleInfo.Result.Friend.Main[4] = ashigara2;

            _battleInfo.Result.Friend.Main[0] = kitakami1;
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now, "軽巡なし");
            _battleInfo.Result.Friend.Main[0] = ooyodo1;

            _battleInfo.Result.Friend.Main[2] = ise2;
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now, "伊勢改二");
        }

        /// <summary>
        /// 264: 「空母機動部隊」西へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_264()
        {
            var count = InjectQuest(264);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);

            _battleInfo.InjectResultStatus(
                new[] {lightcarrier, aircraftcarrier, lightcruiser, lightcruiser, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(42, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(42, 5);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);

            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0].NowHp = 0;
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now, "轟沈あり");
        }

        /// <summary>
        /// 266: 「水上反撃部隊」突入せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_266()
        {
            var count = InjectQuest(266);
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);
            var heavycruiser = NewShip(59);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, heavycruiser, lightcruiser, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(25, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(25, 5);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);

            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[1].NowHp = 0;
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now, "轟沈あり");
            _battleInfo.Result.Friend.Main[1].NowHp = 1;

            _battleInfo.Result.Friend.Main[0] = lightcruiser;
            _battleInfo.Result.Friend.Main[2] = destroyer;
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now, "旗艦が軽巡");
            _battleInfo.Result.Friend.Main[0] = destroyer;
            _battleInfo.Result.Friend.Main[2] = lightcruiser;

            _battleInfo.Result.Friend.Main[3] = lightcruiser;
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now, "軽巡が2隻");
        }

        /// <summary>
        /// 280: 兵站線確保！海上警備を強化実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_280()
        {
            var count = InjectQuest(280);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var battlecruiser   = NewShip(78);
            var lightcarrier    = NewShip(89);

            _battleInfo.InjectResultStatus(
                new[] {lightcarrier, destroyer, escort, escort, battlecruiser, battlecruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(12, 4);
            InjectBattleResult("S");
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}));

            InjectBattleResult("S");
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            InjectMapNext(14, 5);
            InjectBattleResult("S");
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));

            _battleInfo.Result.Friend.Main = new[] {lightcarrier, escort, escort, battlecruiser, battlecruiser, battlecruiser};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));

            _battleInfo.Result.Friend.Main = new[] {battlecruiser, escort, escort, escort, battlecruiser, battlecruiser};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, destroyer, escort, escort, battlecruiser, battlecruiser};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 2}));

            _battleInfo.Result.Friend.Main = new[] {destroyer, torpedocruiser, destroyer, escort, battlecruiser, battlecruiser};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 3}));

            _battleInfo.Result.Friend.Main = new[] {destroyer, destroyer, trainingcruiser, destroyer, battlecruiser, battlecruiser};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 4}));
        }

        /// <summary>
        /// 284: 南西諸島方面「海上警備行動」発令！
        /// </summary>
        [TestMethod]
        public void BattleResult_284()
        {
            var count = InjectQuest(284);
            var escort        = NewShip(517);
            var destroyer     = NewShip(1);
            var battlecruiser = NewShip(78);
            var lightcarrier  = NewShip(89);

            _battleInfo.InjectResultStatus(
                new[] {lightcarrier, destroyer, escort, escort, battlecruiser, battlecruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(14, 4);
            InjectBattleResult("S");
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}));

            InjectBattleResult("S");
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            InjectMapNext(22, 5);
            InjectBattleResult("S");
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));

            // 艦種チェックは280と共通
        }

        /// <summary>
        /// 840: 【節分任務】節分作戦二〇二三
        /// </summary>
        [TestMethod]
        public void BattleResult_840()
        {
            var count = InjectQuest(840);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var lightcarrier    = NewShip(89);

            _battleInfo.InjectResultStatus(
                new[] {lightcarrier, destroyer, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "駆逐が不足");

            _battleInfo.Result.Friend.Main = new[] {destroyer, lightcruiser, destroyer, destroyer};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "旗艦指定");

            _battleInfo.Result.Friend.Main = new[] {lightcarrier, destroyer, escort, escort};
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "B勝は不可");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}));

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, destroyer, destroyer, destroyer};
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0}));

            _battleInfo.Result.Friend.Main = new[] {trainingcruiser, escort, escort, escort};
            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0}), "2-3は2022年まで");

            _battleInfo.Result.Friend.Main = new[] {trainingcruiser, escort, escort, escort};
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1}));

            _battleInfo.Result.Friend.Main = new[] {torpedocruiser, escort, escort, escort};
            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {2, 1, 1}));
        }

        /// <summary>
        /// 841: 【節分任務】南西海域節分作戦二〇二三
        /// </summary>
        [TestMethod]
        public void BattleResult_841()
        {
            var count = InjectQuest(841);
            var seaplanecarrier = NewShip(102);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            _battleInfo.InjectResultStatus(
                new[] {seaplanecarrier, aviationcruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(722, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "旗艦水母と随伴の艦種が一致しない");

            _battleInfo.Result.Friend.Main = new[] {seaplanecarrier, seaplanecarrier};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "7-2-2はA勝不可");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 1, 0}));

            InjectMapNext(732, 5);
            _battleInfo.Result.Friend.Main = new[] {aviationcruiser, heavycruiser};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 1, 0}), "旗艦航巡と随伴の艦種が一致しない");

            _battleInfo.Result.Friend.Main = new[] {aviationcruiser, aviationcruiser};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 1, 0}), "7-3-2はA勝不可");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 1, 1, 0}));

            InjectMapNext(74, 5);
            _battleInfo.Result.Friend.Main = new[] {seaplanecarrier, heavycruiser};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 1, 1, 0}), "旗艦水母と随伴の艦種が一致しない");

            _battleInfo.Result.Friend.Main = new[] {heavycruiser, heavycruiser};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 1, 1, 0}), "重巡は2022年まで");

            _battleInfo.Result.Friend.Main = new[] {aviationcruiser, aviationcruiser};
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 1, 1, 0}), "7-4はB勝不可");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));

            InjectMapNext(753, 5);
            _battleInfo.Result.Friend.Main = new[] {aviationcruiser, aviationcruiser};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "7-5-3はA勝不可");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 843: 【節分拡張任務】節分作戦二〇二三、全力出撃！
        /// </summary>
        [TestMethod]
        public void BattleResult_843()
        {
            var count = InjectQuest(843);
            var naka               = NewShip(56);
            var tama               = NewShip(100);
            var agano              = NewShip(137);
            var ooyodo             = NewShip(183);
            var battlecruiser      = NewShip(78);
            var battleship         = NewShip(26);
            var aviationbattleship = NewShip(286);
            var lightcarrier       = NewShip(89);
            var aircraftcarrier    = NewShip(83);
            var armoredcarrier     = NewShip(153);

            _battleInfo.InjectResultStatus(
                new[] {battlecruiser, lightcarrier, ooyodo},
                new ShipStatus[0],new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(25, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "戦艦空母が不足");
            _battleInfo.Result.Friend.Main = new[] {battlecruiser, battleship, ooyodo};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝不可");
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}));

            InjectMapNext(52, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "5-2は2022年まで");

            InjectMapNext(45, 5);
            _battleInfo.Result.Friend.Main = new[] {lightcarrier, aircraftcarrier, agano};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "指定の軽巡が不足");
            _battleInfo.Result.Friend.Main = new[] {lightcarrier, aircraftcarrier, naka};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "川内型は2022年まで");
            _battleInfo.Result.Friend.Main = new[] {lightcarrier, aircraftcarrier, tama};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "A勝不可");
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}));

            InjectMapNext(55, 5);
            _battleInfo.Result.Friend.Main = new[] {lightcarrier, battleship, tama};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "戦艦空母が不足");
            _battleInfo.Result.Friend.Main = new[] {aviationbattleship, battleship, tama};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "A勝不可");
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));

            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "6-4は2022年まで");

            InjectMapNext(65, 5);
            _battleInfo.Result.Friend.Main = new[] {aviationbattleship, aircraftcarrier, ooyodo};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "戦艦空母が不足");
            _battleInfo.Result.Friend.Main = new[] {armoredcarrier, aircraftcarrier, ooyodo};
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "A勝不可");
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 822: 沖ノ島海域迎撃戦
        /// 854: 戦果拡張任務！「Z作戦」前段作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_822_854()
        {
            InjectQuestList(new[] {822, 854});
            var c822 = _questInfo.Quests[0].Count;
            var c854 = _questInfo.Quests[1].Count;

            InjectMapNext(24, 4);
            InjectBattleResult("S");

            PAssert.That(() => c854.NowArray.SequenceEqual(new[] {0, 0, 0, 0}));
            Assert.AreEqual(0, c822.Now);

            InjectMapNext(24, 5);
            InjectBattleResult("A");
            InjectMapNext(61, 5);
            InjectBattleResult("A");
            InjectMapNext(63, 5);
            InjectBattleResult("A");
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => c854.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
            Assert.AreEqual(0, c822.Now);
            InjectMapNext(24, 5);
            InjectBattleResult("S");
            PAssert.That(() => c854.NowArray.SequenceEqual(new[] {2, 1, 1, 1}));
            Assert.AreEqual(1, c822.Now);

            _battleInfo.InjectDeck(2);
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => c854.NowArray.SequenceEqual(new[] {2, 1, 1, 1}));
        }

        /// <summary>
        /// 845: 発令！「西方海域作戦」
        /// </summary>
        [TestMethod]
        public void BattleResult_845()
        {
            var count = InjectQuest(845);

            InjectMapNext(41, 4);
            InjectBattleResult("S");
            InjectMapNext(41, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}));

            InjectMapNext(41, 5);
            InjectBattleResult("S");
            InjectMapNext(42, 5);
            InjectBattleResult("S");
            InjectMapNext(43, 5);
            InjectBattleResult("S");
            InjectMapNext(44, 5);
            InjectBattleResult("S");
            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}));
        }

        /// <summary>
        /// 861: 強行輸送艦隊、抜錨！
        /// </summary>
        [TestMethod]
        public void MapNext_861()
        {
            var count = InjectQuest(861);
            var destroyer          = NewShip(1);
            var aviationbattleship = NewShip(286);
            var fleetoiler         = NewShip(460);

            _battleInfo.InjectResultStatus(
                new[] {aviationbattleship, fleetoiler, destroyer, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(16, 4);
            Assert.AreEqual(0, count.Now);

            InjectMapNext(16, 8);
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[1].NowHp = 0;
            InjectMapNext(16, 8);
            Assert.AreEqual(1, count.Now, "轟沈あり");
            _battleInfo.Result.Friend.Main[1].NowHp = 1;

            _battleInfo.Result.Friend.Main[2] = aviationbattleship;
            InjectMapNext(16, 8);
            Assert.AreEqual(1, count.Now, "補給・航戦が3隻");
        }

        /// <summary>
        /// 862: 前線の航空偵察を実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_862()
        {
            var count = InjectQuest(862);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var seaplanecarrier = NewShip(102);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, lightcruiser, lightcruiser, destroyer, destroyer, seaplanecarrier},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(63, 4);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(63, 5);
            InjectBattleResult("B");
            Assert.AreEqual(0, count.Now);

            InjectBattleResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[1].NowHp = 0;
            InjectBattleResult("A");
            Assert.AreEqual(1, count.Now, "轟沈あり");
            _battleInfo.Result.Friend.Main[1].NowHp = 1;

            _battleInfo.Result.Friend.Main[3] = lightcruiser;
            _battleInfo.Result.Friend.Main[4] = seaplanecarrier;
            InjectBattleResult("A");
            Assert.AreEqual(2, count.Now, "軽巡3隻水母2隻");
        }

        /// <summary>
        /// 872: 戦果拡張任務！「Z作戦」後段作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_872()
        {
            var count = InjectQuest(872);

            InjectMapNext(55, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}));
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}));
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 1, 0, 0}));

            InjectMapNext(62, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 1, 1, 0}));
            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 1, 1, 1}));
            _questCounter.InspectMapNext(Js(new
            {
                api_maparea_id = 7,
                api_mapinfo_no = 2,
                api_no = 15,
                api_event_id = 5
            }));
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "7-2M");

            _battleInfo.InjectDeck(3);
            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 873: 北方海域警備を実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_873()
        {
            var count = InjectQuest(873);
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);

            _battleInfo.InjectResultStatus(
                new[] {lightcruiser, destroyer, destroyer, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(31, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}));

            InjectMapNext(31, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}));
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}));

            _battleInfo.Result.Friend.Main[0] = destroyer;
            InjectBattleResult("A");
            Assert.AreEqual(1, _questInfo.Quests[0].Count.NowArray[0], "軽巡なし");
            _battleInfo.Result.Friend.Main[0] = lightcruiser;

            InjectMapNext(32, 5);
            InjectBattleResult("A");
            InjectMapNext(33, 5);
            InjectBattleResult("A");
            Assert.IsTrue(_questInfo.Quests[0].Count.NowArray.SequenceEqual(new[] {1, 1, 1}));
        }

        /// <summary>
        /// 875: 精鋭「三一駆」、鉄底海域に突入せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_875()
        {
            var count = InjectQuest(875);
            var naganami2    = NewShip(543);
            var asashimo1    = NewShip(344);
            var asashimo2    = NewShip(578);
            var takanami1    = NewShip(345);
            var okinami1     = NewShip(359);
            var Iowa1        = NewShip(360);
            var SaratogaMkII = NewShip(545);
            var shoukaku2k   = NewShip(466);
            var mochizuki1   = NewShip(261);

            _battleInfo.InjectResultStatus(
                new[] {naganami2, Iowa1, SaratogaMkII, shoukaku2k, mochizuki1, asashimo1},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(54, 4);
            InjectBattleResult("S");
            Assert.AreEqual(0, count.Now);

            InjectMapNext(54, 5);
            InjectBattleResult("A");
            Assert.AreEqual(0, count.Now);

            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[5].NowHp = 0;
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now, "朝霜改轟沈");
            _battleInfo.Result.Friend.Main[5].NowHp = 1;

            _battleInfo.Result.Friend.Main[0] = takanami1;
            InjectBattleResult("S");
            Assert.AreEqual(1, count.Now, "長波改二なし");
            _battleInfo.Result.Friend.Main[0] = naganami2;

            _battleInfo.Result.Friend.Main[5] = takanami1;
            InjectBattleResult("S");
            Assert.AreEqual(2, count.Now, "高波改");

            _battleInfo.Result.Friend.Main[5] = okinami1;
            InjectBattleResult("S");
            Assert.AreEqual(3, count.Now, "沖波改");

            _battleInfo.Result.Friend.Main[5] = asashimo2;
            InjectBattleResult("S");
            Assert.AreEqual(4, count.Now, "朝霜改二");
        }

        /// <summary>
        /// 888: 新編成「三川艦隊」、鉄底海峡に突入せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_888()
        {
            var count = InjectQuest(888);
            var choukai2  = NewShip(427);
            var furutaka2 = NewShip(416);
            var kako2     = NewShip(417);
            var aoba1     = NewShip(264);
            var kinugasa2 = NewShip(142);
            var tenryuu2  = NewShip(477);
            var yuubari2t = NewShip(623);
            var myoukou2  = NewShip(319);
            var ayanami2  = NewShip(195);
            var yuudachi2 = NewShip(144);

            _battleInfo.InjectResultStatus(
                new[] {choukai2, aoba1, kinugasa2, kako2, yuudachi2, ayanami2},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(51, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}));

            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}));

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}));

            _battleInfo.Result.Friend.Main[0].NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}), "轟沈あり");
            _battleInfo.Result.Friend.Main[0].NowHp = 1;

            _battleInfo.Result.Friend.Main[0] = myoukou2;
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}), "三川艦隊3隻");

            _battleInfo.Result.Friend.Main[0] = yuubari2t;
            InjectMapNext(53, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0}));

            _battleInfo.Result.Friend.Main[0] = tenryuu2;
            _battleInfo.Result.Friend.Main[1] = furutaka2;
            InjectMapNext(54, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1}));
        }

        /// <summary>
        /// 893: 泊地周辺海域の安全確保を徹底せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_893()
        {
            var count = InjectQuest(893);

            InjectMapNext(15, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}));

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "1-5");

            InjectMapNext(71, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "7-1");

            _questCounter.InspectMapNext(Js(new
            {
                api_maparea_id = 7,
                api_mapinfo_no = 2,
                api_no = 7,
                api_event_id = 5
            }));
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "7-2G");

            _questCounter.InspectMapNext(Js(new
            {
                api_maparea_id = 7,
                api_mapinfo_no = 2,
                api_no = 15,
                api_event_id = 5
            }));
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "7-2M");
        }

        /// <summary>
        /// 894: 空母戦力の投入による兵站線戦闘哨戒
        /// </summary>
        [TestMethod]
        public void BattleResult_894()
        {
            var count = InjectQuest(894);
            var destroyer    = NewShip(1);
            var lightcarrier = NewShip(89);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, destroyer, destroyer, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "空母なしはカウントしない");

            _battleInfo.Result.Friend.Main[0] = lightcarrier;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}), "1-3");

            InjectMapNext(14, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}), "1-4");

            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0, 0}), "1-4");

            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}), "2-1");

            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 0}), "2-2");

            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}), "2-3");
        }

        /// <summary>
        /// 拡張「六水戦」、最前線へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_903()
        {
            var count = InjectQuest(903);
            var yuubari1  = NewShip(293);
            var yuubari2  = NewShip(622);
            var yura1     = NewShip(220);
            var yura2     = NewShip(488);
            var mutsuki   = NewShip(1);
            var kisaragi  = NewShip(2);
            var yayoi     = NewShip(164);
            var uzuki     = NewShip(165);
            var kikuzuki  = NewShip(30);
            var mochizuki = NewShip(31);
            var ayanami   = NewShip(13);

            _battleInfo.InjectResultStatus(
                new[] {yuubari2, mutsuki, ayanami},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(51, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "六水戦駆逐が1隻");

            _battleInfo.Result.Friend.Main[2] = kisaragi;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "5-1");

            InjectMapNext(54, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "ボス以外はカウントしない");

            _battleInfo.Result.Friend.Main[0] = yuubari1;
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "旗艦が夕張改");

            _battleInfo.Result.Friend.Main = new[] {mutsuki, kisaragi, yuubari2};
            InjectMapNext(54, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "旗艦が夕張改二ではない");

            _battleInfo.Result.Friend.Main = new[] {yuubari2, yayoi, uzuki};
            InjectMapNext(54, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "5-4");

            _battleInfo.Result.Friend.Main = new[] {yuubari2, kikuzuki, mochizuki};
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "6-4");

            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "6-5");

            _battleInfo.Result.Friend.Main = new[] {yuubari2, yura1};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "由良改");

            _battleInfo.Result.Friend.Main = new[] {yuubari2, yura2};
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 2}), "由良改二");
        }

        /// <summary>
        /// 904: 精鋭「十九駆」、躍り出る！
        /// </summary>
        [TestMethod]
        public void BattleResult_904()
        {
            var count = InjectQuest(904);
            var ayanami2   = NewShip(195);
            var shikinami  = NewShip(208);
            var shikinami2 = NewShip(627);

            _battleInfo.InjectResultStatus(
                new[] {ayanami2, shikinami},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(25, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "敷波はカウントしない");

            _battleInfo.Result.Friend.Main[1] = shikinami2;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "2-5");

            InjectMapNext(34, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(34, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "3-4");

            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "4-5");

            InjectMapNext(53, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "5-3");
        }

        /// <summary>
        /// 905: 「海防艦」、海を護る！
        /// </summary>
        [TestMethod]
        public void BattleResult_905()
        {
            var count = InjectQuest(905);
            var escort    = NewShip(517);
            var destroyer = NewShip(1);

            _battleInfo.InjectResultStatus(
                new[] {escort, escort, escort, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "6隻はカウントしない");

            _battleInfo.Result.Friend.Main = new[] {escort, escort, escort, destroyer, destroyer};
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "B勝利はカウントしない");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}), "1-1");

            InjectMapNext(12, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0, 0}), "1-2");

            _battleInfo.Result.Friend.Main[0] = destroyer;
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0, 0}), "海防艦2隻はカウントしない");

            _battleInfo.Result.Friend.Main[0] = escort;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}), "1-3");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 0}), "1-5");

            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}), "1-6");
        }

        /// <summary>
        /// 912: 工作艦「明石」護衛任務
        /// </summary>
        [TestMethod]
        public void BattleResult_912()
        {
            var count = InjectQuest(912);
            var akashi    = NewShip(182);
            var escort    = NewShip(517);
            var destroyer = NewShip(1);

            _battleInfo.InjectResultStatus(
                new[] {akashi, destroyer, destroyer, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "駆逐艦2隻はカウントしない");

            _battleInfo.Result.Friend.Main[3] = destroyer;
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "B勝利はカウントしない");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}), "1-3");

            InjectMapNext(21, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}), "ボス以外はカウントしない");

            _battleInfo.Result.Friend.Main[0] = destroyer;
            _battleInfo.Result.Friend.Main[1] = akashi;
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}), "旗艦明石以外はカウントしない");

            _battleInfo.Result.Friend.Main[0] = akashi;
            _battleInfo.Result.Friend.Main[1] = destroyer;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0, 0}), "2-1");

            InjectMapNext(22, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}), "2-2");

            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 0}), "2-3");

            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}), "1-6");
        }

        /// <summary>
        /// 912: 重巡戦隊、西へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_914()
        {
            var count = InjectQuest(914);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            _battleInfo.InjectResultStatus(
                new[] {heavycruiser, heavycruiser, heavycruiser, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(41, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "駆逐艦なしはカウントしない");

            _battleInfo.Result.Friend.Main[3] = destroyer;
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "B勝利はカウントしない");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "4-1");

            InjectMapNext(42, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(42, 5);
            _battleInfo.Result.Friend.Main[0] = aviationcruiser;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "重巡2隻はカウントしない");

            _battleInfo.Result.Friend.Main[0] = heavycruiser;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "4-2");

            InjectMapNext(43, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "4-3");

            InjectMapNext(44, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "4-3");
        }

        /// <summary>
        /// 928: 歴戦「第十方面艦隊」、全力出撃！
        /// </summary>
        [TestMethod]
        public void BattleResult_928()
        {
            var count = InjectQuest(928);
            var haguro2  = NewShip(194);
            var nachi    = NewShip(63);
            var myoukou  = NewShip(62);
            var takao    = NewShip(66);
            var kamikaze = NewShip(471);
            var escort   = NewShip(517);

            _battleInfo.InjectResultStatus(
                new[] {haguro2, nachi, escort, escort, escort, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(732, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}));

            _battleInfo.Result.Friend.Main[1] = myoukou;
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}));
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}));

            _battleInfo.Result.Friend.Main[0] = myoukou;
            _battleInfo.Result.Friend.Main[1] = takao;
            InjectMapNext(722, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0}));

            _battleInfo.Result.Friend.Main[0] = kamikaze;
            _battleInfo.Result.Friend.Main[1] = takao;
            InjectMapNext(42, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1}));
        }

        /// <summary>
        /// 944: 鎮守府近海海域の哨戒を実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_944()
        {
            var count = InjectQuest(944);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            _battleInfo.InjectResultStatus(
                new[] {aviationcruiser, escort, escort, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "旗艦航巡はカウントしない");

            _battleInfo.Result.Friend.Main[0] = escort;
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "旗艦海防はカウントしない");

            _battleInfo.Result.Friend.Main[0] = destroyer;
            _battleInfo.Result.Friend.Main[1] = heavycruiser;
            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "随伴重巡&駆逐海防不足はカウントしない");

            _battleInfo.Result.Friend.Main[1] = destroyer;
            InjectMapNext(12, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(13, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "B勝はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}), "1-2");

            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0}), "1-3");

            _battleInfo.Result.Friend.Main[0] = heavycruiser;
            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1}), "1-4");
        }

        /// <summary>
        /// 945: 南西方面の兵站航路の安全を図れ！
        /// </summary>
        [TestMethod]
        public void BattleResult_945()
        {
            var count = InjectQuest(945);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);

            _battleInfo.InjectResultStatus(
                new[] {torpedocruiser, escort, escort, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "旗艦雷巡はカウントしない");

            _battleInfo.Result.Friend.Main[0] = escort;
            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "旗艦海防はカウントしない");

            _battleInfo.Result.Friend.Main[0] = destroyer;
            _battleInfo.Result.Friend.Main[1] = lightcruiser;
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "随伴軽巡&駆逐海防不足はカウントしない");

            _battleInfo.Result.Friend.Main[1] = destroyer;
            InjectMapNext(15, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(21, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "B勝はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}), "1-5");

            _battleInfo.Result.Friend.Main[0] = lightcruiser;
            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 1}), "1-6");

            _battleInfo.Result.Friend.Main[0] = trainingcruiser;
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1}), "2-1");
        }

        /// <summary>
        /// 946: 空母機動部隊、出撃！敵艦隊を迎撃せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_946()
        {
            var count = InjectQuest(946);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);
            var armoredcarrier  = NewShip(153);

            _battleInfo.InjectResultStatus(
                new[] {heavycruiser, lightcarrier, aviationcruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "旗艦重巡はカウントしない");

            _battleInfo.Result.Friend.Main[0] = aviationcruiser;
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "旗艦航巡はカウントしない");

            _battleInfo.Result.Friend.Main[0] = lightcarrier;
            InjectMapNext(24, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "重巡航巡不足はカウントしない");

            _battleInfo.Result.Friend.Main[1] = heavycruiser;
            InjectMapNext(22, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0}), "A勝はカウントしない");

            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0}), "2-2");

            _battleInfo.Result.Friend.Main[0] = aircraftcarrier;
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0}), "2-3");

            _battleInfo.Result.Friend.Main[0] = armoredcarrier;
            InjectMapNext(24, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1}), "2-4");
        }

        /// <summary>
        /// 947: AL作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_947()
        {
            var count = InjectQuest(947);
            var destroyer    = NewShip(1);
            var lightcarrier = NewShip(89);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, lightcarrier, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(31, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "軽空不足はカウントしない");

            _battleInfo.Result.Friend.Main[2] = lightcarrier;
            InjectMapNext(33, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(34, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝はカウントしない");

            InjectMapNext(31, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "3-1");

            InjectMapNext(33, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "3-3");

            InjectMapNext(34, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "3-4");

            InjectMapNext(35, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "3-5");
        }

        /// <summary>
        /// 948: 機動部隊決戦
        /// </summary>
        [TestMethod]
        public void BattleResult_948()
        {
            var count = InjectQuest(948);
            var destroyer       = NewShip(1);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);
            var armoredcarrier  = NewShip(153);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, lightcarrier},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(52, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "旗艦空母以外はカウントしない");

            _battleInfo.Result.Friend.Main[0] = lightcarrier;
            _battleInfo.Result.Friend.Main[1] = destroyer;
            InjectMapNext(55, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "ボス以外はカウントしない");

            _battleInfo.InjectDeck(4);
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "第一艦隊以外はカウントしない");
            _battleInfo.InjectDeck(1);

            InjectMapNext(52, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "5-2 A勝はカウントしない");

            InjectMapNext(55, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "5-5 A勝はカウントしない");

            InjectMapNext(64, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "6-4 B勝はカウントしない");

            InjectMapNext(65, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "6-5 A勝はカウントしない");

            InjectMapNext(52, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "5-2");

            _battleInfo.Result.Friend.Main[0] = aircraftcarrier;
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "5-5");

            _battleInfo.Result.Friend.Main[0] = armoredcarrier;
            InjectMapNext(64, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "6-4 A勝");

            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "6-5");
        }

        /// <summary>
        /// 953: 【梅雨限定任務】梅雨の海上輸送航路を護れ！
        /// </summary>
        [TestMethod]
        public void BattleResult_953()
        {
            var count = InjectQuest(953);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var seaplanecarrier = NewShip(102);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);

            _battleInfo.InjectResultStatus(
                new[] {lightcruiser, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝は不可");

            InjectMapNext(12, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}));

            _battleInfo.Result.Friend.Main = new[] {seaplanecarrier, destroyer, destroyer, destroyer};
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}));

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, destroyer, destroyer, destroyer, escort};
            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));

            _battleInfo.Result.Friend.Main = new[] {seaplanecarrier, destroyer, destroyer, destroyer, torpedocruiser};
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));


            _battleInfo.Result.Friend.Main = new[] {lightcruiser, destroyer, destroyer, escort};
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "駆逐が不足、海防艦は代替不可");

            _battleInfo.Result.Friend.Main = new[] {torpedocruiser, destroyer, destroyer, destroyer};
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "雷巡旗艦は不可");

            _battleInfo.Result.Friend.Main = new[] {destroyer, lightcruiser, destroyer, destroyer};
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "駆逐旗艦は不可");
        }

        /// <summary>
        /// 954: 【梅雨拡張任務】雨の重巡作戦！
        /// </summary>
        [TestMethod]
        public void BattleResult_954()
        {
            var count = InjectQuest(954);
            var lightcruiser    = NewShip(51);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            _battleInfo.InjectResultStatus(
                new[] {heavycruiser, heavycruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(74, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0}), "A勝は不可");

            InjectMapNext(74, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0}), "ボス以外はカウントしない");

            InjectMapNext(74, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0}));

            _battleInfo.Result.Friend.Main = new[] {aviationcruiser, aviationcruiser};
            InjectMapNext(732, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1}));

            _battleInfo.Result.Friend.Main = new[] {aviationcruiser, heavycruiser};
            InjectMapNext(74, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {2, 1}));

            _battleInfo.Result.Friend.Main = new[] {heavycruiser, aviationcruiser};
            InjectMapNext(732, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {2, 2}));


            _battleInfo.Result.Friend.Main = new[] {aviationcruiser, lightcruiser};
            InjectMapNext(74, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {2, 2}), "艦種不足");

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, heavycruiser, aviationcruiser};
            InjectMapNext(74, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {2, 2}), "旗艦指定間違い");
        }

        /// <summary>
        /// 955: 【梅雨限定月間任務】南方ソロモン方面漸減作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_955()
        {
            var count = InjectQuest(955);
            var lightcruiser       = NewShip(51);
            var agano              = NewShip(137);
            var ooyodo             = NewShip(183);
            var battlecruiser      = NewShip(78);
            var battleship         = NewShip(26);
            var aviationbattleship = NewShip(286);

            _battleInfo.InjectResultStatus(
                new[] {battleship, battlecruiser, agano},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "A勝は不可");

            InjectMapNext(51, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(51, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0, 0}));

            _battleInfo.Result.Friend.Main = new[] {battleship, battlecruiser, ooyodo};
            InjectMapNext(52, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0, 0}));

            _battleInfo.Result.Friend.Main = new[] {ooyodo, aviationbattleship, battlecruiser};
            InjectMapNext(53, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}));

            _battleInfo.Result.Friend.Main = new[] {aviationbattleship, battleship, ooyodo};
            InjectMapNext(54, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 0}));

            _battleInfo.Result.Friend.Main = new[] {aviationbattleship, battleship, agano};
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}));


            _battleInfo.Result.Friend.Main = new[] {aviationbattleship, ooyodo, agano};
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}), "戦艦が不足");

            _battleInfo.Result.Friend.Main = new[] {aviationbattleship, battleship, lightcruiser};
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}), "阿賀野型大淀型以外の軽巡は不可");
        }

        /// <summary>
        /// 973: 日英米合同水上艦隊、抜錨せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_973()
        {
            var count = InjectQuest(973);
            var perth      = NewShip(618);
            var gambierBay = NewShip(707);
            var fletcher   = NewShip(629);
            var scamp      = NewShip(715);
            var warspite   = NewShip(364);

            _battleInfo.InjectResultStatus(
                new[] {perth, warspite, fletcher},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "米英艦不足はカウントしない");

            _battleInfo.Result.Friend.Main = new[] {gambierBay, warspite, fletcher};
            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "空母系を含む場合はカウントしない");

            _battleInfo.Result.Friend.Main = new[] {scamp, warspite, fletcher};
            InjectMapNext(31, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(31, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "B勝はカウントしない");

            InjectMapNext(722, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "誤出撃はカウントしない");

            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "3-1");

            InjectMapNext(33, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "3-3");

            InjectMapNext(43, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "4-3");

            InjectMapNext(732, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "7-3-2");
        }

        /// <summary>
        /// 975: 精鋭「第十九駆逐隊」、全力出撃！
        /// </summary>
        [TestMethod]
        public void BattleResult_975()
        {
            var count = InjectQuest(975);
            var isonami    = NewShip(206);
            var isonami2   = NewShip(666);
            var uranami    = NewShip(368);
            var uranami2   = NewShip(647);
            var ayanami    = NewShip(207);
            var ayanami2   = NewShip(195);
            var shikinami  = NewShip(208);
            var shikinami2 = NewShip(627);
            var fubuki2    = NewShip(201);

            _battleInfo.InjectResultStatus(
                new[] {isonami2, uranami2, ayanami2},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "指定艦が不足");

            _battleInfo.Result.Friend.Main = new[] {isonami, uranami2, ayanami2, shikinami2};
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "指定艦が改二になってない");

            _battleInfo.Result.Friend.Main = new[] {isonami2, fubuki2, ayanami2, shikinami2};
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "改二の指定艦が間違ってる");

            _battleInfo.Result.Friend.Main = new[] {isonami2, uranami2, ayanami2, shikinami2};
            InjectMapNext(15, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝はカウントしない");

            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "誤出撃はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "1-5");

            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "3-3");

            InjectMapNext(32, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "4-3");

            _battleInfo.Result.Friend.Main = new[] {fubuki2, uranami2, ayanami2, isonami2, shikinami2};
            InjectMapNext(53, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "7-3-2");
        }

        /// <summary>
        /// 988: 【Xmas限定任務】聖夜の哨戒線
        /// </summary>
        [TestMethod]
        public void BattleResult_988()
        {
            var count = InjectQuest(988);
            var tenryu = NewShip(51);
            var ooikai = NewShip(57);
            var katori = NewShip(154);
            var kako = NewShip(60);

            _battleInfo.InjectResultStatus(
                new[] {kako},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "旗艦は軽巡級");

            _battleInfo.Result.Friend.Main = new[] {tenryu};
            InjectMapNext(12, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "A勝はカウントしない");

            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {0, 0, 0, 0}), "誤出撃はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 0, 0, 0}), "1-2");

            _battleInfo.Result.Friend.Main = new[] {ooikai};
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 0, 0}), "1-3");

            _battleInfo.Result.Friend.Main = new[] {katori};
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}), "1-5");

            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}), "2-1");
        }

        /// <summary>
        /// 280と854以降を同時に遂行していると854以降がカウントされないことがある
        /// </summary>
        [TestMethod]
        public void BattleResult_280_854()
        {
            InjectQuestList(new[] {280, 854});
            var escort = NewShip(517);

            _battleInfo.InjectResultStatus(
                new[] {escort, escort, escort, escort, escort, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(24, 5);
            InjectBattleResult("S");
            Assert.AreEqual(1, _questInfo.Quests[1].Count.NowArray[0]);
        }

        /// <summary>
        /// 888と893以降を同時に遂行していると893以降がカウントされないことがある
        /// </summary>
        [TestMethod]
        public void BattleResult_888_893()
        {
            InjectQuestList(new[] {888, 893});
            var escort = NewShip(517);

            _battleInfo.InjectResultStatus(
                new[] {escort, escort, escort, escort, escort, escort},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);

            InjectMapNext(71, 5);
            InjectBattleResult("S");
            Assert.AreEqual(1, _questInfo.Quests[1].Count.NowArray[1]);
        }

        /// <summary>
        /// 302: 大規模演習
        /// 303: 「演習」で練度向上！
        /// 304: 「演習」で他提督を圧倒せよ！
        /// 311: 精鋭艦隊演習
        /// 313: 秋季大演習
        /// 314: 冬季大演習
        /// 315: 春季大演習
        /// 326: 夏季大演習
        /// </summary>
        [TestMethod]
        public void PracticeResult_303_304_302_311_313_314_315_326()
        {
            InjectQuestList(new[] {302, 303, 304, 311, 313, 314, 315, 326});
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, lightcruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("C");
            InjectPracticeResult("A");
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[]
                    {
                        new {Id = 302, Now = 1}, new {Id = 303, Now = 2}, new {Id = 304, Now = 1},
                        new {Id = 311, Now = 1}, new {Id = 313, Now = 1}, new {Id = 314, Now = 1},
                        new {Id = 315, Now = 1}, new {Id = 326, Now = 1}
                    }));
        }

        /// <summary>
        /// 318: 給糧艦「伊良湖」の支援
        /// </summary>
        [TestMethod]
        public void PracticeResult_318()
        {
            var count = InjectQuest(318);
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, lightcruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "軽巡1隻");

            _battleInfo.Result.Friend.Main[0] = lightcruiser;
            _questCounter.StartPractice("api%5Fdeck%5Fid=2");
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "第2艦隊");

            _questCounter.StartPractice("api%5Fdeck%5Fid=1"); // 第一艦隊
            InjectPracticeResult("C");
            Assert.AreEqual(0, count.Now, "敗北");

            InjectPracticeResult("B");
            Assert.AreEqual(1, count.Now);

            count.Now = 2;
            InjectQuestList(new[] {318});
            Assert.AreEqual(2, count.Now, "進捗調節しない");
        }

        /// <summary>
        /// 329: 【節分任務】節分演習！二〇二三
        /// </summary>
        [TestMethod]
        public void PracticeResult_329()
        {
            var count = InjectQuest(329);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);

            _battleInfo.InjectResultStatus(
                new[] {lightcruiser, lightcruiser, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "駆逐が不足");

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, lightcruiser, destroyer, destroyer, destroyer};
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "A勝利でカウントしない");

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, destroyer, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "軽巡が不足");

            _battleInfo.Result.Friend.Main = new[] {torpedocruiser, lightcruiser, destroyer, destroyer, escort};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "雷巡は対象外");

            _battleInfo.Result.Friend.Main = new[] {trainingcruiser, lightcruiser, destroyer, destroyer, escort};
            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now, "練巡、軽巡、駆逐、海防");

            _battleInfo.Result.Friend.Main = new[] {lightcruiser, lightcruiser, destroyer, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now, "軽巡2 駆逐3");
        }

        /// <summary>
        /// 330: 空母機動部隊、演習始め！
        /// </summary>
        [TestMethod]
        public void PracticeResult_330()
        {
            var count = InjectQuest(330);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);
            var armoredcarrier  = NewShip(153);

            _battleInfo.InjectResultStatus(
                new[] {armoredcarrier, lightcarrier, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("B");
            Assert.AreEqual(1, count.Now, "装甲空母、軽空母");

            _battleInfo.Result.Friend.Main = new[] {aircraftcarrier, lightcarrier, destroyer, destroyer};
            InjectPracticeResult("B");
            Assert.AreEqual(2, count.Now, "正規空母、軽空母");

            count.Now = 0;
            InjectPracticeResult("C");
            Assert.AreEqual(0, count.Now, "敗北");

            _battleInfo.Result.Friend.Main = new[] {destroyer, lightcarrier, aircraftcarrier, destroyer};
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "旗艦空母以外");

            _battleInfo.Result.Friend.Main = new[] {aircraftcarrier, destroyer, destroyer, destroyer};
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "空母一隻");

            _battleInfo.Result.Friend.Main = new[] {aircraftcarrier, lightcarrier, lightcruiser, destroyer};
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "駆逐一隻");
        }

        /// <summary>
        /// 337: 「十八駆」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_337()
        {
            var count = InjectQuest(337);
            var arare    = NewShip(48);
            var kagerou  = NewShip(17);
            var shiranui = NewShip(18);
            var kuroshio = NewShip(19);
            var suzukaze = NewShip(47);
            var kasumi   = NewShip(49);
            var kasumi2  = NewShip(464);

            _battleInfo.InjectResultStatus(
                new[] {kasumi, arare, kagerou, shiranui, kuroshio},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "A");

            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = suzukaze;
            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now, "霰→涼風");

            _battleInfo.Result.Friend.Main[4] = kasumi2;
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now, "黒潮→霞改二");
        }

        /// <summary>
        /// 339: 「十九駆」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_339()
        {
            var count = InjectQuest(339);
            var isonami   = NewShip(12);
            var uranami   = NewShip(486);
            var ayanami   = NewShip(13);
            var shikinami = NewShip(14);
            var hatsuyuki = NewShip(32);
            var miyuki    = NewShip(11);

            _battleInfo.InjectResultStatus(
                new[] {isonami, uranami, ayanami, shikinami, hatsuyuki},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "A");

            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = miyuki;
            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now, "磯波→深雪");
        }

        /// <summary>
        /// 342: 小艦艇群演習強化任務
        /// </summary>
        [TestMethod]
        public void PracticeResult_342()
        {
            var count = InjectQuest(342);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var heavycruiser    = NewShip(59);
            var lightcarrier    = NewShip(89);

            _battleInfo.InjectResultStatus(
                new[] {escort, escort, destroyer, heavycruiser},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now);

            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[2] = lightcruiser;
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "駆逐海防が不足");

            _battleInfo.Result.Friend.Main[2] = destroyer;
            _battleInfo.Result.Friend.Main[3] = destroyer;
            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[3] = lightcruiser;
            InjectPracticeResult("A");
            Assert.AreEqual(2, count.Now);

            _battleInfo.Result.Friend.Main[3] = torpedocruiser;
            InjectPracticeResult("A");
            Assert.AreEqual(3, count.Now);

            _battleInfo.Result.Friend.Main[3] = trainingcruiser;
            InjectPracticeResult("A");
            Assert.AreEqual(4, count.Now);

            _battleInfo.Result.Friend.Main[3] = lightcarrier;
            InjectPracticeResult("A");
            Assert.AreEqual(4, count.Now, "軽空は間違い");
        }

        /// <summary>
        /// 345: 演習ティータイム！
        /// </summary>
        [TestMethod]
        public void PracticeResult_345()
        {
            var count = InjectQuest(345);
            var Warspite  = NewShip(439);
            var kongou    = NewShip(78);
            var ArkRoyal  = NewShip(515);
            var Perth     = NewShip(613);
            var Jervis    = NewShip(519);
            var Janus     = NewShip(520);
            var Richelieu = NewShip(492);

            _battleInfo.InjectResultStatus(
                new[] {Warspite, kongou, ArkRoyal, Richelieu, Perth},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now);

            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[3] = Jervis;
            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = Janus;
            InjectPracticeResult("A");
            Assert.AreEqual(2, count.Now);

            _battleInfo.Result.Friend.Main[1] = Richelieu;
            InjectPracticeResult("A");
            Assert.AreEqual(2, count.Now);
        }

        /// <summary>
        /// 346: 最精鋭！主力オブ主力、演習開始！
        /// </summary>
        [TestMethod]
        public void PracticeResult_346()
        {
            var count = InjectQuest(346);
            var yuugumo2  = NewShip(542);
            var makigumo2 = NewShip(563);
            var kazagumo2 = NewShip(564);
            var akigumo1  = NewShip(301);
            var akigumo2  = NewShip(648);
            var ayanami2  = NewShip(195);

            _battleInfo.InjectResultStatus(
                new[] {yuugumo2, makigumo2, kazagumo2, akigumo1},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now);

            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[3] = akigumo2;
            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = ayanami2;
            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);
        }

        /// <summary>
        /// 348: 「精鋭軽巡」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_348()
        {
            var count = InjectQuest(348);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);

            _battleInfo.InjectResultStatus(
                new[] {lightcruiser, lightcruiser, trainingcruiser, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now);

            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = torpedocruiser;
            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = trainingcruiser;
            _battleInfo.Result.Friend.Main[4] = escort;
            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[4] = destroyer;
            InjectPracticeResult("A");
            Assert.AreEqual(2, count.Now);
        }

        /// <summary>
        /// 350: 精鋭「第七駆逐隊」演習開始！
        /// </summary>
        [TestMethod]
        public void PracticeResult_350()
        {
            var count = InjectQuest(350);
            var oboro    = NewShip(93);
            var akebono  = NewShip(15);
            var sazanami = NewShip(94);
            var ushio    = NewShip(16);
            var akatsuki = NewShip(34);
            var asashio  = NewShip(95);

            _battleInfo.InjectResultStatus(
                new[] {oboro, akebono, sazanami, ushio},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "B");

            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[3] = akatsuki;
            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now, "潮を配置と一字間違いで暁");

            _battleInfo.Result.Friend.Main[3] = asashio;
            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now, "「潮」が艦名に含まれる判定のチェック");
        }

        /// <summary>
        /// 353: 「巡洋艦戦隊」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_353()
        {
            var count = InjectQuest(353);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            _battleInfo.InjectResultStatus(
                new[] {destroyer, heavycruiser, aviationcruiser, heavycruiser, aviationcruiser, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = heavycruiser;
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[1] = destroyer;
            _battleInfo.Result.Friend.Main[2] = escort;
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[2] = aviationcruiser;
            InjectPracticeResult("B");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = aviationcruiser;
            InjectPracticeResult("A");
            Assert.AreEqual(2, count.Now);

            _battleInfo.Result.Friend.Main[0] = heavycruiser;
            InjectPracticeResult("A");
            Assert.AreEqual(3, count.Now);
        }

        /// <summary>
        /// 354: 「改装特務空母」任務部隊演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_354()
        {
            var count = InjectQuest(354);
            var gambierBay  = NewShip(396);
            var gambierBay2 = NewShip(707);
            var johnston    = NewShip(562);
            var fletcher    = NewShip(596);
            var samuel      = NewShip(561);
            var jervis      = NewShip(519);

            _battleInfo.InjectResultStatus(
                new[] {gambierBay, johnston, samuel},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = gambierBay2;
            _battleInfo.Result.Friend.Main[1] = jervis;
            _battleInfo.Result.Friend.Main[2] = samuel;
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = gambierBay2;
            _battleInfo.Result.Friend.Main[1] = johnston;
            _battleInfo.Result.Friend.Main[2] = jervis;
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = fletcher;
            _battleInfo.Result.Friend.Main[1] = johnston;
            _battleInfo.Result.Friend.Main[2] = gambierBay2;
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = gambierBay2;
            _battleInfo.Result.Friend.Main[1] = johnston;
            _battleInfo.Result.Friend.Main[2] = samuel;
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now);

            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = gambierBay2;
            _battleInfo.Result.Friend.Main[1] = johnston;
            _battleInfo.Result.Friend.Main[2] = fletcher;
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now);

            _battleInfo.Result.Friend.Main[0] = gambierBay2;
            _battleInfo.Result.Friend.Main[1] = samuel;
            _battleInfo.Result.Friend.Main[2] = fletcher;
            InjectPracticeResult("S");
            Assert.AreEqual(3, count.Now);
        }

        /// <summary>
        /// 355: 精鋭「第十五駆逐隊」第一小隊演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_355()
        {
            var count = InjectQuest(355);
            var kuroshio  = NewShip(227);
            var kuroshio2 = NewShip(568);
            var oyashio   = NewShip(362);
            var oyashio2  = NewShip(670);
            var kagero2   = NewShip(566);
            var shiranui2 = NewShip(567);

            _battleInfo.InjectResultStatus(
                new[] {oyashio2},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.InjectResultStatus(
                new[] {kuroshio2, oyashio, kagero2},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = kuroshio;
            _battleInfo.Result.Friend.Main[1] = oyashio2;
            _battleInfo.Result.Friend.Main[2] = shiranui2;
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = kagero2;
            _battleInfo.Result.Friend.Main[1] = oyashio2;
            _battleInfo.Result.Friend.Main[2] = kuroshio2;
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = kuroshio2;
            _battleInfo.Result.Friend.Main[1] = shiranui2;
            _battleInfo.Result.Friend.Main[2] = oyashio2;
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now);

            _battleInfo.Result.Friend.Main[0] = oyashio2;
            _battleInfo.Result.Friend.Main[1] = kuroshio2;
            _battleInfo.Result.Friend.Main[2] = shiranui2;
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now);

            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main[0] = kuroshio2;
            _battleInfo.Result.Friend.Main[1] = oyashio2;
            _battleInfo.Result.Friend.Main[2] = kagero2;
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now);
        }

        /// <summary>
        /// 356: 精鋭「第十九駆逐隊」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_356()
        {
            var count = InjectQuest(356);
            var isonami    = NewShip(206);
            var isonami2   = NewShip(666);
            var uranami    = NewShip(368);
            var uranami2   = NewShip(647);
            var ayanami    = NewShip(207);
            var ayanami2   = NewShip(195);
            var shikinami  = NewShip(208);
            var shikinami2 = NewShip(627);
            var fubuki2    = NewShip(201);

            _battleInfo.InjectResultStatus(
                new[] {isonami2, uranami2, ayanami2},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "指定艦が不足");

            _battleInfo.Result.Friend.Main = new[] {isonami, uranami2, ayanami2, shikinami2};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "指定艦が改二になってない");

            _battleInfo.Result.Friend.Main = new[] {isonami2, fubuki2, ayanami2, shikinami2};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "改二の指定艦が間違ってる");

            _battleInfo.Result.Friend.Main = new[] {isonami2, uranami2, ayanami2, shikinami2};
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "S勝が取れなかった");

            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main = new[] {ayanami2, isonami2, uranami2, shikinami2};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now, "旗艦指定はなし");

            _battleInfo.Result.Friend.Main = new[] {fubuki2, uranami2, ayanami2, isonami2, shikinami2};
            InjectPracticeResult("S");
            Assert.AreEqual(3, count.Now, "旗艦指定はなし");
        }

        /// <summary>
        /// 357: 「大和型戦艦」第一戦隊演習、始め！
        /// </summary>
        [TestMethod]
        public void PracticeResult_357()
        {
            var count = InjectQuest(357);
            var yamato  = NewShip(131);
            var musashi = NewShip(143);

            var escort         = NewShip(517);
            var destroyer      = NewShip(1);
            var lightcruiser   = NewShip(51);
            var torpedocruiser = NewShip(58);

            _battleInfo.InjectResultStatus(
                new[] {yamato, musashi, lightcruiser, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "駆逐が不足");

            _battleInfo.Result.Friend.Main = new[] {yamato, musashi, lightcruiser, destroyer, escort};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "海防は無関係");

            _battleInfo.Result.Friend.Main = new[] {yamato, musashi, torpedocruiser, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "雷巡は無関係");

            _battleInfo.Result.Friend.Main = new[] {musashi, lightcruiser, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "大和不在");

            _battleInfo.Result.Friend.Main = new[] {yamato, lightcruiser, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(0, count.Now, "武蔵不在");

            _battleInfo.Result.Friend.Main = new[] {yamato, musashi, lightcruiser, destroyer, destroyer};
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "S勝が取れなかった");

            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main = new[] {escort, yamato, musashi, lightcruiser, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now, "旗艦指定はなし");
        }

        /// <summary>
        /// 362: 特型初代「第十一駆逐隊」演習スペシャル！
        /// </summary>
        [TestMethod]
        public void PracticeResult_362()
        {
            var count = InjectQuest(362);
            var fubuki    = NewShip(9);
            var shirayuki = NewShip(10);
            var hatsuyuki = NewShip(32);
            var miyuki    = NewShip(11);
            var isonami   = NewShip(12);

            _battleInfo.InjectResultStatus(
                new[] {fubuki, shirayuki, hatsuyuki},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "指定艦が不足");

            _battleInfo.Result.Friend.Main = new[] {miyuki, shirayuki, hatsuyuki, fubuki};
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "A勝が取れなかった");

            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main = new[] {shirayuki, miyuki, fubuki, isonami};
            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now, "指定艦間違い");
        }

        /// <summary>
        /// 367: 【梅雨限定任務】水雷戦隊、雨中演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_367()
        {
            var count = InjectQuest(367);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);

            _battleInfo.InjectResultStatus(
                new[] {lightcruiser, destroyer, destroyer, destroyer, destroyer, destroyer},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("B");
            Assert.AreEqual(0, count.Now, "B勝は不可");

            InjectPracticeResult("A");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main = new[] {trainingcruiser, destroyer, destroyer, destroyer, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now);


            _battleInfo.Result.Friend.Main = new[] {lightcruiser, destroyer, destroyer, destroyer, destroyer, escort};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now, "駆逐が不足、海防艦は代替不可");

            _battleInfo.Result.Friend.Main = new[] {torpedocruiser, destroyer, destroyer, destroyer, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now, "旗艦雷巡は不可");

            _battleInfo.Result.Friend.Main = new[] {destroyer, lightcruiser, destroyer, destroyer, destroyer, destroyer};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now, "旗艦駆逐は不可");
        }

        /// <summary>
        /// 368: 「十六駆」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_368()
        {
            var count = InjectQuest(368);
            var hatsukaze   = NewShip(190);
            var yukikaze    = NewShip(20);
            var amatsukaze  = NewShip(181);
            var tokitsukaze = NewShip(186);
            var shimakaze   = NewShip(50);
            var maikaze     = NewShip(122);

            _battleInfo.InjectResultStatus(
                new[] {yukikaze, amatsukaze},
                new ShipStatus[0], new ShipStatus[0], new ShipStatus[0]);
            InjectPracticeResult("A");
            Assert.AreEqual(0, count.Now, "A勝は不可");

            InjectPracticeResult("S");
            Assert.AreEqual(1, count.Now);

            _battleInfo.Result.Friend.Main = new[] {shimakaze, maikaze, hatsukaze, tokitsukaze};
            InjectPracticeResult("S");
            Assert.AreEqual(2, count.Now);

            _battleInfo.Result.Friend.Main = new[] {amatsukaze, tokitsukaze};
            InjectPracticeResult("S");
            Assert.AreEqual(3, count.Now);

            _battleInfo.Result.Friend.Main = new[] {hatsukaze, yukikaze};
            InjectPracticeResult("S");
            Assert.AreEqual(4, count.Now);


            _battleInfo.Result.Friend.Main = new[] {yukikaze, shimakaze};
            InjectPracticeResult("S");
            Assert.AreEqual(4, count.Now, "島風は艦違い");

            _battleInfo.Result.Friend.Main = new[] {hatsukaze, maikaze};
            InjectPracticeResult("S");
            Assert.AreEqual(4, count.Now, "舞風は艦違い");
        }

        /// <summary>
        /// 402: 「遠征」を3回成功させよう！
        /// 403: 「遠征」を10回成功させよう！
        /// 404: 大規模遠征作戦、発令！
        /// 410: 南方への輸送作戦を成功させよ！
        /// 411: 南方への鼠輸送を継続実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_402_403_404_410_411()
        {
            InjectQuestList(new[] {402, 403, 404, 410, 411});

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 6}},
                    new {api_id = 3, api_mission = new[] {2, 37}},
                    new {api_id = 4, api_mission = new[] {2, 2}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 2}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 0}));
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[]
                    {
                        new {Id = 402, Now = 2}, new {Id = 403, Now = 2}, new {Id = 404, Now = 2},
                        new {Id = 410, Now = 1}, new {Id = 411, Now = 1}
                    }));
        }

        /// <summary>
        /// 426: 海上通商航路の警戒を厳とせよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_426()
        {
            var count = InjectQuest(426);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 3}},
                    new {api_id = 3, api_mission = new[] {2, 4}},
                    new {api_id = 4, api_mission = new[] {2, 5}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));
            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 10}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 428: 近海に侵入する敵潜を制圧せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_428()
        {
            var count = InjectQuest(428);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 4}},
                    new {api_id = 3, api_mission = new[] {2, 101}},
                    new {api_id = 4, api_mission = new[] {2, 102}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1}));
        }

        /// <summary>
        /// 434: 特設護衛船団司令部、活動開始！
        /// </summary>
        [TestMethod]
        public void MissionResult_434()
        {
            var count = InjectQuest(434);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 3}},
                    new {api_id = 3, api_mission = new[] {2, 5}},
                    new {api_id = 4, api_mission = new[] {2, 100}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 101}},
                    new {api_id = 3, api_mission = new[] {2, 9}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}));
        }

        /// <summary>
        /// 436: 練習航海及び警備任務を実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_436()
        {
            var count = InjectQuest(436);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 1}},
                    new {api_id = 3, api_mission = new[] {2, 2}},
                    new {api_id = 4, api_mission = new[] {2, 3}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 4}},
                    new {api_id = 3, api_mission = new[] {2, 10}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}));
        }

        /// <summary>
        /// 437: 小笠原沖哨戒線の強化を実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_437()
        {
            var count = InjectQuest(437);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 4}},
                    new {api_id = 3, api_mission = new[] {2, 104}},
                    new {api_id = 4, api_mission = new[] {2, 105}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 110}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 438: 南西諸島方面の海上護衛を強化せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_438()
        {
            var count = InjectQuest(438);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 4}},
                    new {api_id = 3, api_mission = new[] {2, 100}},
                    new {api_id = 4, api_mission = new[] {2, 9}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 114}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 439: 兵站強化遠征任務【基本作戦】
        /// </summary>
        [TestMethod]
        public void MissionResult_439()
        {
            var count = InjectQuest(439);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 5}},
                    new {api_id = 3, api_mission = new[] {2, 100}},
                    new {api_id = 4, api_mission = new[] {2, 11}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 110}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 440: 兵站強化遠征任務【拡張作戦】
        /// </summary>
        [TestMethod]
        public void MissionResult_440()
        {
            var count = InjectQuest(440);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 41}},
                    new {api_id = 3, api_mission = new[] {2, 5}},
                    new {api_id = 4, api_mission = new[] {2, 40}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 142}},
                    new {api_id = 3, api_mission = new[] {2, 46}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}));
        }

        /// <summary>
        /// 442: 西方連絡作戦準備を実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_442()
        {
            var count = InjectQuest(442);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 131}},
                    new {api_id = 3, api_mission = new[] {2, 29}},
                    new {api_id = 4, api_mission = new[] {2, 30}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 133}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1}));
        }

        /// <summary>
        /// 444: 新兵装開発資材輸送を船団護衛せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_444()
        {
            var count = InjectQuest(444);

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 5}},
                    new {api_id = 3, api_mission = new[] {2, 12}},
                    new {api_id = 4, api_mission = new[] {2, 9}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 0, 0}));

            _questCounter.InspectDeck(Js(
                new[]
                {
                    new {api_id = 2, api_mission = new[] {2, 110}},
                    new {api_id = 3, api_mission = new[] {2, 11}}
                }));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", Js(new {api_clear_result = 1}));
            _questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", Js(new {api_clear_result = 1}));
            PAssert.That(() => count.NowArray.SequenceEqual(new[] {1, 1, 1, 1, 1}));
        }

        /// <summary>
        /// 503: 艦隊大整備！
        /// 504: 艦隊酒保祭り！
        /// </summary>
        [TestMethod]
        public void PowerUp_503_504()
        {
            InjectQuestList(new[] {503, 504});

            _questCounter.CountNyukyo();
            _questCounter.CountCharge();
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[] {new {Id = 503, Now = 1}, new {Id = 504, Now = 1}}));
        }

        /// <summary>
        /// 605: 新装備「開発」指令
        /// 606: 新造艦「建造」指令
        /// 607: 装備「開発」集中強化！
        /// 608: 艦娘「建造」艦隊強化！
        /// 609: 軍縮条約対応！
        /// 619: 装備の改修強化
        /// </summary>
        [TestMethod]
        public void Kousyou_605_606_607_608_609_619()
        {
            InjectQuestList(new[] {605, 606, 607, 608, 609, 619});

            _questCounter.InspectCreateItem(
                "api_verno=1&api_item1=10&api_item2=10&api_item3=30&api_item4=10&api_multiple_flag=0");
            _questCounter.InspectCreateItem(
                "api_verno=1&api_item1=10&api_item2=10&api_item3=30&api_item4=10&api_multiple_flag=1");
            _questCounter.CountCreateShip();
            _questCounter.InspectDestroyShip("api%5Fship%5Fid=98159%2C98166%2C98168&api%5Fverno=1");
            _questCounter.CountRemodelSlot();
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[]
                    {
                        new {Id = 605, Now = 4}, new {Id = 606, Now = 1}, new {Id = 607, Now = 4},
                        new {Id = 608, Now = 1}, new {Id = 609, Now = 3}, new {Id = 619, Now = 1}
                    }));
        }

        /// <summary>
        /// 613: 資源の再利用
        /// 626: 精鋭「艦戦」隊の新編成
        /// 628: 機種転換
        /// 638: 対空機銃量産
        /// 643: 主力「陸攻」の調達
        /// 645: 「洋上補給」物資の調達
        /// 653: 工廠稼働！次期作戦準備！
        /// 654: 精鋭複葉機飛行隊の編成
        /// 657: 新型兵装開発整備の強化
        /// 663: 新型艤装の継続研究
        /// 673: 装備開発力の整備
        /// 674: 工廠環境の整備
        /// 675: 運用装備の統合整備
        /// 676: 装備開発力の集中整備
        /// 677: 継戦支援能力の整備
        /// 678: 主力艦上戦闘機の更新
        /// 680: 対空兵装の整備拡充
        /// 681: 航空戦力の再編増強準備
        /// 688: 航空戦力の強化
        /// 1103: 潜水艦強化兵装の量産
        /// 1104: 潜水艦電子兵装の量産
        /// 1105: 夏の格納庫整備＆航空基地整備
        /// 1107: 【鋼材輸出】基地航空兵力を増備せよ！
        /// 1119: 【期間限定任務】Halloweenはお掃除も！
        /// 1120: 【機種整理統合】新型戦闘機の量産計画
        /// </summary>
        [TestMethod]
        public void DestroyItem()
        {
            InjectItems(new[]
            {
                _sniffer.ItemMaster[1],   // 12cm単装砲
                _sniffer.ItemMaster[37],  // 7.7mm機銃
                _sniffer.ItemMaster[19],  // 九六式艦戦
                _sniffer.ItemMaster[4],   // 14cm単装砲
                _sniffer.ItemMaster[11],  // 15.2cm単装砲
                _sniffer.ItemMaster[75],  // ドラム缶(輸送用)
                _sniffer.ItemMaster[7],   // 35.6cm連装砲
                _sniffer.ItemMaster[25],  // 零式水上偵察機
                _sniffer.ItemMaster[13],  // 61cm三連装魚雷
                _sniffer.ItemMaster[20],  // 零式艦戦21型
                _sniffer.ItemMaster[28],  // 22号水上電探
                _sniffer.ItemMaster[31],  // 32号水上電探
                _sniffer.ItemMaster[35],  // 三式弾
                _sniffer.ItemMaster[23],  // 九九式艦爆
                _sniffer.ItemMaster[16],  // 九七式艦攻
                _sniffer.ItemMaster[3],   // 10cm連装高角砲
                _sniffer.ItemMaster[121], // 94式高射装置
                _sniffer.ItemMaster[242], // Swordfish
                _sniffer.ItemMaster[249], // Fulmar
                _sniffer.ItemMaster[21],  // 零式艦戦52型
                _sniffer.ItemMaster[125], // 61cm三連装(酸素)魚雷
                _sniffer.ItemMaster[106], // 13号対空電探改
                _sniffer.ItemMaster[168], // 九六式陸攻
                _sniffer.ItemMaster[82],  // 九七式艦攻(九三一空)
            });
            var questList = new[]
                {613, 626, 628, 638, 643, 645, 655, 653, 654, 657, 663, 673, 674, 675, 676, 677, 678, 680, 681, 686, 688, 1103, 1104, 1105, 1107, 1119, 1120, 1123};
            InjectQuestList(questList);
            _questCounter.InspectDestroyItem(
                $"api%5Fslotitem%5Fids={string.Join("%2C", Enumerable.Range(1, _sniffer.ItemInventory.Count))}&api%5Fverno=1");
            var scalar = new[]
            {
                new {Id = 613, Now = 1}, new {Id = 628, Now = 1}, new {Id = 638, Now = 1}, new {Id = 643, Now = 1},
                new {Id = 645, Now = 1}, new {Id = 653, Now = 1}, new {Id = 663, Now = 1}, new {Id = 673, Now = 2},
                new {Id = 674, Now = 1}, new {Id = 1103, Now = 1}, new {Id = 1104, Now = 1}, new {Id = 1105, Now = 1},
                new {Id = 1123, Now = 1}
            };
            foreach (var e in scalar)
            {
                var c = Array.Find(_questInfo.Quests, q => q.Id == e.Id).Count;
                Assert.AreEqual(e.Id, c.Id);
                Assert.AreEqual(e.Now, c.Now, $"{c.Id}");
            }
            var array = new[]
            {
                new {Id = 626, NowArray = new[] {1, 1}},
                new {Id = 654, NowArray = new[] {1, 1}}, new {Id = 655, NowArray = new[] {2, 1, 1, 1, 3}},
                new {Id = 657, NowArray = new[] {2, 1, 2}},
                new {Id = 675, NowArray = new[] {4, 1}}, new {Id = 676, NowArray = new[] {1, 1, 1}},
                new {Id = 677, NowArray = new[] {1, 1, 2}}, new {Id = 678, NowArray = new[] {1, 1}},
                new {Id = 680, NowArray = new[] {1, 3}}, new {Id = 686, NowArray = new[] {1, 1}},
                new {Id = 681, NowArray = new[] {1, 3}},
                new {Id = 688, NowArray = new[] {4, 1, 3, 1}},
                new {Id = 1107, NowArray = new[] {4, 3}},
                new {Id = 1119, NowArray = new[] {2, 1, 1}}, new {Id = 1120, NowArray = new[] {4, 1, 3}}
            };
            foreach (var e in array)
            {
                var c = Array.Find(_questInfo.Quests, q => q.Id == e.Id).Count;
                Assert.AreEqual(e.Id, c.Id);
                PAssert.That(() => c.NowArray.SequenceEqual(e.NowArray), $"{c.Id}");
            }
        }

        private void InjectItems(IEnumerable<ItemSpec> specs)
        {
            _sniffer.ItemInventory.Add(specs.Select((s, i) => new ItemStatus {Id = i + 1, Spec = s}));
        }

        /// <summary>
        /// 702: 艦の「近代化改修」を実施せよ！
        /// 703: 「近代化改修」を進め、戦備を整えよ！
        /// </summary>
        [TestMethod]
        public void PowerUp_702_703()
        {
            InjectQuestList(new[] {702, 703});
            _questCounter.InspectPowerUp("", Js(new {api_powerup_flag = 1}));
            PAssert.That(() =>
                _questInfo.Quests.Select(q => new {q.Id, q.Count.Now})
                    .SequenceEqual(new[] {new {Id = 702, Now = 1}, new {Id = 703, Now = 1}}));
        }

        /// <summary>
        /// 714: 「駆逐艦」の改修工事を実施せよ！
        /// 715: 続：「駆逐艦」の改修工事を実施せよ！
        /// </summary>
        [TestMethod]
        public void PowerUp_714_715()
        {
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);
            var ships = new[]
            {
                destroyer, destroyer, destroyer, destroyer,
                lightcruiser, lightcruiser, lightcruiser, lightcruiser
            };
            _sniffer.ShipInventory.Add(ships.Select((s, i) =>
            {
                s.Id = i + 1;
                return s;
            }));

            var q714 = InjectQuest(714);
            _questCounter.InspectPowerUp("api_id=3&api_id_items=1,2", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(0, q714.Now);
            _questCounter.InspectPowerUp("api_id=5&api_id_items=1,2,3", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(0, q714.Now);
            _questCounter.InspectPowerUp("api_id=4&api_id_items=1,2,3", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(1, q714.Now);

            var q715 = InjectQuest(715);
            _questCounter.InspectPowerUp("api_id=4&api_id_items=1,2,3", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(0, q715.Now);
            _questCounter.InspectPowerUp("api_id=4&api_id_items=5,6,7", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(1, q715.Now);
        }

        /// <summary>
        /// 716: 「軽巡」級の改修工事を実施せよ！
        /// 717: 続：「軽巡」級の改修工事を実施せよ！
        /// </summary>
        [TestMethod]
        public void PowerUp_716_717()
        {
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);
            var ships = new[]
            {
                lightcruiser, lightcruiser, torpedocruiser, trainingcruiser,
                heavycruiser, aviationcruiser, aviationcruiser
            };
            _sniffer.ShipInventory.Add(ships.Select((s, i) =>
            {
                s.Id = i + 1;
                return s;
            }));

            var q716 = InjectQuest(716);
            _questCounter.InspectPowerUp("api_id=1&api_id_items=2,3", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(0, q716.Now);
            _questCounter.InspectPowerUp("api_id=1&api_id_items=2,3,5", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(0, q716.Now);
            _questCounter.InspectPowerUp("api_id=5&api_id_items=2,3,4", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(0, q716.Now);
            _questCounter.InspectPowerUp("api_id=1&api_id_items=2,3,4", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(1, q716.Now);

            var q717 = InjectQuest(717);
            _questCounter.InspectPowerUp("api_id=1&api_id_items=3,4,5", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(0, q717.Now);
            _questCounter.InspectPowerUp("api_id=1&api_id_items=5,6,7", Js(new {api_powerup_flag = 1}));
            Assert.AreEqual(1, q717.Now);
        }
    }
}