﻿// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KancolleSniffer.Util;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class StringUtilTest
    {
        [TestMethod]
        public void Cat()
        {
            PAssert.That(() => StringUtil.Cat("a", " ", "b") == "a b");
            PAssert.That(() => StringUtil.Cat("a", "\n", "b") == "a\nb");

            PAssert.That(() => StringUtil.Cat("a", "", "b") == "ab");
            PAssert.That(() => StringUtil.Cat("a", null, "b") == "ab");

            PAssert.That(() => StringUtil.Cat("a", " ", "") == "a");
            PAssert.That(() => StringUtil.Cat("a", " ", null) == "a");
            PAssert.That(() => StringUtil.Cat("a", "\n", "") == "a");
            PAssert.That(() => StringUtil.Cat("a", "\n", null) == "a");

            PAssert.That(() => StringUtil.Cat("", " ", "b") == "b");
            PAssert.That(() => StringUtil.Cat(null, " ", "b") == "b");
            PAssert.That(() => StringUtil.Cat("", "\n", "b") == "b");
            PAssert.That(() => StringUtil.Cat(null, "\n", "b") == "b");

            PAssert.That(() => StringUtil.Cat("", " ", "") == "");
            PAssert.That(() => StringUtil.Cat(null, " ", null) == "");
            PAssert.That(() => StringUtil.Cat(null, "\n", null) == "");
        }

        [TestMethod]
        public void ToS()
        {
            PAssert.That(() => StringUtil.ToS(0.95) == "0.9");
            PAssert.That(() => StringUtil.ToS(0.94) == "0.9");
            PAssert.That(() => StringUtil.ToS(0.05) == "0.0");
            PAssert.That(() => StringUtil.ToS(0.04) == "0.0");

            PAssert.That(() => StringUtil.ToS(-0.95) == "-1.0");
            PAssert.That(() => StringUtil.ToS(-0.94) == "-1.0");
            PAssert.That(() => StringUtil.ToS(-0.05) == "-0.1");
            PAssert.That(() => StringUtil.ToS(-0.04) == "-0.1");

            PAssert.That(() => StringUtil.ToS(0.123, 2) == "0.12");
            PAssert.That(() => StringUtil.ToS(0.1234, 3) == "0.123");
        }
    }
}
