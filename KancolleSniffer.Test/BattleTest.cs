﻿// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    using Sniffer = SnifferTest.TestingSniffer;
    using static SnifferTest;

    [TestClass]
    public class BattleTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            ExpressionToCodeConfiguration.GlobalAssertionConfiguration = ExpressionToCodeConfiguration
                .GlobalAssertionConfiguration.WithPrintedListLengthLimit(200).WithMaximumValueLength(1000);
        }

        /// <summary>
        /// 昼戦でダメコンを消費する
        /// https://www.youtube.com/watch?v=GllfzhmMm0I&t=753s
        /// </summary>
        [TestMethod]
        public void ConsumeDameconInDay()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "damecon_day_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 42);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 13);

            // 戦闘開始時には、メイン画面では撃沈前未消費
            SniffLogFile(sniffer, "damecon_day_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 42);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 13);

            // 戦況画面では撃沈済み消費済み
            var shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 3);
            PAssert.That(() => shipResult.Items.Count == 1);
            PAssert.That(() => shipResult.Items[0].Spec.Id == 2);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            SniffLogFile(sniffer, "damecon_day_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 3);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 13);

            // 帰投後にダメコン所持数を更新
            SniffLogFile(sniffer, "damecon_day_104");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 3);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 12);
        }

        /// <summary>
        /// 昼戦で女神を消費する
        /// https://www.youtube.com/watch?v=GllfzhmMm0I&t=365s
        /// </summary>
        [TestMethod]
        public void ConsumeMegamiInDay()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "megami_day_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 24);

            // 戦闘開始時には、メイン画面では撃沈前未消費
            SniffLogFile(sniffer, "megami_day_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 24);

            // 戦況画面では撃沈済み消費済み
            var shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 16);
            PAssert.That(() => shipResult.Items.Count == 1);
            PAssert.That(() => shipResult.Items[0].Spec.Id == 2);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            SniffLogFile(sniffer, "megami_day_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 24);

            // 帰投後にダメコン所持数を更新
            SniffLogFile(sniffer, "megami_day_104");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);
        }

        /// <summary>
        /// 夜戦で女神カウンター
        /// https://www.youtube.com/watch?v=GllfzhmMm0I&t=1275s
        /// </summary>
        [TestMethod]
        public void MegamiCounterInNight()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "megami_night_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 4);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 昼戦では被弾回避
            SniffLogFile(sniffer, "megami_night_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 4);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 戦況画面でも状況変わらず
            var dayResult = sniffer.Battle.Result;
            var shipDayResult = dayResult.Friend.Main[1];
            PAssert.That(() => shipDayResult.NowHp == 4);
            PAssert.That(() => shipDayResult.Items.Count == 2);
            PAssert.That(() => shipDayResult.Items[0].Id == 213820);
            PAssert.That(() => shipDayResult.Items[0].Spec.Id == 2);
            PAssert.That(() => shipDayResult.Items[1].Id == 73455);
            PAssert.That(() => shipDayResult.Items[1].Spec.Id == 43);

            // 夜戦開始時には、メイン画面では撃沈前未消費
            SniffLogFile(sniffer, "megami_night_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 4);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 戦況画面では撃沈済み消費済み
            var nightResult = sniffer.Battle.Result;
            var shipNightResult = nightResult.Friend.Main[1];
            PAssert.That(() => shipNightResult.NowHp == 16);
            PAssert.That(() => shipNightResult.Items.Count == 1);
            PAssert.That(() => shipNightResult.Items[0].Id == 213820);
            PAssert.That(() => shipNightResult.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.P);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            SniffLogFile(sniffer, "megami_night_104");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 帰投後にダメコン所持数を更新
            SniffLogFile(sniffer, "megami_night_105");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 22);

            // 帰投後の戦況昼戦では撃沈前未消費
            shipDayResult = dayResult.Friend.Main[1];
            PAssert.That(() => shipDayResult.NowHp == 4);
            PAssert.That(() => shipDayResult.Items.Count == 2);
            PAssert.That(() => shipDayResult.Items[0].Id == 213820);
            PAssert.That(() => shipDayResult.Items[0].Spec.Id == 2);
            PAssert.That(() => shipDayResult.Items[1].Id == 73455);
            PAssert.That(() => shipDayResult.Items[1].Spec.Id == 43);

            // 帰投後の戦況夜戦では撃沈済み消費済み
            shipNightResult = nightResult.Friend.Main[1];
            PAssert.That(() => shipNightResult.NowHp == 16);
            PAssert.That(() => shipNightResult.Items.Count == 1);
            PAssert.That(() => shipNightResult.Items[0].Id == 213820);
            PAssert.That(() => shipNightResult.Items[0].Spec.Id == 2);
        }

        /// <summary>
        /// 補強増設の女神を消費する
        /// </summary>
        [TestMethod]
        public void ConsumeSlotExMegami()
        {
            var sniffer = new Sniffer();
            // 戦闘開始時には、メイン画面では撃沈前未消費
            SniffLogFile(sniffer, "megami_slotex_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 43);

            // 戦況画面では撃沈済み消費済み
            var shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 32);
            PAssert.That(() => shipResult.Items.Count == 0);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            SniffLogFile(sniffer, "megami_slotex_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 32);
            PAssert.That(() => ship.Items.Count == 0);

            // 次の戦闘で戦闘結果エラーが出ない
            SniffLogFile(sniffer, "megami_slotex_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 32);
            PAssert.That(() => ship.Items.Count == 0);

            shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 23);
            PAssert.That(() => shipResult.Items.Count == 0);
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// うずしおで弾薬消費
        /// </summary>
        [TestMethod]
        public void UzushioConsumeBull()
        {
            var sniffer = new Sniffer();
            // うずしお前の残量
            SniffLogFile(sniffer, "uzushio_bull_101");
            PAssert.That(() => sniffer.CellInfo.Current == "4-3 １戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "4-3 次資源");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 4);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Fuel == 12);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Bull == 16);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            SniffLogFile(sniffer, "uzushio_bull_102");
            PAssert.That(() => sniffer.CellInfo.Current == "4-3 資源");
            PAssert.That(() => sniffer.CellInfo.Next == "4-3 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 4);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Fuel == 12);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Bull == 16);

            // うずしお通過、ここで減らす
            SniffLogFile(sniffer, "uzushio_bull_103");
            PAssert.That(() => sniffer.CellInfo.Current == "4-3 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "4-3 次２戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 2);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Fuel == 12);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Bull == 11);
        }

        /// <summary>
        /// 出撃直後にうずしお
        /// </summary>
        [TestMethod]
        public void UzushioAfterStart()
        {
            var sniffer = new Sniffer();
            // うずしお前の残量
            SniffLogFile(sniffer, "uzushio_after_start_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            SniffLogFile(sniffer, "uzushio_after_start_102");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // うずしお通過、ここで減らす
            SniffLogFile(sniffer, "uzushio_after_start_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-2 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 次気のせい");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 14);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);
        }

        /// <summary>
        /// 無戦闘マスのあとにうずしお
        /// </summary>
        [TestMethod]
        public void UzushioAfterEmpty()
        {
            var sniffer = new Sniffer();
            // うずしお前の残量
            SniffLogFile(sniffer, "uzushio_after_empty_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "5-4 次気のせい");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 10);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            SniffLogFile(sniffer, "uzushio_after_empty_102");
            PAssert.That(() => sniffer.CellInfo.Current == "5-4 気のせい");
            PAssert.That(() => sniffer.CellInfo.Next == "5-4 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 10);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // うずしお通過、ここで減らす
            SniffLogFile(sniffer, "uzushio_after_empty_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-4 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-4 次１戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 7);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);
        }

        /// <summary>
        /// 能動分岐のあとにうずしお
        /// </summary>
        [TestMethod]
        public void UzushioAfterSelectRoute()
        {
            var sniffer = new Sniffer();
            // うずしお前の残量
            SniffLogFile(sniffer, "uzushio_after_select_route_101");
            PAssert.That(() => sniffer.CellInfo.Current == "5-5 １戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "5-5 次能動分岐");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 9);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            SniffLogFile(sniffer, "uzushio_after_select_route_102");
            PAssert.That(() => sniffer.CellInfo.Current == "5-5 能動分岐");
            PAssert.That(() => sniffer.CellInfo.Next == "5-5 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 9);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // うずしお通過、ここで減らす
            SniffLogFile(sniffer, "uzushio_after_select_route_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-5 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-5 次２戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 6);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);
        }

        /// <summary>
        /// うずしおのあと進撃続行
        /// </summary>
        [TestMethod]
        public void UzushioToNextBattle()
        {
            var sniffer = new Sniffer();
            // うずしお前の残量
            SniffLogFile(sniffer, "uzushio_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次１戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 85);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 120);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 25);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            SniffLogFile(sniffer, "uzushio_102");
            PAssert.That(() => sniffer.CellInfo.Current == "3-3 １戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 68);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 96);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 16);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 20);

            // うずしお通過、ここで減らす
            SniffLogFile(sniffer, "uzushio_103");
            PAssert.That(() => sniffer.CellInfo.Current == "3-3 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次２戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 59);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 96);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 13);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 20);

            // 戦闘後の進路指示で残量が矛盾しない
            SniffLogFile(sniffer, "uzushio_104");
            PAssert.That(() => sniffer.CellInfo.Current == "3-3 ２戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次ボス戦");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 42);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 72);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 10);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 15);
        }

        /// <summary>
        /// うずしお消費が表示される前にブラウザ再読み込み
        /// </summary>
        [TestMethod]
        public void UzushioToBrowserReload()
        {
            var sniffer = new Sniffer();
            // うずしお前の残量
            SniffLogFile(sniffer, "uzushio_reload_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            SniffLogFile(sniffer, "uzushio_reload_102");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // うずしお通過前にブラウザ再読み込み、減ってる
            SniffLogFile(sniffer, "uzushio_reload_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-2 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 14);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // そのまま出撃しても戦闘まで減らない
            SniffLogFile(sniffer, "uzushio_reload_104");
            PAssert.That(() => sniffer.CellInfo.Current == "4-5 能動分岐");
            PAssert.That(() => sniffer.CellInfo.Next == "4-5 次１戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 14);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);
        }

        /// <summary>
        /// 4-2-1で開幕対潜雷撃を含む戦闘を行う
        /// </summary>
        [TestMethod]
        public void NormalBattleWithVariousTypesOfAttack()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "battle_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.A);
            AssertEqualBattleResult(sniffer,
                new[] {57, 66, 50, 65, 40, 42}, new[] {34, 5, 0, 0, 0, 0});
        }

        private void AssertEqualBattleResult(Sniffer sniffer, IEnumerable<int> expected, IEnumerable<int> enemy,
            string msg = null)
        {
            var result = sniffer.Fleets[0].Ships.Select(s => s.NowHp);
            PAssert.That(() => expected.SequenceEqual(result), msg);
            var enemyResult = sniffer.Battle.Result.Enemy.Main.Select(s => s.NowHp);
            PAssert.That(() => enemy.SequenceEqual(enemyResult), msg);
        }

        /// <summary>
        /// 開幕夜戦で潜水艦同士がお見合いする
        /// </summary>
        [TestMethod]
        public void SpMidnightWithoutBattle()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "sp_midnight_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.D);
        }

        /// <summary>
        /// 夜戦で戦艦が攻撃すると一回で三発分のデータが来る
        /// そのうち存在しない攻撃はターゲット、ダメージともに-1になる
        /// </summary>
        [TestMethod]
        public void BattleShipAttackInMidnight()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "midnight_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.S);
        }

        /// <summary>
        /// 7隻編成の戦闘で7隻目が攻撃される
        /// </summary>
        [TestMethod]
        public void Ship7Battle()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "ship7battle_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.P);
        }

        /// <summary>
        /// 友軍航空戦によるダメージを戦果ランクの計算に反映させる
        /// </summary>
        [TestMethod]
        public void FriendlyKouku()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "friendly_kouku_001");
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 航空支援が全機撃墜されてもエラーにならない
        /// </summary>
        [TestMethod]
        public void SupportAiratackAllShootdown()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "support_airatack_stage3_null_101");
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 潜水航空戦でのダメージを戦果ランクの計算に反映させる
        /// </summary>
        [TestMethod]
        public void AswAirBattle()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "asw-airbattle_101");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Enemy.Main[4].NowHp == -1);
        }

        /// <summary>
        /// 演習のあとのportで戦闘結果の検証を行わない
        /// </summary>
        [TestMethod]
        public void NotVerifyBattleResultAfterPractice()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "practice_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 演習でダメコンを発動させない
        /// </summary>
        [TestMethod]
        public void NotTriggerDameConInPractice()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "practice_002");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 演習中の艦を要修復リストに載せない
        /// </summary>
        [TestMethod]
        public void DamagedShipListNotShowShipInPractice()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "practice_003");
            PAssert.That(() => sniffer.RepairList.Select(s => s.Name).SequenceEqual(new[] {"飛龍改二", "翔鶴改二"}));
        }

        /// <summary>
        /// 連合艦隊が開幕雷撃で被弾する
        /// </summary>
        [TestMethod]
        public void OpeningTorpedoInCombinedBattle()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "combined_battle_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 連合艦隊が閉幕雷撃で被弾する
        /// </summary>
        [TestMethod]
        public void ClosingTorpedoInCombinedBattle()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "combined_battle_002");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 第一が6隻未満の連合艦隊で戦闘する
        /// </summary>
        [TestMethod]
        public void SmallCombinedFleetBattle()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "combined_battle_003");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.CellInfo.Current == "E-3 １戦目");
        }

        /// <summary>
        /// 護衛退避する
        /// </summary>
        [TestMethod]
        public void EscapeWithEscort()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "escape_001");
            var fleets = sniffer.Fleets;
            PAssert.That(() => fleets[0].Ships[5].Escaped &&
                               fleets[1].Ships[2].Escaped);
        }

        /// <summary>
        /// 開幕夜戦に支援が来る
        /// </summary>
        [TestMethod]
        public void SpMidnightSupportAttack()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "sp_midnight_002");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.SupportType == 2);
        }

        /// <summary>
        /// 払暁戦を行う
        /// </summary>
        [TestMethod]
        public void NightToDay()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "nighttoday_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 第二期の開幕夜戦のセル情報を表示する
        /// </summary>
        [TestMethod]
        // ReSharper disable once InconsistentNaming
        public void SpMidnightIn2ndSequence()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "sp_midnight_003");
            PAssert.That(() => sniffer.CellInfo.Current == "5-3 １戦目(夜戦)");
        }

        /// <summary>
        /// 単艦退避する
        /// </summary>
        [TestMethod]
        public void EscapeWithoutEscort()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "escape_002");
            PAssert.That(() => sniffer.Fleets[2].Ships[1].Escaped);
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 出撃時に大破している艦娘がいたら警告する
        /// </summary>
        [TestMethod]
        public void DamagedShipWarningOnMapStart()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "mapstart_001");
            PAssert.That(() => sniffer.BadlyDamagedShips.SequenceEqual(new[] {"大潮"}));
        }

        /// <summary>
        /// 連合艦隊に大破艦がいる状態で第3艦隊が出撃したときに警告しない
        /// </summary>
        [TestMethod]
        public void NotWarnDamagedShipInCombinedFleetOnMapStart()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "mapstart_002");
            PAssert.That(() => !sniffer.BadlyDamagedShips.Any());
        }

        /// <summary>
        /// 連合艦隊の第二旗艦の大破を警告しない
        /// </summary>
        [TestMethod]
        public void NotWarnDamaged1StShipInGuardFleet()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "combined_battle_004");
            PAssert.That(() => !sniffer.BadlyDamagedShips.Any());
        }

        /// <summary>
        /// Nelson Touchに対応する
        /// </summary>
        [TestMethod]
        public void NelsonTouch()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "nelsontouch_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 100);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 100);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // ship_deckでフラグを引き継ぐ
            SniffLogFile(sniffer, "nelsontouch_002");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 100);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // 夜戦
            var night = new Sniffer();
            SniffLogFile(night, "nelsontouch_003");
            PAssert.That(() => night.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => night.Battle.Result.Friend.Main[0].SpecialAttack.Type == 100);
            PAssert.That(() => night.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            // 海戦をまたがってフラグを引き継ぐ
            var fired = new Sniffer();
            SniffLogFile(fired, "nelsontouch_004");
            PAssert.That(() => fired.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => fired.Fleets[0].Ships[0].SpecialAttack.Type == 100);
            PAssert.That(() => fired.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 長門改二の一斉射に対応する
        /// </summary>
        [TestMethod]
        // ReSharper disable once IdentifierTypo
        public void NagatoSpecial()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "nagatospecial_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 101);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 101);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 陸奥改二の一斉射に対応する
        /// </summary>
        [TestMethod]
        // ReSharper disable once IdentifierTypo
        public void MutsuSpecial()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "mutsuspecial_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 102);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 102);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// Coloradoの一斉射に対応する
        /// </summary>
        [TestMethod]
        // ReSharper disable once IdentifierTypo
        public void ColoradoSpecial()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "coloradospecial_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 103);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 103);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 連合艦隊時の僚艦夜戦突撃に対応する
        /// </summary>
        [TestMethod]
        public void KongoSpecial()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "kongospecial_101");
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Type == 0);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Count == 0);

            var dayResult = sniffer.Battle.Result;
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Fire == false);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Type == 0);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Count == 0);

            SniffLogFile(sniffer, "kongospecial_102");
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Count == 1);

            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Fire == false);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Type == 0);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Count == 0);
            var nightResult = sniffer.Battle.Result;
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Fire == true);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Type == 104);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Count == 1);

            SniffLogFile(sniffer, "kongospecial_103");
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.IsBattleResultError);

            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Fire == false);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Type == 0);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Count == 0);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Fire == true);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Type == 104);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Count == 1);
            var result = sniffer.Battle.Result;
            PAssert.That(() => result.Friend.Guard[0].SpecialAttack.Fire == true);
            PAssert.That(() => result.Friend.Guard[0].SpecialAttack.Type == 104);
            PAssert.That(() => result.Friend.Guard[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 連合艦隊時の僚艦夜戦突撃2発目に対応する
        /// </summary>
        [TestMethod]
        public void KongoSpecial2()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "kongospecial_002_1");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // ship_deckでフラグを引き継ぐ
            SniffLogFile(sniffer, "kongospecial_002_2");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // 2発目
            SniffLogFile(sniffer, "kongospecial_002_3");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 2);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 2);
        }

        /// <summary>
        /// 大和型改二の特殊砲撃(2隻)に対応する
        /// </summary>
        [TestMethod]
        public void YamatoSpecial2Ships()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "yamatospecial_2ships_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 401);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 401);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 大和型改二の特殊砲撃(3隻)に対応する
        /// </summary>
        [TestMethod]
        public void YamatoSpecial3Ships()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "yamatospecial_3ships_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire == true);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 400);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Fire == false);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 400);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 潜水艦特殊攻撃で潜水艦補給物資を消費する
        /// </summary>
        [TestMethod]
        public void SubmarineSpecial()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "submarine_special_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(34).ToString() == "潜水艦補給物資 x18");

            SniffLogFile(sniffer, "submarine_special_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(34).ToString() == "潜水艦補給物資 x17");
        }

        /// <summary>
        /// レーダー射撃戦に対応する
        /// </summary>
        [TestMethod]
        public void LdShooting()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "ld_shooting_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 敵連合艦隊の護衛の装備を正しく読み取る
        /// </summary>
        [TestMethod]
        public void EnemyGuardSlot()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "enemy_combined_001");
            PAssert.That(() => sniffer.Battle.Result.Enemy.Guard[0].Items[0].Spec.Id == 506);
        }

        /// <summary>
        /// 敵艦隊の制空値を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPower()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "enemy_combined_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            PAssert.That(() => fp.Interception.Aircrafts == 277);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 264);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 61);
            PAssert.That(() => fp.Interception.FighterPower == 215);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 209);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 102);
        }

        /// <summary>
        /// ダメコン進撃する
        /// </summary>
        [TestMethod]
        public void NotWarnDamagedShipWithDamageControl()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "damecon_advance_001");
            PAssert.That(() => sniffer.BadlyDamagedShips.Length == 0);
        }

        /// <summary>
        /// 迎撃機ありの基地空襲戦
        /// </summary>
        [TestMethod]
        public void AirRaidBattleWithInterceptor()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airraid_battle_001");
            var battle = sniffer.Battle;
            var abr = sniffer.Battle.AirBattleResult;
            Assert.AreEqual(BattleState.AirRaid, battle.BattleState);
            Assert.AreEqual(2, battle.AirControlLevel);
            Assert.AreEqual(425, battle.FighterPower.Min);
            Assert.AreEqual(231, battle.EnemyFighterPower.Interception.FighterPower);
            Assert.AreEqual(231, battle.EnemyFighterPower.LoggerAirCombat.FighterPower);
            Assert.AreEqual(231, battle.EnemyFighterPower.ActualAirCombat.FighterPower);
            Assert.AreEqual(BattleResultRank.S, battle.ResultRank);
            var ships = battle.Result.Friend.Main;
            Assert.IsTrue(new[] {200, 200, 200}.SequenceEqual(ships.Select(ship => ship.NowHp)));
            Assert.IsTrue(new[] {"基地航空隊1", "基地航空隊2", "基地航空隊3"}.SequenceEqual(ships.Select(ship => ship.Name)));
            Assert.IsTrue(new[] {"烈風改(三五二空/熟練)", "雷電", "雷電", "烈風改"}.SequenceEqual(ships[2].Items.Select(item => item.Spec.Name)));
            Assert.IsTrue(new[] {18, 18, 18, 18}.SequenceEqual(ships[2].Items.Select(item => item.OnSlot)));
            Assert.AreEqual(abr.Result[abr.ResultPanelInitIndex()].PhaseName, "空襲");
        }

        /// <summary>
        /// 迎撃機なしの基地航空戦
        /// </summary>
        [TestMethod]
        public void AirRaidBattleWithoutInterceptor()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airraid_battle_002");
            var battle = sniffer.Battle;
            Assert.AreEqual(BattleState.AirRaid, battle.BattleState);
            Assert.AreEqual(4, battle.AirControlLevel);
            Assert.AreEqual(0, battle.FighterPower.Min);
            Assert.AreEqual(231, battle.EnemyFighterPower.Interception.FighterPower);
            Assert.AreEqual(231, battle.EnemyFighterPower.LoggerAirCombat.FighterPower);
            Assert.AreEqual(231, battle.EnemyFighterPower.ActualAirCombat.FighterPower);
            Assert.AreEqual(BattleResultRank.B, battle.ResultRank);
            var ships = battle.Result.Friend.Main;
            Assert.IsTrue(new[] {82, 174, 147}.SequenceEqual(ships.Select(ship => ship.NowHp)));
        }

        /// <summary>
        /// 基地航空戦直後のボス戦
        /// </summary>
        [TestMethod]
        public void AirRaidBattleBeforeBoss()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airraid_battle_004");
            var battle = sniffer.Battle;
            Assert.AreEqual(BattleState.Result, battle.BattleState);
            Assert.AreEqual(0, sniffer.BadlyDamagedShips.Length);
        }

        /// <summary>
        /// 22冬e-5の超重爆迎撃Good判定
        /// </summary>
        [TestMethod]
        public void AirRaidBattleWithUserInput()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airraid-input_good_101");
            var battle = sniffer.Battle;
            var airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 53);
            var abr = sniffer.Battle.AirBattleResult;
            Assert.AreEqual(356, battle.FighterPower.Min);
            Assert.AreEqual(battle.FighterPower.Min, airbase.CalcInterceptionFighterPower(2).Min);
            Assert.AreEqual(47, abr.Result[2].Stage1.FriendCount);
            Assert.AreEqual(abr.Result[2].Stage1.FriendCount, airbase.AirCorps.Take(2).SelectMany(aircorps => aircorps.Planes).Where(plane => plane.Deploying).Sum(plane => plane.Item.OnSlot));
            Assert.AreEqual(abr.Result[abr.ResultPanelInitIndex()].PhaseName, "空襲3");

            Assert.IsTrue(new[] {0, 0, 1, 1}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.State)));
            Assert.IsTrue(new[] {0, 0, 1, 1}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.State)));
            Assert.IsTrue(new[] {0, 0, 1, 1}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.State)));

            Assert.IsTrue(new[] {4, 2}.SequenceEqual(airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {7, 7}.SequenceEqual(airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {7, 7}.SequenceEqual(airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {11, 12}.SequenceEqual(airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] {12, 12}.SequenceEqual(airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] {18, 18}.SequenceEqual(airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));

            Assert.IsTrue(battle.Result.Friend.Main[0].Items.Select(item => item.Alv).SequenceEqual(airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            Assert.IsTrue(battle.Result.Friend.Main[1].Items.Select(item => item.Alv).SequenceEqual(airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            Assert.IsTrue(battle.Result.Friend.Main[2].Items.Select(item => item.Alv).SequenceEqual(airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            Assert.IsTrue(battle.Result.Friend.Main[0].Items.Select(item => item.OnSlot).SequenceEqual(airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(battle.Result.Friend.Main[1].Items.Select(item => item.OnSlot).SequenceEqual(airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(battle.Result.Friend.Main[2].Items.Select(item => item.OnSlot).SequenceEqual(airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
        }

        /// <summary>
        /// 基地航空隊分散出撃時の残機数変動
        /// </summary>
        [TestMethod]
        public void AirbaseOnSlotInSortie()
        {
            var sniffer = new Sniffer();
            // 出撃前
            SniffLogFile(sniffer, "strike_point_split_101");
            var airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            Assert.IsTrue(new[] {1, 1, 1, 1}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.State)));
            Assert.IsTrue(new[] {1, 1, 1, 1}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.State)));
            Assert.IsTrue(new[] {1, 1, 1, 1}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.State)));

            Assert.IsTrue(new[] {5, 5, 0, 7}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {3, 4, 3, 7}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {7, 7, 7, 7}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {18, 18, 18, 18}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] {18, 18, 18, 18}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] { 1, 18,  5,  4}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.AreEqual(134, airbase.AirCorps[0].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(108, airbase.AirCorps[1].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(263, airbase.AirCorps[2].CalcFighterPower().Interception.Min);

            // 基地空襲、防空あり、地上撃破あり
            SniffLogFile(sniffer, "strike_point_split_102");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            Assert.IsTrue(new[] {5, 5, 0, 7}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {3, 4, 3, 7}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {7, 7, 7, 7}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {18, 18, 18, 18}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] {18, 18, 18, 18}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] { 1, 18,  5,  4}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.AreEqual(134, airbase.AirCorps[0].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(108, airbase.AirCorps[1].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(263, airbase.AirCorps[2].CalcFighterPower().Interception.Min);

            // 基地航空隊出撃1 (対艦)
            SniffLogFile(sniffer, "strike_point_split_103");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            Assert.IsTrue(new[] {5, 5, 0, 7}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {3, 4, 3, 7}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {7, 7, 7, 7}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {18, 18, 18, 18}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] {18, 18, 18, 18}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] { 1, 18,  5,  4}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.AreEqual(134, airbase.AirCorps[0].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(108, airbase.AirCorps[1].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(263, airbase.AirCorps[2].CalcFighterPower().Interception.Min);
            var battle = sniffer.Battle;
            Assert.AreEqual(airbase.AirCorps[1].Planes.Sum(plane => plane.Item.OnSlot), battle.AirBattleResult.Result[0].Stage1.FriendCount);

            // 基地航空隊出撃2 (ボス)
            SniffLogFile(sniffer, "strike_point_split_104");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            Assert.IsTrue(new[] {5, 5, 0, 7}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {3, 4, 3, 7}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {7, 7, 7, 7}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv)));
            Assert.IsTrue(new[] {17, 18, 18, 18}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] {16, 16, 17, 17}.SequenceEqual(airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.IsTrue(new[] { 1, 18,  5,  4}.SequenceEqual(airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot)));
            Assert.AreEqual(133, airbase.AirCorps[0].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(106, airbase.AirCorps[1].CalcFighterPower().AirCombat.Min);
            Assert.AreEqual(263, airbase.AirCorps[2].CalcFighterPower().Interception.Min);
            battle = sniffer.Battle;
            Assert.AreEqual(airbase.AirCorps[0].Planes.Sum(plane => plane.Item.OnSlot), battle.AirBattleResult.Result[0].Stage1.FriendCount);
            Assert.AreEqual(airbase.AirCorps[1].Planes.Sum(plane => plane.Item.OnSlot), battle.AirBattleResult.Result[2].Stage1.FriendCount);
        }

        /// <summary>
        /// 配置転換中でも戦闘時に残機数0が送られてくる
        /// </summary>
        [TestMethod]
        public void AirbaseOnSlotInSortieWhenChanging()
        {
            var sniffer = new Sniffer();
            // 出撃前
            SniffLogFile(sniffer, "airbase_onslot_slip_101");
            var airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 56);
            Assert.IsTrue(new[] {2, 1, 2, 1}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.State)));
            Assert.IsTrue(new[] {0, 9, 0, 4}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot)));

            // 出動地点到着
            SniffLogFile(sniffer, "airbase_onslot_slip_102");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 56);
            Assert.IsTrue(new[] {2, 1, 2, 1}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.State)));
            Assert.IsTrue(new[] {0, 9, 0, 4}.SequenceEqual(airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot)));
        }

        /// <summary>
        /// 噴式機で敵空母を撃沈したあとの制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerAfterJetBomber()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "jetbomber_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == 109);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 108);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 54);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.FighterPower == 17);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 16);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 8);
            PAssert.That(() => fp.AirCombatAircraftsOffset == 54);
            PAssert.That(() => fp.InterceptionAircraftsOffset == 54);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 基地航空隊の噴式機のあとの制空値と搭載数を通常の基地航空戦で計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerAfterAirBaseJetBomber()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airbase_jetbomber_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.Aircrafts == 179);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 176);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 98);
            PAssert.That(() => fp.Interception.FighterPower == 47);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 46);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 36);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 演習での噴式機対噴式機の空戦の制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerJetBomberVsJetBomber()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "jet_vs_jet_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == abr.First.Stage1.EnemyCount + abr.EnemyLostJets);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 58);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 48);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 230);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 214);
        }

        /// <summary>
        /// 通常対敵連合での基地航空戦の制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerOnAirBase()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "aircraft_count_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.Aircrafts == 227);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 216);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 45);
            PAssert.That(() => fp.Interception.FighterPower == 206);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 201);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 72);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 連合対敵通常、連合対敵連合での基地航空戦の制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerOnAirBaseAndCombined()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "aircraft_count_combined_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.Aircrafts == 15);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 0);
            PAssert.That(() => fp.Interception.FighterPower == 3);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 0);

            SniffLogFile(sniffer, "aircraft_count_combined_002");
            var fp2 = sniffer.Battle.EnemyFighterPower;
            var abr2 = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp2.Interception.Aircrafts == abr2.First.Stage1.EnemyCount);
            PAssert.That(() => fp2.Interception.Aircrafts == 291);
            PAssert.That(() => fp2.LoggerAirCombat.Aircrafts == 286);
            PAssert.That(() => fp2.ActualAirCombat.Aircrafts == 184);
            PAssert.That(() => fp2.Interception.FighterPower == 331);
            PAssert.That(() => fp2.LoggerAirCombat.FighterPower == 329);
            PAssert.That(() => fp2.ActualAirCombat.FighterPower == 262);
        }

        /// <summary>
        /// 空戦2回マスでの戦況初期表示
        /// </summary>
        [TestMethod]
        public void ResultPanelInitIndexAirbattle2Times()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "airbattle_2times_101");
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 緊急泊地修理
        /// 退避後に泊地修理する
        /// 泊地修理で緊急修理資材を消費する
        /// </summary>
        [TestMethod]
        public void AnchorageRepair()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "anchorage_repair_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x49");

            SniffLogFile(sniffer, "anchorage_repair_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x48");
            PAssert.That(() => sniffer.Fleets[1].Ships[2].Escaped);
            PAssert.That(() => sniffer.Fleets[1].Ships[4].Escaped);

            SniffLogFile(sniffer, "anchorage_repair_103");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x48");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 大破進撃しても平気なマスでの大破警告抑制
        /// </summary>
        [TestMethod]
        public void IgnoreDamagedShips()
        {
            var sniffer = new Sniffer();
            SniffLogFile(sniffer, "endpoint_001");
            PAssert.That(() => sniffer.BadlyDamagedShips.Length == 0);
        }
    }
}