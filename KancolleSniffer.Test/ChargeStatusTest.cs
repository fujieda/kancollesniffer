﻿// Copyright (C) 2019 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KancolleSniffer.Model;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace KancolleSniffer.Test
{
    [TestClass]
    public class ChargeStatusTest
    {
        private ShipInventory _shipInventory;
        private Fleet _fleet;

        [TestInitialize]
        public void Initialize()
        {
            _shipInventory = new ShipInventory();
            _fleet = new Fleet(_shipInventory, 6, null);
        }

        public ShipStatus NewShip()
        {
            var ship = new ShipStatus(_shipInventory.MaxId + 1);
            _shipInventory.Add(ship);
            return ship;
        }

        [TestMethod]
        public void NoShips()
        {
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 0 && stat.Bull == 0);
            PAssert.That(() => stat.Empty);
        }

        [TestMethod]
        public void FullFlagshipOnly()
        {
            var fs = NewShip();
            fs.Fuel = 9;
            fs.Spec.FuelMax = 9;
            fs.Bull = 9;
            fs.Spec.BullMax = 9;
            _fleet.SetShips(new[] {fs.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 0 && stat.FuelRemains == 1 && stat.FuelPenalty == 0.0);
            PAssert.That(() => stat.Bull == 0 && stat.BullRemains == 1 && stat.BullPenalty == 0.0);
        }

        [TestMethod]
        public void NoPenalty()
        {
            var fs = NewShip();
            fs.Fuel = 75;
            fs.Spec.FuelMax = 100;
            fs.Bull = 50;
            fs.Spec.BullMax = 100;
            _fleet.SetShips(new[] {fs.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 1 && stat.FuelRemains == 0.75 && stat.FuelPenalty == 0.0);
            PAssert.That(() => stat.Bull == 1 && stat.BullRemains == 0.5  && stat.BullPenalty == 0.0);
        }

        [TestMethod]
        public void LightPenalty()
        {
            var fs = NewShip();
            fs.Fuel = 35;
            fs.Spec.FuelMax = 100;
            fs.Bull = 25;
            fs.Spec.BullMax = 100;
            _fleet.SetShips(new[] {fs.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 2 && stat.FuelRemains == 0.35 && stat.FuelPenalty == 40.0);
            PAssert.That(() => stat.Bull == 2 && stat.BullRemains == 0.25 && stat.BullPenalty == 50.0);
        }

        [TestMethod]
        public void HeavyPenalty()
        {
            var fs = NewShip();
            fs.Fuel = 1;
            fs.Spec.FuelMax = 100;
            fs.Bull = 1;
            fs.Spec.BullMax = 100;
            _fleet.SetShips(new[] {fs.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 3 && stat.FuelRemains == 0.01 && stat.FuelPenalty == 74.0);
            PAssert.That(() => stat.Bull == 3 && stat.BullRemains == 0.01 && stat.BullPenalty == 98.0);
        }

        [TestMethod]
        public void Empty()
        {
            var fs = NewShip();
            fs.Fuel = 0;
            fs.Spec.FuelMax = 100;
            fs.Bull = 0;
            fs.Spec.BullMax = 100;
            _fleet.SetShips(new[] {fs.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 4 && stat.FuelRemains == 0.0 && stat.FuelPenalty == 75.0);
            PAssert.That(() => stat.Bull == 4 && stat.BullRemains == 0.0 && stat.BullPenalty == 100.0);
        }

        [TestMethod]
        public void FlagshipOnly()
        {
            var fs = NewShip();
            fs.Fuel = 3;
            fs.Spec.FuelMax = 9;
            fs.Bull = 0;
            fs.Spec.BullMax = 9;
            _fleet.SetShips(new[] {fs.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 3 && stat.FuelRemains == 3 / 9.0 && Math.Round(stat.FuelPenalty, 2) == 41.67);
            PAssert.That(() => stat.Bull == 4 && stat.BullRemains == 0.0     && stat.BullPenalty == 100.0);
        }

        [TestMethod]
        public void FullFlagshipAndFullOther()
        {
            var fs = NewShip();
            fs.Fuel = 9;
            fs.Spec.FuelMax = 9;
            fs.Bull = 9;
            fs.Spec.BullMax = 9;
            var other = NewShip();
            other.Fuel = 9;
            other.Spec.FuelMax = 9;
            other.Bull = 9;
            other.Spec.BullMax = 9;
            _fleet.SetShips(new[] {fs.Id, other.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 5 && stat.FuelRemains == 1 && stat.FuelPenalty == 0.0);
            PAssert.That(() => stat.Bull == 5 && stat.BullRemains == 1 && stat.BullPenalty == 0.0);
        }

        [TestMethod]
        public void FullFlagshipAndOther()
        {
            var fs = NewShip();
            fs.Fuel = 9;
            fs.Spec.FuelMax = 9;
            fs.Bull = 9;
            fs.Spec.BullMax = 9;
            var other = NewShip();
            other.Fuel = 7;
            other.Spec.FuelMax = 9;
            other.Bull = 3;
            other.Spec.BullMax = 9;
            _fleet.SetShips(new[] {fs.Id, other.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 6 && stat.FuelRemains == 7 / 9.0 && stat.FuelPenalty == 0.0);
            PAssert.That(() => stat.Bull == 7 && stat.BullRemains == 3 / 9.0 && Math.Round(stat.BullPenalty, 2) == 33.33);
        }

        [TestMethod]
        public void FullFlagshipAndOthers()
        {
            var fs = NewShip();
            fs.Fuel = 9;
            fs.Spec.FuelMax = 9;
            fs.Bull = 9;
            fs.Spec.BullMax = 9;
            var second = NewShip();
            second.Fuel = 3;
            second.Spec.FuelMax = 9;
            second.Bull = 9;
            second.Spec.BullMax = 9;
            var third = NewShip();
            third.Fuel = 7;
            third.Spec.FuelMax = 9;
            third.Bull = 0;
            third.Spec.BullMax = 9;
            _fleet.SetShips(new[] {fs.Id, second.Id, third.Id});
            var stat = _fleet.ChargeStatus;
            PAssert.That(() => stat.Fuel == 8 && stat.FuelRemains == 3 / 9.0 && Math.Round(stat.FuelPenalty, 2) == 41.67);
            PAssert.That(() => stat.Bull == 9 && stat.BullRemains == 0.0     && stat.BullPenalty == 100.0);
        }
    }
}