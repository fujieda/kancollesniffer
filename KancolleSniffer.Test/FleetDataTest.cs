﻿// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.View.ListWindow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    using Sniffer = SnifferTest.TestingSniffer;

    [TestClass]
    public class FleetDataTest
    {
        /// <summary>
        /// 編成で艦隊をまたがって艦娘を交換する
        /// </summary>
        [TestMethod]
        public void ExchangeFleetMember()
        {
            var sniffer = new Sniffer();
            var expected = new FleetSource.Data
            {
                ShipId = 756,
                Ship = "島風改 Lv130",
                ShipTooltip = "燃17 弾21",
                Spec = "砲64.0 命122.6 潜82.3",
                SpecTooltip = "雷104.0\n砲撃支援:63 命中:96.6" // " 夜158.0" 夜戦可否判定に2期以降で追加されたapi_raigが必要になったため、古いテストデータの互換性が失われつつある
            };

            SnifferTest.SniffLogFile(sniffer, "deck_002");
            SnifferTest.SniffLogFile(sniffer, "deck_003");
            var source = new FleetSource();
            source.CreateData(sniffer);
            Console.WriteLine(source.GetData[13].Spec);
            Console.WriteLine(source.GetData[13].SpecTooltip);
            PAssert.That(() => CompareFleetRecord(source.GetData[13], expected));
        }

        private bool CompareFleetRecord(FleetSource.Data a, FleetSource.Data b)
        {
            foreach (var property in typeof(FleetSource.Data).GetProperties())
            {
                var aVal = property.GetValue(a);
                var bVal = property.GetValue(b);
                if (aVal == null)
                {
                    if (bVal == null)
                        continue;
                    return false;
                }
                if (aVal.ToString() == bVal.ToString())
                    continue;
                return false;
            }
            return true;
        }

        [TestMethod]
        public void ShowSpeed()
        {
            var sniffer = new Sniffer();
            SnifferTest.SniffLogFile(sniffer, "speed_001");
            var source = new FleetSource();
            source.CreateData(sniffer);
            PAssert.That(() => source.GetData[0].Fleet == "第一 高速+   火541 空600 潜44 索724");
            PAssert.That(() => source.GetData[37].Fleet == "第二 高速   火185 空215 潜242 索166");
        }

        [TestMethod]
        public void MissionStatusNoneAircraft()
        {
            var sniffer = new Sniffer();
            SnifferTest.SniffLogFile(sniffer, "deck_007");
            var source = new FleetSource();
            source.CreateData(sniffer);
            PAssert.That(() => source.GetData[0].Fleet == "第一 高速   火84 空103 潜40 索74");
        }

        [TestMethod]
        public void ItemString()
        {
            var item = new ItemStatus
            {
                Spec = new ItemSpec
                {
                    Name = "大発動艇(八九式中戦車&陸戦隊)"
                }
            };
            Assert.AreEqual(item.Spec.Name, FleetSource.CreateEquipRecord(item).AdjustedEquipText(0));
            item.Level = 10;
            Assert.AreEqual("大発動艇(八九式中戦車&陸戦★10", FleetSource.CreateEquipRecord(item).AdjustedEquipText(0));

            var aircraft = new ItemStatus
            {
                Spec = new ItemSpec
                {
                    Name = "零式艦戦53型(岩本隊)",
                    Type = 6
                },
                OnSlot = 7,
                MaxEq = 7
            };
            Assert.AreEqual(aircraft.Spec.Name, FleetSource.CreateEquipRecord(aircraft).AdjustedEquipText(0));
            aircraft.Level = 10;
            Assert.AreEqual("零式艦戦53型(岩本★10", FleetSource.CreateEquipRecord(aircraft).AdjustedEquipText(0));
        }
    }
}