﻿// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using KancolleSniffer.Forms;

namespace KancolleSniffer.View.ListWindow
{
    public class CompositionPanel : PanelWithToolTip, IPanelResize
    {
        public const int LineHeight = 14;
        public const int LabelHeight = 12;
        private readonly List<Row> _rows = new List<Row>();
        public ISource Source { get; set; }
        public string ScrollLockPosition { get; set; } = String.Empty;
        public string TemporaryPosition { get; set; } = String.Empty;

        public interface ISource
        {
            public void CreateData(Sniffer sniffer);
            public Row CreateRow();
            public int DataCount();
            public void Assign(int i, Row row, ToolTip tooltip);
        }

        public abstract class Row : ControlsArranger
        {
            public Color DefaultBackColor { get; set; }
            public Color CurrentBackColor { get; set; }
            public abstract Control[] BackColorControls { get; }
            public abstract void AdjustLocation();
            public abstract bool MoveToTopLevel(string fn);
            public abstract bool MoveToSecondLevel(int id);
        }

        public CompositionPanel()
        {
            ToolTip.AutoPopDelay = 10000;
        }

        public void Update(Sniffer sniffer)
        {
            Source.CreateData(sniffer);
            SuspendLayout();
            CreateRows();
            ResizeRows();
            Assign();
            ResumeLayout();
            MoveToTopLevel(ScrollLockPosition != String.Empty ? ScrollLockPosition : TemporaryPosition);
        }

        public void ApplyResize()
        {
            SuspendLayout();
            ResizeRows();
            Assign(); // TODO: 装備の改修レベルを末尾に付ける都合で、毎回長さを計算しなおす処理を削除したい
            ResumeLayout();
            MoveToTopLevel(ScrollLockPosition != String.Empty ? ScrollLockPosition : TemporaryPosition);
        }

        private void CreateRows()
        {
            for (var i = _rows.Count; i < Source.DataCount(); i++)
            {
                var row = Source.CreateRow();
                _rows.Add(row);
                var y = 1 + LineHeight * i;
                row.BackPanel = new Panel {Location = new Point(0, y), Size = new Size(ListForm.PanelWidth, CompositionPanel.LineHeight)};
                var backColor = CustomColors.ColumnColors.BrightFirst(i);
                row.DefaultBackColor = row.CurrentBackColor = backColor;
                row.Arrange(this, backColor);
                row.Scale();
                row.Move(AutoScrollPosition);
            }
        }

        private void ResizeRows()
        {
            var width = Width - SystemInformation.VerticalScrollBarWidth - 2;
            foreach (var row in _rows)
            {
                row.BackPanel.Width = width;
                row.AdjustLocation();
            }
        }

        private void Assign()
        {
            for (var i = 0; i < Source.DataCount(); i++)
            {
                Source.Assign(i, _rows[i], ToolTip);
                foreach (var control in _rows[i].BackColorControls)
                    control.BackColor = _rows[i].CurrentBackColor;
                _rows[i].BackPanel.Visible = true;
            }
            for (var i = Source.DataCount(); i < _rows.Count; i++)
                _rows[i].BackPanel.Visible = false;
        }

        public void MoveToTopLevel(string fn)
        {
            if (String.IsNullOrWhiteSpace(fn))
                return;
            var i = _rows.FindIndex(row => row.MoveToTopLevel(fn));
            if (i == -1)
                return;
            var y = Scaler.ScaleHeight(LineHeight * i);
            AutoScrollPosition = new Point(0, y);
        }

        public void MoveToSecondLevel(int id)
        {
            var i = _rows.FindIndex(row => row.MoveToSecondLevel(id));
            if (i == -1)
                return;
            var y = Scaler.ScaleHeight(LineHeight * i);
            AutoScrollPosition = new Point(0, y);
        }
    }
}
