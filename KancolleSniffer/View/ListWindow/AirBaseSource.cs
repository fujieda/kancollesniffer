﻿// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using KancolleSniffer.Forms;
using KancolleSniffer.Model;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.View.ListWindow
{
    public class AirBaseSource : CompositionPanel.ISource
    {
        private List<Data> _data = new List<Data>();

        public class Data
        {
            public string AirBase { get; set; } = "";
            public string AirBaseTooltip { get; set; } = "";
            public string AirCorps { get; set; } = "";
            public string AirCorpsTooltip { get; set; } = "";
            public string PlaneState { get; set; } = "";
            public ItemSpec ItemSpec { get; set; } = new ItemSpec();
            public int ItemLevel { get; set; } = 0;
            public string ItemTooltip { get; set; } = "";
            public string Spec { get; set; } = "";
            public string SpecTooltip { get; set; } = "";
            public Color BackColor { get; set; } = Color.Empty;

            public string ItemLevelText => ItemLevel > 0 ? "★" + ItemLevel : "";

            public string PlaneText => ItemSpec.Empty ? PlaneState : (ItemLevelText + " " + ItemSpec.Name).Trim();

            public string ClipboardText()
            {
                var text = (AirBase + "\r\n" + AirBaseTooltip).Trim();
                text = (text + "\r\n" + AirCorps + "\r\n" + AirCorpsTooltip).Trim();
                text = (text + "\r\n" + PlaneText + "\r\n" + ItemTooltip).Trim();
                text = (text + "\r\n" + Spec + "\r\n" + SpecTooltip).Trim();
                return text;
            }

            public string AdjustedPlaneText(int adjust)
            {
                if (ItemSpec.Empty)
                    return PlaneState;

                var level = ItemLevelText;
                return StringTruncator.Truncate(ItemSpec.Name, level, adjust + (ItemSpec.IsAircraft ? 132 : 180)) + level;
            }
        }

        public void CreateData(Sniffer sniffer)
        {
            _data.Clear();
            if (sniffer.AirBase == null)
            {
                _data.Add(new Data {AirBase = "出撃海域選択画面に進むと更新されます。"});
                return;
            }

            foreach (var baseInfo in sniffer.AirBase.OrderBy(baseInfo => baseInfo.ListWindowSortKey))
            {
                var ifp = baseInfo.CalcInterceptionFighterPower(highAltitude: false);
                var specTooltip = "";
                if (baseInfo.HighAltitudeArea && baseInfo.CanIntercept())
                {
                    var highAltitudeModifier = baseInfo.HighAltitudeModifier();
                    var rawIfp = ifp;
                    ifp *= highAltitudeModifier;
                    if(ifp.Diff)
                        specTooltip = $"防空:{ifp.RangeString}\n";
                    specTooltip += $"高高度補正:{highAltitudeModifier}\n通常防空:{RangeString(rawIfp)}";
                }
                else if(ifp.Diff)
                {
                    specTooltip = ifp.RangeString;
                }

                _data.Add(new Data
                {
                    AirBase = $"{baseInfo.AreaNameShort} 整備Lv{baseInfo.MaintenanceLevel}",
                    Spec = $"防空:{ifp.Min}",
                    SpecTooltip = specTooltip
                });

                var i = 0;
                foreach (var airCorps in baseInfo.AirCorps)
                {
                    _data.Add(CreateAirCorpsRecord(airCorps, i++));
                    _data.AddRange(airCorps.Planes.Select(CreatePlaneRecord));
                }
            }
        }

        public static Data CreateAirCorpsRecord(AirBase.AirCorpsInfo airCorps, int number)
        {
            var corpsFp = airCorps.CalcFighterPower();
            string spec;
            string specTooltip;
            if (airCorps.Action == 2)
            {
                spec = $"制空:{corpsFp.Interception.Min} 距離:{airCorps.Distance.Total}";
                specTooltip = $"制空:{RangeString(corpsFp.Interception)} 距離:{airCorps.Distance}";
            }
            else
            {
                spec = $"制空:{corpsFp.AirCombat.Min} 距離:{airCorps.Distance.Total}";
                specTooltip = $"制空:{RangeString(corpsFp.AirCombat)} 距離:{airCorps.Distance}";
            }

            var cost = airCorps.CostForSortie;
            return new Data
            {
                AirCorps = $"{AirCorpsName(number)} {airCorps.ActionName} {airCorps.CondName}".Trim(),
                AirCorpsTooltip = $"出撃コスト:燃{cost[0]}弾{cost[1]}",
                Spec = spec,
                SpecTooltip = specTooltip,
                BackColor = airCorps.BackColor
            };
        }

        private static string AirCorpsName(int number) => new[] {"第一", "第二", "第三"}[number];

        public static Data CreatePlaneRecord(AirBase.PlaneInfo plane)
        {
            if (plane.Deploying)
            {
                var itemTooltip = "";
                var shipBomberPower = plane.ShipBomberPower;
                if (shipBomberPower.Count > 0)
                    itemTooltip = Cat(itemTooltip, "\n", string.Join("\n", shipBomberPower.Select(power => power.ToString())));
                var landBomberPower = plane.LandBomberPower;
                if (landBomberPower.BomberPower > 0)
                    itemTooltip = Cat(itemTooltip, "\n", landBomberPower.ToString());
                var aswBomberPower = plane.AswBomberPower;
                if (aswBomberPower.Min > 0)
                    itemTooltip = Cat(itemTooltip, "\n", $"対潜:{aswBomberPower.RangeString}");
                var steelCost = plane.Item.SteelCost;
                if (steelCost > 0)
                    itemTooltip = Cat(itemTooltip, "\n", $"鋼材消費:{steelCost}");

                return new Data
                {
                    ItemSpec = plane.Item.Spec,
                    ItemLevel = plane.Item.Level,
                    ItemTooltip = itemTooltip,
                    Spec = $"+{plane.Item.Alv} {plane.Item.OnSlot}/{plane.Item.MaxEq}"
                };
            }
            else
                return new Data {PlaneState = plane.StateName};
        }

        private static string RangeString(Range range) => range.Diff ? range.RangeString : range.Min.ToString();

        public CompositionPanel.Row CreateRow() => new Row().SetEventHandler();

        public int DataCount() => _data.Count;

        public void Assign(int i, CompositionPanel.Row row, ToolTip tooltip) => ((Row)row).Assign(_data[i], tooltip);

        private class Row : CompositionPanel.Row
        {
            public Label AirBase { get; set; } = new Label {Location = new Point(1, 2), AutoSize = true};
            public Label AirCorps { get; set; } = new Label {Location = new Point(10, 2), AutoSize = true};
            public Label Plane { get; set; } = new Label {Location = new Point(38, 2), AutoSize = true, Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top};
            public Label PlaneColor { get; set; } = new Label {Location = new Point(35, 2), Size = new Size(4, CompositionPanel.LabelHeight - 2)};
            public GrowLeftLabel Spec { get; set; } = new GrowLeftLabel {Location = new Point(217, 2), GrowLeft = true, Anchor = AnchorStyles.Right | AnchorStyles.Top};
            public string ClipboardText { get; set; } = "";

            public override void AdjustLocation() => Spec.AdjustLocation();

            public override bool MoveToTopLevel(string fn) => AirBase.Text.StartsWith(fn);

            public override bool MoveToSecondLevel(int id) => false;

            // 後ろの要素ほど奥に隠れるため順番が重要
            public override Control[] Controls => new Control[] {Spec, AirBase, AirCorps, Plane, PlaneColor};

            public override Control[] BackColorControls => new Control[] {Spec, AirBase, AirCorps, Plane, BackPanel};

            public Row SetEventHandler()
            {
                AirBase.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                AirCorps.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                Plane.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                Spec.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                return this;
            }

            public void Assign(Data data, ToolTip tooltip)
            {
                AirBase.Text = data.AirBase;
                tooltip.SetToolTip(AirBase, data.AirBaseTooltip);

                AirCorps.Text = data.AirCorps;
                tooltip.SetToolTip(AirCorps, data.AirCorpsTooltip);

                Plane.Text = data.AdjustedPlaneText(Scaler.DownWidth(BackPanel.Width) - ListForm.PanelWidth);
                tooltip.SetToolTip(Plane, data.ItemTooltip);
                PlaneColor.BackColor = data.ItemSpec.Empty ? DefaultBackColor : data.ItemSpec.Color;

                // 他の項目にかぶさるので少し余白が欲しいけど、何もないときは余白もいらない
                Spec.Text = (" " + data.Spec).TrimEnd();
                tooltip.SetToolTip(Spec, data.SpecTooltip);

                ClipboardText = data.ClipboardText();
                CurrentBackColor = data.BackColor == Color.Empty ? DefaultBackColor : data.BackColor;
            }
        }

#if DEBUG
        public List<Data> GetData => _data;
#endif
    }
}
