﻿// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using KancolleSniffer.Forms;
using KancolleSniffer.Model;
using StringUtil = KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.View.ListWindow
{
    public class FleetSource : CompositionPanel.ISource
    {
        private List<Data> _data = new List<Data>();

        public class Data
        {
            public string Fleet { get; set; } = "";
            public string FleetTooltip { get; set; } = "";
            public Color FleetForeColor { get; set; } = Control.DefaultForeColor;
            public string Ship { get; set; } = "";
            public string ShipTooltip { get; set; } = "";
            public int ShipId { get; set; } = -1;
            public ItemSpec ItemSpec { get; set; } = new ItemSpec();
            public int ItemLevel { get; set; } = 0;
            public string ItemTooltip { get; set; } = "";
            public string Spec { get; set; } = "";
            public string SpecTooltip { get; set; } = "";

            public string ItemLevelText => ItemLevel > 0 ? "★" + ItemLevel : "";

            public string EquipText => (ItemLevelText + " " + ItemSpec.Name).Trim();

            public string ClipboardText()
            {
                var text = (Fleet + "\r\n" + FleetTooltip).Trim();
                text = (text + "\r\n" + Ship + "\r\n" + ShipTooltip).Trim();
                text = (text + "\r\n" + EquipText + "\r\n" + ItemTooltip).Trim();
                text = (text + "\r\n" + Spec + "\r\n" + SpecTooltip).Trim();
                return text;
            }

            public string AdjustedEquipText(int adjust)
            {
                var level = ItemLevelText;
                return StringTruncator.Truncate(ItemSpec.Name, level, adjust + (ItemSpec.IsAircraft ? 132 : 180)) + level;
            }
        }

        public void CreateData(Sniffer sniffer)
        {
            _data.Clear();
            foreach (var fleet in sniffer.Fleets)
            {
                _data.Add(CreateFleetRecord(sniffer.Fleets, fleet.Number));
                foreach (var ship in fleet.Ships)
                {
                    _data.Add(CreateShipRecord(ship));
                    _data.AddRange(ship.Items.Select(CreateEquipRecord));
                }
            }
        }

        public static Data CreateFleetRecord(IReadOnlyList<Fleet> fleets, int number)
        {
            var fleet = fleets[number];
            var total = Total.Calc(fleet);
            return new Data
            {
                Fleet = FleetName(number) + " " + ShipStatus.SpeedName(fleet.Speed) + "   " + SpecTotal(total),
                FleetTooltip = Concat(FleetParams(total, fleet.DaihatsuBonus),
                    Concat(GetTp(fleets, number), GetRadarShips(fleet), " "), "\r\n"),
                FleetForeColor = total.HasNoneAircraft ? CUDColors.Red : Control.DefaultForeColor
            };
        }

        private static string FleetName(int number) => new[] {"第一", "第二", "第三", "第四"}[number];

        private static string GetTp(IReadOnlyList<Fleet> fleets, int number)
        {
            double tp = 0;
            if (fleets[0].IsCombined)
            {
                if (number == 0)
                    tp = fleets[0].TransportPoint + fleets[1].TransportPoint;
                else if (number == 1)
                    return "";
            }
            else
                tp = fleets[number].TransportPoint;
            return $"TP:S{(int)tp} A{(int)(tp * 0.7)}";
        }

        private static string GetRadarShips(Fleet fleet) => HideIfZero("電探:", fleet.RadarShips, "隻");

        private static string SpecTotal(Total total) => $"火{total.ViewFirePower} 空{total.ViewAntiAir} 潜{total.ViewAsw} 索{total.ViewLoS}";

        private static string FleetParams(Total total, double daihatsuBonus)
            => (total.HasNoneAircraft ? "搭載数0の機体があります\r\n遠征成功条件を満たせてない可能性があります\r\n" : "") +
                $"計: Lv{total.Level}" + HideIfZero(" ド", total.Drum) + HideIfZero("(", total.DrumShips, "隻)") +
                HideIfZero(" 大", daihatsuBonus, "%") + "\r\n" +
                $"戦闘:燃{total.Fuel / 5}弾{total.Bull / 5} 支援:燃{total.Fuel / 2}弾{(int)(total.Bull * 0.8)}";

        private class Total
        {
            public int Drum;
            public int DrumShips;
            public int Level;
            public int FirePower;
            public int Asw;
            public int AntiAir;
            public int LoS;
            public int Fuel;
            public int Bull;
            public bool HasNoneAircraft;

            public static Total Calc(Fleet fleet) => fleet.Ships.Aggregate(new Total(), (sum, next) => sum.Add(next));

            public Total Add(ShipStatus s)
            {
                var drum = s.Items.Count(item => item.Spec.IsDrum);
                DrumShips += (drum > 0 ? 1 : 0);
                Drum += drum;
                Level += s.Level;
                FirePower += s.MissionFirepower;
                Asw += s.MissionAsw;
                AntiAir += s.MissionAntiAir;
                LoS += s.MissionLoS;
                Fuel += s.EffectiveFuelMax;
                Bull += s.EffectiveBullMax;
                HasNoneAircraft |= s.HasNoneAircraft;
                return this;
            }

            // 遠征時の装備改修ボーナスは小数第一位まで有効なため、10倍の値の整数値で管理してるのを元に戻す
            public static int CutOverFlow(int value) => Math.Min(value / 10, 999);

            public int ViewFirePower => CutOverFlow(FirePower);

            public int ViewAntiAir => CutOverFlow(AntiAir);

            public int ViewAsw => CutOverFlow(Asw);

            public int ViewLoS => CutOverFlow(LoS);
        }

        public static Data CreateShipRecord(ShipStatus ship)
        {
            var effectiveFirepower = ship.EffectiveFirepower;
            var effectiveAccuracy = ship.EffectiveAccuracy;
            var enableAsw = ship.EnableAsw;
            var enableOpeningAsw = ship.EnableOpeningAsw;
            var effectiveAsw = enableAsw || enableOpeningAsw ? ship.EffectiveAsw : 0;

            var spec = StringUtil.Cat(HideIfZero("砲", effectiveFirepower, $" 命{StringUtil.ToS(effectiveAccuracy)}"), " ",
                HideIfZero($"潜{(ship.UnresolvedAsw ? "?" : "")}", effectiveAsw, (enableAsw && enableOpeningAsw ? "*" : enableOpeningAsw ? "~" : "")));
            var specTooltip = StringUtil.Cat(HideIfZero("雷", ship.EffectiveTorpedo), " ", HideIfZero("夜", ship.NightBattlePower));
            if (String.IsNullOrWhiteSpace(spec))
            {
                spec = specTooltip;
                specTooltip = "";
            }
            specTooltip = StringUtil.Cat(specTooltip, "\n", HideIfZero("徹甲弾:", effectiveFirepower * ship.APShellFirepowerModifier, $" 命中:{StringUtil.ToS(effectiveAccuracy * ship.APShellAccuracyModifier)}"));
            specTooltip = StringUtil.Cat(specTooltip, "\n", HideIfZero("砲撃支援:", ship.SupportFirepower, $" 命中:{StringUtil.ToS(ship.SupportAccuracy)}"));

            return new Data
            {
                Ship = $"{(ship.Escaped ? "[避]" : "")}{ship.Name} Lv{ship.Level}",
                ShipTooltip = $"燃{ship.EffectiveFuelMax} 弾{ship.EffectiveBullMax}",
                ShipId = ship.Id,
                Spec = spec,
                SpecTooltip = specTooltip
            };
        }

        public static Data CreateEquipRecord(ItemStatus item)
        {
            var data = new Data {ItemSpec = item.Spec, ItemLevel = item.Level};
            if (item.Spec.IsAircraft)
                data.Spec = $"+{item.Alv} {item.OnSlot}/{item.MaxEq}";

            var itemTooltip = "";
            var bomberPower = item.CalcBomberPower();
            if (bomberPower.Length > 0)
                itemTooltip = $"航空戦:{string.Join("/", bomberPower.Select(power => StringUtil.ToS(power)))}";
            var airSupport = item.CalcAirSupportPower();
            if (airSupport.Length > 0)
                itemTooltip = StringUtil.Cat(itemTooltip, "\n", $"航空支援:{string.Join("/", airSupport.Select(power => StringUtil.ToS(power)))}");
            var aswSupport = item.CalcAswSupportPower();
            if (aswSupport.Length > 0)
                itemTooltip = StringUtil.Cat(itemTooltip, "\n", $"対潜支援:{string.Join("/", aswSupport.Select(power => StringUtil.ToS(power)))}");
            var steelCost = item.SteelCost;
            if (steelCost > 0)
                itemTooltip = StringUtil.Cat(itemTooltip, "\n", $"鋼材消費:{steelCost}");
            data.ItemTooltip = itemTooltip;
            return data;
        }

        private static string Concat(string a, string b, string separator) => a == "" ? b : b == "" ? a : a + separator + b;

        private static string HideIfZero(string name, double value, string suffix = "") => value > 0 ? name + StringUtil.ToS(value) + suffix : "";

        private static string HideIfZero(string name, int value, string suffix = "") => value > 0 ? name + value + suffix : "";

        public CompositionPanel.Row CreateRow() => new Row().SetEventHandler();

        public int DataCount() => _data.Count;

        public void Assign(int i, CompositionPanel.Row row, ToolTip tooltip) => ((Row)row).Assign(_data[i], tooltip);

        private class Row : CompositionPanel.Row
        {
            public Label Fleet { get; set; } = new Label {Location = new Point(1, 2), AutoSize = true};
            // 海外艦の名前が間延びしないよう、ただのLabelではなくShipLabel.Nameを使う
            public ShipLabel.Name Ship { get; set; } = new ShipLabel.Name(new Point(10, 2), ShipNameWidth.Max);
            public Label Equip { get; set; } = new Label {Location = new Point(38, 2), AutoSize = true, Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top};
            public Label EquipColor { get; set; } = new Label {Location = new Point(35, 2), Size = new Size(4, CompositionPanel.LabelHeight - 2)};
            public GrowLeftLabel Spec { get; set; } = new GrowLeftLabel {Location = new Point(217, 2), GrowLeft = true, Anchor = AnchorStyles.Right | AnchorStyles.Top};
            public string ClipboardText { get; set; } = "";
            public int ShipId { get; set; } = -1;

            public override void AdjustLocation() => Spec.AdjustLocation();

            public override bool MoveToTopLevel(string fn) => Fleet.Text.StartsWith(fn);

            public override bool MoveToSecondLevel(int id) => ShipId == id;

            // 後ろの要素ほど奥に隠れるため順番が重要
            public override Control[] Controls => new Control[] {Spec, Fleet, Ship, Equip, EquipColor};

            public override Control[] BackColorControls => new Control[] {Spec, Fleet, Ship, Equip, BackPanel};

            public Row SetEventHandler()
            {
                Fleet.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                Ship.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                Equip.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                Spec.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
                return this;
            }

            public void Assign(Data data, ToolTip tooltip)
            {
                Fleet.Text = data.Fleet;
                tooltip.SetToolTip(Fleet, data.FleetTooltip);
                Fleet.ForeColor = data.FleetForeColor;

                Ship.SetName(data.Ship);
                tooltip.SetToolTip(Ship, data.ShipTooltip);
                ShipId = data.ShipId;

                Equip.Text = data.AdjustedEquipText(Scaler.DownWidth(BackPanel.Width) - ListForm.PanelWidth);
                tooltip.SetToolTip(Equip, data.ItemTooltip);
                EquipColor.BackColor = data.ItemSpec.Empty ? DefaultBackColor : data.ItemSpec.Color;

                // 他の項目にかぶさるので少し余白が欲しいけど、何もないときは余白もいらない
                Spec.Text = (" " + data.Spec).TrimEnd();
                tooltip.SetToolTip(Spec, data.SpecTooltip);

                ClipboardText = data.ClipboardText();
            }
        }

#if DEBUG
        public List<Data> GetData => _data;
#endif
    }
}
