﻿// Copyright (C) 2020 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Drawing;
using System.Windows.Forms;
using KancolleSniffer.Model;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.View.MainWindow
{
    public class FighterPower
    {
        private readonly Label _fighterPower = new Label
        {
            Location = new Point(28, 117),
            Size = new Size(29, 12),
            Text = "0",
            TextAlign = ContentAlignment.MiddleRight
        };

        private readonly Label _fighterPowerCaption = new Label
        {
            AutoSize = true,
            Location = new Point(2, 117),
            Text = "制空"
        };

        private readonly ShipInfoPanel _parent;

        public UpdateContext Context { private get; set; }

        public FighterPower(ShipInfoPanel parent)
        {
            _parent = parent;
            parent.Controls.AddRange(new Control[] {_fighterPowerCaption, _fighterPower});
        }

        public void Reset()
        {
            _fighterPower.ForeColor = Control.DefaultForeColor;
            _fighterPowerCaption.Text = "制空";
        }

        public void UpdateFighterPower()
        {
            UpdateFighterPower(IsCombinedFighterPower);
        }

        private bool IsCombinedFighterPower => _parent.CombinedFleet &&
                                               (Context.Sniffer.Battle.BattleState == BattleState.None ||
                                                Context.Sniffer.Battle.EnemyIsCombined);

        private void UpdateFighterPower(bool combined)
        {
            var fleets = Context.Sniffer.Fleets;
            var currentFleet = fleets[_parent.CurrentFleet];
            var fp = combined
                ? fleets[0].FighterPower + fleets[1].FighterPower
                : currentFleet.FighterPower;
            var minfp = fp.Min.ToString();
            var contactRate = Context.Sniffer.IsCombinedFleet && _parent.CurrentFleet == 0
                ? AirContactRates.Calc(fleets[0], fleets[1], combined)
                : AirContactRates.Calc(currentFleet);

            var text = $"制空: {(fp.Diff ? fp.RangeString : minfp)}";
            text = Cat(text, "\n", contactRate.ToString());
            text = Cat(text, "\n", $"航空支援制空: {currentFleet.AirSupportFighter}");
            text = Cat(text, "\n", $"対潜支援制空: {currentFleet.AswSupportFighter}");

            _fighterPower.Text = minfp;
            _parent.ToolTip.SetToolTip(_fighterPower, text);
            _parent.ToolTip.SetToolTip(_fighterPowerCaption, text);
        }

        public void UpdateBattleFighterPower()
        {
            var battle = Context.Sniffer.Battle;
            _fighterPower.ForeColor = AirControlLevelColor(battle);
            _fighterPowerCaption.Text = AirControlLevelString(battle);
            if (battle.BattleState == BattleState.AirRaid)
            {
                UpdateAirRaidFighterPower();
            }
            else
            {
                UpdateFighterPower(Context.Sniffer.IsCombinedFleet && battle.EnemyIsCombined);
            }
        }

        private void UpdateAirRaidFighterPower()
        {
            var fp = Context.Sniffer.Battle.FighterPower;
            var contactRate = Context.Sniffer.Battle.AirContactRates;
            var minfp = fp.Min.ToString();
            var text = $"制空: {(fp.Diff ? fp.RangeString : minfp)}";
            text = Cat(text, "\n", contactRate.ToString());

            _fighterPower.Text = minfp;
            _parent.ToolTip.SetToolTip(_fighterPower, text);
            _parent.ToolTip.SetToolTip(_fighterPowerCaption, text);
        }

        private static Color AirControlLevelColor(BattleInfo battle)
        {
            return AirBattleResult.AirControlLevelColors[battle.BattleState == BattleState.Night ? AirBattleResult.AirControlLevelNull : battle.AirControlLevel];
        }

        private static string AirControlLevelString(BattleInfo battle)
        {
            return AirBattleResult.AirControlLevelNamesForMain[battle.BattleState == BattleState.Night ? AirBattleResult.AirControlLevelNull : battle.AirControlLevel];
        }
    }
}