﻿// Copyright (C) 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;

namespace KancolleSniffer.Model
{
    public class ShipMaster
    {
        private readonly Dictionary<int, ShipSpec> _shipSpecs = new Dictionary<int, ShipSpec>();
        public AdditionalData AdditionalData { get; set; }

        public void Inspect(dynamic json)
        {
            var stype = new Dictionary<double, string>();
            foreach (var entry in json.api_mst_stype)
                stype[entry.api_id] = entry.api_name;
            stype[8] = "巡洋戦艦";

            var upgradeItems = new Dictionary<int, Dictionary<int, int>>();
            foreach (var entry in json.api_mst_shipupgrade)
            {
                if (!entry.api_current_ship_id())
                    continue;
                var id = (int)entry.api_current_ship_id;
                if (id < 1)
                    continue;

                var items = new Dictionary<int, int>();
                var drawing = (int)entry.api_drawing_count;
                if (drawing > 0)
                    items[58] = drawing;
                var catapult = entry.api_catapult_count() ? (int)entry.api_catapult_count : 0;
                if (catapult > 0)
                    items[65] = catapult;
                var report = entry.api_report_count() ? (int)entry.api_report_count : 0;
                if (report > 0)
                    items[78] = report;
                var aviationMat = entry.api_aviation_mat_count() ? (int)entry.api_aviation_mat_count : 0;
                if (aviationMat > 0)
                    items[77] = aviationMat;
                var armsMat = entry.api_arms_mat_count() ? (int)entry.api_arms_mat_count : 0;
                if (armsMat > 0)
                    items[94] = armsMat;
                upgradeItems[id] = items;
            }
            // 武蔵改二 新型砲熕兵装資材3個
            if (upgradeItems.ContainsKey(148))
                upgradeItems[148][75] = 3;
            // 金剛改二丙 新型砲熕兵装資材2個
            if (upgradeItems.ContainsKey(149))
                upgradeItems[149][75] = 2;
            // 比叡改二丙 新型砲熕兵装資材2個
            if (upgradeItems.ContainsKey(150))
                upgradeItems[150][75] = 2;
            // 磯波改二 新型砲熕兵装資材1個
            if (upgradeItems.ContainsKey(206))
                upgradeItems[206][75] = 1;
            // 大和改二 新型砲熕兵装資材3個
            if (upgradeItems.ContainsKey(136))
                upgradeItems[136][75] = 3;

            AdditionalData.LoadEnemySlot();
            AdditionalData.LoadNumEquips();
            AdditionalData.LoadMinAsw();
            AdditionalData.LoadTbBonus();
            foreach (var entry in json.api_mst_ship)
            {
                var id = (int)entry.api_id;
                var shipSpec = _shipSpecs[id] = new ShipSpec
                {
                    Id = id,
                    SortId = entry.api_sort_id() ? (int)entry.api_sort_id : 0,
                    Name = ShipName(entry),
                    Yomi = entry.api_yomi,
                    MinFirepower = entry.api_houg() ? (int)entry.api_houg[0] : 0,
                    MinTorpedo = entry.api_raig() ? (int)entry.api_raig[0] : 0,
                    MinAntiAir = entry.api_tyku() ? (int)entry.api_tyku[0] : 0,
                    MinArmor = entry.api_souk() ? (int)entry.api_souk[0] : 0,
                    FuelMax = entry.api_fuel_max() ? (int)entry.api_fuel_max : 0,
                    BullMax = entry.api_bull_max() ? (int)entry.api_bull_max : 0,
                    SlotNum = (int)entry.api_slot_num,
                    ShipType = (int)entry.api_stype,
                    ShipTypeName = stype[entry.api_stype],
                    ShipClass = entry.api_ctype() ? (int)entry.api_ctype : 0,
                    UpgradeItems = upgradeItems.TryGetValue(id, out var items) ? items : new Dictionary<int, int>()
                };
                if (entry.api_afterlv())
                {
                    shipSpec.Remodel.Level = (int)entry.api_afterlv;
                    shipSpec.Remodel.After = int.Parse(entry.api_aftershipid);
                }
                shipSpec.GetMinAsw = entry.api_tais()
                    ? (Func<int>)(() => (int)entry.api_tais[0])
                    : () => AdditionalData.MinAsw(shipSpec.Id);
                shipSpec.GetMaxEq = entry.api_maxeq()
                    ? (Func<int[]>)(() => entry.api_maxeq)
                    : () => AdditionalData.EnemySlot(shipSpec.Id);
                shipSpec.GetNumEquips = () => AdditionalData.NumEquips(shipSpec.Id);
                shipSpec.SetNumEquips = num => AdditionalData.RecordNumEquips(shipSpec.Id, shipSpec.Name,num);
            }
            _shipSpecs[-1] = new ShipSpec();
            SetRemodelBaseAndStep();
        }

        // 深海棲艦の名前にelite/flagshipを付ける
        private string ShipName(dynamic json)
        {
            var name = json.api_name;
            var flagship = json.api_yomi;
            if (!ShipSpec.IsEnemyId((int)json.api_id) || flagship == "-" || flagship == "")
                return name;
            return name + "(" + flagship + ")";
        }

        public ShipSpec GetSpec(int id) => _shipSpecs.TryGetValue(id, out var spec) ? spec : new ShipSpec();

        private void SetRemodelBaseAndStep()
        {
            // 改造後のデータをマーク
            foreach (var spec in _shipSpecs.Values)
            {
                // 宗谷は id:699(特務艦) -> id:645(灯台補給船) -> id:650(南極観測船) -> id:699(特務艦) と改造状態がループするため、初期状態を特定できない。
                // また、艦隊晒しページでは id:645(灯台補給船) を初期状態と判定してしまっており、修正の目途は立たないようなので、ここでも id:645 を初期状態として扱う。
                // spec.Remodel.Base は艦隊晒しテキストの出力でしか使われてないため、当面は問題ないはず。
                if (spec.Remodel.After == 0 || spec.Remodel.After == 645)
                    continue;
                _shipSpecs[spec.Remodel.After].Remodel.Base = 1;
            }
            foreach (var spec in _shipSpecs.Values)
            {
                if (spec.Remodel.Base != 0)
                    continue;
                var step = 0;
                var hash = new HashSet<int> {spec.Id};
                var s = spec;
                s.Remodel.Base = spec.Id;
                while (s.Remodel.After != 0)
                {
                    s.Remodel.Step = ++step;
                    if (!hash.Add(s.Remodel.After))
                        break;
                    s = _shipSpecs[s.Remodel.After];
                    s.Remodel.Base = spec.Id;
                }
            }
        }

#if DEBUG
        /// <summary>
        /// テスト用
        /// </summary>
        /// <param name="id"></param>
        public void InjectSpec(int id) => _shipSpecs[id] = new ShipSpec {Id = id};
#endif
    }
}