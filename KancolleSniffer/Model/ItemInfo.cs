﻿// Copyright (C) 2013, 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;

namespace KancolleSniffer.Model
{
    public class ItemInfo
    {
        private readonly ItemMaster _itemMaster;
        private readonly ItemInventory _itemInventory;
        public readonly List<ItemStatus[]> PresetItems = new List<ItemStatus[]>();

        private readonly Dictionary<int, int> _useItem = new Dictionary<int, int>();
        private readonly Dictionary<int, int> _remodelSlotReqUseitem = new Dictionary<int, int>();
        private int fcoin;

        private readonly int fcoinId = 44;
        private readonly int exslotId = 64;
        private readonly int ringId = 55;
        private readonly int dockOpeningKeyId = 49;
        private readonly int airBaseExpandId = 73;
        private readonly int anchorageRepairId = 91;
        private readonly int submarineSpecialId = 95;
        private readonly int furnitureFairyId = 52;
        private readonly int pumpkinId = 96;
        private readonly string remodelSlotReqUseitemIdKey = "api_req_useitem_id";
        private readonly string remodelSlotReqUseitemNumKey = "api_req_useitem_num";

        private Dictionary<int, int> _payItem = null;
        private readonly Dictionary<int, PayItemContent[]> _payItemContents = new Dictionary<int, PayItemContent[]>{
            // 応急修理要員
            {11, new PayItemContent[]{new PayItemContent{ItemId = 42, Count = 1}}},
            // ダメコン特盛セット
            {12, new PayItemContent[]{new PayItemContent{ItemId = 42, Count = 3}, new PayItemContent{ItemId = 43, Count = 2}}},
            // 応急修理女神
            {14, new PayItemContent[]{new PayItemContent{ItemId = 43, Count = 1}}},
            // 戦闘糧食
            {24, new PayItemContent[]{new PayItemContent{ItemId = 145, Count = 3}}},
            // 洋上補給
            {25, new PayItemContent[]{new PayItemContent{ItemId = 146, Count = 2}}},
        };

        public AlarmCounter Counter { get; }

        class PayItemContent
        {
            public int ItemId { get; set; }
            public int Count { get; set; }
        }

        public ItemInfo(ItemMaster itemMaster, ItemInventory itemInventory)
        {
            _itemMaster = itemMaster;
            _itemInventory = itemInventory;
            Counter = new AlarmCounter(() => _itemInventory.Count) {Margin = 5};
        }

        public void InspectBasic(dynamic json)
        {
            _itemInventory.ClearInflated();
            Counter.Max = (int)json.api_max_slotitem;
            fcoin = (int)json.api_fcoin;
        }

        public void InspectMaster(dynamic json)
        {
            _itemMaster.InspectMaster(json);
        }

        public void InspectSlotItem(dynamic json, bool full = false)
        {
            if (!json.IsArray)
                json = new[] {json};
            ItemInventory backupInventory;
            if (full)
            {
                backupInventory = new ItemInventory();
                backupInventory.Add(_itemInventory.AllItems);
                _itemInventory.Clear();
            }
            else
            {
                backupInventory = _itemInventory;
            }

            foreach (var entry in json)
            {
                var id = (int)entry.api_id;
                if (id == -1)
                    continue;
                var item = _itemInventory[id] = backupInventory.Contains(id) ? backupInventory[id] : new ItemStatus(id);
                item.Spec = _itemMaster[(int)entry.api_slotitem_id];
                item.Level = entry.api_level() ? (int)entry.api_level : 0;
                item.Alv = entry.api_alv() ? (int)entry.api_alv : 0;
                item.Locked = entry.api_locked() && (int)entry.api_locked == 1;
            }
        }

        public void InspectUseItem(dynamic json)
        {
            _useItem.Clear();
            foreach (var entry in json)
            {
                _useItem[(int)entry.api_id] = (int)entry.api_count;
            }
        }

        public void InspectPayItem(dynamic json)
        {
            // アイテム一覧を開かないと取れないので、ログイン後まだ開いてない状態を区別したい
            if (_payItem == null)
                _payItem = new Dictionary<int, int>();

            _payItem.Clear();
            foreach (var entry in json)
            {
                if (int.TryParse(entry.api_payitem_id, out int payitemId))
                {
                    _payItem[payitemId] = (int)entry.api_count;
                }
            }
        }

        public void InspectPayItemUse(string request, dynamic data)
        {
            if (_payItem == null)
                return;
            if (data.api_caution_flag != 0)
                return;

            var values = HttpUtility.ParseQueryString(request);
            if (int.TryParse(values["api_payitem_id"], out int payitemId) && _payItem.ContainsKey(payitemId))
            {
                _payItem[payitemId]--;
                if (_payItem[payitemId] < 1)
                {
                    _payItem.Remove(payitemId);
                }
            }
        }

        public void InspectCreateItem(dynamic json)
        {

            if (json.api_slot_item())
            {
                InspectSlotItem(json.api_slot_item);
            }
            else if (json.api_get_items())
            {
                InspectSlotItem(json.api_get_items);
            }
        }

        public void InspectGetShip(dynamic json)
        {
            if (json.api_slotitem == null) // まるゆにはスロットがない
                return;
            InspectSlotItem(json.api_slotitem);
        }

        public void InspectDestroyItem(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            DeleteItems(values["api_slotitem_ids"].Split(',').Select(int.Parse));
        }

        public void InspectRemodelSlotlistDetail(dynamic json)
        {
            _remodelSlotReqUseitem.Clear();
            foreach (KeyValuePair<string, dynamic> kv in json)
            {
                if (kv.Key.StartsWith(remodelSlotReqUseitemIdKey))
                {
                    var numKey = remodelSlotReqUseitemNumKey + kv.Key.Substring(remodelSlotReqUseitemIdKey.Length);
                    _remodelSlotReqUseitem[(int)kv.Value] = (int)json[numKey];
                }
            }
        }

        public void InspectRemodelSlot(dynamic json)
        {
            if ((int)json.api_remodel_flag == 1)
            {
                foreach (KeyValuePair<int, int> item in _remodelSlotReqUseitem)
                {
                    ConsumeUseItem(item.Key, item.Value);
                }
            }
            if (json.api_after_slot())
                InspectSlotItem(json.api_after_slot);
            if (!json.api_use_slot_id())
                return;
            DeleteItems((int[])json.api_use_slot_id);
        }

        private void DeleteItems(IEnumerable<int> ids)
        {
            _itemInventory.Remove(ids);
        }

        public void InspectPresetSlot(dynamic data)
        {
            for (var i = PresetItems.Count; i < data.api_max_num; i++)
                PresetItems.Add(new ItemStatus[0]);
            foreach (var preset in data.api_preset_items)
            {
                var items = new List<ItemStatus>();
                if (preset.api_slot_item())
                    foreach (var item in preset.api_slot_item)
                        items.Add(new ItemStatus{Spec = _itemMaster[(int)item.api_id], Level = (int)item.api_level});
                if (preset.api_slot_item_ex())
                    items.Add(new ItemStatus{Spec = _itemMaster[(int)preset.api_slot_item_ex.api_id], Level = (int)preset.api_slot_item_ex.api_level});
                PresetItems[(int)preset.api_preset_no - 1] = items.ToArray();
            }
        }

        public void InspectPresetSlotRegister(string request, ItemStatus[] items)
        {
            var values = HttpUtility.ParseQueryString(request);
            if (int.TryParse(values["api_preset_id"], out int id))
                PresetItems[id - 1] = items;
        }

        public void InspectPresetSlotDelete(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            if (int.TryParse(values["api_preset_id"], out int id))
                PresetItems[id - 1] = new ItemStatus[0];
        }

        public void InspectPresetSlotExpand(dynamic data)
        {
            for (var i = PresetItems.Count; i < data.api_max_num; i++)
                PresetItems.Add(new ItemStatus[0]);
            ConsumeUseItem(dockOpeningKeyId);
        }

        public void InspectRemodeling(Dictionary<int, int> upgradeItems)
        {
            foreach (KeyValuePair<int, int> item in upgradeItems)
            {
                ConsumeUseItem(item.Key, item.Value);
            }
        }

        public void InspectOpenExslot()
        {
            ConsumeUseItem(exslotId);
        }

        public void InspectMarriage()
        {
            ConsumeUseItem(ringId);
        }

        public void InspectPresetExpand()
        {
            ConsumeUseItem(dockOpeningKeyId);
        }

        public void InspectExpandBase()
        {
            ConsumeUseItem(airBaseExpandId);
        }

        public void InspectExpandMaintenanceLevel()
        {
            ConsumeUseItem(airBaseExpandId);
        }

        public void InspectAnchorageRepair()
        {
            ConsumeUseItem(anchorageRepairId);
        }

        public void InspectFurnitureBuy(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            if (int.TryParse(values["api_type"], out int type) && int.TryParse(values["api_no"], out int no))
            {
                var price = _itemMaster.GetFurniturePrice(type, no);
                if (price < 2000)
                {
                    fcoin -= price;
                }
                else if (price < 20000)
                {
                    ConsumeUseItem(furnitureFairyId);
                    fcoin -= price;
                }
                else if (price < 100000)
                {
                    fcoin -= price;
                }
                else
                {
                    if (values["api_discount_flag"] != null)
                    {
                        ConsumeUseItem(furnitureFairyId);
                        price = price % 100000 / 10;
                    }
                    fcoin -= price;
                }
            }
        }

        public void InspectBattleResult(bool submarineSpecial)
        {
            if (submarineSpecial)
                ConsumeUseItem(submarineSpecialId);
        }

        public void InspectPowerUp(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            if ((values["api_limited_feed_type"] ?? "0") == "1")
                ConsumeUseItem(pumpkinId);
        }

        private void ConsumeUseItem(int id, int num = 1)
        {
            if (_useItem.ContainsKey(id))
            {
                _useItem[id] -= num;
                if (_useItem[id] < 1)
                {
                    _useItem.Remove(id);
                }
            }
        }

        public ItemSpec GetSpecByItemId(int id) => _itemMaster[id];

        public string GetName(int id) => GetStatus(id).Spec.Name;

        public ItemStatus GetStatus(int id)
        {
            return _itemInventory[id];
        }

        public void ClearHolder()
        {
            foreach (var item in _itemInventory.AllItems)
                item.Holder = new ShipStatus();
        }

        public IEnumerable<ItemStatus> SlotItems => _itemInventory.AllItems;

        public ItemStatus[] ItemList => _itemInventory.AllItems.Concat(PayItemList).ToArray();

        public string GetUseItemName(int id) => _itemMaster.GetUseItemName(id);

        private IEnumerable<ItemStatus> PayItemList
        {
            get
            {
                if (_payItem == null)
                    return Enumerable.Empty<ItemStatus>();

                return _payItem.Where(item => _payItemContents.ContainsKey(item.Key)).
                    SelectMany(item => _payItemContents[item.Key].SelectMany(content =>
                    Enumerable.Repeat(new ItemStatus
                    {
                        Spec = GetSpecByItemId(content.ItemId),
                        Holder = new ShipStatus(-2)
                    }, item.Value * content.Count)));
            }
        }

        public IEnumerable<MaterialItem> UseItems => UseItemsSeed.Concat(
                                                     _useItem.OrderBy(item => item.Key).Select(item => new MaterialItem{Id = item.Key, Type = MaterialItem.UseItemLabel, Name = _itemMaster.GetUseItemName(item.Key), Count = item.Value}));

        public IEnumerable<MaterialItem> PayItems => _payItem == null ? null :
                                                     _payItem.OrderBy(item => item.Key).Select(item => new MaterialItem{Id = item.Key, Type = MaterialItem.PayItemLabel, Name = _itemMaster.GetPayItemName(item.Key), Count = item.Value});

        IEnumerable<MaterialItem> UseItemsSeed
        {
            get
            {
                var seed = new List<MaterialItem>();
                if (fcoin > 0)
                {
                    seed.Add(new MaterialItem{Id = fcoinId, Type = MaterialItem.UseItemLabel, Name = _itemMaster.GetUseItemName(fcoinId), Count = fcoin});
                }
                return seed;
            }
        }

#if DEBUG
        public Dictionary<int, int> RemodelSlotReqUseitem => _remodelSlotReqUseitem;
#endif
    }

    public class MaterialItem
    {
        public static string UseItemLabel = "保有アイテム";
        public static string PayItemLabel = "購入済みアイテム";

        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }

        public override string ToString()
        {
            return $"{Name} x{Count}";
        }

        public string ToCsv()
        {
            return $"{Type},{Name},,,{Count}";
        }
    }
}