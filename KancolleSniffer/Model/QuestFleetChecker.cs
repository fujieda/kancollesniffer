// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using static KancolleSniffer.Model.ShipTypeCode;
using static KancolleSniffer.Model.ShipClassCode;

namespace KancolleSniffer.Model
{
    public class QuestFleetChecker
    {
		public ShipSpec[] Specs { get; }
		public NameChecker Names { get; }
		public int[] Ids { get; }
		public int[] Types { get; }
		public int[] Classes { get; }
		public ShipSpec Flagship { get; }
		public int FlagshipType { get; }
		public int Deck { get; set; }

		public class NameChecker
		{
			private readonly string[] _names;

			public NameChecker(ShipSpec[] specs)
			{
				_names = specs.Select(spec => spec.Name).ToArray();
			}

			public bool Contains(string demand)
			{
				return _names.Any(name => name.StartsWith(demand));
			}

			public int Count(params string[] demands)
			{
				return demands.Sum(demand => _names.Count(name => name.StartsWith(demand)));
			}
		}

		public QuestFleetChecker(BattleInfo battleInfo) : this(battleInfo.Result?.Friend.Main ?? new ShipStatus[0], battleInfo.Deck)
		{
		}

		public QuestFleetChecker(ShipStatus[] ships, int deck)
		{
			Specs = ships.Where(s => !s.Dropout).Select(ship => ship.Spec).ToArray();
			Names = new NameChecker(Specs);
			Ids = Specs.Select(spec => spec.Id).ToArray();
			Types = Specs.Select(spec => spec.ShipType).ToArray();
			Classes = Specs.Select(spec => spec.ShipClass).ToArray();
			Flagship = Specs.FirstOrDefault() ?? new ShipSpec();
			FlagshipType = Types.FirstOrDefault();
			Deck = deck;
		}

        public bool Check(int id)
        {
            switch (id)
            {
                case 249:
                    return Names.Count("妙高", "那智", "羽黒") == 3;
                case 257:
                    return Flagship.ShipType == CL &&
                           Specs.Count(spec => spec.ShipType == CL) < 4 &&
                           Specs.Any(spec => spec.ShipType == DD) &&
                           Specs.All(spec => spec.ShipType == DD || spec.ShipType == CL);
                case 259:
                    return Types.Count(type => type == 3) > 0 && Classes.Count(c => new[]
                    {
                        2, // 伊勢型
                        19, // 長門型
                        26, // 扶桑型
                        37 // 大和型
                    }.Contains(c)) == 3;
                case 264:
                    return Types.Count(type => type == 2) >= 2 &&
                           Specs.Count(spec => spec.IsAircraftCarrier) >= 2;
                case 266:
                    return FlagshipType == 2 &&
                           Types.OrderBy(x => x).SequenceEqual(new[] {2, 2, 2, 2, 3, 5});
                case 280:
                case 284:
                    return Types.Count(type => type == 1 || type == 2) >= 3 &&
                           Types.Intersect(new[] {3, 4, 7, 21}).Any();
                case 840: // 【節分任務】節分作戦二〇二三 軽空軽巡級旗艦 駆逐海防x3
                    return (Flagship.IsLightCruiserClass || Flagship.ShipType == 7) &&
                           Specs.Count(spec => spec.IsEscortDestroyer) > 2;
                case 841: // 【節分任務】南西海域節分作戦二〇二三 水母航巡旗艦 旗艦と同種の随伴艦
                    return (Flagship.ShipType == 16 || Flagship.ShipType == 6) &&
                           Specs.Skip(1).Any(spec => spec.ShipType == Flagship.ShipType);
                case 843: // 【節分拡張任務】節分作戦二〇二三、全力出撃！ 戦艦2または空母2 大淀または球磨型
                    return Specs.Any(spec => spec.ShipClass == 4 || spec.ShipClass == 52) &&
                          (Specs.Count(spec => spec.IsBattleship) > 1 ||
                           Specs.Count(spec => spec.IsAircraftCarrier) > 1);
                case 854:
                    return Deck == 1;
                case 861:
                    return Types.Count(s => s == 10 || s == 22) == 2;
                case 862:
                    return Types.Count(s => s == 3) >= 2 && Types.Count(s => s == 16) >= 1;
                case 872:
                    return Deck == 1;
                case 873:
                    return Types.Count(type => type == 3) >= 1;
                case 875:
                    return Names.Contains("長波改二") &&
                           Names.Count("朝霜改", "高波改", "沖波改") > 0;
                case 888:
                    return Names.Count("鳥海", "青葉", "衣笠", "加古", "古鷹", "天龍", "夕張") >= 4;
                case 894:
                    return Specs.Any(spec => spec.IsAircraftCarrier);
                case 903:
                    return Flagship.Name.StartsWith("夕張改二") &&
                           (Names.Count("睦月", "如月", "弥生", "卯月", "菊月", "望月") >= 2 || Names.Contains("由良改二"));
                case 904:
                    return Names.Count("綾波改二", "敷波改二") == 2;
                case 905:
                    return Types.Count(type => type == 1) >= 3 && Types.Length <= 5;
                case 912:
                    return Flagship.Name.StartsWith("明石") && Types.Count(type => type == 2) >= 3;
                case 914:
                    return Types.Count(type => type == 5) >= 3 && Types.Count(type => type == 2) >= 1;
                case 928:
                    return Names.Count("羽黒", "足柄", "妙高", "高雄", "神風") >= 2;
                case 944:
                    return new[] {2, 5}.Contains(FlagshipType) &&
                           Types.Skip(1).Count(type => new[] {1, 2}.Contains(type)) >= 3;
                case 945:
                    return new[] {2, 3, 21}.Contains(FlagshipType) &&
                           Types.Skip(1).Count(type => new[] {1, 2}.Contains(type)) >= 3;
                case 946:
                    return Flagship.IsAircraftCarrier &&
                           Types.Count(type => new[] {5, 6}.Contains(type)) >= 2;
                case 947:
                    return Types.Count(type => type == 7) >= 2;
                case 948:
                    return Deck == 1 && Flagship.IsAircraftCarrier;
                case 953:
                    return (Flagship.ShipType == CL || Flagship.ShipType == AV) &&
                           Specs.Count(spec => spec.ShipType == DD) > 2;
                case 954:
                    return (Flagship.ShipType == CA || Flagship.ShipType == CAV) &&
                           Specs.Skip(1).Any(spec => spec.ShipType == CA || spec.ShipType == CAV);
                case 955:
                    return Specs.Count(spec => spec.ShipType == FBB || spec.ShipType == BB ||  spec.ShipType == BBV) > 1 &&
                           Specs.Any(spec => spec.ShipClass == Agano || spec.ShipClass == Oyodo);
                case 973:
                    return Specs.Count(spec => spec.Nation == 32 || spec.Nation == 33) > 2 &&
                           Specs.All(spec => !spec.IsAircraftCarrier);
                case 975:
                    return Ids.Contains(666) && Ids.Contains(647) && Ids.Contains(195) && Ids.Contains(627);
                case 988:
                    return Flagship.IsLightCruiserClass;
                case 318:
                    return Types.Count(type => type == 3) >= 2;
                case 329: // 【節分任務】節分演習！二〇二三 軽巡練巡x2 駆逐海防x3
                    return Specs.Count(spec => spec.ShipType == 21 || spec.ShipType == 3) > 1 &&
                           Specs.Count(spec => spec.IsEscortDestroyer) > 2;
                case 330:
                    return Flagship.IsAircraftCarrier &&
                           Specs.Count(spec => spec.IsAircraftCarrier) >= 2 &&
                           Types.Count(type => type == 2) >= 2;
                case 337:
                    return Names.Count("陽炎", "不知火", "霰", "霞") == 4;
                case 339:
                    return Names.Count("磯波", "浦波", "綾波", "敷波") == 4;
                case 342:
                    var t12 = Specs.Count(spec => spec.IsEscortDestroyer);
                    return t12 > 3 || t12 > 2 && Specs.Any(spec => spec.IsLightCruiserClass);
                case 345:
                    return Names.Count("Warspite", "金剛", "Ark Royal", "Nelson", "Jervis", "Janus") >= 4;
                case 346:
                    return Names.Count("夕雲改二", "巻雲改二", "風雲改二", "秋雲改二") == 4;
                case 348:
                    return new[] {3, 21}.Contains(FlagshipType) &&
                           Types.Skip(1).Count(type => new[] {3, 4, 21}.Contains(type)) >= 2 &&
                           Types.Count(type => type == 2) >= 2;
                case 350:
                    return Names.Count("朧", "曙", "漣", "潮") == 4;
                case 353:
                    return new[] {5, 6}.Contains(FlagshipType) &&
                           Types.Count(type => new[] {5, 6}.Contains(type)) >= 4 &&
                           Types.Count(type => type == 2) >= 2;
                case 354:
                    return Flagship.Id == 707 && // Gambier Bay Mk.II
                           // Fletcher級またはJohn C.Butler級
                           Classes.Count(klass => klass == 91 || klass == 87) >= 2;
                case 355:
                    // 黒潮改二と親潮改二が1番と2番にいる。順不同
                    return Ids.Take(2).Contains(568) && Ids.Take(2).Contains(670);
                case 356:
                    return Ids.Contains(666) && Ids.Contains(647) && Ids.Contains(195) && Ids.Contains(627);
                case 357:
                    return Specs.Any(spec => spec.HullNumber == 111) &&
                           Specs.Any(spec => spec.HullNumber == 112) &&
                           Specs.Any(spec => spec.ShipType == 3) &&
                           Specs.Count(spec => spec.ShipType == 2) > 1;
                case 362:
                    return Specs.Any(spec => spec.HullNumber == 1401) &&
                           Specs.Any(spec => spec.HullNumber == 1402) &&
                           Specs.Any(spec => spec.HullNumber == 1403) &&
                           Specs.Any(spec => spec.HullNumber == 1404);
                case 367:
                    return (Flagship.ShipType == CL || Flagship.ShipType == CT) &&
                           Specs.Count(spec => spec.ShipType == DD) > 4;
                case 368:
                    return Specs.Count(spec => spec.HullNumber == 1607 || spec.HullNumber == 1608 || spec.HullNumber == 1609 || spec.HullNumber == 1610) > 1;
                default:
                    return true;
            }
        }
	}
}
