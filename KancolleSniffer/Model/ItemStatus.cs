﻿// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;

namespace KancolleSniffer.Model
{
    public class ItemStatus
    {
        public int Id { get; set; }
        public ItemSpec Spec { get; set; } = new ItemSpec();
        public int Level { get; set; }
        public int Alv { get; set; }
        public bool Locked { get; set; }
        public int OnSlot { get; set; }
        public int MaxEq { get; set; }
        public int TbBonus { get; set; }
        public int ApBonus { get; set; }
        public ShipStatus Holder { get; set; }

        public ItemStatus(int id = -1)
        {
            Id = id;
        }

        public ItemStatus FillAirBaseOnSlotMaxEq(int onSlot, int maxEq)
        {
            OnSlot = onSlot;
            MaxEq = maxEq;
            TbBonus = 0;
            ApBonus = 0;
            return this;
        }

        public bool HasNoneAircraft => OnSlot < 1 && Spec.IsAircraft;

        public bool HasAnyBomber => OnSlot > 0 && Spec.IsAnyBomber;

        public bool HasCarrierBasedBomber => OnSlot > 0 && Spec.IsCarrierBasedBomber;

        public bool HasNightAircraft => OnSlot > 0 && Spec.IsNightAircraft;

        public bool HasNightAircraftSub => OnSlot > 0 && Spec.IsNightAircraftSub;

        public bool HasSwordfishTorpedoBomber => OnSlot > 0 && Spec.IsSwordfishTorpedoBomber;

        public bool HasAswEnabledBomber => OnSlot > 0 && Spec.IsAswEnabledBomber;

        public bool HasAswTorpedoBomber => OnSlot > 0 && Spec.IsAswTorpedoBomber;

        public bool HasAswPatrol => OnSlot > 0 && Spec.IsAswPatrol;

        public bool HasContactTrigger => OnSlot > 0 && Spec.IsContactTrigger;

        public bool HasContactSelection => OnSlot > 0 && Spec.IsContactSelection;

        public bool HasNightRecon => OnSlot > 0 && Spec.IsNightRecon;

        public bool HasAutoGiro => OnSlot > 0 && Spec.IsAutoGiro;

        public bool HasHelicopter => OnSlot > 0 && Spec.IsHelicopter;

        public bool HasFlyingBoat => OnSlot > 0 && Spec.IsFlyingBoat;

        public double[] CalcBomberPower()
        {
            if (!Spec.IsAnyBomber || OnSlot < 1)
                return new double[] {};

            var basePower = (25 + (Spec.BomberSpec + BomberLevelBonus + TbBonus + ApBonus) * Math.Sqrt(OnSlot)) * Spec.JetBomberModifier;
            if (Spec.IsJetBomber) // 噴式機
            {
                return new double[] {CalcJetPower(), basePower};
            }
            if (Spec.IsTorpedoBomber) // 雷撃
            {
                return new double[] {basePower * 0.8, basePower * 1.5};
            }
            // 爆撃
            return new double[] {basePower};
        }

        public double CalcJetPower()
        {
            return 25 + (Spec.BomberSpec + BomberLevelBonus) * Math.Sqrt(OnSlot);
        }

        public double[] CalcAirSupportPower()
        {
            if (!Spec.IsAnyBomber || OnSlot < 1)
                return new double[] {};

            double formula(double typedPower)
            {
                return (int)typedPower * 1.35;
            }

            var basePower = 3 + Spec.BomberSpec * Math.Sqrt(OnSlot);
            if (Spec.IsJetBomber) // 噴式機
            {
                return new double[] {formula(basePower / Math.Sqrt(2))};
            }
            if (Spec.IsTorpedoBomber) // 雷撃
            {
                return new double[] {formula(basePower * 0.8), formula(basePower * 1.5)};
            }
            // 爆撃
            return new double[] {formula(basePower)};
        }

        public double[] CalcAswSupportPower()
        {
            if (!Spec.CanAswSupport || OnSlot < 1)
                return new double[] {};

            var basePower = (int)(3 + (int)(Spec.Asw * 0.6) * Math.Sqrt(OnSlot)) * 1.75;
            return new double[] {basePower * 1.2, basePower * 1.5, basePower * 2.0};
        }

        public double CalcAirbaseShipBomberPower(double landBasedReconModifier, double bomberSpec = -1)
        {
            if ((!Spec.IsAnyBomber && !Spec.IsAnyLandBasedBomber) || OnSlot < 1)
                return 0;

            if (bomberSpec < 0)
                bomberSpec = Spec.AirbaseShipBomberSpec;

            return (int)((25 + (bomberSpec + BomberLevelBonus) * Math.Sqrt(OnSlot * Spec.AirbaseLargeLandBasedBomberModifier)) * Spec.JetBomberModifier * Spec.AirbaseAircraftTypeModifier) * Spec.AirbaseLandBasedBomberModifier * landBasedReconModifier;
        }

        public double CalcAirbaseLandBomberPower(double landBasedReconModifier, bool isolatedIsland = false, bool supplyDepot = false)
        {
            if ((!Spec.IsAnyBomber && !Spec.IsAnyLandBasedBomber) || OnSlot < 1)
                return 0;

            var isolatedIslandBaseModifier = isolatedIsland ? 1.18 : 1.0;
            var isolatedIslandBomberModifier = isolatedIsland ? Spec.AirbaseIsolatedIslandBomberModifier : 1.0;
            var supplyDepotBaseModifier = supplyDepot ? 100 : 0;
            var supplyDepotBomberModifier = supplyDepot ? Spec.AirbaseSupplyDepotBomberModifier : 1.0;

            return (int)((int)((25 + Spec.AirbaseLandBomberSpec * Math.Sqrt(OnSlot * 1.8)) * Spec.JetBomberModifier * Spec.AirbaseAircraftTypeModifier * isolatedIslandBaseModifier * supplyDepotBomberModifier + supplyDepotBaseModifier) * isolatedIslandBomberModifier) * Spec.AirbaseLandBasedBomberModifier * landBasedReconModifier;
        }

        public RangeD CalcAirbaseAswBomberPower(double landBasedReconModifier)
        {
            if (Spec.Asw < 7 || OnSlot < 1)
                return new RangeD();

            double formula(double randomModifier)
            {
                return (int)((25 + (Spec.Asw + Math.Sqrt(Level) * 2 / 3) * Math.Sqrt(OnSlot * 1.8)) * randomModifier) * Spec.AirbaseLandBasedBomberModifier * landBasedReconModifier;
            }

            return new RangeD(formula(Spec.AirbaseAswRandomModifierMin), formula(Spec.AirbaseAswRandomModifierMax));
        }

        public int CalcAirbaseAccuracy()
        {
            return 95 + Spec.Accuracy * 7;
        }

        public double CalcNightAircraftPower()
        {
            double a, b;
            if (HasNightAircraftSub) {
                a = 0.0;
                b = 0.3;
            } else if (HasNightAircraft) {
                a = 3.0;
                b = 0.45;
            } else {
                return TbBonus;
            }

            return Spec.Firepower + Spec.BomberSpec + TbBonus +
                   a * OnSlot +
                   b * (Spec.Firepower + Spec.Torpedo + Spec.Bomber + Spec.Asw) * Math.Sqrt(OnSlot) +
                   Math.Sqrt(Level);
        }

        public double CalcSwordfishTorpedoPower()
        {
            return (HasSwordfishTorpedoBomber ? Spec.Firepower + Spec.Torpedo + Math.Sqrt(Level) : 0) + TbBonus;
        }

        public Range CalcFighterPower()
        {
            if (!Spec.CanAirCombat || OnSlot < 1)
                return new Range();
            var withoutAlv = (Spec.AntiAir + FighterPowerLevelBonus) * Math.Sqrt(OnSlot);
            return new Range(withoutAlv, AlvBonus);
        }

        public int CalcPracticeFighterPower(bool useMax)
        {
            var num = useMax ? MaxEq : OnSlot;
            if (!Spec.CanAirCombat || num < 1)
                return 0;
            return (int)((Spec.AntiAir + FighterPowerLevelBonus) * Math.Sqrt(num) + AlvBonus.Max);
        }

        public int CalcAirSupportFighter()
        {
            if (!Spec.CanAirCombat || OnSlot < 1)
                return 0;
            return (int)(Spec.AntiAir * Math.Sqrt(OnSlot));
        }

        public int CalcAswSupportFighter()
        {
            if (!Spec.CanAswSupport || OnSlot < 1)
                return 0;
            return (int)(Spec.AntiAir * Math.Sqrt(OnSlot));
        }

        private RangeD AlvBonus
        {
            get
            {
                var table = AlvTypeBonusTable;
                if (table == null)
                    return new RangeD();
                return _alvBonus[Alv] + table[Alv];
            }
        }

        public AirCorpsFighterPower.Range CalcFighterPowerInBase()
        {
            if (!Spec.IsAircraft || OnSlot < 1)
                return new AirCorpsFighterPower.Range();
            var withoutAlv =
                (new AirCorpsFighterPower.Pair(Spec.Interception * 1.5, Spec.AntiBomber * 2 + Spec.Interception) +
                 Spec.AntiAir + FighterPowerLevelBonus) * Math.Sqrt(OnSlot);
            return new AirCorpsFighterPower.Range(withoutAlv, AlvBonusInBase + FighterPowerFixedBonusInBase);
        }

        public RangeD FighterPowerFixedBonusInBase
        {
            get
            {
                if (Spec.IsFlyingBoat && OnSlot > 3 && Level > 3)
                    return new RangeD(1, 1);

                if (Spec.IsLandBasedRecon && Level > 1 && Alv > 6)
                    return new RangeD(0, 1);

                return new RangeD();
            }
        }

        public int CalcContactTriggerRate()
        {
            return (int)(Spec.LoS * Math.Sqrt(OnSlot));
        }

        public double CalcContactSelectionFailRate(double modifier)
        {
            return 1.0 - Math.Min(Spec.LoS / modifier, 1.0);
        }

        public double CalcNightContactFailRate(int shipLevel)
        {
            return 1.0 - Math.Min(Math.Floor(Math.Sqrt(Spec.LoS * shipLevel)) * 4 / 100.0, 1.0);
        }

        public double CalcAirReconScore()
        {
            switch (Spec.Type)
            {
                case 10: // 水偵
                case 11: // 水爆
                    return Spec.LoS * Math.Sqrt(Math.Sqrt(OnSlot));
                case 41: // 大艇
                    return Spec.LoS * Math.Sqrt(OnSlot);
                default:
                    return 0;
            }
        }

        private RangeD AlvBonusInBase
        {
            get
            {
                switch (Spec.Type)
                {
                    case  9: // 艦偵
                    case 10: // 水偵
                    case 41: // 大艇
                    case 49: // 陸偵
                        return _alvBonus[Alv];
                    default:
                        return AlvBonus;
                }
            }
        }

        private readonly RangeD[] _alvBonus =
            new[]
            {
                new RangeD(0.0, 0.9), new RangeD(1.0, 2.4), new RangeD(2.5, 3.9), new RangeD(4.0, 5.4),
                new RangeD(5.5, 6.9), new RangeD(7.0, 8.4), new RangeD(8.5, 9.9), new RangeD(10.0, 12.0)
            }.Select(range => range.Sqrt()).ToArray();

        private int[] AlvTypeBonusTableFighter = new[] {0, 0, 2, 5, 9, 14, 14, 22};
        private int[] AlvTypeBonusTableSeaplaneBomber = new[] {0, 0, 1, 1, 1, 3, 3, 6};
        private int[] AlvTypeBonusTableOther = new[] {0, 0, 0, 0, 0, 0, 0, 0};

        private int[] AlvTypeBonusTable
        {
            get
            {
                switch (Spec.Type)
                {
                    case  6: // 艦戦
                    case 45: // 水戦
                    case 48: // 局地戦闘機
                    case 56: // 噴式戦闘機
                        return AlvTypeBonusTableFighter;
                    case 11: // 水爆
                        return AlvTypeBonusTableSeaplaneBomber;
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case 47: // 陸攻
                    case 57: // 噴式戦闘爆撃機
                    case 58: // 噴式攻撃機
                        return AlvTypeBonusTableOther;
                    default:
                        return Spec.IsFighterAswPatrol ? AlvTypeBonusTableFighter : null;
                }
            }
        }

        private double FighterPowerLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  6: // 艦戦
                    case 45: // 水戦
                    case 48: // 陸戦・局戦
                        return 0.2 * Level;
                    case  7: // 爆戦
                        return Spec.IsBakusen ? 0.25 * Level : 0;
                    case 47: // 陸攻
                    case 53: // 重爆
                        return 0.5 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public int AviationPersonnelFirepowerBonus
        {
            get
            {
                if (!Spec.IsSkilledDeckAviationMaintenance)
                    return 0;

                if (Level > 9)
                    return 3;
                if (Level > 6)
                    return 2;
                if (Level > 0)
                    return 1;
                return 0;
            }
        }

        public int AviationPersonnelTorpedoBonus
        {
            get
            {
                if (!Spec.IsSkilledDeckAviationMaintenance)
                    return 0;

                if (Level > 4)
                    return 1;
                return 0;
            }
        }

        public int AviationPersonnelBomberBonus
        {
            get
            {
                if (!Spec.IsSkilledDeckAviationMaintenance)
                    return 0;

                if (Level > 3)
                    return 1;
                return 0;
            }
        }

        public int AviationPersonnelNightBattleBonus => AviationPersonnelFirepowerBonus + AviationPersonnelTorpedoBonus + AviationPersonnelBomberBonus;

        public int SteelCost => (!Spec.IsJetBomber || OnSlot < 1) ? 0 : (int)Math.Round(OnSlot * Spec.Cost * 0.2);

        public double EffectiveLoS => (Spec.LoS + LoSLevelBonus) * Spec.LoSScaleFactor;

        private double LoSLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  9: // 艦偵
                    case 10: // 水偵
                    case 41: // 飛行艇
                    case 94: // 艦上偵察機（II）
                        return 1.2 * Math.Sqrt(Level);
                    case 11: // 水爆
                        return 1.15 * Math.Sqrt(Level);
                    case 12: // 小型電探
                        return 1.25 * Math.Sqrt(Level);
                    case 13: // 大型電探
                        return 1.4 * Math.Sqrt(Level);
                    case 26: // 対潜哨戒機
                        return Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double FirepowerLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  1: // 小口径
                    case  2: // 中口径
                    case 18: // 三式弾
                    case 19: // 徹甲弾
                    case 21: // 対空機銃
                    case 24: // 上陸用舟艇
                    case 29: // 探照灯
                    case 32: // 潜水艦魚雷
                    case 34: // 司令部施設
                    case 35: // 航空要員
                    case 36: // 高射装置
                    case 37: // 対地噴進砲
                    case 39: // 水上艦要員
                    case 42: // 大型探照灯
                    case 46: // 特型内火艇
                    case 54: // 水上艦装備(煙幕)
                        return Math.Sqrt(Level);
                    case  3: // 大口径
                        return 1.5 * Math.Sqrt(Level);
                    case  4: // 副砲
                        return SecondaryGunLevelBonus;
                    case 14: // ソナー
                    case 40: // 大型ソナー
                    case 15: // 爆雷投射機、その他爆雷
                        return Spec.IsDC ? 0 : 0.75 * Math.Sqrt(Level);
                    case  7: // 艦爆(爆戦以外)
                        return Spec.IsBakusen ? 0 : 0.2 * Level;
                    case  8: // 艦攻
                        return 0.2 * Level;
                    default:
                        return 0;
                }
            }
        }

        private double SecondaryGunLevelBonus
        {
            get
            {
                switch (Spec.Id)
                {
                    case  11: // 15.2cm単装砲
                    case 134: // OTO 152mm三連装速射砲
                    case 135: // 90mm単装高角砲
                        return Math.Sqrt(Level);
                    case 467: // 5inch連装砲(副砲配置) 集中配備
                        return 0.3 * Level;
                    default:
                        return (Spec.IconType == 16 ? 0.2 : 0.3) * Level;
                }
            }
        }

        public double AccuracyLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  1: // 小口径
                    case  2: // 中口径
                    case  3: // 大口径
                    case  4: // 副砲
                    case 14: // ソナー
                    case 18: // 三式弾
                    case 19: // 徹甲弾
                    case 29: // 探照灯
                    case 36: // 高射装置
                    case 39: // 水上艦要員
                    case 40: // 大型ソナー
                    case 42: // 大型探照灯
                        return Math.Sqrt(Level);
                    case 12: // 小型電探
                    case 13: // 大型電探
                        return (Spec.Accuracy > 2 ? 1.7 : 1.0) * Math.Sqrt(Level);
                    case 15: // 爆雷投射機、その他爆雷
                        return Spec.IsDC ? 0 : Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double TorpedoLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  5: // 魚雷
                    case 21: // 機銃
                        return 1.2 * Math.Sqrt(Level);
                    case 32: // 潜水艦魚雷
                        return 0.2 * Level;
                    default:
                        return 0;
                }
            }
        }

        public double AswLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case 14: // ソナー
                    case 40: // 大型ソナー
                    case 15: // 爆雷
                        return Math.Sqrt(Level);
                    case  7: // 艦爆
                    case  8: // 艦攻
                        return 0.2 * Level;
                    case 25: // 回転翼機
                        return (Spec.Asw > 10 ? 0.3 : 0.2) * Level;
                    case 26: // 対潜哨戒機
                        return (Spec.Asw > 8 ? 0.3 : 0.2) * Level;
                    default:
                        return 0;
                }
            }
        }

        public double BomberLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  7: // 艦爆(爆戦以外)
                        return Spec.IsBakusen ? 0 : 0.2 * Level;
                    case  8: // 艦攻
                        return 0.2 * Level;
                    case 11: // 水爆
                        return 0.2 * Level;
                    case 47: // 陸攻
                    case 53: // 重爆
                        return 0.7 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double NightBattleLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  1: // 小口径
                    case  2: // 中口径
                    case  3: // 大口径
                    case  5: // 魚雷
                    case 18: // 三式弾
                    case 19: // 徹甲弾
                    case 22: // 特殊潜航艇
                    case 24: // 上陸用舟艇
                    case 29: // 探照灯
                    case 34: // 司令部施設
                    case 35: // 航空要員
                    case 36: // 高射装置
                    case 37: // 対地噴進砲
                    case 39: // 水上艦要員
                    case 42: // 大型探照灯
                    case 46: // 特型内火艇
                    case 54: // 水上艦装備(煙幕)
                        return Math.Sqrt(Level);
                    case  4: // 副砲
                        return SecondaryGunLevelBonus;
                    case 32: // 潜水艦魚雷
                        return 0.2 * Level;
                    default:
                        return 0;
                }
            }
        }

        public double EffectiveAntiAirForShip
        {
            get
            {
                switch (Spec.IconType)
                {
                    case 15: // 機銃
                        return 6 * Spec.AntiAir + (Spec.AntiAir > 7 ? 6 : 4) * Math.Sqrt(Level);
                    case 16: // 高角砲
                        return 4 * Spec.AntiAir + (Spec.AntiAir > 7 ? 3 : 2) * Math.Sqrt(Level);
                    case 11: // 電探
                        return 3 * Spec.AntiAir;
                    case 30: // 高射装置
                        return 4 * Spec.AntiAir + 2 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double EffectiveAntiAirForFleet
        {
            get
            {
                switch (Spec.IconType)
                {
                    case 11: // 電探
                        return 0.4 * Spec.AntiAir + (Spec.AntiAir > 0 ? 1.5 : 0) * Math.Sqrt(Level);
                    case 12: // 三式弾
                        return 0.6 * Spec.AntiAir;
                    case 16: // 高角砲
                        return 0.35 * Spec.AntiAir + (Spec.AntiAir > 7 ? 3 : 2) * Math.Sqrt(Level);
                    case 30: // 高射装置
                        return 0.35 * Spec.AntiAir + 2 * Math.Sqrt(Level);
                    default:
                        return (Spec.Id == 9 ? 0.25 : 0.2) * Spec.AntiAir; // id:9 46cm三連装砲
                }
            }
        }

        // 遠征時の装備改修ボーナスは小数第一位まで有効なため、10倍の値の整数値で管理する
        // 搭載数0の場合は無効のためマイナスする。ボーナス値も無効だが単純には計算できないため、警告を出す
        public int MissionFirepowerLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.Firepower * -10;

                switch (Spec.Type)
                {
                    case  1: // 小口径
                    case  4: // 副砲
                    case 12: // 小型電探
                    case 19: // 徹甲弾
                    case 21: // 対空機銃
                        return (int)(5 * Math.Sqrt(Level));
                    case  2: // 中口径
                    case  3: // 大口径
                    case 13: // 大型電探
                        return (int)(10 * Math.Sqrt(Level));
                    default:
                        return 0;
                }
            }
        }

        public int MissionAntiAirLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.AntiAir * -10;

                switch (Spec.IconType)
                {
                    case 15: // 機銃
                    case 16: // 高角砲
                        return (int)(10 * Math.Sqrt(Level));
                    default:
                        return 0;
                }
            }
        }

        public int MissionAswLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.Asw * -10;

                switch (Spec.Type)
                {
                    case 14: // ソナー
                    case 40: // 大型ソナー
                    case 15: // 爆雷
                        return (int)(10 * Math.Sqrt(Level));
                    case 10: // 水上偵察機
                        return MissionAswAircraft((int)(5 * Math.Sqrt(Level)));
                    case 25: // 回転翼機
                        return MissionAswAircraft((int)(10 * Math.Sqrt(Level)));
                    case  6: // 艦戦
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case  9: // 艦偵
                    case 11: // 水爆
                    case 26: // 対潜哨戒機
                    case 41: // 飛行艇
                    case 45: // 水戦
                    case 57: // 噴式戦闘爆撃機
                    case 58: // 噴式攻撃機
                    case 94: // 艦上偵察機（II）
                        return MissionAswAircraft(0);
                    default:
                        return 0;
                }
            }
        }

        public int MissionAswAircraft(int levelBonus)
        {
            var aws = Spec.Asw * 10;
            if (aws < 1)
                return -aws;

            // TODO: 熟練度補正込みの計算式が確定したら反映する
            var multiplier = (0.65 + Math.Sqrt(Math.Max(OnSlot - 2, 0)) * 0.1);
            return (int)((aws + levelBonus) * multiplier) - aws;
        }

        public int MissionLoSLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.LoS * -10;

                switch (Spec.Type)
                {
                    case 10: // 水上偵察機
                    case 12: // 小型電探
                    case 13: // 大型電探
                        return (int)(10 * Math.Sqrt(Level));
                    default:
                        return 0;
                }
            }
        }
    }
}