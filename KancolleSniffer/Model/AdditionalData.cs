﻿// Copyright (C) 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KancolleSniffer.Model
{
    public class AdditionalData
    {
        private const string EnemySlotFileName = "EnemySlot.csv";
        private static readonly string EnemySlotFile = AppInfo.AbsPath(EnemySlotFileName);
        private Dictionary<int, int[]> _maxEq;

        public void LoadEnemySlot()
        {
            try
            {
                _maxEq = File.ReadLines(EnemySlotFile)
                    .Select(line => line.Split(','))
                    .Where(f => f.Length > 5 &&
                        int.TryParse(f[0], out var id) &&
                        int.TryParse(f[1], out var slot1) &&
                        int.TryParse(f[2], out var slot2) &&
                        int.TryParse(f[3], out var slot3) &&
                        int.TryParse(f[4], out var slot4) &&
                        int.TryParse(f[5], out var slot5))
                    .GroupBy(f => f[0])
                    .ToDictionary(f => int.Parse(f.Key),
                        f => f.Last().Skip(1).Take(5).Select(s => int.Parse(s)).ToArray());
            }
            catch (IOException)
            {
            }
            catch (Exception ex)
            {
                throw new Exception(EnemySlotFileName + "が壊れています。", ex);
            }
        }

        public int[] EnemySlot(int id) =>
            _maxEq != null ? _maxEq.TryGetValue(id, out var slot) ? slot : null : null;


        private const string TpFileName = "TP.csv";
        private static readonly string TpFile = AppInfo.AbsPath(TpFileName);
        private Dictionary<int, double> _tpSpec;

        public void LoadTpSpec()
        {
            try
            {
                _tpSpec = File.ReadAllLines(TpFile)
                    .Select(line => line.Split(','))
                    .Where(f => f.Length > 2 && int.TryParse(f[0], out var id) && double.TryParse(f[2], out var tp))
                    .GroupBy(f => f[0])
                    .ToDictionary(f => int.Parse(f.Key), f => double.Parse(f.Last()[2]));
            }
            catch (IOException)
            {
            }
            catch (Exception ex)
            {
                throw new Exception(TpFileName + "が壊れています。", ex);
            }
        }

        public double ItemTp(int id) =>
            _tpSpec != null ? _tpSpec.TryGetValue(id, out var tp) ? tp : -1 : -1;


        private const string NumEquipsFileName = "NumEquips.csv";
        private static readonly string NumEquipsFile = AppInfo.AbsPath(NumEquipsFileName);
        private Dictionary<int, int> _numEquips = new Dictionary<int, int>();
#if DEBUG
        public bool UseNumEquipsFile { get; set; } = true;
#endif

        public void LoadNumEquips()
        {
            try
            {
#if DEBUG
                if (!UseNumEquipsFile)
                    return;
#endif
                _numEquips = File.ReadLines(NumEquipsFile)
                    .Select(line => line.Split(','))
                    .Where(f => f.Length > 2 && int.TryParse(f[0], out var id) && int.TryParse(f[2], out var num))
                    .GroupBy(f => f[0])
                    .ToDictionary(f => int.Parse(f.Key), f => int.Parse(f.Last()[2]));
            }
            catch (IOException)
            {
            }
            catch (Exception ex)
            {
                throw new Exception(NumEquipsFileName + "が壊れています。", ex);
            }
        }

        public int NumEquips(int id) =>
            _numEquips.TryGetValue(id, out var num) ? num : -1;

        public void RecordNumEquips(int id, string name, int numEquips)
        {
            _numEquips[id] = numEquips;
#if DEBUG
            if (!UseNumEquipsFile)
                return;
#endif
            File.AppendAllText(NumEquipsFile, $"{id},{name},{numEquips}\r\n");
        }


        private const string MinAswFileName = "minasw.csv";
        private static readonly string MinAswFile = AppInfo.AbsPath(MinAswFileName);
        private Dictionary<int, int> _minasw;

        public AdditionalData LoadMinAsw()
        {
            try
            {
                _minasw = File.ReadAllLines(MinAswFile)
                    .Select(line => line.Split(','))
                    .Where(f => f.Length > 1 && int.TryParse(f[0], out var id) && int.TryParse(f[1], out var minasw))
                    .GroupBy(f => f[0])
                    .ToDictionary(f => int.Parse(f.Key), f => int.Parse(f.Last()[1]));
            }
            catch (IOException)
            {
            }
            catch (Exception ex)
            {
                throw new Exception(MinAswFileName + "が壊れています。", ex);
            }
            return this;
        }

        public int MinAsw(int id) =>
            _minasw != null ? _minasw.TryGetValue(id, out var minasw) ? minasw : -1 : -1;


        private const string TbBonusFileName = "tbbonus.csv";
        private static readonly string TbBonusFile = AppInfo.AbsPath(TbBonusFileName);
        private IEnumerable<TbBonus> _tbbonus;

        public AdditionalData LoadTbBonus()
        {
            try
            {
                _tbbonus = File.ReadAllLines(TbBonusFile)
                    .Select(line => line.Split(','))
                    .Where(f => f.Length > 4 &&
                        int.TryParse(f[0], out var itemId) &&
                        int.TryParse(f[2], out var shipId) &&
                        int.TryParse(f[4], out var torpedo))
                    .Select(f => new TbBonus{ItemId = int.Parse(f[0]), ShipId = int.Parse(f[2]), Torpedo = int.Parse(f[4])});
            }
            catch (IOException)
            {
            }
            catch (Exception ex)
            {
                throw new Exception(TbBonusFile + "が壊れています。", ex);
            }
            return this;
        }

        public int TbBonus(int shipId, IEnumerable<int> itemIds)
        {
            if (_tbbonus == null)
                return 0;

            var bonus = _tbbonus.Where(bonus => shipId == bonus.ShipId && itemIds.Contains(bonus.ItemId)).OrderBy(bonus => bonus.Torpedo).FirstOrDefault();
            return bonus != null ? bonus.Torpedo : 0;
        }
    }

    class TbBonus
    {
        public int ItemId { get; set; }
        public int ShipId { get; set; }
        public int Torpedo { get; set; }
    }
}