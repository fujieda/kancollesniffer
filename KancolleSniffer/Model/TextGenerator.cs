﻿// Copyright (C) 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using KancolleSniffer.Util;

namespace KancolleSniffer.Model
{
    public static class TextGenerator
    {
        public static string GenerateShipList(IEnumerable<ShipStatus> shipList)
            => "ID,艦種,艦名,レベル,次レベルまで,cond,改修耐久,表示対潜,素対潜,改修対潜,Lv99対潜,運\r\n" +
               string.Join("\r\n",
                   from ship in shipList
                   orderby ship.Spec.ShipType, -ship.Level, ship.ExpToNext
                   select $"{ship.Id},{ship.Spec.ShipTypeName},{ship.Name},{ship.Level},{ship.ExpToNext},{ship.Cond},{ship.ImprovedMaxHp},{ship.ShownAsw},{ship.RawAsw},{ship.ImprovedAsw},{ship.MaxAsw},{ship.Lucky}");

        public static string GeneratePresetDecks(IEnumerable<Fleet> presetDecks)
        {
            var sb = new StringBuilder();
            foreach (var fleet in presetDecks)
            {
                sb.Append($"プリセット{fleet.Number},艦種,艦名,レベル,次レベルまで,cond,改修耐久,表示対潜,素対潜,改修対潜,Lv99対潜,運\r\n");
                foreach (var ship in fleet.Ships)
                    sb.Append($"{ship.Id},{ship.Spec.ShipTypeName},{ship.Name},{ship.Level},{ship.ExpToNext},{ship.Cond},{ship.ImprovedMaxHp},{ship.ShownAsw},{ship.RawAsw},{ship.ImprovedAsw},{ship.MaxAsw},{ship.Lucky}\r\n");
            }
            return sb.ToString();
        }

        public static string GenerateKantaiSarashiData(IEnumerable<ShipStatus> shipList)
        {
            return ".2|" +
                   string.Join("|", from ship in shipList
                       where ship.Locked
                       group ship by ship.Spec.Remodel.Base
                       into grp
                       orderby grp.Key
                       select grp.Key + ":" + string.Join(",", from ship in grp
                           orderby -ship.Level
                           select ship.Level +
                                  (ship.Level >= ship.Spec.Remodel.Level && ship.Spec.Remodel.Step != 0
                                      ? "." + ship.Spec.Remodel.Step
                                      : "")));
        }

        public static string GenerateItemList(IEnumerable<ItemStatus> itemList, IEnumerable<MaterialItem> useItems, IEnumerable<MaterialItem> payItems)
            => "区分,装備名,熟練度,改修,個数\r\n" +
               string.Join("\r\n",
                   (from item in itemList
                       where !item.Spec.Empty
                       orderby item.Spec.Type, item.Spec.Id, item.Alv, item.Level
                       group item by
                           $"{item.Spec.TypeName},{item.Spec.Name},{item.Alv},{item.Level}"
                       into grp
                       select grp.Key + $",{grp.Count()}")) +
               "\r\n" + string.Join("\r\n", useItems.Select(item => item.ToCsv())) +
               (payItems == null ? "" : "\r\n" + string.Join("\r\n", payItems.Select(item => item.ToCsv())));

        public static string GenerateKantaiBunsekiItemList(IEnumerable<ItemStatus> itemList)
            => "[" + string.Join(",",
                   (from item in itemList where item.Locked
                       select $"{{\"api_slotitem_id\":{item.Spec.Id},\"api_level\":{item.Level}}}")) + "]";

        public static string GenerateFleetData(Sniffer sniffer)
        {
            var dict = new ItemName();
            var sb = new StringBuilder();
            for (var f = 0; f < ShipInfo.FleetCount; f++)
                sb.Append(GenerateFleetData(sniffer, f, dict));
            sb.Append(GenerateAirBase(sniffer, dict));
            return sb.ToString();
        }

        public static string GenerateFleetCsv(Sniffer sniffer)
        {
            var dict = new ItemName();
            var sb = new StringBuilder();
            for (var f = 0; f < ShipInfo.FleetCount; f++)
                sb.Append(GenerateFleetCsv(sniffer, f, dict));
            sb.Append(GenerateAirBaseCsv(sniffer, dict));
            return sb.ToString();
        }

        public static string GenerateFleetData(Sniffer sniffer, int fleet)
        {
            return GenerateFleetData(sniffer, fleet, new ItemName()).ToString();
        }

        private static StringBuilder GenerateFleetData(Sniffer sniffer, int fleet, ItemName dict)
        {
            var target = sniffer.Fleets[fleet];
            var sb = new StringBuilder();
            var fn = new[] {"第一艦隊", "第二艦隊", "第三艦隊", "第四艦隊"};
            sb.Append(fn[fleet] + "\r\n");
            sb.Append(string.Concat(target.Ships.Select(ship => $"{ship.Name} Lv{ship.Level} " + string.Join(",", ship.Items.Select(item => dict[item.Spec.Name] + ItemStatusString(item))) + "\r\n")));
            var fp = target.FighterPower;
            sb.Append($"制空: {(fp.Diff ? fp.RangeString : fp.Min.ToString())} 索敵: {StringUtil.ToS(target.GetLineOfSights(1))}\r\n");
            return sb;
        }

        private static StringBuilder GenerateAirBase(Sniffer sniffer, ItemName dict)
        {
            var sb = new StringBuilder();
            if (sniffer.AirBase == null)
                return sb;
            foreach (var baseInfo in sniffer.AirBase)
            {
                sb.Append(baseInfo.AreaName + " 基地航空隊 整備Lv" + baseInfo.MaintenanceLevel + "\r\n");
                var i = 0;
                var name = new[] {"第一 ", "第二 ", "第三 "};
                foreach (var airCorps in baseInfo.AirCorps)
                {
                    sb.Append(name[i++]);
                    sb.Append(
                        string.Join(",",
                            from plane in airCorps.Planes
                            select plane.State == 1
                                ? dict[plane.Item.Spec.Name] + ItemStatusString(plane.Item)
                                : plane.StateName) + "\r\n");
                }
            }
            return sb;
        }

        private static StringBuilder GenerateFleetCsv(Sniffer sniffer, int fleet, ItemName dict)
        {
            var fn = new[] {"第一艦隊", "第二艦隊", "第三艦隊", "第四艦隊"};
            var sb = new StringBuilder();
            var target = sniffer.Fleets[fleet];
            var fp = target.FighterPower;

            sb.Append(fn[fleet]);
            sb.Append(",制空: ");
            sb.Append(fp.Diff ? fp.RangeString : fp.Min.ToString());
            sb.Append(",索敵: ");
            sb.Append(StringUtil.ToS(target.GetLineOfSights(1)));
            sb.Append("\r\n");

            foreach (var ship in target.Ships)
            {
                sb.Append(ship.Name);
                sb.Append(",Lv");
                sb.Append(ship.Level);
                foreach (var item in ship.Items)
                {
                    sb.Append(",");
                    sb.Append(dict[item.Spec.Name]);
                    sb.Append(ItemStatusString(item));
                }
                sb.Append("\r\n");
            }
            return sb;
        }

        private static StringBuilder GenerateAirBaseCsv(Sniffer sniffer, ItemName dict)
        {
            var sb = new StringBuilder();
            if (sniffer.AirBase == null)
                return sb;

            var name = new[] {"第一", "第二", "第三"};
            foreach (var baseInfo in sniffer.AirBase.OrderBy(baseInfo => baseInfo.ListWindowSortKey))
            {
                sb.Append(baseInfo.AreaName);
                sb.Append(",整備Lv");
                sb.Append(baseInfo.MaintenanceLevel);
                sb.Append("\r\n");

                var i = 0;
                foreach (var airCorps in baseInfo.AirCorps)
                {
                    sb.Append(name[i++]);
                    foreach (var plane in airCorps.Planes)
                    {
                        sb.Append(",");
                        if (plane.Deploying)
                        {
                            sb.Append(dict[plane.Item.Spec.Name]);
                            sb.Append(ItemStatusString(plane.Item));
                        }
                        else
                        {
                            sb.Append(plane.StateName);
                        }
                    }
                    sb.Append("\r\n");
                }
            }
            return sb;
        }

        private static string ItemStatusString(ItemStatus item)
            => (item.Alv == 0 ? "" : "+" + item.Alv) + (item.Level == 0 ? "" : "★" + item.Level);

        private class ItemName
        {
            private readonly Dictionary<string, string> _dict = new Dictionary<string, string>();

            public ItemName()
            {
                try
                {
                    foreach (var line in File.ReadLines("ItemName.csv"))
                    {
                        var cols = line.Split(',');
                        _dict[cols[0]] = cols[1];
                    }
                }
                catch (IOException)
                {
                }
            }

            public string this[string name] => _dict.TryGetValue(name, out var shortName) ? shortName : name;
        }

        public static string GenerateDeckBuilderData(Sniffer sniffer)
        {
            var sb = new StringBuilder("{\"version\": 4,");
            foreach (var fleet in sniffer.Fleets)
            {
                if (fleet.Number != 0)
                    sb.Append(",");
                sb.Append($"\"f{fleet.Number + 1}\":{{");
                for (var s = 0; s < fleet.Ships.Length; s++)
                {
                    if (s != 0)
                        sb.Append(",");
                    var ship = fleet.Ships[s];
                    sb.Append($"\"s{s + 1}\":{{\"id\":\"{ship.Spec.Id}\",\"lv\":{ship.Level},\"luck\":{ship.Lucky},\"items\":{{");
                    for (var i = 0; i < ship.Items.Count; i++)
                    {
                        var item = ship.Items[i];
                        if (i != 0)
                            sb.Append(",");
                        sb.Append($"\"i{i + 1}\":{{\"id\":{item.Spec.Id},\"rf\":{item.Level},\"mas\":{item.Alv}}}");
                    }
                    sb.Append("}}");
                }
                sb.Append("}");
            }
            sb.Append("}");
            return sb.ToString();
        }

        public static string GenerateNoro6KcToolsManagerData(IEnumerable<ShipStatus> shipList)
        {
            var sb = new StringBuilder("[");
            foreach (var ship in shipList)
            {
                if (sb.Length > 1)
                    sb.Append(",");
                sb.Append($"{{\"api_id\":{ship.Id},\"api_ship_id\":{ship.Spec.Id},\"api_lv\":{ship.Level},\"api_exp\":[{ship.ExpTotal},{ship.ExpToNext},{ship.ExpToNextPercent}],\"api_kyouka\":[{ship.ImprovedFirepower},{ship.ImprovedTorpedo},{ship.ImprovedAntiAir},{ship.ImprovedArmor},{ship.ImprovedLucky},{ship.ImprovedMaxHp},{ship.ImprovedAsw}],\"api_slot_ex\":{ship.SlotEx}");
                if (ship.SallyArea > 0)
                    sb.Append($",\"api_sally_area\":{ship.SallyArea}");
                sb.Append("}");
            }
            sb.Append("]");
            return sb.ToString();
        }

        public static string GenerateNoro6KcToolsManagerEquipData(IEnumerable<ItemStatus> itemList)
        {
            var sb = new StringBuilder("[");
            foreach (var item in itemList)
            {
                if (sb.Length > 1)
                    sb.Append(",");
                sb.Append($"{{\"api_slotitem_id\":{item.Spec.Id},\"api_level\":{item.Level}}}");
            }
            sb.Append("]");
            return sb.ToString();
        }
    }
}