﻿// Copyright (C) 2016 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;

namespace KancolleSniffer.Model
{
    public class PresetDeck
    {
        private readonly ShipInventory _shipInventory;
        private readonly List<int[]> _presetDecks = new List<int[]>();
        private int _hqLevel;

        public PresetDeck(ShipInventory shipInventory)
        {
            _shipInventory = shipInventory;
        }

        public void InspectBasic(dynamic json)
        {
            _hqLevel = (int)json.api_level;
        }

        public void Inspect(dynamic json)
        {
            for (var i = _presetDecks.Count; i < json.api_max_num; i++)
                _presetDecks.Add(new int[0]);
            foreach (KeyValuePair<string, dynamic> entry in json.api_deck)
                InspectRegister(entry.Value);
        }

        public void InspectRegister(dynamic json)
        {
            _presetDecks[(int)json.api_preset_no - 1] = json.api_ship;
        }

        public void InspectDelete(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            if (int.TryParse(values["api_preset_no"], out int no))
                _presetDecks[no - 1] = new int[0];
        }

        public void InspectExpand()
        {
            _presetDecks.Add(new int[0]);
        }

        public IEnumerable<Fleet> Decks => _presetDecks.Select((preset, index) =>
            new Fleet(_shipInventory, index + 1, () => _hqLevel){State = FleetState.Preset, CombinedType = CombinedType.None}.SetShips(preset));
    }
}