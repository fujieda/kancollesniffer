﻿// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class AirContactRates
    {
        public Rate[] Rates { get; private set; }
        public NightRate Night { get; private set; }

        private static int levelBorder = 2;
        private static double[] triggerModifiers = new[] {25.0, 40.0, 55.0};
        private static double[] selectModifiers = new[] {14.0, 16.0, 18.0};

        public static AirContactRates Calc(Fleet main, Fleet guard = null, bool combined = false)
        {
            var mainItems = main.ShipsWithoutDropout.SelectMany(ship => ship.Items).ToList();
            var baseTrigger = mainItems.Where(item => item.HasContactTrigger).Select(item => item.CalcContactTriggerRate()).Sum();
            var select20 = mainItems.Where(item => item.HasContactSelection && item.Spec.Accuracy >  levelBorder).ToList();
            var select17 = mainItems.Where(item => item.HasContactSelection && item.Spec.Accuracy == levelBorder).ToList();
            var select12 = mainItems.Where(item => item.HasContactSelection && item.Spec.Accuracy <  levelBorder).ToList();
            if (combined && guard != null)
            {
                var guardItems = guard.ShipsWithoutDropout.SelectMany(ship => ship.Items).ToList();
                baseTrigger += guardItems.Where(item => item.HasContactTrigger).Select(item => item.CalcContactTriggerRate()).Sum();
                select20.AddRange(guardItems.Where(item => item.HasContactSelection && item.Spec.Accuracy >  levelBorder));
                select17.AddRange(guardItems.Where(item => item.HasContactSelection && item.Spec.Accuracy == levelBorder));
                select12.AddRange(guardItems.Where(item => item.HasContactSelection && item.Spec.Accuracy <  levelBorder));
            }
            if (select20.Any() || select17.Any() || select12.Any())
                baseTrigger += 1;

            return new AirContactRates
            {
                Rates = Enumerable.Range(0, 3).Select(i => Rate.Calc(baseTrigger, select20, select17, select12, triggerModifiers[i], selectModifiers[i])).ToArray(),
                Night = NightRate.Calc(guard != null ? guard.Ships : main.Ships)
            };
        }

        public override string ToString()
        {
            var levels = new List<string>();
            if (Rates[0].Trigger == 0.0)
                levels.Add("触接開始: 0.0");
            else
            {
                levels.Add($"触接開始: {string.Join("/", Rates.Select(rate => ToS(rate.Trigger * 100.0)))}");
                if (Rates.Select(rate => rate.Select20).Sum() > 0.0)
                    levels.Add($"触接20%: {string.Join("/", Rates.Select(rate => ToS(rate.Select20 * 100.0)))}");
                if (Rates.Select(rate => rate.Select17).Sum() > 0.0)
                    levels.Add($"触接17%: {string.Join("/", Rates.Select(rate => ToS(rate.Select17 * 100.0)))}");
                if (Rates.Select(rate => rate.Select12).Sum() > 0.0)
                    levels.Add($"触接12%: {string.Join("/", Rates.Select(rate => ToS(rate.Select12 * 100.0)))}");
                if (levels.Count > 2 && Rates.Select(rate => rate.Fail).Sum() > 0.0)
                    levels.Add($"触接失敗: {string.Join("/", Rates.Select(rate => ToS(rate.Fail * 100.0)))}");
            }

            if (Night.Fail < 1.0)
                levels.Add($"夜偵: {ToS(Night.Select7 * 100.0)}/{ToS(Night.Select5 * 100.0)}/{ToS(Night.Fail * 100.0)}");

            return string.Join("\n", levels);
        }

        public class Rate
        {
            public double Trigger { get; private set; }
            public double Select20 { get; private set; }
            public double Select17 { get; private set; }
            public double Select12 { get; private set; }
            public double Fail { get; private set; }

            public static Rate Calc(int baseTrigger, IEnumerable<ItemStatus> select20Items, IEnumerable<ItemStatus> select17Items, IEnumerable<ItemStatus> select12Items, double triggerModifier, double selectModifier)
            {
                var trigger = baseTrigger / triggerModifier;
                var fail = Math.Min(trigger, 1.0);
                var fixedFail = 1.0 - fail;
                var select20Rate = 1.0 - select20Items.Aggregate(1.0, (fail, item) => fail * item.CalcContactSelectionFailRate(selectModifier));
                var select17Rate = 1.0 - select17Items.Aggregate(1.0, (fail, item) => fail * item.CalcContactSelectionFailRate(selectModifier));
                var select12Rate = 1.0 - select12Items.Aggregate(1.0, (fail, item) => fail * item.CalcContactSelectionFailRate(selectModifier));

                var select20 = fail * select20Rate;
                fail -= select20;
                var select17 = fail * select17Rate;
                fail -= select17;
                var select12 = fail * select12Rate;
                fail -= select12;
                fail += fixedFail;

                return new Rate
                {
                    Trigger = trigger,
                    Select20 = select20,
                    Select17 = select17,
                    Select12 = select12,
                    Fail = fail
                };
            }
        }

        public class NightRate
        {
            public double Select7 { get; private set; }
            public double Select5 { get; private set; }
            public double Fail { get; private set; }

            private static int levelBorder = 2;

            public static NightRate Calc(IEnumerable<ShipStatus> ships)
            {
                var select7Rate = 1.0 - ships.Aggregate(1.0, (fail, ship) => ship.Items.Where(item => item.HasNightRecon && item.Spec.Accuracy == levelBorder).Aggregate(fail, (fail, item) => fail * item.CalcNightContactFailRate(ship.Level)));
                var select5Rate = 1.0 - ships.Aggregate(1.0, (fail, ship) => ship.Items.Where(item => item.HasNightRecon && item.Spec.Accuracy <  levelBorder).Aggregate(fail, (fail, item) => fail * item.CalcNightContactFailRate(ship.Level)));

                var fail = 1.0;
                var select7 = fail * select7Rate;
                fail -= select7;
                var select5 = fail * select5Rate;
                fail -= select5;

                return new NightRate
                {
                    Select7 = select7,
                    Select5 = select5,
                    Fail = fail
                };
            }
        }
    }
}
