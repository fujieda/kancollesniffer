﻿// Copyright (C) 2016 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using KancolleSniffer.Util;
using KancolleSniffer.View;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class AirBase
    {
        private readonly ItemInfo _itemInfo;
        private List<int> _relocatingPlanes = new List<int>();

        public AirBase(ItemInfo item)
        {
            _itemInfo = item;
        }

        public BaseInfo[] AllBase { get; private set; }

        public BaseInfo GetAirBase(int areaId)
        {
            return AllBase?.FirstOrDefault(b => b.AreaId == areaId);
        }

        public class BaseInfo
        {
            public int AreaId { get; set; }
            public int MaintenanceLevel { get; set; }
            public AirCorpsInfo[] AirCorps { get; set; }
            // 最近のイベント海域の番号から逆算して、21以降をイベント海域と仮定し、一覧では優先して表示する
            public bool IsEvent => AreaId > 20;
            // 2019年夏以降の基地空襲は高高度爆撃
            public bool HighAltitudeArea => AreaId > 44;
            public int ListWindowSortKey => IsEvent ? AreaId : AreaId * 1000;

            public string AreaName
            {
                get
                {
                    switch (AreaId)
                    {
                        case 5:
                            return "南方海域";
                        case 6:
                            return "中部海域";
                        case 7:
                            return "南西海域";
                        default:
                            return "限定海域";
                    }
                }
            }

            public string AreaNameShort
            {
                get
                {
                    switch (AreaId)
                    {
                        case 5:
                            return "南方";
                        case 6:
                            return "中部";
                        case 7:
                            return "南西";
                        default:
                            return "イベ";
                    }
                }
            }

            public bool CanIntercept(int scc = 3)
            {
                return AirCorps.Where(airCorps => airCorps.Action == 2).Take(scc).Count() > 0;
            }

            public Range CalcInterceptionFighterPower(int scc = 3, bool highAltitude = true)
            {
                var range = AirCorps.Where(airCorps => airCorps.Action == 2).Take(scc).Aggregate(new Range(0, 0),
                    (all, cur) => all + cur.CalcFighterPower().Interception);
                return highAltitude && HighAltitudeArea ? range * HighAltitudeModifier(scc) : range;
            }

            public double HighAltitudeModifier(int scc = 3)
            {
                var highAltitudeInterceptors = AirCorps.Where(airCorps => airCorps.Action == 2).Take(scc).
                    Sum(airCorps => airCorps.Planes.Count(plane => plane.Item.Spec.IsHighAltitudeInterceptor));
                switch (highAltitudeInterceptors)
                {
                    case 0:
                        return 0.5;
                    case 1:
                        return 0.8;
                    case 2:
                        return 1.1;
                    default:
                        return 1.2;
                }
            }

            public void CalcLandBasedReconModifier()
            {
                foreach (var airCorp in AirCorps)
                    airCorp.CalcLandBasedReconModifier();
            }
        }

        public class Distance
        {
            public int Base { get; set; }
            public int Bonus { get; set; }

            public int Total => Base + Bonus;
            public override string ToString() => Bonus > 0 ? $"{Base}+{Bonus}" : Base.ToString();
        }

        public class AirCorpsInfo
        {
            public Distance Distance { get; set; }
            public int Action { get; set; }
            public PlaneInfo[] Planes { get; set; }

            public string ActionName
            {
                get
                {
                    switch (Action)
                    {
                        case 0:
                            return "待機";
                        case 1:
                            return "出撃";
                        case 2:
                            return "防空";
                        case 3:
                            return "退避";
                        case 4:
                            return "休息";
                        default:
                            return "";
                    }
                }
            }

            public int Cond => Planes.Select(plane => plane.Cond).Max();

            public string CondName
            {
                get
                {
                    switch (Cond)
                    {
                        case 2:
                            return "疲労";
                        case 3:
                            return "過労";
                        default:
                            return "";
                    }
                }
            }

            public Color BackColor
            {
                get
                {
                    switch (Cond)
                    {
                        case 2:
                            return CUDColors.Orange;
                        case 3:
                            return CUDColors.Red;
                        default:
                            switch (Action)
                            {
                                case 1:
                                case 2:
                                    return Color.Empty;
                                default:
                                    return Distance.Base > 0 ? CUDColors.Yellow : Color.Empty;
                            }
                    }
                }
            }

            public AirCorpsFighterPower CalcFighterPower()
            {
                var reconPlaneBonus = Planes.Aggregate(new AirCorpsFighterPower.Pair(), (max, plane) =>
                {
                    var bonus = plane.Item.Spec.ReconPlaneAirBaseBonus;
                    return AirCorpsFighterPower.Pair.Max(max, bonus);
                });
                var range = (Planes.Aggregate(new AirCorpsFighterPower.Range(), (previous, plane) =>
                {
                    if (!plane.Deploying)
                        return previous;
                    var current = plane.Item.CalcFighterPowerInBase();
                    return previous + current;
                }) * reconPlaneBonus).Floor();
                return new AirCorpsFighterPower(range);
            }

            public int[] CostForSortie => Planes.Aggregate(new[] {0, 0}, (prev, plane) =>
            {
                if (!plane.Deploying)
                    return prev;
                int fuel, bull;
                switch (plane.Item.Spec.Type)
                {
                    case 47: // 陸攻
                        fuel = (int)Math.Ceiling(plane.Item.OnSlot * 1.5);
                        bull = (int)(plane.Item.OnSlot * 0.7);
                        break;
                    case 53: // 重爆
                        fuel = plane.Item.OnSlot * 2;
                        bull = plane.Item.OnSlot * 2;
                        break;
                    default:
                        fuel = plane.Item.OnSlot;
                        bull = (int)Math.Ceiling(plane.Item.OnSlot * 0.6);
                        break;
                }
                return new[] {prev[0] + fuel, prev[1] + bull};
            });

            public void CalcLandBasedReconModifier()
            {
                var landBasedReconModifier = Planes.Max(plane => plane.LandBasedReconModifier);
                foreach (var plane in Planes)
                    plane.EffectiveLandBasedReconModifier = landBasedReconModifier;
            }
        }

        public class PlaneInfo
        {
            public int State { get; set; }
            public int Cond { get; set; }
            public ItemStatus Item { get; set; }
            public double EffectiveLandBasedReconModifier { get; set; }

            public string StateName
            {
                get
                {
                    switch (State)
                    {
                        case 0:
                            return "未配備";
                        case 1:
                            return "配備中";
                        case 2:
                            return "配置転換中";
                        default:
                            return "";
                    }
                }
            }

            public bool Undeployed => State == 0;
            public bool Deploying => State == 1;

            public AirCorpsFighterPower FighterPower => new AirCorpsFighterPower(Item.CalcFighterPowerInBase());

            public List<ShipBomberPower> ShipBomberPower
            {
                get
                {
                    var bomberPowers = new List<ShipBomberPower>();

                    if (Item.Spec.IsJetBomber)
                        bomberPowers.Add(new ShipBomberPower("噴式強襲", Item.CalcJetPower(), 95));

                    var power = Item.CalcAirbaseShipBomberPower(EffectiveLandBasedReconModifier);
                    if (power > 0)
                        bomberPowers.Add(new ShipBomberPower("対艦", power, Item.CalcAirbaseAccuracy()));

                    var specialModifiers = SpecialModifiers;
                    if (specialModifiers != null)
                        foreach (var specialModifier in specialModifiers)
                            bomberPowers.Add(specialModifier.CalcSpecialModifier(this));

                    return bomberPowers;
                }
            }

            private static List<ShipBomberPower> SpecialModifierNonakatai = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", -1, 5)
            };
            private static List<ShipBomberPower> SpecialModifierGingaEgusa = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", -1, 5)
            };
            private static List<ShipBomberPower> SpecialModifierHs293 = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", 14.3, 0)
            };
            private static List<ShipBomberPower> SpecialModifierFritzX = new List<ShipBomberPower>{
                new ShipBomberPower("戦艦", 24.0, 0)
            };
            private static List<ShipBomberPower> SpecialModifierHiryuMissile = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", 17.25, -7),
                new ShipBomberPower("軽巡", 17.25, 7),
                new ShipBomberPower("重巡軽空戦艦", 17.25, 0),
            };
            private static List<ShipBomberPower> SpecialModifierHiryuMissileSkilled = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", 19.5, 0),
                new ShipBomberPower("軽巡", 19.5, 7),
                new ShipBomberPower("重巡軽空戦艦", 19.5, 0),
            };
            private static List<ShipBomberPower> SpecialModifierKi102 = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", -1, 7)
            };
            private static List<ShipBomberPower> SpecialModifierKi102Missile = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", 16.24, -15),
                new ShipBomberPower("軽巡", 16.24, 7),
                new ShipBomberPower("重巡軽空戦艦", 16.24, 0),
            };
            private static List<ShipBomberPower> SpecialModifierB25 = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", 19.6, 21),
                new ShipBomberPower("軽巡", 18, 21),
                new ShipBomberPower("重巡", 16, 21),
                new ShipBomberPower("軽空戦艦補給", 11.5, 0),
            };
            private static List<ShipBomberPower> SpecialModifierHayabusa65 = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐PT", 25, 0)
            };
            private static List<ShipBomberPower> SpecialModifierHayabusa20Skilled = new List<ShipBomberPower>{
                new ShipBomberPower("駆逐", 30, 0)
            };

            public List<ShipBomberPower> SpecialModifiers
            {
                get
                {
                    switch (Item.Spec.Id)
                    {
                        case 170: // 一式陸攻(野中隊)
                            return SpecialModifierNonakatai;
                        case 388: // 銀河(江草隊)
                            return SpecialModifierGingaEgusa;
                        case 405: // Do 217 E-5+Hs293初期型
                            return SpecialModifierHs293;
                        case 406: // Do 217 K-2+Fritz-X
                            return SpecialModifierFritzX;
                        case 444: // 四式重爆 飛龍+イ号一型甲 誘導弾
                            return SpecialModifierHiryuMissile;
                        case 484: // 四式重爆 飛龍(熟練)+イ号一型甲 誘導弾
                            return SpecialModifierHiryuMissileSkilled;
                        case 453: // キ102乙
                            return SpecialModifierKi102;
                        case 454: // キ102乙改+イ号一型乙 誘導弾
                            return SpecialModifierKi102Missile;
                        case 459: // B-25
                            return SpecialModifierB25;
                        case 224: // 爆装一式戦 隼III型改(65戦隊)
                            return SpecialModifierHayabusa65;
                        case 491: // 一式戦 隼III型改(熟練/20戦隊)
                            return SpecialModifierHayabusa20Skilled;
                        default:
                            return null;
                    }
                }
            }

            public LandBomberPower LandBomberPower => new LandBomberPower(Item.CalcAirbaseLandBomberPower(EffectiveLandBasedReconModifier), Item.CalcAirbaseLandBomberPower(EffectiveLandBasedReconModifier, isolatedIsland: true), Item.CalcAirbaseLandBomberPower(EffectiveLandBasedReconModifier, supplyDepot: true));

            public RangeD AswBomberPower => Item.CalcAirbaseAswBomberPower(EffectiveLandBasedReconModifier);

            public double LandBasedReconModifier => Deploying && Item.OnSlot > 0 ? Item.Spec.AirbaseLandBasedReconModifier : 1.0;
        }

        public class ShipBomberPower
        {
            public string Target { get; private set; }
            public double BomberPower { get; private set; }
            public int Accuracy { get; private set; }

            public ShipBomberPower(string target, double bomberPower, int accuracy)
            {
                Target = target;
                BomberPower = bomberPower;
                Accuracy = accuracy;
            }

            public override string ToString()
            {
                return $"{Target}:{ToS(BomberPower)} 命中:{Accuracy}";
            }

            public ShipBomberPower CalcSpecialModifier(PlaneInfo plane)
            {
                return new ShipBomberPower(Target, plane.Item.CalcAirbaseShipBomberPower(plane.EffectiveLandBasedReconModifier, BomberPower), plane.Item.CalcAirbaseAccuracy() + Accuracy);
            }
        }

        public class LandBomberPower
        {
            public double BomberPower { get; private set; }
            public double IsolatedIsland { get; private set; }
            public double SupplyDepot { get; private set; }

            public LandBomberPower(double bomberPower, double isolatedIsland, double supplyDepot)
            {
                BomberPower = bomberPower;
                IsolatedIsland = isolatedIsland;
                SupplyDepot = supplyDepot;
            }

            public override string ToString()
            {
                return $"対地:{ToS(BomberPower)} 離島:{ToS(IsolatedIsland)} 集積:{ToS(SupplyDepot)}";
            }
        }

        public void Inspect(dynamic json)
        {
            AllBase = (from entry in (dynamic[])json
                group
                    new AirCorpsInfo
                    {
                        Distance = CreateDistance(entry.api_distance),
                        Action = (int)entry.api_action_kind,
                        Planes = (from plane in (dynamic[])entry.api_plane_info
                            select new PlaneInfo
                            {
                                Item = _itemInfo.GetStatus((int)plane.api_slotid).FillAirBaseOnSlotMaxEq(
                                    plane.api_count() ? (int)plane.api_count : 0,
                                    plane.api_max_count() ? (int)plane.api_max_count : 0),
                                State = (int)plane.api_state,
                                Cond = plane.api_cond() ? (int)plane.api_cond : 0
                            }).ToArray()
                    } by entry.api_area_id() ? (int)entry.api_area_id : 0
                into grp
                select new BaseInfo {AreaId = grp.Key, AirCorps = grp.ToArray()}).ToArray();
        }

        public void InspectExpandedInfo(dynamic json)
        {
            if (AllBase == null)
                return;
            foreach (var expandedInfo in json)
            {
                var baseInfo = GetAirBase((int)expandedInfo.api_area_id);
                if (baseInfo == null) continue;
                baseInfo.MaintenanceLevel = (int)expandedInfo.api_maintenance_level;
            }
        }

        public void InspectSetPlane(string request, dynamic json)
        {
            if (AllBase == null)
                return;
            var values = HttpUtility.ParseQueryString(request);
            var baseId = json.api_rid() ? (int)json.api_rid : int.Parse(values["api_base_id"]);
            var airCorps = GetBaseInfo(values).AirCorps[baseId - 1];
            if (json.api_distance()) // 2016春イベにはない
                airCorps.Distance = CreateDistance(json.api_distance);
            foreach (var planeInfo in json.api_plane_info)
            {
                var planeId = (int)planeInfo.api_squadron_id - 1;
                var prev = airCorps.Planes[planeId];
                var state = (int)planeInfo.api_state;
                if (state == 2)
                    _relocatingPlanes.Add(prev.Item.Id);
                airCorps.Planes[planeId] = new PlaneInfo
                {
                    Item = _itemInfo.GetStatus((int)planeInfo.api_slotid).FillAirBaseOnSlotMaxEq(
                        planeInfo.api_count() ? (int)planeInfo.api_count : 0,
                        planeInfo.api_max_count() ? (int)planeInfo.api_max_count : 0),
                    State = state,
                    Cond = planeInfo.api_cond() ? (int)planeInfo.api_cond : 0
                };
            }
        }

        public void InspectChangeDeploymentBase(string request, dynamic json)
        {
            if (AllBase == null)
                return;
            foreach (var baseItem in json.api_base_items)
            {
                InspectSetPlane(request, baseItem);
            }
        }

        private Distance CreateDistance(dynamic distance) => distance is double
            // ReSharper disable once PossibleInvalidCastException
            ? new Distance {Base = (int)distance}
            : new Distance {Base = (int)distance.api_base, Bonus = (int)distance.api_bonus};

        public void InspectSupply(string request, dynamic json)
        {
            InspectSetPlane(request, json);
        }

        public void InspectSetAction(string request)
        {
            if (AllBase == null)
                return;
            var values = HttpUtility.ParseQueryString(request);
            var airCorps = GetBaseInfo(values).AirCorps;
            foreach (var entry in
                values["api_base_id"].Split(',')
                    .Zip(values["api_action_kind"].Split(','), (b, a) => new {baseId = b, action = a}))
            {
                airCorps[int.Parse(entry.baseId) - 1].Action = int.Parse(entry.action);
            }
        }

        public void InspectExpandBase(string request, dynamic json)
        {
            var values = HttpUtility.ParseQueryString(request);
            var baseInfo = GetBaseInfo(values);
            var airCorps = baseInfo.AirCorps;
            Array.Resize(ref airCorps, airCorps.Length + 1);
            baseInfo.AirCorps = airCorps;
            airCorps[airCorps.Length - 1] = new AirCorpsInfo
            {
                Distance = new Distance(),
                Planes = ((dynamic[])json[0].api_plane_info).Select(plane =>
                    new PlaneInfo {Item = new ItemStatus()}).ToArray()
            };
        }

        public void InspectExpandMaintenanceLevel(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            var baseInfo = GetBaseInfo(values);
            if (baseInfo != null)
                baseInfo.MaintenanceLevel++;
        }

        private BaseInfo GetBaseInfo(NameValueCollection values)
        {
            var areaId = int.Parse(values["api_area_id"] ?? "0"); // 古いAPIに対応するため
            return AllBase.FirstOrDefault(b => b.AreaId == areaId);
        }

        public void InspectPlaneInfo(dynamic json)
        {
            _relocatingPlanes = json.api_base_convert_slot()
                ? new List<int>((int[])json.api_base_convert_slot)
                : new List<int>();
        }

        public void InspectEventObject(dynamic json)
        {
            InspectPlaneInfo(json);
        }

        public void SetItemHolder()
        {
            if (AllBase == null)
                return;
            var name = new[] {"第一", "第二", "第三"};
            foreach (var baseInfo in AllBase.Select((data, i) => new {data, i}))
            {
                var areaName = baseInfo.data.AreaName;
                foreach (var airCorps in baseInfo.data.AirCorps.Select((data, i) => new {data, i}))
                {
                    var ship = new ShipStatus
                    {
                        Id = 10000 + baseInfo.i * 1000 + airCorps.i,
                        Spec = new ShipSpec {Name = areaName + " " + name[airCorps.i] + "航空隊"}
                    };
                    foreach (var plane in airCorps.data.Planes)
                    {
                        if (!plane.Deploying)
                            continue;
                        _itemInfo.GetStatus(plane.Item.Id).Holder = ship;
                    }
                }
            }
            if (_relocatingPlanes == null)
                return;
            var relocating = new ShipStatus {Id = 1500, Spec = new ShipSpec {Name = "配置転換中"}};
            foreach (var id in _relocatingPlanes)
                _itemInfo.GetStatus(id).Holder = relocating;
        }

        public void CalcLandBasedReconModifier()
        {
            if (AllBase == null)
                return;
            foreach (var baseInfo in AllBase)
                baseInfo.CalcLandBasedReconModifier();
        }
    }
}