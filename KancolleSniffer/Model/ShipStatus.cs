﻿// Copyright (C) 2017 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using static System.Math;
using KancolleSniffer.Util;
using static KancolleSniffer.Model.ShipClassCode;

namespace KancolleSniffer.Model
{
    public class ShipStatus : ICloneable
    {
        public int Id { get; set; }
        public bool Empty => Id == -1;
        public bool Unopened => Id == -2; // 購入済み未開封アイテムに設定した仮の装備艦
        public Fleet Fleet { get; set; }
        public int DeckIndex { get; set; }
        public ShipSpec Spec { get; set; } = new ShipSpec();

        public string Name => Spec.Name;

        public int Level { get; set; }
        public int ExpTotal { get; set; }
        public int ExpToNext { get; set; }
        public int ExpToNextPercent { get; set; }
        public int MaxHp { get; set; }
        public int NowHp { get; set; }
        public int Speed { get; set; }
        public int Cond { get; set; }
        public int Fuel { get; set; }
        public int Bull { get; set; }
        public int[] NdockItem { get; set; }
        public int LoS { get; set; }
        public int Firepower { get; set; }
        public int Torpedo { get; set; }
        public int ShownAsw { get; set; }
        public int MaxAsw { get; set; }
        public int AntiAir { get; set; }
        public int Armor { get; set; }
        public int Lucky { get; set; }
        public int ImprovedFirepower { get; set; }
        public int ImprovedTorpedo { get; set; }
        public int ImprovedAntiAir { get; set; }
        public int ImprovedArmor { get; set; }
        public int ImprovedLucky { get; set; }
        public int ImprovedMaxHp { get; set; }
        public int ImprovedAsw { get; set; }
        public bool Locked { get; set; }
        public int SallyArea { get; set; }

        public int RawFirepower => Spec.MinFirepower + ImprovedFirepower;
        public int RawTorpedo => Spec.MinTorpedo + ImprovedTorpedo;
        public int RawAntiAir => Spec.MinAntiAir + ImprovedAntiAir;
        public int RawArmor => Spec.MinArmor + ImprovedArmor;

        public Damage DamageLevel => CalcDamage(NowHp, MaxHp);
        public bool Escaped { get; set; }
        public bool Dropout => Escaped || DamageLevel == Damage.Sunk;
        public bool CanTransport => !Escaped && DamageLevel < Damage.Badly;
        public bool SupportShip => MaxHp == -1;
        public SpecialAttackStatus SpecialAttack { get; set; } = new SpecialAttackStatus();

        public int[] Slot { get; private set; } = new int[0];
        public int SlotEx { get; private set; } = 0;
        public int ItemNum => Slot.Count(slot => slot > 0);
        public bool SlotFull => Slot.Take(Spec.SlotNum).All(slot => slot > 0);
        public bool SlotSemi => Slot.Take(Spec.SlotNum).Any(slot => slot > 0);
        public bool SlotExEmpty => SlotEx == -1;
        public List<ItemStatus> Items { get; private set; } = new List<ItemStatus>();
        public Func<int, ItemStatus> GetItem { get; set; } = itemId => new ItemStatus(itemId);

        public ShipStatus(int id = -1)
        {
            Id = id;
        }

        protected IEnumerable<ItemStatus> ItemsWithoutEx => ItemsEx(false);

        protected IEnumerable<ItemStatus> ExFirstItems => ItemsEx(true);

        private IEnumerable<ItemStatus> ItemsEx(bool moveTop)
        {
            var items = Items.ToList();
            if (SlotEx > 0)
            {
                var ex = items.Find(item => item.Id == SlotEx);
                if (ex != null)
                {
                    items.Remove(ex);
                    if (moveTop)
                        items.Insert(0, ex);
                }
            }
            return items;
        }

        public void ConsumeItemExFirst(int specId)
        {
            var item = ExFirstItems.FirstOrDefault(item => item.Spec.Id == specId);
            if (item == null)
                return;

            Items.Remove(item);
            if (SlotEx == item.Id)
            {
                SlotEx = -1;
            }
            else
            {
                var index = Array.FindIndex(Slot, slot => slot == item.Id);
                if (index > -1)
                    Slot[index] = -1;
            }
        }

        public void OpenExslot()
        {
            SlotEx = -1;
        }

        public ShipStatus SetItems(int[] slot, int slotEx = 0)
        {
            Slot = slot;
            SlotEx = slotEx;
            Items.Clear();
            Items.AddRange(Slot.Where(slot => slot > 0).Select(slot => GetItem(slot)));
            if (SlotEx > 0)
            {
                Items.Add(GetItem(SlotEx));
            }
            return this;
        }

        public ShipStatus FillOnSlotMaxEq(int[] onSlot)
        {
            var index = 0;
            foreach(var item in ItemsWithoutEx)
            {
                item.OnSlot = onSlot?.ElementAtOrDefault(index) ?? 0;
                item.MaxEq = item.Spec.IsFlyingBoat ? 1 : Spec.MaxEq?.ElementAtOrDefault(index) ?? 0;
                index++;
            }
            return this;
        }

        public ShipStatus ResolveTbBonus(AdditionalData additionalData)
        {
            foreach(var item in Items)
                item.TbBonus = 0;

            var bonusItem = Items.Where(item => item.HasAnyBomber).OrderByDescending(item => item.Spec.BomberSpec).ThenByDescending(item => item.OnSlot).FirstOrDefault();
            if (bonusItem != null)
                bonusItem.TbBonus = additionalData.TbBonus(Spec.Id, Items.Select(item => item.Spec.Id));

            return this;
        }

        public ShipStatus ResolveApBonus()
        {
            var bonusItem = GetSkilledDeckAviationMaintenance;
            foreach(var item in Items)
            {
                if (item.Spec.IsDiveBomber)
                    item.ApBonus = bonusItem.AviationPersonnelBomberBonus;
                else if (item.Spec.IsTorpedoBomber)
                    item.ApBonus = bonusItem.AviationPersonnelTorpedoBonus;
                else
                    item.ApBonus = 0;
            }
            return this;
        }

        public ShipStatus FillEnemyStatus()
        {
            Firepower = RawFirepower + Items.Sum(item => item.Spec.Firepower);
            Torpedo = RawTorpedo + Items.Sum(item => item.Spec.Torpedo);
            AntiAir = RawAntiAir + Items.Sum(item => item.Spec.AntiAir);
            Armor = RawArmor + Items.Sum(item => item.Spec.Armor);
            return this;
        }

        public enum Damage
        {
            Minor,
            Small,
            Half,
            Badly,
            Sunk
        }

        public void Repair()
        {
            NowHp = MaxHp;
            Cond = Max(40, Cond);
        }

        public string DisplayHp => SupportShip ? "-/-" : $"{NowHp:D}/{MaxHp:D}";

        public string DisplayHpPercent => SupportShip ? "100%" : $"{(int)Floor(NowHp * 100.0 / MaxHp):D}%";

        public static Damage CalcDamage(int now, int max)
        {
            if (max == -1)
                return Damage.Minor;
            if (now == 0 && max > 0)
                return Damage.Sunk;
            var ratio = max == 0 ? 1 : (double)now / max;
            return ratio > 0.75 ? Damage.Minor :
                ratio > 0.5 ? Damage.Small :
                ratio > 0.25 ? Damage.Half : Damage.Badly;
        }

        public TimeSpan RepairTime => TimeSpan.FromSeconds((int)(RepairTimePerHp.TotalSeconds * (MaxHp - NowHp)) + 30);

        public TimeSpan RepairTimePerHp =>
            TimeSpan.FromSeconds(Spec.RepairWeight *
                                 (Level < 12
                                     ? Level * 10
                                     : Level * 5 + Floor(Sqrt(Level - 11)) * 10 + 50));

        // 速吸改、山汐丸(艦攻艦爆装備、0機でも)
        private bool IsRyuseiAttack => Spec.IsOilerAircraftCarrier && Items.Any(item => item.Spec.IsCarrierBasedBomber);

        // 速吸改、山汐丸(対潜可能機装備、0機でも)
        private bool IsRyuseiAsw => Spec.IsOilerAircraftCarrier && Items.Any(item => item.Spec.IsAnyBomber || item.Spec.IsAswPatrol);

        // 第百一号輸送艦(爆雷装備)
        private bool IsNo101Asw => Spec.ShipClass == No101LandingShip && Items.Any(item => item.Spec.IsDCs);

        private ItemStatus GetSkilledDeckAviationMaintenance => Items.Where(item => item.Spec.IsSkilledDeckAviationMaintenance).OrderByDescending(item => item.Level).FirstOrDefault() ?? new ItemStatus();

        public double EffectiveFirepower
        {
            get
            {
                if (Spec.IsSubmarine)
                    return 0;

                if (Spec.IsAircraftCarrier || IsRyuseiAttack)
                    return EffectiveAircraftCarrierFirepower;

                return Firepower + Items.Sum(item => item.FirepowerLevelBonus) + Fleet.CombinedFirepowerBonus + 5 + ShipTypeGunTypeFirepower;
            }
        }

        public int SupportFirepower
        {
            get
            {
                if (Spec.IsSubmarine)
                    return 0;

                if (Spec.IsAircraftCarrier)
                    return SupportAircraftCarrierFirepower;

                return Firepower + 4;
            }
        }

        public double EffectiveAircraftCarrierFirepower
        {
            get
            {
                if (!Items.Any(item => item.HasCarrierBasedBomber))
                    return 0;

                var ap = GetSkilledDeckAviationMaintenance;
                var torpedo = Items.Where(item => item.Spec.IsAircraft || item.Spec.IsAviationPersonnel).Sum(item => item.Spec.Torpedo) + ap.AviationPersonnelTorpedoBonus;
                var bomber = Items.Where(item => item.Spec.IsAircraft || item.Spec.IsAviationPersonnel).Sum(item => item.Spec.Bomber) + ap.AviationPersonnelBomberBonus;

                return (int)((Firepower + torpedo + Items.Sum(item => item.FirepowerLevelBonus + item.TbBonus) +
                              (int)(bomber * 1.3) + Fleet.CombinedFirepowerBonus) * 1.5) + 55;
            }
        }

        public int SupportAircraftCarrierFirepower
        {
            get
            {
                if (!Items.Any(item => item.HasCarrierBasedBomber))
                    return 0;

                var ap = GetSkilledDeckAviationMaintenance;
                var torpedo = Items.Where(item => item.Spec.IsAircraft || item.Spec.IsAviationPersonnel).Sum(item => item.Spec.Torpedo) + ap.AviationPersonnelTorpedoBonus;
                var bomber = Items.Where(item => item.Spec.IsAircraft || item.Spec.IsAviationPersonnel).Sum(item => item.Spec.Bomber) + ap.AviationPersonnelBomberBonus;

                return (int)((Firepower + torpedo + (int)(bomber * 1.3) - 1) * 1.5) + 55;
            }
        }

        public double ShipTypeGunTypeFirepower
        {
            get
            {
                if (Spec.IsLightCruiserClass)
                    return Sqrt(Items.Count(item => item.Spec.IsCLSingleGun)) + Sqrt(Items.Count(item => item.Spec.IsCLTwinGun)) * 2;

                if (Spec.ShipClass == Zara)
                    return Sqrt(Items.Count(item => item.Spec.IsItalyCAGun));

                return 0;
            }
        }

        public double ShipTypeGunTypeAccuracy
        {
            get
            {
                if (Spec.IsLightCruiserClass)
                    return CLGunTypeAccuracy;

                if (Spec.ShipClass == Zara)
                    return Items.Count(item => item.Spec.IsItalyCAGun);

                if (Spec.ShipType == 1)
                    return -13;

                return 0;
            }
        }

        public double CLGunTypeAccuracy
        {
            get
            {
                var married = Level > 99;
                var accuracy = married ? Items.Count(item => item.Spec.Is140mmGun) : 0.0;

                if (Spec.ShipClass == Agano)
                    accuracy += (married ? 6 : 5) * Math.Sqrt(Items.Count(item => item.Spec.Is152mmTwinGun));
                else
                    accuracy += married ? Items.Count(item => item.Spec.Is152mmTwinGun) : 0;

                accuracy += (married ? -1.8 : -3) * Items.Count(item => item.Spec.Is152mmTripleGun);

                if (Spec.ShipClass == Atlanta)
                    accuracy += married ? 1.5 * Items.Count(item => item.Spec.Is127mmConcentratedGun) : 0;
                else
                    accuracy += (married ? -1.8 : -3) * Items.Count(item => item.Spec.Is127mmConcentratedGun);

                if (Spec.ShipClass == Oyodo)
                    accuracy += (married ? -0.6 : -1) * Items.Count(item => item.Spec.Is155mmTripleGun);
                else
                    accuracy += (married ? -0.5 : -2.5) * Items.Count(item => item.Spec.Is155mmTripleGun);

                if (Spec.ShipClass == Agano)
                    accuracy += (married ? -3 : -4) * Items.Count(item => item.Spec.Is203mmTwinGun);
                else if (Spec.ShipClass == Oyodo)
                    accuracy += (married ? -3 : -5) * Items.Count(item => item.Spec.Is203mmTwinGun);
                else
                    accuracy += (married ? -1.8 : -3) * Items.Count(item => item.Spec.Is203mmTwinGun);

                accuracy += (married ? -8.8 : -11) * Items.Count(item => item.Spec.Is203mmTripleGun);

                return accuracy;
            }
        }

        public double EffectiveAccuracy => Fleet.BaseAccuracy + Sqrt(Level) * 2 + Sqrt(Lucky) * 1.5 + EquipAccuracy + ShipTypeGunTypeAccuracy;

        public double SupportAccuracy => 64 + Sqrt(Level) * 2 + Sqrt(Lucky) * 1.5 + SupportEquipAccuracy;

        public int APShellModifierRank
        {
            get
            {
                if (Items.Any(item => item.Spec.IsPrimaryGun) && Items.Any(item => item.Spec.IsAPShell))
                {
                    var hasSecondaryGun = Items.Any(item => item.Spec.IsSecondaryGun);
                    var hasRadar = Items.Any(item => item.Spec.IsRadar);

                    if (hasSecondaryGun && hasRadar)
                        return 4;
                    if (hasRadar)
                        return 3;
                    if (hasSecondaryGun)
                        return 2;

                    return 1;
                }

                return 0;
            }
        }

        public double APShellFirepowerModifier
        {
            get
            {
                switch (APShellModifierRank)
                {
                    case 1:
                        return 1.08;
                    case 2:
                        return 1.15;
                    case 3:
                        return 1.1;
                    case 4:
                        return 1.15;
                    default:
                        return 0;
                }
            }
        }

        public double APShellAccuracyModifier
        {
            get
            {
                switch (APShellModifierRank)
                {
                    case 1:
                        return 1.1;
                    case 2:
                        return 1.2;
                    case 3:
                        return 1.25;
                    case 4:
                        return 1.3;
                    default:
                        return 0;
                }
            }
        }

        public double EffectiveTorpedo
        {
            get
            {
                if (RawTorpedo > 0)
                    return Torpedo + Items.Sum(item => item.TorpedoLevelBonus) + Fleet.CombinedTorpedoPenalty + 5;

                return 0;
            }
        }

        public double EffectiveAsw
        {
            get
            {
                var rawAsw = RawAsw;
                // 表示対潜から素対潜と対潜攻撃力に計算されない装備を引くことで、ボーナス込みの装備対潜とする
                var itemAsw = ShownAsw - rawAsw - Items.Sum(item => item.Spec.IneffectiveAsw);
                var smallSonar = Items.Any(item => item.Spec.IsSmallSonar);
                var largeSonar = Items.Any(item => item.Spec.IsLargeSonar);
                var dct = Items.Any(item => item.Spec.IsDCT);
                var dc = Items.Any(item => item.Spec.IsDC);
                var miscDC = Items.Any(item => item.Spec.IsMiscDC);
                var levelBonus = Items.Sum(item => item.AswLevelBonus);

                var bonus = 1.0;
                if (smallSonar && dct && dc)
                    bonus = 1.15 * 1.25;
                else if (largeSonar && dct && dc)
                    bonus = 1.15 * 1.1;
                else if ((smallSonar || largeSonar) && (dct || dc || miscDC))
                    bonus = 1.15;
                else if (dct && dc)
                    bonus = 1.1;

                return bonus * (Sqrt(rawAsw) * 2 + itemAsw * 1.5 + levelBonus + EffectiveAswBase);
            }
        }

        public int EffectiveAswBase
        {
            get
            {
                // 加賀改二護、速吸改、山汐丸(対潜可能機装備、0機でも)
                if (Spec.Id == 646 || IsRyuseiAsw)
                    return 8;

                // 第百一号輸送艦(爆雷装備)
                if (IsNo101Asw)
                    return 13;

                switch (Spec.ShipType)
                {
                    case  1: // 海防艦
                    case  2: // 駆逐
                    case  3: // 軽巡
                    case  4: // 雷巡
                    case 21: // 練巡
                    case 22: // 補給艦
                        return 13;
                    case  6: // 航巡
                    case  7: // 軽空
                    case 10: // 航戦
                    case 16: // 水母
                    case 17: // 揚陸艦
                        return 8;
                    default:
                        return 0;
                }
            }
        }

        public int RawAsw
        {
            get
            {
                // 対潜最大値はapiから取得可能。この値が0なら、素対潜は改修分(瑞鳳改二など)のみと判定
                if (MaxAsw < 1)
                    return ImprovedAsw;

                // 対潜最大値が設定されていて対潜最小値が未定義(新艦など)なら、表示値から装備対潜を引いて仮の素対潜とする
                if (Spec.MinAsw < 0)
                    return ShownAsw - Items.Sum(item => item.Spec.Asw);

                // レベルと最大値最小値から現在の素対潜を計算する
                return (int)((MaxAsw - Spec.MinAsw) * Level / 99.0) + Spec.MinAsw + ImprovedAsw;
            }
        }

        public bool UnresolvedAsw => MaxAsw > 0 && Spec.MinAsw < 0;

        private bool HasSonar => Items.Any(item => item.Spec.IsSonar);

        private bool HasAswEnabledAircraft => Items.Any(item => item.HasAswEnabledBomber || item.HasAswPatrol || item.HasFlyingBoat);

        public bool EnableAsw
        {
            get
            {
                // 軽空母、加賀改二護、速吸改、山汐丸(艦攻装備、0機でも)
                if (Spec.ShipType == 7 || Spec.Id == 646 || IsRyuseiAttack)
                    return Items.Any(item => item.HasCarrierBasedBomber) &&
                              (Items.Any(item => item.HasAswEnabledBomber) ||
                               Items.Any(item => item.HasAnyBomber) && Items.Any(item => item.HasAswPatrol));

                // 速吸改、山汐丸(対潜可能機装備、0機でも)
                if (IsRyuseiAsw)
                    return HasAswEnabledAircraft;

                // 第百一号輸送艦(爆雷装備)
                if (IsNo101Asw)
                    return true;

                switch (Spec.ShipType)
                {
                    case  1: // 海防艦
                    case  2: // 駆逐
                    case  3: // 軽巡
                    case  4: // 雷巡
                    case 21: // 練巡
                    case 22: // 補給艦
                        return RawAsw > 0;
                    case  6: // 航巡
                    case 10: // 航戦
                    case 16: // 水母
                    case 17: // 揚陸艦
                        return HasAswEnabledAircraft;
                    default:
                        return false;
                }
            }
        }

        public bool EnableOpeningAsw
        {
            get
            {
                if (Spec.Id == 508 || Spec.Id == 509) // 鈴谷航改二、熊野航改二
                    return false;

                if (Spec.IsAswCruiserDestroyer)
                    return true;

                if (Spec.IsAswAircraftCarrier)
                    return HasAswEnabledAircraft;

                if (Spec.Id == 554) // 日向改二
                    return Items.Count(item => item.HasAutoGiro) > 1 ||
                           Items.Count(item => item.HasHelicopter) > 0;

                if (Spec.ShipType == 1) // 海防艦
                    return ShownAsw > 74 && Items.Sum(item => item.Spec.Asw) > 3 ||
                           ShownAsw > 59 && HasSonar;

                if (Spec.ShipType == 7) // 軽空母
                    return HasAswEnabledAircraft &&
                          (ShownAsw > 99 && HasSonar ||
                           ShownAsw > 64 && Items.Any(item => item.Spec.IsAswTorpedoBomber || item.Spec.IsAswPatrol) ||
                           ShownAsw > 49 && Items.Any(item => item.Spec.IsAswTorpedoBomber || item.Spec.IsAswPatrol) && HasSonar);

                return ShownAsw > 99 && HasSonar && EnableAsw;
            }
        }

        public double NightBattlePower
        {
            get
            {
                if (Items.Any(item => item.HasNightAircraft) && // 夜戦か夜攻
                    (Spec.IsNightAircraftCarrier ||            // 夜戦空母
                     Spec.IsAircraftCarrier && Items.Any(item => item.Spec.IsNightOperationAviationPersonnel))) // 空母かつ夜間作戦航空要員
                    return RawFirepower + GetSkilledDeckAviationMaintenance.AviationPersonnelNightBattleBonus + Items.Sum(item => item.CalcNightAircraftPower());

                // アークロイヤル+Swordfish
                if (Spec.ShipClass == 78 && Items.Any(item => item.HasSwordfishTorpedoBomber))
                    return RawFirepower + GetSkilledDeckAviationMaintenance.AviationPersonnelNightBattleBonus + Items.Sum(item => item.CalcSwordfishTorpedoPower());

                if (Spec.EnableNightShelling)
                    return Firepower + Torpedo + Items.Sum(item => item.NightBattleLevelBonus);

                return 0;
            }
        }

        // 遠征時の装備改修ボーナスは小数第一位まで有効なため、10倍の値の整数値で管理する
        public int MissionFirepower => Firepower * 10 + Items.Sum(item => item.MissionFirepowerLevelBonus);

        public int MissionAntiAir => AntiAir * 10 + Items.Sum(item => item.MissionAntiAirLevelBonus);

        public int MissionAsw => ShownAsw * 10 + Items.Sum(item => item.MissionAswLevelBonus);

        public int MissionLoS => LoS * 10 + Items.Sum(item => item.MissionLoSLevelBonus);

        public bool HasNoneAircraft => Items.Any(item => item.HasNoneAircraft);

        public int FindDamageControl => ExFirstItems.FirstOrDefault(item => item.Spec.IsDamageControl)?.Spec.Id ?? -1;

        public int PreparedDamageControl => DamageLevel != Damage.Badly ? -1 : FindDamageControl;

        public double TransportPoint => Spec.TransportPoint + Items.Sum(item => item.Spec.TransportPoint);

        public double EquipBonusAntiAir => AntiAir - RawAntiAir - Items.Sum(item => item.Spec.AntiAir);

        public int EffectiveAntiAirForShip
        {
            get
            {
                if (!Items.Any())
                    return RawAntiAir;

                if (Spec.IsEnemy)
                    return 2 * (int)Sqrt(RawAntiAir + Items.Sum(item => item.Spec.AntiAir)) + (int)Items.Sum(item => item.EffectiveAntiAirForShip);

                return (int)((RawAntiAir + Items.Sum(item => item.EffectiveAntiAirForShip) + EquipBonusAntiAir * 0.75) / 2) * 2;
            }
        }

        public int EffectiveAntiAirForFleet => (int)Items.Sum(item => item.EffectiveAntiAirForFleet);

        public double PropShootdown => EffectiveAntiAirForShip * Fleet.AntiAirModifierCombined / 4.0;

        public double FixedShootdownModifierEnemy => Spec.IsEnemy ? 0.09375 : 0.1;

        public double CalcFixedShootdown(double formationModifier) => (EffectiveAntiAirForShip + Fleet.CalcEffectiveAntiAir(formationModifier) + EquipBonusAntiAir * 0.5) * Fleet.AntiAirModifierCombined * FixedShootdownModifierEnemy;

        public double FixedShootdown => CalcFixedShootdown(Fleet.AntiAirModifierFormation);

        public double AntiAirPropellantBarrageChance
        {
            get
            {
                if (!Spec.EnableAntiAirPropellantBarrage)
                    return 0;
                var launcherCount = Items.Count(item => item.Spec.Id == 274);
                if (launcherCount == 0)
                    return 0;
                var iseClass = Spec.ShipClass == 2;
                var baseChance = (EffectiveAntiAirForShip + 0.9 * Lucky) / 281.0;
                return (baseChance + 0.15 * (launcherCount - 1) + (iseClass ? 0.25 : 0)) * 100;
            }
        }

        public double EquipAccuracy => Items.Sum(item => item.Spec.Accuracy + item.AccuracyLevelBonus);

        public int SupportEquipAccuracy => Items.Sum(item => item.Spec.Accuracy);

        public int EffectiveFuelMax => Max((int)(Spec.FuelMax * (Level >= 100 ? 0.85 : 1.0)), Empty ? 0 : 1);

        public int EffectiveBullMax => Max((int)(Spec.BullMax * (Level >= 100 ? 0.85 : 1.0)), Empty ? 0 : 1);

        public int RawLoS => LoS - Items.Sum(item => item.Spec.LoS);

        public EnemyAircrafts EnemyLoggerAirCombat() =>
            new()
            {
                FighterPower = Spec.IsEnemy
                    ? Items.Where(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0 && item.MaxEq > 0).Sum(item => (int)Floor(item.Spec.AntiAir * Sqrt(item.MaxEq)))
                    : Items.Sum(item => item.CalcPracticeFighterPower(true)),
                Aircrafts = Items.Where(item => item.Spec.CanAirCombat && item.MaxEq > 0).Sum(item => item.MaxEq),
                Unresolved = Spec.MaxEq == null && Items.Any(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0)
            };

        public EnemyAircrafts EnemyActualAirCombat() =>
            new()
            {
                FighterPower = Spec.IsEnemy
                    ? Items.Where(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0 && item.OnSlot > 0).Sum(item => (int)Floor(item.Spec.AntiAir * Sqrt(item.OnSlot)))
                    : Items.Sum(item => item.CalcPracticeFighterPower(false)),
                Aircrafts = Items.Where(item => item.Spec.CanAirCombat && item.OnSlot > 0).Sum(item => item.OnSlot),
                Unresolved = Spec.MaxEq == null && Items.Any(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0)
            };

        public EnemyAircrafts EnemyInterception() =>
            Spec.IsEnemy ? new()
            {
                FighterPower = Items.Where(item => item.Spec.IsAircraft && item.Spec.AntiAir > 0 && item.MaxEq > 0).Sum(item => (int)Floor(item.Spec.AntiAir * Sqrt(item.MaxEq))),
                Aircrafts = Items.Where(item => item.Spec.IsAircraft && item.MaxEq > 0).Sum(item => item.MaxEq),
                Unresolved = Spec.MaxEq == null && Items.Any(item => item.Spec.IsAircraft && item.Spec.AntiAir > 0)
            } : new();

        public string GetToolTipString()
        {
            var result = new List<string>();
            if (Spec.IsEnemy)
            {
                result.Add("id:" + Spec.Id);
                result.Add($"装備命中:{EquipAccuracy}");
                result.Add($"装甲:{Armor} ({new RangeD(Armor * 0.7, Armor * 1.3).RangeString})");
                result.Add($"対空 割合:{StringUtil.ToS(PropShootdown)}% 固定:{StringUtil.ToS(FixedShootdown)}");

                if (Items.Any(item => item.Spec.IsAircraft))
                {
                    var airCombat    = EnemyLoggerAirCombat();
                    var interception = EnemyInterception();
                    var airCombatText    =    airCombat.Unresolved ? "?" : airCombat.FighterPower.ToString();
                    var interceptionText = interception.Unresolved ? "?" : interception.FighterPower.ToString();
                    result.Add($"制空:{airCombatText} 対基地:{interceptionText}");
                }
            }

            result.AddRange(Items.Select(item => item.Spec.Name +
                            (item.Level > 0 ? $"★{item.Level}" : "") +
                            (item.Spec.IsAircraft ? $" +{item.Alv} {item.OnSlot}/{item.MaxEq}" : "")));

            return string.Join("\r\n", result);
        }

        public static string SpeedName(int speed) => speed < 0 ? "未定" : speed > 20 ? "不明" : new[] {"地上", "低速", "高速", "高速+", "最速"}[speed / 5];

        public object Clone()
        {
            var r = (ShipStatus)MemberwiseClone();
            r.Items = Items.ToList(); // 戦闘中のダメコンの消費が見えないように複製する
            r.Slot = Slot.ToArray();
            r.SpecialAttack = (SpecialAttackStatus)SpecialAttack.Clone();
            return r;
        }
    }

    public class SpecialAttackStatus : ICloneable
    {
        public bool Fire { get; set; }
        public int Type { get; set; }
        public int Count { get; set; }

        public void Trigger(int type)
        {
            Fire = true;
            Type = type;
            Count++;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }

    public class EnemyAircrafts
    {
        public int FighterPower { get; set; }
        public int Aircrafts { get; set; }
        public bool Unresolved { get; set; }

        public EnemyAircrafts Add(EnemyAircrafts other)
        {
            FighterPower += other.FighterPower;
            Aircrafts    += other.Aircrafts;
            Unresolved   |= other.Unresolved;
            return this;
        }

        public bool Equals(EnemyAircrafts other)
        {
            return FighterPower == other.FighterPower && Aircrafts == other.Aircrafts && Unresolved == other.Unresolved;
        }
    }
}
