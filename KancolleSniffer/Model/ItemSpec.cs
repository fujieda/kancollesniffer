﻿// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;

namespace KancolleSniffer.Model
{
    public class ItemSpec
    {
        public int Id;
        public bool Empty => Id == -1;
        public string Name;
        public int Type;
        public string TypeName;
        public int IconType;
        public int SubType;
        public int SubType2;
        public int Firepower;
        public int Accuracy;
        public int AntiAir;
        public int Armor;
        public int LoS;
        public int Asw;
        public int Torpedo;
        public int Bomber;
        public int Interception;
        public int AntiBomber;
        public int Cost;
        public int Distance;

        public ItemSpec()
        {
            Id = -1;
            Name = "";
        }

        public bool IsPrimaryGun => Type == 1 || Type == 2 || Type == 3;

        public bool IsSecondaryGun => Type == 4;

        public bool IsCLSingleGun =>
            Id ==  4 || // 14cm単装砲
            Id == 11;   // 15.2cm単装砲

        public bool IsCLTwinGun =>
            Id == 119 || // 14cm連装砲
            Id == 310 || // 14cm連装砲改
            Id ==  65 || // 15.2cm連装砲
            Id == 139 || // 15.2cm連装砲改
            Id == 407 || // 15.2cm連装砲改二
            Id == 359 || // 6inch 連装速射砲 Mk.XXI
            Id == 303 || // Bofors 15.2cm連装砲 Model 1930
            Id == 360 || // Bofors 15cm連装速射砲 Mk.9 Model 1938
            Id == 361;   // Bofors 15cm連装速射砲 Mk.9改+単装速射砲 Mk.10改 Model 1938

        public bool IsItalyCAGun =>
            Id == 162; // 203mm/53 連装砲

        public bool Is140mmGun =>
            Id ==   4 || // 14cm単装砲
            Id == 119 || // 14cm連装砲
            Id == 310;   // 14cm連装砲改

        public bool Is152mmTwinGun =>
            Id ==  65 || // 15.2cm連装砲
            Id == 139 || // 15.2cm連装砲改
            Id == 407 || // 15.2cm連装砲改二
            Id == 359 || // 6inch 連装速射砲 Mk.XXI
            Id == 303 || // Bofors 15.2cm連装砲 Model 1930
            Id == 360 || // Bofors 15cm連装速射砲 Mk.9 Model 1938
            Id == 361;   // Bofors 15cm連装速射砲 Mk.9改+単装速射砲 Mk.10改 Model 1938

        public bool Is152mmTripleGun =>
            Id == 340 || // 152mm/55 三連装速射砲
            Id == 341 || // 152mm/55 三連装速射砲改
            Id == 386 || // 6inch三連装速射砲 Mk.16
            Id == 387 || // 6inch三連装速射砲 Mk.16 mod.2
            Id == 399;   // 6inch Mk.XXIII三連装砲

        public bool Is127mmConcentratedGun =>
            Id == 362 || // 5inch連装両用砲(集中配備)
            Id == 363;   // GFCS Mk.37+5inch連装両用砲(集中配備)

        public bool Is155mmTripleGun =>
            Id ==   5 || // 15.5cm三連装砲
            Id == 235;   // 15.5cm三連装砲改

        public bool Is203mmTwinGun =>
            Id ==   6 || // 20.3cm連装砲
            Id ==  50 || // 20.3cm(3号)連装砲
            Id ==  90 || // 20.3cm(2号)連装砲
            Id == 123 || // SKC34 20.3cm連装砲
            Id == 162;   // 203mm/53 連装砲

        public bool Is203mmTripleGun =>
            Id == 356 || // 8inch三連装砲 Mk.9
            Id == 357;   // 8inch三連装砲 Mk.9 mod.2

        public bool CanAirCombat
        {
            get
            {
                switch (Type)
                {
                    case  6: // 艦戦
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case 11: // 水爆
                    case 45: // 水戦
                    case 56: // 噴式戦闘機
                    case 57: // 噴式戦闘爆撃機
                    case 58: // 噴式攻撃機
                        return true;
                    default:
                        return IsFighterAswPatrol;
                }
            }
        }

        public bool CanAswSupport
        {
            get
            {
                switch (Type)
                {
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case 10: // 水偵
                    case 11: // 水爆
                    case 25: // 回転翼機
                    case 26: // 対潜哨戒機
                    case 41: // 大艇
                    case 45: // 水戦
                        return Asw > 0;
                    default:
                        return false;
                }
            }
        }

        // http://ja.kancolle.wikia.com/wiki/%E3%83%9E%E3%83%83%E3%83%97%E7%B4%A2%E6%95%B5
        public double LoSScaleFactor
        {
            get
            {
                switch (Type)
                {
                    case  8: // 艦攻
                        return 0.8;
                    case  9: // 艦偵
                        return 1;
                    case 10: // 水偵
                        return 1.2;
                    case 11: // 水爆
                        return 1.1;
                    default:
                        return 0.6;
                }
            }
        }

        public bool IsAircraft
        {
            get
            {
                switch (Type)
                {
                    case  6: // 艦戦
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case  9: // 艦偵
                    case 10: // 水偵
                    case 11: // 水爆
                    case 25: // オートジャイロ
                    case 26: // 対潜哨戒機
                    case 41: // 大艇
                    case 45: // 水戦
                    case 47: // 陸上攻撃機
                    case 48: // 局地戦闘機
                    case 49: // 陸上偵察機
                    case 53: // 大型陸上機
                    case 56: // 噴式戦闘機
                    case 57: // 噴式戦闘爆撃機
                    case 58: // 噴式攻撃機
                    case 59: // 噴式偵察機
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsDiveBomber => Type == 7 || Type == 11 || Type == 57;

        public bool IsTorpedoBomber => Type == 8 || Type == 58;

        public bool IsAnyBomber => IsDiveBomber || IsTorpedoBomber || IsBomberAswPatrol;

        public bool IsCarrierBasedBomber => Type == 7 || Type == 8 || Type == 57 || Type == 58;

        public bool IsJetBomber => Type == 57;

        public bool IsLandBasedBomber => Type == 47;

        public bool IsLargeLandBasedBomber => Type == 53;

        public bool IsAnyLandBasedBomber => IsLandBasedBomber || IsLargeLandBasedBomber;

        public bool IsLandBasedRecon => Type == 49;

        //  60: 零式艦戦62型(爆戦)
        // 154: 零戦62型(爆戦/岩井隊)
        // 219: 零式艦戦63型(爆戦)
        // 447: 零式艦戦64型(複座KMX搭載機)
        public bool IsBakusen => Type == 7 && SubType == 12;

        public bool IsNightAircraft => IconType == 45 || IconType == 46;

        // 242: Swordfish
        // 243: Swordfish Mk.II(熟練)
        // 244: Swordfish Mk.III(熟練)
        public bool IsSwordfishTorpedoBomber => IsTorpedoBomber && SubType == 28;

        // 154: 零戦62型(爆戦/岩井隊)
        // 320: 彗星一二型(三一号光電管爆弾搭載機)
        // Swordfish系艦攻
        public bool IsNightAircraftSub => Id == 154 || Id == 320 || IsSwordfishTorpedoBomber;

        // 258: 夜間作戦航空要員
        // 259: 夜間作戦航空要員+熟練甲板員
        public bool IsNightOperationAviationPersonnel => Id == 258 || Id == 259;

        public bool IsAswEnabledBomber => (IsDiveBomber || IsTorpedoBomber) && Asw > 0;

        public bool IsAswTorpedoBomber => IsTorpedoBomber && Asw > 6;

        public bool IsAviationPersonnel => Type == 35;

        // 478: 熟練甲板要員+航空整備員
        public bool IsSkilledDeckAviationMaintenance => Id == 478;

        public int IneffectiveAsw
        {
            get
            {
                switch (Type)
                {
                    case 14: // ソナー
                    case 40: // 大型ソナー
                    case 15: // 爆雷
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case 11: // 水爆
                    case 57: // 噴式戦闘爆撃機
                    case 58: // 噴式攻撃機
                    case 25: // オートジャイロ
                    case 26: // 対潜哨戒機
                    case 41: // 大艇
                        return 0;
                    default:
                        return Asw;
                }
            }
        }

        public bool IsSmallSonar => Type == 14;

        public bool IsLargeSonar => Type == 40;

        public bool IsSonar => IsSmallSonar || IsLargeSonar;

        public bool IsDCs => Type == 15;

        public bool IsDCT => // Depth Charge Thrower
            Id == 44  || // 九四式爆雷投射機
            Id == 45  || // 三式爆雷投射機
            Id == 287 || // 三式爆雷投射機 集中配備
            Id == 288 || // 試製15cm9連装対潜噴進砲
            Id == 377 || // RUR-4A Weapon Alpha改
            Id == 472;   // Mk.32 対潜魚雷(Mk.2落射機)

        public bool IsDC => // Depth Charge
            Id == 226 || // 九五式爆雷
            Id == 227 || // 二式爆雷
            Id == 378 || // 対潜短魚雷(試作初期型)
            Id == 439 || // Hedgehog(初期型)
            Id == 488;   // 二式爆雷改二

        public bool IsMiscDC => IsDCs && !IsDCT && !IsDC;

        public bool IsRadar => IconType == 11;

        // 索敵5以上の電探
        public bool IsSurfaceRadar => IsRadar && LoS > 4;

        public bool IsSubmarineRadar => Type == 51;

        public bool IsAPShell => Type == 19;

        public bool IsRepairFacility => Type == 31;

        public bool IsDrum => Id == 75;

        // Type == 23 でいいが、古いテストが対応できない
        public bool IsDamageControl => Id == 42 || Id == 43;

        public bool IsAswPatrol => Type == 25 || Type == 26;

        public bool IsFighterAswPatrol => IsAswPatrol && AntiAir > 0;

        public bool IsBomberAswPatrol => IsAswPatrol && Bomber > 0;

        public bool IsContactTrigger =>
            Type ==  9 || // 艦偵
            Type == 10 || // 水偵
            Type == 41 || // 大艇
            Type == 49;   // 陸偵

        public bool IsContactSelection => IsContactTrigger || Type ==  8; // 艦攻

        // Id == 102 (九八式夜偵) は過去データとの互換性
        public bool IsNightRecon => IconType == 50 || Id == 102;

        public bool IsAutoGiro => Type == 25;

        public bool IsHelicopter => SubType2 == 44;

        public bool IsFlyingBoat => Type == 41;

        // Type == 48 && SubType = 35 || SubType = 36 でいいかもしれない。新しい機体が追加されたら確認
        public bool IsHighAltitudeInterceptor =>
            Id == 350 || // Me163B
            Id == 351 || // 試製 秋水
            Id == 352;   // 秋水

        public Func<double> GetItemTp { get; set; }

        public double TransportPoint
        {
            get
            {
                var tp = GetItemTp?.Invoke();
                if (tp >= 0)
                    return (double)tp;

                switch (Type)
                {
                    case 30: // 簡易輸送部材
                        return 5.0;
                    case 24: // 上陸用舟艇
                        return 8.0;
                    case 46: // 特型内火艇
                        return 2.0;
                    case 43: // 戦闘糧食
                        return 1.0;
                    default:
                        return 0;
                }
            }
        }

        public AirCorpsFighterPower.Pair ReconPlaneAirBaseBonus
        {
            get
            {
                switch (Type)
                {
                    case 9:
                        return new AirCorpsFighterPower.Pair(1.0, LoS <= 7 ? 1.2 : 1.3);
                    case 10:
                    case 41:
                        return new AirCorpsFighterPower.Pair(1.0, LoS <= 7 ? 1.1 : LoS <= 8 ? 1.13 : 1.16);
                    case 49:
                        return new AirCorpsFighterPower.Pair(LoS <= 8 ? 1.15 : 1.18, LoS <= 8 ? 1.18 : 1.24);
                    default:
                        return new AirCorpsFighterPower.Pair(1.0, 1.0);
                }
            }
        }

        public int BomberSpec => IsDiveBomber ? Bomber : IsTorpedoBomber ? Torpedo : 0;

        public int AirbaseBomberSpec => IsBomberAswPatrol ? Bomber : BomberSpec;

        public int AirbaseShipBomberSpec => IsAnyLandBasedBomber ? Torpedo : AirbaseBomberSpec;

        public int AirbaseLandBomberSpec => IsAnyLandBasedBomber ? Bomber : AirbaseBomberSpec;

        public double JetBomberModifier => IsJetBomber ? 1 / Math.Sqrt(2) : 1.0;

        public double AirbaseLandBasedBomberModifier => IsLandBasedBomber ? 1.8 : 1.0;

        public double AirbaseLargeLandBasedBomberModifier => IsLargeLandBasedBomber ? 1.0 : 1.8;

        public double AirbaseAircraftTypeModifier => IsLandBasedBomber ? 0.8 : 1.0;

        public double AirbaseIsolatedIslandBomberModifier => IsAnyLandBasedBomber || IsDiveBomber || IsAswPatrol ? 1.7 : 1.0;

        public double AirbaseSupplyDepotBomberModifier => IsAnyLandBasedBomber || IsDiveBomber || IsAswPatrol ? 2.1 : 1.0;

        public double AirbaseAswRandomModifierMin => Asw < 10 ? 0.35 : 0.7;

        public double AirbaseAswRandomModifierMax => Asw < 10 ? 0.8 : 1.0;

        public double AirbaseLandBasedReconModifier
        {
            get
            {
                switch (Type)
                {
                    case 49:
                        return Accuracy > 2 ? 1.15 : 1.125;
                    default:
                        return 1.0;
                }
            }
        }

        public Color Color
        {
            get
            {
                switch (IconType)
                {
                    case  1: // 小口径
                    case  2: // 中口径
                    case  3: // 大口径
                    case 13: // 徹甲弾
                        return Color.FromArgb(209, 89, 89);
                    case  4: // 副砲
                        return Color.FromArgb(253, 233, 0);
                    case  5: // 魚雷
                        return Color.FromArgb(88, 134, 170);
                    case  6: // 艦戦
                        return Color.FromArgb(93, 179, 108);
                    case  7: // 艦爆
                        return Color.FromArgb(223, 102, 102);
                    case  8: // 艦攻
                        return Color.FromArgb(95, 173, 234);
                    case  9: // 艦偵
                    case 39: // 噴式景雲改
                    case 40: // 橘花改
                        return Color.FromArgb(254, 191, 0);
                    case 10: // 水上機
                    case 43: // 水上戦闘機
                        return Color.FromArgb(142, 203, 152);
                    case 11: // 電探
                        return Color.FromArgb(231, 153, 53);
                    case 12: // 三式弾
                        return Color.FromArgb(69, 175, 88);
                    case 14: // 応急修理要員
                        return Color.FromArgb(254, 254, 254);
                    case 15: // 機銃
                    case 16: // 高角砲
                        return Color.FromArgb(102, 204, 118);
                    case 17: // 爆雷
                    case 18: // ソナー
                        return Color.FromArgb(126, 203, 215);
                    case 19: // 缶
                        return Color.FromArgb(254, 195, 77);
                    case 20: // 上陸用舟艇
                    case 36: // 特型内火艇
                    case 52: // 陸戦部隊
                        return Color.FromArgb(154, 163, 90);
                    case 21: // オートジャイロ
                        return Color.FromArgb(99, 203, 115);
                    case 22: // 対潜哨戒機
                        return Color.FromArgb(125, 205, 217);
                    case 23: // 追加装甲
                        return Color.FromArgb(152, 124, 172);
                    case 24: // 探照灯
                    case 27: // 照明弾
                        return Color.FromArgb(254, 155, 0);
                    case 25: // ドラム缶
                    case 54: // 水上艦装備(煙幕)
                        return Color.FromArgb(161, 161, 160);
                    case 26: // 艦艇修理施設
                        return Color.FromArgb(175, 156, 126);
                    case 28: // 司令部施設
                        return Color.FromArgb(204, 172, 252);
                    case 29: // 航空要員
                        return Color.FromArgb(206, 166, 108);
                    case 30: // 高射装置
                        return Color.FromArgb(137, 153, 77);
                    case 31: // 対地装備
                        return Color.FromArgb(253, 49, 49);
                    case 32: // 水上艦要員
                        return Color.FromArgb(188, 238, 155);
                    case 33: // 大型飛行艇
                        return Color.FromArgb(142, 203, 152);
                    case 34: // 戦闘糧食
                        return Color.FromArgb(254, 254, 254);
                    case 35: // 補給物資
                        return Color.FromArgb(90, 200, 155);
                    case 37: // 陸上攻撃機
                    case 38: // 局地戦闘機
                    case 44: // 陸軍戦闘機
                    case 48: // 襲撃機
                    case 49: // 大型陸上機
                        return Color.FromArgb(57, 182, 78);
                    case 42: // 潜水艦機材
                        return Color.FromArgb(158, 187, 226);
                    case 45: // 夜間戦闘機
                    case 46: // 夜間攻撃機
                    case 50: // 夜間偵察機
                    case 51: // 夜間水上爆撃機
                        return Color.FromArgb(128, 121, 161);
                    case 47: // 陸上対潜哨戒機
                        return Color.FromArgb(91, 113, 209);
                    default:
                        return SystemColors.Control;
                }
            }
        }
    }
}