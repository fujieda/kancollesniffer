﻿// Copyright (C) 2019 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using KancolleSniffer.View;

namespace KancolleSniffer.Model
{
    public class AirBattleResult
    {
        public static readonly string[] AirControlLevelNames = new[] {"拮抗", "確保", "優勢", "劣勢", "喪失", "不明"};
        public static readonly string[] AirControlLevelNamesForMain = new[] {"拮抗", "確保", "優勢", "劣勢", "喪失", "制空"};
        public static readonly string[] AirControlLevelNamesForList = new[] {"制空拮抗", "制空確保", "航空優勢", "航空劣勢", "制空喪失", ""};
        public static readonly Color[] AirControlLevelColors = new[] {Control.DefaultForeColor, CUDColors.Blue, CUDColors.Green, CUDColors.Orange, CUDColors.Red, Control.DefaultForeColor};
        public static readonly int AirControlLevelNull = AirControlLevelNames.Length - 1;

        public List<AirBattleRecord> Result { get; } = new List<AirBattleRecord>();

        private readonly Func<int, bool, string> _getShipName;
        private readonly Func<int[], string[]> _getItemNames;

        public AirBattleResult(Func<int, bool, string> getShipName, Func<int[], string[]> getItemNames)
        {
            _getShipName = getShipName;
            _getItemNames = getItemNames;
        }

        public class AirBattleRecord
        {
            public string PhaseName { get; set; }
            public bool JetBomber { get; set; }
            public bool AirBase { get; set; }
            public bool IsFleetAirControl { get; set; }
            public int AirControlLevel { get; set; }
            public StageResult Stage1 { get; set; }
            public StageResult Stage2 { get; set; }
            public AirFireResult AirFire { get; set; }
            public string AirControlLevelName => AirControlLevelNames[AirControlLevel];
            public Color AirControlLevelColor => AirControlLevelColors[AirControlLevel];
        }

        public void Clear()
        {
            Result.Clear();
        }

        public class StageResult
        {
            public int FriendCount { get; set; }
            public int FriendLost { get; set; }
            public int EnemyCount { get; set; }
            public int EnemyLost { get; set; }

            public double EnemySurviveRate => (EnemyCount - EnemyLost) / (double)EnemyCount;
        }

        public class AirFireResult
        {
            public string ShipName { get; set; }
            public int Kind { get; set; }
            public string[] Items { get; set; }
        }

        public AirBattleRecord First => Result.Where(result => !result.JetBomber).FirstOrDefault();

        public int EnemyLostJets => Result.Where(result => result.JetBomber).Sum(result => result.Stage1.EnemyLost + result.Stage2.EnemyLost);

        public int ResultPanelInitIndex()
        {
            var index = Result.FindIndex(result => result.IsFleetAirControl);
            return index == -1 ? Result.Count - 1 : index;
        }

        public AirBattleRecord Add(dynamic json, string phaseName, bool airBase = false, bool jetBomber = false, bool isFleetAirControl = false, bool isAlliedFleet = false)
        {
            var stage1 = json.api_stage1;
            if (stage1 == null || (stage1.api_f_count == 0 && stage1.api_e_count == 0))
                return null;
            var result = new AirBattleRecord
            {
                PhaseName = phaseName,
                AirBase = airBase,
                JetBomber = jetBomber,
                IsFleetAirControl = isFleetAirControl,
                AirControlLevel = json.api_stage1.api_disp_seiku() ? (int)json.api_stage1.api_disp_seiku : AirControlLevelNull,
                Stage1 = CreateStageResult(json.api_stage1),
                Stage2 = json.api_stage2 == null
                    ? new StageResult()
                    : CreateStageResult(json.api_stage2),
                AirFire = CreateAirFireResult(json, isAlliedFleet)
            };
            Result.Add(result);
            return result;
        }

        private StageResult CreateStageResult(dynamic stage)
        {
            return new StageResult
            {
                FriendCount = (int)stage.api_f_count,
                FriendLost = (int)stage.api_f_lostcount,
                EnemyCount = (int)stage.api_e_count,
                EnemyLost = (int)stage.api_e_lostcount
            };
        }

        private AirFireResult CreateAirFireResult(dynamic json, bool isAlliedFleet)
        {
            if (json.api_stage2 == null || !json.api_stage2.api_air_fire())
                return null;
            var airFire = json.api_stage2.api_air_fire;
            var idx = (int)airFire.api_idx;
            return new AirFireResult
            {
                ShipName = _getShipName(idx, isAlliedFleet),
                Kind = (int)airFire.api_kind,
                Items = _getItemNames((int[])airFire.api_use_items)
            };
        }
    }
}