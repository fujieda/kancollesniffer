﻿// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;

namespace KancolleSniffer.Util
{
    public static class StringUtil
    {
        public static string Cat(string parent, string separator, string child)
        {
            if (String.IsNullOrEmpty(parent) || String.IsNullOrEmpty(separator) || String.IsNullOrEmpty(child))
                return parent + child;
            else
                return parent + separator + child;
        }

        public static string ToS(double value)
        {
            return ToS(value, 1);
        }

        public static string ToS(double value, int digits)
        {
            // MidpointRounding.ToNegativeInfinity に切り替えたい
            var pow = Math.Pow(10, digits);
            return (Math.Floor(value * pow) / pow).ToString($"F{digits}");
        }
    }
}
